'use strict';
var mysql = require('mysql');
var credentials = require('../model/db.js');
var _TokenValidator = require('../../middleware/TokenValidator');

var _TokenValidator2 = _interopRequireDefault(_TokenValidator);

var _axios ,Axios= require('axios');

var _axios2 = _interopRequireDefault(_axios);
import xmlparser from 'express-xml-bodyparser';
import xml from 'xml2js';
const xmlOptions = {
  charkey: 'value',
  trim: false,
  explicitRoot: false,
  explicitArray: false,
  normalizeTags: false,
  mergeAttrs: true,
};
const builder = new xml.Builder({
  renderOpts: { 'pretty': false }
});
const bustHeaders = (request, response, next) => {
  request.app.isXml = false;

  if (request.headers['content-type'] === 'application/xml'
    || request.headers['accept'] === 'application/xml'
  ) {
    request.app.isXml = true;
  }

  next();
};
const buildResponse = (response, statusCode, data, preTag) => {
  response.format({
    'application/json': () => {
      response.status(statusCode).json(data);
    },
    'application/xml': () => {
      response.status(statusCode).send(builder.buildObject({ [preTag]: data }));
    },
    'default': () => {
      // log the request and respond with 406
      response.status(406).send('Not Acceptable');
    }
  });
};
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
var nodemailer = require("nodemailer");
// var Zoom = require("zoomus")({
//   key : "Hq5Y-YnRQC-mLQ4-9x3_Mg",
//   secret : "nCE71q39XyQbnHJ1f0scUAzxamOs77WBs5xL"
// });
var transport = nodemailer.createTransport({
  host: "smtp.mail.gov.in",
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
    user: "noreply-mprojgar@mp.gov.in",
    pass: "X7$nF0#eO0"
  }
});
module.exports = function (app) {
  var categ = require('../controller/appController');
  var city = require('../controller/cityController');
  var skill = require('../controller/skillController');
  var state = require('../controller/stateController');
  var district = require('../controller/districtController');
  var registration = require('../controller/registrationController');
  var education = require('../controller/educationController');
  var course = require('../controller/courseController');
  var specialization = require('../controller/specializationController');
  var university = require('../controller/universityController');
  var coursetype = require('../controller/coursetypeController');
  var year = require('../controller/yearController');
  var gradingsystem = require('../controller/gradingsystemController');
  var validatelogin = require('../controller/validateloginController');
  var graduation = require('../controller/graduationController');
  var department = require('../controller/departmentController');
  var designation = require('../controller/designationController');
  var jobfair = require('../controller/jobfairController');
  var resumeheadline = require('../controller/resumeheadlineController');
  var keyskill = require('../controller/keyskillController');
  var attachresume = require('../controller/attachresumeController');
  var employement = require('../controller/employementController');
  var itskill = require('../controller/itskillController');
  var districtoffice = require('../controller/districtofficeController');
  var grievenceform = require('../controller/grievenceformController');
  var feedback = require('../controller/feedbackController');
  var employerregistrations = require('../controller/employerregistrationsController');
  var validateemplogin = require('../controller/validateemployerloginController');
  var jobAlert = require('../controller/jobAlertController');
  var jobseekerimage = require('../controller/jobseekerimageController');
  var footerDetail = require('../controller/FooterDetailController');
  var JobSeekerForgotPass = require('../controller/JobSeekerForgotPassController');
  var city8 = require('../controller/8CityController');
  var personaldetails = require('../controller/personaldetailsController');
  var profileSummary = require('../controller/profileSummaryController');
  var buildResume = require('../controller/buildresumeController');
  var project = require('../controller/projectController');
  var menu = require('../controller/menuController');
  var role = require('../controller/roleController');
  var rolemenumapping = require('../controller/rolemenumappingController');
  var identificationdetails = require('../controller/identificationdetailsController');
  var logininfo = require('../controller/logininfoController');
  var month = require('../controller/monthController');
  var industry = require('../controller/industryController');
  var subject = require('../controller/subjectController');
  var validateJobSeekerReg = require('../controller/ValidateJobSeekerRegController');
  var physicaldetails = require('../controller/physicaldetailController');
  var Language = require('../controller/languageController');
  var Typing = require('../controller/typingstenoController');
  var certification = require('../controller/certificationController');
  var jobfairreg = require('../controller/jobfairregController');
  var Delete = require('../controller/deleteController');
  var Resume = require('../controller/jobseekerResumeController');
  var jobseeker_verification = require('../controller/jobseeker_verificationController');
  var setpreference = require('../controller/setpreferenceController');
  var village = require('../controller/villagecontroller');
  var ProfileStatus = require('../controller/profileStatusController');
  var CompanyEmails = require('../controller/CompanyEmailsController');
  var AreaOfInterest = require('../controller/areaOfInterestController');
  var Employmentstatus = require('../controller/employmentStatusController');
  var EyeSight = require('../controller/eyeSightController');
  var HCCategory = require('../controller/HCcategoryController');
  var HCsubCategory = require('../controller/HCsubCategoryController');
  var Religion = require('../controller/religionController');
  var SportsName = require('../controller/sportsNameController');
  var SportsCertificate = require('../controller/sportsCertificateController');
  var Languagetype = require('../controller/languagetypeController');
  var Maritalstatus = require('../controller/maritalstatusController');
  var LicenseType = require('../controller/licenseTypeController');
  var PrefJobLocation = require('../controller/prefJobLocationController');
  var TypingInstitute = require('../controller/typingInstituteController');
  var QualificationDivision = require('../controller/qualificationDivisionController');
  var SkillSet = require('../controller/skillSetController');
  var BloodGroup = require('../controller/bloodGroupController');
  var Status = require('../controller/StatusController');
  var Nationality = require('../controller/NationalityController');
  var Resident = require('../controller/ResidentController');
  var DisablityPriority = require('../controller/DisablityPriorityController');
  var DomicileDocs = require('../controller/DomicileDocsController');
  var area = require('../controller/areaController');
  var typingstenomaster = require('../controller/typingstenomasterController');
  var salary = require('../controller/salaryController');
  var ncccertificate = require('../controller/ncccertificateController');
  var qualification = require('../controller/qualificationController');
  var subjectgroup = require('../controller/subjectgroupController');
  var subjectnew = require('../controller/subjectnewController');
  var jobprefrences = require('../controller/jobprefrencesController');
  var vacancytype = require('../controller/vacancytypeController');
  var sector = require('../controller/sectorController');
  var companytype = require('../controller/companyTypeController');
  var sectortype = require('../controller/sectortypeController');
  var exchangeoffice = require('../controller/exchangeofficeController');
  var Qualification = require('../controller/qualificationLevelController');
  var AdminMenu = require('../controller/adminMenuController');
  var AdminSubMenu = require('../controller/adminSubMenuController');
  var AdminRegType = require('../controller/adminRegTypeController');
  var jobseeker = require('../controller/careercounsellingjobseekerController');
  var AdminUserType = require('../controller/adminUserTypeController');
  var adminresetPass = require('../controller/adminresetPassController');
  var officetype = require('../controller/officetypeController');
  var deleteadm = require('../controller/deleteadmController');
  var AdminExchangeOffice = require('../controller/adminexchangemasterController');
  var companyprofile = require('../controller/companyProfileController');
  var adminDesignation = require('../controller/adminDesignationController');
  var exchjfdtls = require('../controller/exchjfdtlsController');
  var monthlytotal = require('../controller/MonthlytotalController');
  var yealytarget = require('../controller/yealytargetController');
  var monthlyreporting = require('../controller/monthlyreportingController');
  var ccexchjfdtls = require('../controller/ccexchjfdtlsController');
  var CCDetails = require('../controller/CCDetailsController');
  var ccregjobseeker = require('../controller/ccregjobseekerController');
  var selectdtls = require('../controller/selectdtlsController');
  var mstwlcategory = require('../controller/mstwlcategoryController');
  var wlurl = require('../controller/wlurlController');
  var fileuploaddtls = require('../controller/fileuploaddtlsController');
  var advcategory = require('../controller/advcategoryController');
  var GroupDiscussion = require('../controller/GroupDiscussionController');
  var advertisement = require('../controller/advertisementController');
  var PhotoDetails = require('../controller/PhotoDetailsController');
  var admexchmstnic = require('../controller/admexchmstnicController');
  var AdminVerification = require('../controller/AdminVerificationController');
  var RegistrationCancellation = require('../controller/RegistrationCancellationController');
  var LIveRegisterReport = require('../controller/LIveRegisterReportController');
  var QualififcationWiseDetail = require('../controller/QualififcationWiseDetailController');
  var JSStatiscalReport = require('../controller/JSStatiscalReportController');
  var admusermenu = require('../controller/admusermenuController');
  var EmpRegister = require('../controller/EmpRegisterController');
  var CategoryWiseSetPostCountN = require('../controller/CategoryWiseSetPostCountNController');
  var GetRptName = require('../controller/GetRptNameController');
  var GetCurrentJFDetailsN = require('../controller/GetCurrentJFDetailsNController');
  var ExchNotWorking = require('../controller/ExchNotWorkingController');
  var RegEmployerDetail = require('../controller/RegEmployerDetailController');
  var employerregistrations = require('../controller/employerregistrationsController');
  var validateemplogin = require('../controller/validateemployerloginController');
  var JobPost = require('../controller/JobPostController');
  var FetchJob = require('../controller/FetchJobController');
  var EmployerAdminVerification = require('../controller/EmployerAdminVerificationController');
  var RenewalCurrentStatus = require('../controller/RenewalCurrentStatusController');
  var regempcount = require('../controller/regempcountController');
  var ProEmpX6 = require('../controller/ProEmpX6Controller');
  var WorkingExchange = require('../controller/WorkingExchangeController');
  var MstDivision = require('../controller/MstDivisionController');
  var validateadmin = require('../controller/validateolexController');
  var AdmProUserUpdatePwd = require('../controller/AdmProUserUpdatePwdController');
  var RptCIJSX63 = require('../controller/rptCIJSX63Controller');
  var Deadregister = require('../controller/deadregisterController');
  var Summaryreport = require('../controller/summaryreportController');
  var Lapsed = require('../controller/lapsedController');
  var Transfer = require('../controller/transferController');
  var Searchutility = require('../controller/searchutilityController');
  var DistrictWithDivision = require('../controller/districtWithDivisionController');
  var Lrstatus = require('../controller/LrstatusController');
  var YearWiseLRStatus = require('../controller/YearwiselrstatusController');
  var candetail = require('../controller/candetailController');
  var Duplicateregstatus = require('../controller/DuplicateregstatusController');
  var Monthlyregstatus = require('../controller/monthlyregstatusController');
  var placementdetails = require('../controller/placementdetailsController');
  var HandicappedDocs = require('../controller/HandicappedDocsController');
  var USPGetSummary1 = require('../controller/USPGetSummary1Controller');
  var USPGetJobfairStatus = require('../controller/USPGetJobfairStatusController');
  var GetJobFairdetails = require('../controller/GetJobFairdetailsController');
  var Summaryjsexchwise = require('../controller/SummaryjsexchwiseController');
  var currentstatusrt = require('../controller/currentstatusrtController');
  var Duplicatereg = require('../controller/DuplicateregController');
  var LRCurrentStatus = require('../controller/LRCurrentStatusController');
  var ExchJSSearch = require('../controller/ExchJSSearchController');
  var RptES11Entry = require('../controller/RptES11EntryController');
  var esverifyrpt = require('../controller/esverifyrptController');
  var rtpnotsendho = require('../controller/rtpnotsendhoController');
  var RptHOES11 = require('../controller/RptHOES11Controller');
  var RptHOExchStatu = require('../controller/RptHOExchStatuController');
  var LRtotaltoday = require('../controller/lrtotaltodayController');
  var Categorywisegenderdetail = require('../controller/categorywisegenderdetailController');
  var exchangebydivision = require('../controller/ExchangeByDivisionController');
  var RptRevert = require('../controller/RptRevertController');
  var Religionwisegenderdetail = require('../controller/religionwisegenderdetailController');
  var Statewisegenderdetail = require('../controller/statewisegenderdetailController');
  var Hcwisegenderdetail = require('../controller/hcwisegenderdetailController');
  var SearchJob = require('../controller/SearchJobsController');
  var FeaturedJob = require('../controller/FeaturedJobController');
  var DataBaseSearch = require('../controller/DataBaseSearchController');
  var EmpChangePass = require('../controller/EmployerChangePasswordController');
  var EmpDesiredSkill = require('../controller/EmpDesiredSkillController');
  // var Savedappliedjobs = require('../controller/SavedappliedjobsController');
  var TopEmployers = require('../controller/TopEmployersController');
  var SavedAppliedjobs = require('../controller/SavedAppliedjobsController');
  var SectorRole = require('../controller/SectorRoleController');
  var job_alert = require('../controller/job_alertController');
  var Test = require('../controller/TestController');
  var SearchJobById = require('../controller/SearchJobByIdController');
  var jobfairregistration = require('../controller/jobfairregistrationController');
  var jobseekerregistration = require('../controller/jobseekerregistrationController');
  var savedjobs = require('../controller/savedjobsController');
  var saveapplyCounter = require('../controller/saveapplyCounterController');
  var appliedjobbyregno = require('../controller/appliedjobbyregnoController');
  var savedjobbyregno = require('../controller/savedjobbyregnoController');
  var newSearchJob = require('../controller/newSearchJobController');
  var employerverification = require('../controller/employerverificationController');
  var EmployerWithJobCount = require('../controller/EmployerWithJobCountController');
  var SearchCandidateId = require('../controller/SearchCandidateIdController');
  var SearchJobByIdereg = require('../controller/SearchJobByIderegController');
  var Vacancy = require('../controller/VacancyController');
  var SearchCandidateByJobId = require('../controller/SearchCandidateByJobIdController');
  var jobseekerreg = require('../controller/jobseekerregController');
  var UpdJobStatus = require('../controller/UpdJobStatusController');
  var Jobseekerinfo = require('../controller/JobseekerinfoController');
  var ActivityMaster = require('../controller/activitymasterController');
  var preflocation = require('../controller/preflocationController');
  var Interestedcandidatejf = require('../controller/InterestedcandidatejfController');
  var itijfexchange = require('../controller/itijfexchangeController');
  var Itiinstitute = require('../controller/itiinstituteController');
  var SelectedCandJobFair = require('../controller/SelectedCandJobFairController');
  var emailconfig = require('../controller/emailconfigController');
  var trademaster = require('../controller/trademasterController');
  var faq = require('../controller/faqController');
  var knowyourregnew = require('../controller/knowyourregnewController');
  var crisp = require('../controller/crispController');
  var sendmailer = require('../controller/sendmailController');
  var Counselling = require('../controller/counsellingformController');
  var er1form = require('../controller/er1Controller');
  var RptES12Report = require('../controller/RptES12ReportController');
  var RptES13Report = require('../controller/RptES13ReportController');
  var RptES21Entry = require('../controller/RptES21EntryController');
  var latest = require('../controller/latestNotificationController');
  
  var olexuser = require('../controller/olexuserController');
  var districtbyemp = require('../controller/districtbyempController');
  var summaryform = require('../controller/summarycounsellingController');

  var categorywise = require('../controller/categorywisequalificationreportController');
  var jobfairmonthlyreporting = require('../controller/jobfairmonthlyreportingController');

  var es14 = require('../controller/es14Controller');
  
  var es24p2 = require('../controller/es24p2Controller');
  
  var es24part1 = require('../controller/es24part1Controller');
  
  var es25p1 = require('../controller/es25p1Controller');
  
  var es25p2 = require('../controller/es25p2Controller');
  
  var es23 = require('../controller/es23Controller');
  
  var interestedcandidatereport = require('../controller/interestedcandidatereportController');

  var latest = require('../controller/latestNotificationController');

  var searchcandidatereports = require('../controller/SearchCandidateReportsController');

  app.route('/getNCOAvailability/:p_NCO_Code').get(RptES12Report.read_a_NCOCodeavail);
  var es22 = require('../controller/ES22Controller');
  
  var revertrpt = require('../controller/revertReportController');
  
  var employerrolemaster = require('../controller/employerrolemasterController');

  var employersubuser = require('../controller/employersubuserController');
  var dt = require('../controller/dtController');
  var SearchLogs = require('../controller/employeraccessController');
  
  var Olexdash = require('../controller/OlexdashController');
  
 
  var Exchangejobfair = require('../controller/ExchangejobfairController');

  var ncs = require('../controller/ncsController');
  //app.route('/candidateregistration_ncs').post(ncs.create_a_ncscandidatedata);
  app.route('/fetch_Yearwisecandidate').get(dt.read_a_yearwisecandidate);
  app.route('/fetch_Candidatedetails/:p_Year').get(dt.read_a_dtCandidate);
  app.route('/fetch_Jobfairdetails').get(dt.read_a_dtJobfair);
  app.route('/fetch_EmployerRegJobfair').get(dt.read_a_dtJobfairEmp);
  app.route('/fetch_EmpVacancyCount').get(dt.read_a_dtEmpvacancycount);

  app.route('/addSubUser').post(employersubuser.create_a_empsubuser);
  app.route('/getSubUser/:p_Emp_Id/:p_RoleId').get(employersubuser.read_a_empsubuser);
  app.route('/DeleteSubUser/:p_SubUserId').get(employersubuser.read_a_DeleteSubUser);
 
  app.route('/getemprolemst/:p_RoleId/:p_IsActive').get(employerrolemaster.read_a_Emprolemst);

  app.route('/jobfairmonthlyreporting').post(jobfairmonthlyreporting.create_a_JobFairReporting);
  app.route('/RevertReport').post(revertrpt.update_a_RptStatus);
  
  app.route('/ES22EntryInsert').post(es22.create_a_ES22Rpt);

  app.route('/Empjobfairdetails').post(exchjfdtls.create_a_empexchjfdtls);
  
  app.route('/ES22EntryUpdate').post(es22.update_a_ES22Entry);

  app.route('/getES22Entry/:p_Ex_id/:p_ToDt/:p_flag').get(es22.read_a_es22RptEntry);
 // app.route('/getNCOAvailability').get(RptES12Report.read_a_NCOCodeavail);
  app.route('/ES13Entry').post(RptES13Report.create_a_ES13Entry);
  app.route('/ES12_RptEntry').post(RptES12Report.create_a_RptES12);
  app.route('/es23Entry').post(es23.create_a_es23RptEntry);
  app.route('/es25p1RptEntry').post(es25p1.create_a_es25P1RptEntry);
  app.route('/es24p1RptEntry').post(es24part1.create_a_es24P1RptEntry);
 
  app.route('/es24p2RptEntry').post(es24p2.create_a_es24p2RptEntry);

  app.route('/interestedcandidatereport/:p_JobId').get(interestedcandidatereport.read_a_Interestedcandidatereport);
  app.route('/jobfairmonthlyReporting/:p_Month /:p_Year/:p_Ex_Id').get(jobfair.read_a_JobFairMonthlyReporting);
  
  app.route('/CounsellingByMonthlyReporting/:p_Month/:p_Year/:p_Ex_Id').get(jobseeker.read_a_CounsellingByMonthWise);

  app.route('/getes14Rpt/:p_Year/:p_Ex_Id').get(es14.read_a_es14Rpt);
 
  app.route('/getes24p2Rpt/:p_FromDate /:p_ToDate/:p_Ex_Id').get(es24p2.read_a_es24P2Rpt);
 
  app.route('/getes24part1Rpt/:p_FromDate/:p_ToDate/:p_Ex_Id').get(es24part1.read_a_es24P1Rpt);
 
  app.route('/getes25p1Rpt/:p_FromDt/:p_ToDt/:p_Ex_Id').get(es25p1.read_a_es25P1Rpt);
 
  app.route('/getes25p2Rpt/:p_FromDt/:p_ToDt/:p_Ex_Id').get(es25p2.read_a_es25P2Rpt);
 
  app.route('/summaryform').post(summaryform.create_a_summaryform);
  app.route('/getditrictbyemp/:p_StateId/:p_EmpId').get(districtbyemp.read_a_districtbyemp);
 
  app.route('/getcounsellingform/:p_Month/:p_Year').get(Counselling.read_a_counsellingform);

app.route('/counsellingform').post(Counselling.create_a_Counselling);

app.route('/counsellingfamily').post(Counselling.create_a_Counsellingfamily);

app.route('/counsellingeducation').post(Counselling.create_a_Counsellingeducation);
app.route('/er1form').post(er1form.create_a_er1form);
app.route('/geter1formbyemp/:p_Year/:p_Month/:p_EmployerId/:p_EntryFrom/:p_EntryBy').get(er1form.read_a_ER1FormbyEmp);

app.route('/er1formvacancy').post(er1form.create_a_createer1formvacancy);

app.route('/er1formunfilled').post(er1form.create_a_createer1formvacancyunfilled);

app.route('/er1formvacancyyashaswi').post(er1form.create_a_createer1formvacancyyashaswi);
  // nic candidate details routes
  // app.route('/getdistricttwisecandidatecount/:p_DistrictId').get(niccandidatedetail.read_a_candidatecount);
  // app.route('/getdistrictwisecandidatedetail/:p_DistrictId').get(niccandidatedetail.read_a_candidatedetails);
  
 //  send mailer routes
 app.route('/getcandidatelistformail/:p_Fromdate/:p_Todate').get(sendmailer.read_a_candidatelist);
 
   

  app.use(_TokenValidator2.default);
  //// Category Route Begin ////
  //.post(categ.list_all_category)
  app.route('/category').post(categ.create_a_category);
  //.get(categ.list_all_category)
  app.route('/category/:p_categoryid/:p_IpAddress/:p_IsActive/:p_UserId').get(categ.read_a_category);
  //.postsecured(categ.list_all_category)
  app.route('/secured/category').post(categ.create_a_category);
  //.getsecured(categ.list_all_category)
  app.route('/secured/category/:p_categoryid/:p_IpAddress/:p_IsActive/:p_UserId').get(categ.read_a_category);
  app.route('/counter').get(categ.read_a_HomePageCounter);


  app.route('/JobFairCounterDashboard/:p_Ex_id').get(categ.read_a_JobFairCounterDashboard);
  //.put(categ.update_a_category)
  //.delete(categ.delete_a_category);

  //.get(categ.list_all_LatestCategories)
  app.route('/JobByIndustry/:p_count').get(categ.read_a_JobByIndustry);
  app.route('/JobByLocation/:p_count').get(categ.read_a_JobByLocation);
  app.route('/JobByTitle/:p_count').get(categ.read_a_JobByTitle);
  app.route('/GovtJobByIndustry/:p_count').get(categ.read_a_GovtJobByIndustry);
  app.route('/GovtJobByLocation/:p_count').get(categ.read_a_GovtJobByLocation);
  app.route('/GovtJobByTitle/:p_count').get(categ.read_a_GovtJobByTitle);

  //// Category Route End ////

  //City Route Begin
  app.route('/city').post(city.create_a_city);
  app.route('/secured/city').post(city.create_a_city);
  app.route('/secured/city/:p_DistrictId/:p_IpAddress/:p_IsActive/:p_UserId').get(city.read_a_city);
  app.route('/city/:p_DistrictId/:p_IpAddress/:p_IsActive/:p_UserId').get(city.read_a_city);
  app.route('/city/:p_DistrictId/:p_IpAddress').get(city.read_some_city);
  //City Route End

  //Skill Route Begin
  app.route('/skill').post(skill.create_a_skill);
  app.route('/secured/skill').post(skill.create_a_skill);
  app.route('/secured/skill/:p_skillid/:p_IpAddress/:p_IsActive/:p_UserId').get(skill.read_a_skill);
  app.route('/skill/:p_skillid/:p_IpAddress/:p_IsActive/:p_UserId').get(skill.read_a_skill);
  //Skill Route End

  //State Route Begin
  app.route('/state').post(state.create_a_state);
  app.route('/state/:p_StateId/:p_IpAddress/:p_IsActive/:p_UserId').get(state.read_a_state);
  app.route('/secured/state/:p_StateId/:p_IpAddress/:p_IsActive/:p_UserId').get(state.read_a_state);
  //State Route End

  //District Route Begin
  app.route('/district').post(district.create_a_district);
  app.route('/secured/district').post(district.create_a_district);
  app.route('/district/:p_StateId/:p_IpAddress/:p_IsActive/:p_UserId').get(district.read_a_district);
  app.route('/secured/district/:p_StateId/:p_IpAddress/:p_IsActive/:p_UserId').get(district.read_a_district);
  //District Route End

  //Registration Route Begin
  app.route('/registration').post(registration.create_a_registration);
  app.route('/registration/:p_CandidateId/:p_IpAddress').get(registration.read_a_registration);
  app.route('/knowregistration/:p_FirstName/:p_LastName/:p_Gender/:p_Aadhar/:p_DOB/:p_Flag').get(registration.read_a_knowregistration);
  app.route('/RegistrationCard/:p_CandidateId/:p_IpAddress').get(registration.read_a_RegistrationCard);
  app.route('/secured/RegistrationCard/:p_CandidateId/:p_IpAddress').get(registration.read_a_RegistrationCard);
  //Registration Route End

  //Education Route Begin
  app.route('/education').post(education.create_a_education);
  app.route('/education/:p_EducationId/:p_IpAddress/:p_IsActive/:p_UserId').get(education.read_a_education);
  app.route('/secured/education').post(education.create_a_education);
  app.route('/secured/education/:p_EducationId/:p_IpAddress/:p_IsActive/:p_UserId').get(education.read_a_education);
  //Education Route End

  //Course Route Begin
  app.route('/secured/course').post(course.create_a_course);
  app.route('/course').post(course.create_a_course);
  app.route('/course/:p_CourseId/:p_IpAddress/:p_IsActive/:p_EducationId/:p_UserId').get(course.read_a_course);
  app.route('/secured/course/:p_CourseId/:p_IpAddress/:p_IsActive/:p_EducationId/:p_UserId').get(course.read_a_course);
  //Course Route End

  //specialization Route Begin
  app.route('/specialization').post(specialization.create_a_specialization);
  app.route('/specialization/:p_specializationid/:p_IpAddress/:p_isactive/:p_courseid/:p_UserId').get(specialization.read_a_specialization);
  //specialization Route End

  //university Route Begin
  app.route('/university').post(university.create_a_university);
  app.route('/university/:p_universityid/:p_IpAddress/:p_isactive/:p_UserId').get(university.read_a_university);
  //university Route End

  //coursetype Route Begin
  app.route('/coursetype').post(coursetype.create_a_coursetype);
  app.route('/coursetype/:p_CourseTypeId/:p_IsActive').get(coursetype.read_a_coursetype);
  app.route('/secured/coursetype').post(coursetype.create_a_coursetype);
  app.route('/secured/coursetype/:p_CourseTypeId/:p_IsActive').get(coursetype.read_a_coursetype);
  //coursetype Route End

  //year Route Begin
  app.route('/year/:p_FinYearId').get(year.read_a_year);
  //year Route End

  //DistrictWithDivision Route Begin
  app.route('/DistrictWithDivision/:p_DivisionId').get(DistrictWithDivision.read_a_DistrictWithDivision);
  app.route('/secured/DistrictWithDivision/p_DivisionId').get(DistrictWithDivision.read_a_DistrictWithDivision);
  //DistrictWithDivision Route End

  //Searchutility Route Begin
  app.route('/Searchutility').get(Searchutility.read_a_Searchutility);
  app.route('/secured/Searchutility').get(Searchutility.read_a_Searchutility);
  //Searchutility Route End

  //Transfer Route Begin
  app.route('/Transfer').get(Transfer.read_a_Transfer);
  app.route('/secured/Transfer').get(Transfer.read_a_Transfer);
  //Transfer Route End

  //Lapsed Route Begin
  app.route('/Lapsed').get(Lapsed.read_a_Lapsed);
  app.route('/secured/Lapsed').get(Lapsed.read_a_Lapsed);
  //Lapsed Route End

  //Summaryreport Route Begin
  app.route('/Summaryreport').get(Summaryreport.read_a_Summaryreport);
  app.route('/secured/Summaryreport').get(Summaryreport.read_a_Summaryreport);

  app.route('/SummaryreportCategoryWise/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_SummaryreportCategoryWise);
  app.route('/secured/SummaryreportCategoryWise/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_SummaryreportCategoryWise);

  app.route('/SummaryreportDifferentlyAbled/:p_Fromdt/:p_Todt/:p_Ex_id').get(Summaryreport.read_a_SummaryreportDifferentlyAbled);
  app.route('/secured/SummaryreportDifferentlyAbled/:p_Fromdt/:p_Todt/:p_Ex_id').get(Summaryreport.read_a_SummaryreportDifferentlyAbled);

  app.route('/SummaryreportAreaWise/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_SummaryreportAreaWise);
  app.route('/secured/SummaryreportAreaWise/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_SummaryreportAreaWise);

  app.route('/SummaryreportReligionWise/:p_Fromdt/:p_Todt/:p_Ex_id').get(Summaryreport.read_a_SummaryreportReligionWise);
  app.route('/secured/SummaryreportReligionWise/:p_Fromdt/:p_Todt/:p_Ex_id').get(Summaryreport.read_a_SummaryreportReligionWise);

  app.route('/ITIQualificationWiseSummaryReport/:p_Month/:p_year/:p_Ex_id/:p_qualificationid').get(Summaryreport.read_a_ITIQualificationWiseSummaryReport);
  app.route('/secured/ITIQualificationWiseSummaryReport/:p_Month/:p_year/:p_Ex_id/:p_qualificationid').get(Summaryreport.read_a_ITIQualificationWiseSummaryReport);

  app.route('/ConsolidateSummaryReport/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_ConsolidateSummaryReport);
  app.route('/secured/ConsolidateSummaryReport/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_ConsolidateSummaryReport);

  app.route('/DetailedSummaryReport/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_DetailedSummaryReport);
  app.route('/secured/DetailedSummaryReport/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_DetailedSummaryReport);

  app.route('/DetailedSummaryReport_Religion/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_DetailedSummaryReportReligion);
  app.route('/secured/DetailedSummaryReport_Religion/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_DetailedSummaryReportReligion);

  app.route('/DetailedSummaryReport_AreaWise/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_DetailedSummaryReportAreaWise);
  app.route('/secured/DetailedSummaryReport_AreaWise/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_DetailedSummaryReportAreaWise);

  app.route('/DetailedSummaryReport_PHWise/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_DetailedSummaryReportPHWise);
  app.route('/secured/DetailedSummaryReport_PHWise/:p_fromdate/:p_todate/:p_Ex_id').get(Summaryreport.read_a_DetailedSummaryReportPHWise);

  //Summaryreport Route End

  //Deadregister Route Begin
  app.route('/Deadregister').get(Deadregister.read_a_Deadregister);
  app.route('/secured/Deadregister').get(Deadregister.read_a_Deadregister);
  //Deadregister Route End

  //RptCIJSX63 Route Begin
  app.route('/RptCIJSX63').get(RptCIJSX63.read_a_RptCIJSX63);
  app.route('/secured/RptCIJSX63').get(RptCIJSX63.read_a_RptCIJSX63);

  //ReportX63
  app.route('/ReportX63/:p_Ex_Id/:p_From/:p_To/:p_Category/:p_Gender/:p_DifferentlyAbled/:p_Resident/:p_Area').get(RptCIJSX63.read_a_ReportX63);
  app.route('/secured/ReportX63/:p_Ex_Id/:p_From/:p_To/:p_Category/:p_Gender/:p_DifferentlyAbled/:p_Resident/:p_Area').get(RptCIJSX63.read_a_ReportX63);
  //RptCIJSX63 Route End

  //AdminUserType Route Begin
  app.route('/AdminUserType/:p_User_Type_Id/:p_Active_YN').get(AdminUserType.read_a_AdminUserType);
  app.route('/secured/AdminUserType/:p_User_Type_Id/:p_Active_YN').get(AdminUserType.read_a_AdminUserType);
  //AdminUserType Route End

  //AdminRegType Route Begin
  app.route('/AdminRegType/:p_Reg_Type_id/:p_Active_YN').get(AdminRegType.read_a_AdminRegtype);
  app.route('/secured/AdminRegType/:p_Reg_Type_id/:p_Active_YN').get(AdminRegType.read_a_AdminRegtype);
  //AdminRegType Route End

  //AdminMenu Route Begin
  app.route('/AdminMenu').post(AdminMenu.create_a_AdminMenu);
  app.route('/secured/AdminMenu').post(AdminMenu.create_a_AdminMenu);

  app.route('/AdminMenu/:p_Menu_Id/:p_Active_YN').get(AdminMenu.read_a_AdminMenu);
  app.route('/secured/AdminMenu/:p_Menu_Id/:p_Active_YN').get(AdminMenu.read_a_AdminMenu);
  //AdminMenu Route End

  //AdminSubMenu Route Begin
  app.route('/AdminSubMenu').post(AdminSubMenu.create_a_AdminSubMenu);
  app.route('/secured/AdminSubMenu').post(AdminSubMenu.create_a_AdminSubMenu);

  app.route('/AdminSubMenu/:p_Sub_Menu_Id/:p_Menu_Id/:p_Active_YN').get(AdminSubMenu.read_a_AdminSubMenu);
  app.route('/secured/AdminSubMenu/:p_Sub_Menu_Id/:p_Menu_Id/:p_Active_YN').get(AdminSubMenu.read_a_AdminSubMenu);
  //AdminSubMenu Route End

  //AreaOfInterest Route Begin
  app.route('/AreaOfInterest/:p_AreaOfInterest_id/:p_Active_YN').get(AreaOfInterest.read_a_AreaOfInterest);
  app.route('/secured/AreaOfInterest/:p_AreaOfInterest_id/:p_Active_YN').get(AreaOfInterest.read_a_AreaOfInterest);
  //AreaOfInterest Route End

  //Employmentstatus Route Begin
  app.route('/Employmentstatus/:p_jobstatus_id/:p_Active_YN').get(Employmentstatus.read_a_Employmentstatus);
  app.route('/secured/Employmentstatus/:p_jobstatus_id/:p_Active_YN').get(Employmentstatus.read_a_Employmentstatus);

  app.route('/EmployerUser/:p_EmpReg').get(Employmentstatus.read_a_EmployerUser);
  app.route('/secured/EmployerUser/:p_EmpReg').get(Employmentstatus.read_a_EmployerUser);

  //Employmentstatus Route End


  //EyeSight Route Begin
  app.route('/EyeSight/:p_Eye_sight_id/:p_Active_YN').get(EyeSight.read_a_EyeSight);
  app.route('/secured/EyeSight/:p_Eye_sight_id/:p_Active_YN').get(EyeSight.read_a_EyeSight);
  //EyeSight Route End


  //HCCategory Route Begin
  app.route('/HCCategory/:p_HC_Category_id/:p_Active_YN').get(HCCategory.read_a_HCCategory);
  app.route('/secured/HCCategory/:p_HC_Category_id/:p_Active_YN').get(HCCategory.read_a_HCCategory);
  //HCCategory Route End


  //HCsubCategory Route Begin
  app.route('/HCsubCategory/:p_HC_SubCategory_id/:p_Active_YN').get(HCsubCategory.read_a_HCsubCategory);
  app.route('/secured/HCsubCategory/:p_HC_SubCategory_id/:p_Active_YN').get(HCsubCategory.read_a_HCsubCategory);

  app.route('/HCsubCategory').post(HCsubCategory.create_a_HCsubCategory);
  app.route('/secured/HCsubCategory').post(HCsubCategory.create_a_HCsubCategory);

  //HCsubCategory Route End


  //Religion Route Begin
  app.route('/Religion/:p_Religion_id/:p_Active_YN').get(Religion.read_a_Religion);
  app.route('/secured/Religion/:p_Religion_id/:p_Active_YN').get(Religion.read_a_Religion);

  app.route('/Religion').post(Religion.create_a_Religion);
  app.route('/secured/Religion').post(Religion.create_a_Religion);

  //Religion Route End


  //SportsName Route Begin
  app.route('/SportsName/:p_S_id/:p_Active_YN').get(SportsName.read_a_SportsName);
  app.route('/secured/SportsName/:p_S_id/:p_Active_YN').get(SportsName.read_a_SportsName);
  //SportsName Route End


  //SportsCertificate Route Begin
  app.route('/SportsCertificate/:p_S_Certificate_id/:p_Active_YN').get(SportsCertificate.read_a_SportsCertificate);
  app.route('/secured/SportsCertificate/:p_S_Certificate_id/:p_Active_YN').get(SportsCertificate.read_a_SportsCertificate);
  //SportsCertificate Route End


  //Languagetype Route Begin
  app.route('/Languagetype/:p_Lang_id/:p_Active_YN').get(Languagetype.read_a_Languagetype);
  app.route('/secured/Languagetype/:p_Lang_id/:p_Active_YN').get(Languagetype.read_a_Languagetype);

  app.route('/Languagetype').post(Languagetype.create_a_Languagetype);
  app.route('/secured/Languagetype').post(Languagetype.create_a_Languagetype);
  //Languagetype Route End


  //Maritalstatus Route Begin
  app.route('/Maritalstatus/:p_Marital_id/:p_Active_YN').get(Maritalstatus.read_a_Maritalstatus);
  app.route('/secured/Maritalstatus/:p_Marital_id/:p_Active_YN').get(Maritalstatus.read_a_Maritalstatus);

  app.route('/Maritalstatus').post(Maritalstatus.create_a_Maritalstatus);
  app.route('/secured/Maritalstatus').post(Maritalstatus.create_a_Maritalstatus);
  //Maritalstatus Route End


  //LicenseType Route Begin
  app.route('/LicenseType/:p_L_Type_id/:p_Active_YN').get(LicenseType.read_a_LicenseType);
  app.route('/secured/LicenseType/:p_L_Type_id/:p_Active_YN').get(LicenseType.read_a_LicenseType);
  //LicenseType Route End


  //PrefJobLocation Route Begin
  app.route('/PrefJobLocation/:p_Location_id/:p_Active_YN').get(PrefJobLocation.read_a_PrefJobLocation);
  app.route('/secured/PrefJobLocation/:p_Location_id/:p_Active_YN').get(PrefJobLocation.read_a_PrefJobLocation);
  //PrefJobLocation Route End


  //TypingInstitute Route Begin
  app.route('/TypingInstitute/:p_TS_Institute_id/:p_Active_YN').get(TypingInstitute.read_a_TypingInstitute);
  app.route('/secured/TypingInstitute/:p_TS_Institute_id/:p_Active_YN').get(TypingInstitute.read_a_TypingInstitute);
  //TypingInstitute Route End


  //QualificationDivision Route Begin
  app.route('/QualificationDivision/:p_Q_Division_id/:p_Active_YN').get(QualificationDivision.read_a_QualificationDivision);
  app.route('/secured/QualificationDivision/:p_Q_Division_id/:p_Active_YN').get(QualificationDivision.read_a_QualificationDivision);
  //QualificationDivision Route End


  //SkillSet Route Begin
  app.route('/SkillSet/:p_ESID/:p_Active_YN').get(SkillSet.read_a_SkillSet);
  app.route('/secured/SkillSet/:p_ESID/:p_Active_YN').get(SkillSet.read_a_SkillSet);
  //SkillSet Route End


  //BloodGroup Route Begin
  app.route('/BloodGroup/:p_bloodgroup_id/:p_Active_YN').get(BloodGroup.read_a_BloodGroup);
  app.route('/secured/BloodGroup/:p_bloodgroup_id/:p_Active_YN').get(BloodGroup.read_a_BloodGroup);
  //BloodGroup Route End

  //Qualification Route Begin
  app.route('/Qualification/:p_Qualif_Level_id/:p_Active_YN').get(Qualification.read_a_Qualification);
  app.route('/secured/Qualification/:p_Qualif_Level_id/:p_Active_YN').get(Qualification.read_a_Qualification);

  app.route('/Qualification').post(Qualification.create_a_Qualificatione);
  app.route('/secured/Qualification').post(Qualification.create_a_Qualificatione);
  //Qualification Route End

  //Gradingsystem Route Begin
  app.route('/gradingsystem/:p_GradingsystemId/:p_IsActive').get(gradingsystem.read_a_gradingsystem);
  //Gradingsystem Route End

  //validatelogin Route Begin
  app.route('/validatelogin').post(validatelogin.create_a_validatelogin);
  //validatelogin Route End

  //graduation route begin
  app.route('/graduation').post(graduation.create_a_graduation);
  app.route('/graduation/:p_CandidateId/:p_GraduationId/:p_IpAddress').get(graduation.read_a_graduation);
  //Education Route End

  //Department Route Begin
  app.route('/department/:p_DepartmentId/:p_IsActive/:p_IpAddress/:p_UserId').get(department.read_a_department);
  app.route('/secured/department/:p_DepartmentId/:p_IsActive/:p_IpAddress/:p_UserId').get(department.read_a_department);
  //Department Route End

  //designation Route Begin
  app.route('/secured/designation').post(designation.create_a_designation);
  app.route('/designation').post(designation.create_a_designation);
  app.route('/designation/:p_DepartmentId/:p_IpAddress/:p_isactive/:p_UserId').get(designation.read_a_designation);
  app.route('/secured/designation/:p_DepartmentId/:p_IpAddress/:p_isactive/:p_UserId').get(designation.read_a_designation);
  //designation Route End

  //jobfair Route Begin
  app.route('/jobfair').post(jobfair.create_a_jobfair);
  app.route('/jobfair/:p_FromDate/:p_ToDate/:p_IpAddress').get(jobfair.read_a_jobfair);
  //jobfair Route End


  //ResumeHeadline Route Begin
  app.route('/secured/resumeheadline').post(resumeheadline.create_a_resumeheadline);
  app.route('/secured/resumeheadline/:p_CandidateId/:p_IpAddress/:p_UserId').get(resumeheadline.read_a_resumeheadline);
  //ResumeHeadline Route End

  //keyskill Route Begin
  app.route('/secured/keyskill').post(keyskill.create_a_keyskill);
  app.route('/secured/keyskill/:p_CandidateId/:p_IpAddress/:p_UserId').get(keyskill.read_a_keyskill);
  app.route('/secured/deletekeyskill/:p_CandidateId').get(keyskill.read_delete_keyskill);
  //keyskill Route End

  //attachresume Route Begin
  app.route('/attachresume').post(attachresume.create_a_attachresume);
  app.route('/attachresume/:p_CandidateId/:p_IpAddress/:p_UserId').get(attachresume.read_a_attachresume);
  //attachresume Route End

  //Employement Route Begin
  app.route('/employement').post(employement.create_a_employement);
  app.route('/employement/:p_CandidateId/:p_IpAddress/:p_UserId').get(employement.read_a_employement);
  //employement Route End

  //Itskill Route Begin
  app.route('/itskill').post(itskill.create_a_itskill);
  app.route('/itskill/:p_CandidateId/:p_IpAddress/:p_UserId').get(itskill.read_a_itskill);
  //Itskill Route End

  //DistrictOffice Route End
  app.route('/districtoffice').get(districtoffice.read_a_districtoffice);
  //DistrictOffice Route End

  //grievenceform Route Begin
  app.route('/grievenceform').post(grievenceform.create_a_grievenceform);
  app.route('/grievenceform/:p_GrievenceId/:p_RegisteredBy/:p_IpAddress/:p_UserId').get(grievenceform.read_a_grievenceform);
  //grievenceform Route End

  //feedbackform Route End
  app.route('/feedback').post(feedback.create_a_feedback);
  app.route('/feedback/:p_FeedBackId').get(feedback.read_a_feedback);
  //feedbackform Route End

  //employerregistrations Route Begin
  app.route('/employerregistrations').post(employerregistrations.create_a_EmployerRegistration);
  app.route('/employerregistrations/:p_EmployerId/:p_IpAddress/:p_UserId').get(employerregistrations.read_a_EmployerRegistration);
  //employerregistrations Route End

  //validateEmplogin Route Begin
  app.route('/validateemplogin/:p_UserId/:p_Password').get(validateemplogin.read_a_validateemplogin);

  //validateEmplogin Route End


  //JobAlert Route Begin
  app.route('/jobAlert').post(jobAlert.create_a_jobAlert);
  //JobAlert Route End
  app.route('/jobseekerimage').post(jobseekerimage.create_a_image);
  app.route('/jobseekerimage/:p_CandidateId').get(jobseekerimage.read_a_image);

  //footerDetail Route Begin
  app.route('/footerDetail/:p_StateId').get(footerDetail.read_a_footerDetail);
  //footerDetail Route End


  //JobSeekerForgotPass Route Begin
  app.route('/jobSeekerForgotPass/:p_UserId').get(JobSeekerForgotPass.read_a_jobSeekerForgotPass);
  //JobSeekerForgotPass Route End


  //Delete Route Begin
  app.route('/Delete/:p_Id/:p_CandidateId/:p_Type').get(Delete.read_a_Delete);

  app.route('/secured/Delete/:p_Id/:p_CandidateId/:p_Type').get(Delete.read_a_Delete);
  //Delete Route End


  //feedbackform Route End

  ////City Route Begin ////
  //.post(categ.list_all_City)
  app.route('/city').post(city.create_a_city);
  //.get(categ.list_all_City)
  app.route('/city/:p_DistrictId/:p_IpAddress/:p_IsActive/:p_UserId').get(city.read_a_city);
  //.getsecured(categ.list_all_City)
  app.route('/secured/city/:p_DistrictId/:p_IpAddress/:p_IsActive/:p_UserId').get(city.read_a_city);
  //// City Route End ////

  //// Skill Route Begin ////
  //.post(categ.list_all_skill)
  app.route('/skill').post(skill.create_a_skill);
  //.postsecured(categ.list_all_skill)
  app.route('/secured/skill').post(skill.create_a_skill);
  //.getsecured(categ.list_all_skill)
  app.route('/secured/skill/:p_skillid/:p_IpAddress/:p_IsActive/:p_UserId').get(skill.read_a_skill);
  //.get(categ.list_all_skill)
  app.route('/skill/:p_skillid/:p_IpAddress/:p_IsActive/:p_UserId').get(skill.read_a_skill);
  //// Skill Route  ////


  //// Typing Route Begin ////
  //.post(categ.list_all_Typing)
  app.route('/Typing').post(Typing.create_a_Typing);
  //.postsecured(categ.list_all_Typing)
  app.route('/secured/Typing').post(Typing.create_a_Typing);
  //.getsecured(categ.list_all_Typing)
  app.route('/secured/Typing/:p_TypingStenoId/:p_CandidateId/:p_IpAddress').get(Typing.read_a_Typing);
  //.get(categ.list_all_Typing)
  app.route('/Typing/:p_TypingStenoId/:p_CandidateId/:p_IpAddress').get(Typing.read_a_Typing);
  //// Typing Route  ////

  //// State Route Begin ////
  //.post(categ.list_all_state)
  app.route('/state').post(state.create_a_state);
  //.get(categ.list_all_state)
  //.postsecured(categ.list_all_state)
  app.route('/secured/state').post(state.create_a_state);
  //.getsecured(categ.list_all_state)
  app.route('/secured/state/:p_StateId/:p_IpAddress/:p_IsActive/:p_UserId').get(state.read_a_state);
  //.get(categ.list_all_state)
  app.route('/state/:p_StateId/:p_IpAddress/:p_IsActive/:p_UserId').get(state.read_a_state);

  //.get(categ.list_all_LatestStates)
  ////State Route End ////

  ////District Route Begin ////
  //.post(categ.list_all_district)
  app.route('/district').post(district.create_a_district);
  //.get(categ.list_all_district)
  app.route('/district/:p_StateId/:p_IpAddress/:p_IsActive/:p_UserId').get(district.read_a_district);
  ////District Route End ////


  //// CompanyEmails Route Begin ////
  //.post(categ.list_all_CompanyEmails)
  app.route('/CompanyEmails').post(CompanyEmails.create_a_CompanyEmails);
  //.get(categ.list_all_CompanyEmails)
  app.route('/CompanyEmails/:p_CompanyId/:p_CandidateId/:p_IpAddress/:p_UserId').get(CompanyEmails.read_a_CompanyEmails);

  app.route('/secured/CompanyEmails').post(CompanyEmails.create_a_CompanyEmails);
  //.get(categ.list_all_CompanyEmails)
  app.route('/secured/CompanyEmails/:p_CompanyId/:p_CandidateId/:p_IpAddress/:p_UserId').get(CompanyEmails.read_a_CompanyEmails);

  //// CompanyEmails Route  ////

  ////Registration Route Begin ////
  //.post(categ.list_all_registration)
  app.route('/registration').post(registration.create_a_registration);
  //.postsecured(categ.list_all_skill)
  app.route('/secured/registration').post(registration.create_a_registration);
  //.get(categ.list_all_registration)
  app.route('/registration/:p_CandidateId/:p_IpAddress').get(registration.read_a_registration);
  app.route('/secured/registration/:p_CandidateId/:p_IpAddress').get(registration.read_a_registration);
  ////Registration Route End ////

  //Education Route Begin
  //.post(categ.list_all_education)
  app.route('/education').post(education.create_a_education);
  //.get(categ.list_all_education)
  app.route('/education/:p_EducationId/:p_IpAddress/:p_IsActive/:p_UserId').get(education.read_a_education);
  //.postsecured(categ.list_all_education)
  app.route('/secured/education').post(education.create_a_education);
  //.getsecured(categ.list_all_education)
  app.route('/secured/education/:p_EducationId/:p_IpAddress/:p_IsActive/:p_UserId').get(education.read_a_education);
  //Education Route End

  ////Course Route Begin ////
  //.post(categ.list_all_course)
  app.route('/course').post(course.create_a_course);
  //.get(categ.list_all_course)
  app.route('/course/:p_CourseId/:p_IpAddress/:p_IsActive/:p_EducationId/:p_UserId').get(course.read_a_course);
  //.postsecured(categ.list_all_course)
  app.route('/secured/course').post(course.create_a_course);
  //.getsecured(categ.list_all_course)
  app.route('/secured/course/:p_CourseId/:p_IpAddress/:p_IsActive/:p_EducationId/:p_UserId').get(course.read_a_course);

  ////Course Route End ////

  ////specialization Route Begin ////
  //.post(categ.list_all_specialization)
  app.route('/specialization').post(specialization.create_a_specialization);
  //.get(categ.list_all_specialization)
  app.route('/specialization/:p_specializationid/:p_IpAddress/:p_isactive/:p_courseid/:p_UserId').get(specialization.read_a_specialization);
  //.postsecured(categ.list_all_specialization)
  app.route('/secured/specialization').post(specialization.create_a_specialization);
  //.getsecured(categ.list_all_specialization)
  app.route('/secured/specialization/:p_specializationid/:p_IpAddress/:p_isactive/:p_courseid/:p_UserId').get(specialization.read_a_specialization);

  ////specialization Route End ////

  ////university Route Begin ////
  //.post(categ.list_all_university)
  app.route('/university').post(university.create_a_university);
  //.get(categ.list_all_university)
  app.route('/university/:p_universityid/:p_IpAddress/:p_isactive/:p_UserId').get(university.read_a_university);
  //.postsecured(categ.list_all_university)
  app.route('/secured/university').post(university.create_a_university);
  //.getsecured(categ.list_all_university)
  app.route('/secured/university/:p_universityid/:p_IpAddress/:p_isactive/:p_UserId').get(university.read_a_university);
  ////university Route End ////

  ////coursetype Route Begin ////
  //.get(categ.list_all_coursetype)
  app.route('/coursetype/:p_CourseTypeId/:p_IsActive').get(coursetype.read_a_coursetype);
  //.getsecured(categ.list_all_coursetype)
  app.route('/secured/coursetype/:p_CourseTypeId/:p_IsActive').get(coursetype.read_a_coursetype);
  ////coursetype Route End ////

  ////year Route Begin ////
  //.get(categ.list_all_year)
  app.route('/year/:p_FinYearId/:p_Limit').get(year.read_a_year);
  //.getsecured(categ.list_all_year)
  app.route('/secured/year/:p_FinYearId/:p_Limit').get(year.read_a_year);
  ////year Route End ////

  ////Gradingsystem Route Begin ////
  //.get(categ.list_all_gradingsystem)
  app.route('/gradingsystem/:p_GradingsystemId/:p_IsActive').get(gradingsystem.read_a_gradingsystem);
  //.getsecured(categ.list_all_gradingsystem)
  app.route('/secured/gradingsystem/:p_GradingsystemId/:p_IsActive').get(gradingsystem.read_a_gradingsystem);
  ///Gradingsystem Route End ////



  ////graduation route begin ////
  //.post(categ.list_all_graduation)
  app.route('/secured/graduation').post(graduation.create_a_graduation);
  //.get(categ.list_all_graduation)
  app.route('/secured/graduation/:p_CandidateId/:p_GraduationId/:p_IpAddress').get(graduation.read_a_graduation);
  ////graduation Route End ////

  ////Department Route Begin ////
  //.get(categ.list_all_department)
  app.route('/department/:p_DepartmentId/:p_IsActive/:p_IpAddress/:p_UserId').get(department.read_a_department);

  //.get(categ.list_all_departmentJob)
  app.route('/departmentJob/:p_DepartmentId/:p_IsActive').get(department.read_a_departmentJob);
  ////Department Route End ////

  ////designation Route Begin ////
  //.post(categ.list_all_designation)
  app.route('/designation').post(designation.create_a_designation);
  //.get(categ.list_all_designation)
  app.route('/designation/:p_DepartmentId/:p_IpAddress/:p_isactive/:p_UserId').get(designation.read_a_designation);
  app.route('/secured/designation/:p_DepartmentId/:p_IpAddress/:p_isactive/:p_UserId').get(designation.read_a_designation);

  app.route('/latestdesignation').get(designation.read_a_latestdesignation);

  //.get(categ.list_all_designationJob)
  app.route('/designationJob/:p_DesignationId/:p_IsActive').get(designation.read_a_designationJob);
  ////designation Route End ////

  ////jobfair Route Begin ////
  //.post(categ.list_all_jobfair)
  app.route('/jobfair').post(jobfair.create_a_jobfair);
  //.get(categ.list_all_jobfair)
  app.route('/jobfair/:p_FromDate/:p_ToDate/:p_IpAddress').get(jobfair.read_a_jobfair);
  //.get(categ.list_all__all___jobfair)
  app.route('/latestJobfair').get(jobfair.read_a_latestJobfair);

  app.route('/secured/CandidateJobFair').post(jobfair.create_a_CandidateJobFair);
  app.route('/CandidateJobFair').post(jobfair.create_a_CandidateJobFair);
  
  app.route('/ParticipantJobFair').post(jobfair.create_a_ParticipantJobFair);
  app.route('/getParticipantJobFair/:p_Jobfair_Id/:p_Selection_Status').get(jobfair.read_a_JobFairParticipant);
  app.route('/updateParticipantJobFair').post(jobfair.update_a_ParticipantJobFair);
  ////jobfair Route End ////

  ////ResumeHeadline Route Begin ////
  //.post(categ.list_all_resumeheadline)
  app.route('/secured/resumeheadline').post(resumeheadline.create_a_resumeheadline);
  //.get(categ.list_all_resumeheadline)
  app.route('/secured/resumeheadline/:p_CandidateId/:p_IpAddress/:p_UserId').get(resumeheadline.read_a_resumeheadline);
  ////ResumeHeadline Route End ////

  ////keyskill Route Begin ////
  //.postsecured(categ.list_all_keyskill)
  app.route('/secured/keyskill').post(keyskill.create_a_keyskill);
  //.getsecured(categ.list_all_keyskill)
  app.route('/secured/keyskill/:p_CandidateId/:p_IpAddress/:p_UserId').get(keyskill.read_a_keyskill);
  ////keyskill Route End ////


  //Language Route Begin
  app.route('/Language').post(Language.create_a_Language);
  app.route('/secured/Language').post(Language.create_a_Language);
  app.route('/Language/:p_LanguageId/:p_CandidateId/:p_IpAddress').get(Language.read_a_Language);
  app.route('/secured/Language/:p_LanguageId/:p_CandidateId/:p_IpAddress').get(Language.read_a_Language);
  //Language Route End


  ////attachresume Route Begin ////
  //.post(categ.list_all_attachresume)
  app.route('/attachresume').post(attachresume.create_a_attachresume);
  //.get(categ.list_all_attachresume)
  app.route('/attachresume/:p_CandidateId/:p_IpAddress/:p_UserId').get(attachresume.read_a_attachresume);
  ////attachresume Route End ////

  ////Employement Route Begin ////
  //.post(categ.list_all_employement)
  app.route('/secured/employement').post(employement.create_a_employement);
  //.get(categ.list_all_employement)
  app.route('/secured/employement/:p_CandidateId').get(employement.read_a_employement);
  ////employement Route End ////

  ////Itskill Route Begin ////
  //.post(categ.list_all_itskill)
  app.route('/itskill').post(itskill.create_a_itskill);
  //.get(categ.list_all_itskill)
  app.route('/itskill/:p_CandidateId/:p_IpAddress/:p_UserId').get(itskill.read_a_itskill);
  //.postsecured(categ.list_all_itskill)
  app.route('/secured/itskill').post(itskill.create_a_itskill);
  //.getsecured(categ.list_all_itskill)
  app.route('/secured/itskill/:p_CandidateId/:p_IpAddress/:p_UserId').get(itskill.read_a_itskill);
  ////Itskill Route End ////

  ////DistrictOffice Route End ////
  //.get(categ.list_all_districtoffice)
  app.route('/districtoffice').get(districtoffice.read_a_districtoffice);
  app.route('/districtofficedetail/:p_DistrictId').get(districtoffice.read_DitrictOfficeDetail);
  ////DistrictOffice Route End ////


  //// PhysicalDetails Route Begin ////
  //.post(categ.list_all_physicaldetails)
  app.route('/physicaldetails').post(physicaldetails.create_a_physicaldetails);
  //.get(categ.list_all_physicaldetails)
  app.route('/physicaldetails/:p_CandidateId/:p_IpAddress/:p_UserId').get(physicaldetails.read_a_physicaldetails);
  //.postsecured(categ.list_all_physicaldetails)
  app.route('/secured/physicaldetails').post(physicaldetails.create_a_physicaldetails);
  //.getsecured(categ.list_all_physicaldetails)
  app.route('/secured/physicaldetails/:p_CandidateId/:p_IpAddress/:p_UserId').get(physicaldetails.read_a_physicaldetails);
  //// PhysicalDetails Route  ////


  ////grievenceform Route Begin ////
  //.post(categ.list_all_grievenceform)
  app.route('/grievenceform').post(grievenceform.create_a_grievenceform);
  //.get(categ.list_all_grievenceform)
  app.route('/grievenceform/:p_GrievenceId/:p_RegisteredBy/:p_IpAddress/:p_UserId').get(grievenceform.read_a_grievenceform);
  ////grievenceform Route End ////

  app.route('/validateJobSeekerReg/:p_UserId').get(validateJobSeekerReg.read_a_validateJobSeekerReg);

  ////Subject Route Begin ////
  //.post(categ.list_all_subject)
  app.route('/subject').post(subject.create_a_subject);
  //.get(categ.list_all_subject)
  app.route('/subject/:p_SubjectId/:p_IsActive/:p_SpecializationId/:p_IpAddress/:p_UserId').get(subject.read_a_subject);
  //.postsecured(categ.list_all_subject)
  app.route('/secured/subject').post(subject.create_a_subject);
  //.getsecured(categ.list_all_subject)
  app.route('/secured/subject/:p_SubjectId/:p_IsActive/:p_SpecializationId/:p_IpAddress/:p_UserId').get(subject.read_a_subject);
  ////Subject Route End ////


  //// Resume Route Begin ////
  //.post(categ.list_all_Resume)
  app.route('/Resume').post(Resume.create_a_Resume);
  //.postsecured(categ.list_all_Resume)
  app.route('/secured/Resume').post(Resume.create_a_Resume);
  //.getsecured(categ.list_all_Resume)
  app.route('/secured/Resume/:p_CandidateId').get(Resume.read_a_Resume);
  //.get(categ.list_all_Resume)
  app.route('/Resume/:p_CandidateId').get(Resume.read_a_Resume);
  app.route('/JobseekerProfile/:p_CandidateId').get(Resume.read_a_Profile);
  app.route('/secured/JobseekerProfile/:p_CandidateId').get(Resume.read_a_Profile);
  app.route('/secured/DeleteResume/:p_CandidateId').get(Resume.read_a_DeleteResume);
  //// Resume Route  ////

  ////feedbackform Route End ////
  //.post(categ.list_all_feedback)
  app.route('/feedback').post(feedback.create_a_feedback);
  //.get(categ.list_all_feedback)
  app.route('/feedback/:p_FeedBackId').get(feedback.read_a_feedback);
  ////feedbackform Route End ////

  ////employerregistrations Route Begin ////
  //.post(categ.list_all_employerregistrations)
  app.route('/employerregistrations').post(employerregistrations.create_a_EmployerRegistration);
  //.get(categ.list_all_employerregistrations)
  app.route('/employerregistrations/:p_EmployerId/:p_IpAddress/:p_UserId').get(employerregistrations.read_a_EmployerRegistration);
  ////employerregistrations Route End ////

  ////validateEmplogin Route Begin ////
  //.get(categ.list_all_validateemplogin)
  app.route('/validateemplogin/:p_UserId/:p_Password').get(validateemplogin.read_a_validateemplogin);
  ////validateEmplogin Route End ////


  ////buildResume Route Begin ////
  //.get(categ.list_all_buildResume)
  app.route('/secured/buildResume/:p_CandidateId').get(buildResume.read_a_buildResume);
  //.get(categ.list_all_educationBuildCV)
  app.route('/secured/educationBuildCV/:p_CandidateId').get(buildResume.read_a_educationBuildCV);
  //.get(categ.list_all_experienceCV)
  app.route('/secured/experienceCV/:p_CandidateId').get(buildResume.read_a_experienceCV);
  ////buildResume Route End ////


  ////JobAlert Route Begin ////
  //.post(categ.list_all_jobAlert)
  app.route('/jobAlert').post(jobAlert.create_a_jobAlert);
  ////JobAlert Route End ////


  ////jobseekerimage Route Begin ////
  //.post(categ.list_all_jobseekerimage)
  app.route('/jobseekerimage').post(jobseekerimage.create_a_image);
  //.get(categ.list_all_jobseekerimage)
  app.route('/jobseekerimage/:p_CandidateId').get(jobseekerimage.read_a_image);
  app.route('/secured/jobseekerimage').post(jobseekerimage.create_a_image);
  //.get(categ.list_all_jobseekerimage)
  app.route('/secured/jobseekerimage/:p_CandidateId').get(jobseekerimage.read_a_image);
  app.route('/secured/deletejobseekerimage/:p_CandidateId').get(jobseekerimage.read_a_DeleteImage);
  ////jobseekerimage Route Begin ////

  ////footerDetail Route Begin ////
  //.get(categ.list_all_footerDetail)
  app.route('/footerDetail/:p_StateId').get(footerDetail.read_a_footerDetail);
  ////footerDetail Route End ////


  //// JobSeekerForgotPass Route Begin ////
  //.get(categ.list_all_jobSeekerForgotPass)
  app.route('/jobSeekerForgotPass/:p_UserId').get(JobSeekerForgotPass.read_a_jobSeekerForgotPass);
  //// JobSeekerForgotPass Route End ////


  ////ProfileSummary Route Begin ////
  //.post(categ.list_all_ProfileSummary)
  app.route('/secured/profileSummary').post(profileSummary.create_a_profileSummary);
  //.get(categ.list_all_ProfileSummary)
  app.route('/secured/profileSummary/:p_CandidateId/:p_IpAddress').get(profileSummary.read_a_profileSummary);
  ////ProfileSummary Route End ////


  //// 8City Route Begin ////
  //.get(categ.list_all_city8)
  app.route('/city8/:p_CityId/:p_IsActive').get(city8.read_a_city8);
  //// 8City Roityute End ////


  //Project Route Begin
  //.post(categ.list_all_Project)
  app.route('/project').post(project.create_a_project);
  //.get(categ.list_all_Project)
  app.route('/project/:p_CandidateId/:p_IpAddress/:p_UserId').get(project.read_a_project);
  //.postsecured(categ.list_all_Project)
  app.route('/secured/project').post(project.create_a_project);
  //.getsecured(categ.list_all_Project)
  app.route('/secured/project/:p_CandidateId/:p_IpAddress/:p_UserId').get(project.read_a_project);
  //Project Route End

  app.route('/personaldetails').post(personaldetails.create_a_personaldetails);

  app.route('/secured/personaldetails').post(personaldetails.create_a_personaldetails);
  app.route('/personaldetails/:p_CandidateId/:p_IpAddress/:p_UserId').get(personaldetails.read_a_personaldetails);

  app.route('/secured/personaldetails/:p_CandidateId/:p_IpAddress/:p_UserId').get(personaldetails.read_a_personaldetails);
  //Identification
  app.route('/secured/identificationdetails').post(identificationdetails.create_a_identificationdetails);

  app.route('/secured/identificationdetails/:p_IdentificationId/:p_CandidateId/:p_IpAddress').get(identificationdetails.read_a_identificationdetails);

  app.route('/identificationdetails/:p_IdentificationId/:p_CandidateId/:p_IpAddress').get(identificationdetails.read_a_identificationdetails);

  //Menu Route Begin

  app.route('/secured/menu').post(menu.create_a_menu);

  app.route('/secured/menu/:p_menuid/:p_MWB/:p_IpAddress/:p_IsActive/:p_UserId').get(menu.read_a_menu);

  app.route('/menu').post(menu.create_a_menu);

  app.route('/menu/:p_menuid/:p_MWB/:p_IpAddress/:p_IsActive/:p_UserId').get(menu.read_a_menu);
  //Menu Route End

  //Role Route Begin

  app.route('/secured/role').post(role.create_a_role);

  app.route('/secured/role/:p_RoleId/:p_IpAddress/:p_IsActive/:p_UserId').get(role.read_a_role);

  app.route('/role').post(role.create_a_role);

  app.route('/role/:p_RoleId/:p_IpAddress/:p_IsActive/:p_UserId').get(role.read_a_role);
  //Role Route End
  //RoleMenuMapping Route Begin

  app.route('/secured/rolemenumapping').post(rolemenumapping.create_a_rolemenumapping);

  app.route('/secured/rolemenumapping/:p_User_Type_Id').get(rolemenumapping.read_a_rolemenumapping);

  app.route('/rolemenumapping').post(rolemenumapping.create_a_rolemenumapping);
  app.route('/rolesubmenumapping').post(rolemenumapping.create_a_rolesubmenumapping);
  
  app.route('/rolemenumapping/:p_User_Type_Id').get(rolemenumapping.read_a_rolemenumapping);
  app.route('/rolesubmenumapping/:p_User_Type_Id/:p_Menu_Id').get(rolemenumapping.read_a_rolesubmenumapping);
  //RoleMenuMapping Route End
  //Login Info Route Begin
  app.route('/logininfo').get(logininfo.read_a_logininfo);
  //Login Info Route End
  //Month Route Begin
  app.route('/secured/month/:p_MonthId').get(month.read_a_month);
  app.route('/month/:p_MonthId').get(month.read_a_month);
  //Month Route End
  //Industry Route Begin
  app.route('/industry').post(industry.create_a_industry);
  app.route('/industry/:p_IndustryId/:p_IpAddress/:p_IsActive/:p_UserId').get(industry.read_a_industry);
  app.route('/secured/industry').post(industry.create_a_industry);
  app.route('/secured/industry/:p_IndustryId/:p_IpAddress/:p_IsActive/:p_UserId').get(industry.read_a_industry);
  //Industry Route End
  app.route('/certification').post(certification.create_a_certification);
  app.route('/certification/:p_CertificationId/:p_CandidateId/:p_IpAddress/:p_IsActive/:p_UserId').get(certification.read_a_certification);
  app.route('/secured/certification').post(certification.create_a_certification);
  app.route('/secured/certification/:p_CertificationId/:p_CandidateId/:p_IpAddress/:p_IsActive/:p_UserId').get(certification.read_a_certification);

  //JobFairReg Route Begin
  app.route('/JobFairReg').post(jobfairreg.create_a_jobfairreg);
  //JobFairReg Route End

  //Feedback Route Begin
  app.route('/feedbackUser/:p_UserName/:p_Type').get(feedback.read_a_FeedbackUser);
  //Feedback Route End


  //ProfileStatus Route Begin
  app.route('/ProfileStatus/:p_CandidateId').get(ProfileStatus.read_a_ProfileStatus);
  //ProfileStatus Route End

  //jobseeker_verification
  app.route('/jobseeker_verification').post(jobseeker_verification.create_a_jobseeker_verification);
  app.route('/secured/jobseeker_verification').post(jobseeker_verification.create_a_jobseeker_verification);
  app.route('/jobseeker_verification/:p_CandidateId').get(jobseeker_verification.read_a_JobSeekerVerification);
  app.route('/secured/jobseeker_verification/:p_CandidateId').get(jobseeker_verification.read_a_JobSeekerVerification);
  //Change Password
  app.route('/secured/changepassword').post(validateJobSeekerReg.create_a_ChangePassword);
  app.route('/changepassword').post(validateJobSeekerReg.create_a_ChangePassword);
  //Set Preference
  app.route('/secured/setpreference').post(setpreference.create_a_setpreference);
  app.route('/setpreference').post(setpreference.create_a_setpreference);
  app.route('/secured/setpreference/:p_CandidateId').get(setpreference.read_a_setpreference);
  app.route('/setpreference/:p_CandidateId').get(setpreference.read_a_setpreference);

  ////Village Route Begin ////
  //.post(categ.list_all_City)
  app.route('/village').post(village.create_a_Village);
  app.route('/secured/village').post(village.create_a_Village);
  //.get(categ.list_all_City)
  app.route('/village/:p_CityId/:p_IpAddress/:p_IsActive/:p_UserId').get(village.read_a_Village);
  //.getsecured(categ.list_all_City)
  app.route('/secured/village/:p_CityId/:p_IpAddress/:p_IsActive/:p_UserId').get(village.read_a_Village);
  //// City Route End ////


  //imageform Route End
  //feedbackform Route End

  ////Status Route Begin ////

  app.route('/Status/:p_Status_id/:p_Active_YN').get(Status.read_a_Status);

  app.route('/secured/Status/:p_Status_id/:p_Active_YN').get(Status.read_a_Status);
  //// Status Route End ////
  ////Nationality Route Begin ////

  app.route('/Nationality/:p_Nationality_id/:p_Active_YN').get(Nationality.read_a_Nationality);

  app.route('/secured/Nationality/:p_Nationality_id/:p_Active_YN').get(Nationality.read_a_Nationality);
  //// Nationality Route End ////
  ////Resident Route Begin ////

  app.route('/Resident/:p_Resident_id/:p_Active_YN').get(Resident.read_a_Resident);

  app.route('/secured/Resident/:p_Resident_id/:p_Active_YN').get(Resident.read_a_Resident);
  //// Resident Route End ////

  ////DisablityPriority Route Begin ////

  app.route('/DisablityPriority/:p_DisablityPriority_id/:p_Active_YN').get(DisablityPriority.read_a_DisablityPriority);

  app.route('/secured/DisablityPriority/:p_DisablityPriority_id/:p_Active_YN').get(DisablityPriority.read_a_DisablityPriority);
  //// DisablityPriority Route End ////

  ////Area Route Begin ////

  app.route('/Area/:p_Area_Id/:p_Active_YN').get(area.read_a_area);

  app.route('/secured/Area/:p_Area_Id/:p_Active_YN').get(area.read_a_area);
  //// Area Route End ////

  ////Area Route Begin ////

  app.route('/typingstenomaster/:p_Type_id/:p_Active_YN').get(typingstenomaster.read_a_typingstenomaster);

  app.route('/secured/typingstenomaster/:p_Type_id/:p_Active_YN').get(typingstenomaster.read_a_typingstenomaster);
  //// Area Route End ////
  ////salary Route Begin ////

  app.route('/salary/:p_salary_id/:p_Active_YN').get(salary.read_a_salary);

  app.route('/secured/salary/:p_salary_id/:p_Active_YN').get(salary.read_a_salary);
  //// salary Route End ////


  ////ncccertificate Route Begin ////
  app.route('/ncccertificate/:p_ncc_Certificate_id/:p_Active_YN').get(ncccertificate.read_a_ncccertificate);
  app.route('/secured/ncccertificate/:p_ncc_Certificate_id/:p_Active_YN').get(ncccertificate.read_a_ncccertificate);

  app.route('/ncccertificate').post(ncccertificate.create_a_ncccertificate);
  app.route('/secured/ncccertificate').post(ncccertificate.create_a_ncccertificate);
  //// ncccertificate Route End ////
  ////qualification Route Begin ////

  app.route('/qualification/:p_Qualif_id/:p_Active_YN/:p_Qualif_Level_id').get(qualification.read_a_qualification);

  app.route('/secured/qualification/:p_Qualif_id/:p_Active_YN/:p_Qualif_Level_id').get(qualification.read_a_qualification);
  //// qualification Route End ////
  ////subjectgroup Route Begin ////

  app.route('/subjectgroup/:p_Sub_Group_id/:p_Qualif_id/:p_Active_YN').get(subjectgroup.read_a_subjectgroup);

  app.route('/secured/subjectgroup/:p_Sub_Group_id/:p_Qualif_id/:p_Active_YN').get(subjectgroup.read_a_subjectgroup);
  //// subjectgroup Route End ///
  ////subjectnew Route Begin ////

  app.route('/subjectnew/:p_Sub_id/:p_Sub_Group_id/:p_Qualif_id/:p_Active_YN').get(subjectnew.read_a_subjectnew);

  app.route('/secured/subjectnew/:p_Sub_Group_id/:p_Sub_id/:p_Qualif_id/:p_Active_YN').get(subjectnew.read_a_subjectnew);
  //// subjectnew Route End ///
  ////jobprefrences Route Begin ////

  app.route('/jobprefrences/:p_ESID/:p_FunctionalArea_id').get(jobprefrences.read_a_jobprefrences);

  app.route('/secured/jobprefrences/:p_ESID/:p_FunctionalArea_id').get(jobprefrences.read_a_jobprefrences);3;
  //// jobprefrences Route End ///
  ////vacancytype Route Begin ////

  app.route('/vacancytype/:p_VacancyType_id/:p_Active_YN').get(vacancytype.read_a_vacancytype);
  app.route('/secured/vacancytype/:p_VacancyType_id/:p_Active_YN').get(vacancytype.read_a_vacancytype);

  app.route('/vacancytype').post(vacancytype.create_a_vacancytype);
  app.route('/secured/vacancytype').post(vacancytype.create_a_vacancytype);

  //// vacancytype Route End ///
  ////sector Route Begin ////
  app.route('/sector/:p_Sector_Id/:p_Active_YN').get(sector.read_a_sector);
  app.route('/secured/sector/:p_Sector_Id/:p_Active_YN').get(sector.read_a_sector);
  //// sector Route End ///
  ////sector Route Begin ////
  app.route('/companytype/:p_companytype_id/:p_Active_YN').get(companytype.read_a_companytype);
  app.route('/secured/companytype/:p_companytype_id/:p_Active_YN').get(companytype.read_a_companytype);

  app.route('/companytype').post(companytype.create_a_companytype);
  app.route('/secured/companytype').post(companytype.create_a_companytype);

  //// sector Route End ///

  ////sectortype Route Begin ////
  app.route('/sectortype/:p_S_Id/:p_Active_YN').get(sectortype.read_a_sectortype);
  app.route('/secured/sectortype/:p_S_Id/:p_Active_YN').get(sectortype.read_a_sectortype);

  app.route('/sectortype').post(sectortype.create_a_sectortype);
  app.route('/secured/sectortype').post(sectortype.create_a_sectortype);

  //// sectortype Route End ///

  ////sectortype Route Begin ////
  app.route('/exchangeoffice/:p_Ex_id/:p_District_id/:p_Active_YN').get(exchangeoffice.read_a_exchangeoffice);
  app.route('/secured/exchangeoffice/:p_Ex_id/:p_District_id/:p_Active_YN').get(exchangeoffice.read_a_exchangeoffice);
  //// sectortype Route End ///
  //.post(categ.list_all_City)
  app.route('/DomicileDocs').post(DomicileDocs.create_a_DomicileDocs);
  app.route('/secured/DomicileDocs').post(DomicileDocs.create_a_DomicileDocs);
  //// DomicileDocs Route End ////
  //Counselling Begin //
  app.route('/jobseeker/:p_RegistrationNumber').get(jobseeker.read_a_jobseeker);
  //Counselling End//
  app.route('/secured/adminresetPass').post(adminresetPass.create_a_adminresetPass);
  //officetype//
  app.route('/officetype').post(officetype.create_a_officetype);
  app.route('/secured/officetype').post(officetype.create_a_officetype);

  app.route('/officetype/:p_Active_YN').get(officetype.read_a_officetype);
  app.route('/secured/officetype/:p_Active_YN').get(officetype.read_a_officetype);

  app.route('/officetype').get(officetype.read_a_officetype);
  app.route('/secured/officetype').get(officetype.read_a_officetype);
  app.route('/secured/officetype/:p_Ex_Type_id/:p_Active_YN').get(officetype.read_a_officetype);

  //delete admin details//
  app.route('/deleteadm/:p_Id/:p_Type').get(deleteadm.read_a_deleteadm);
  app.route('/secured/deleteadm/:p_Id/:p_Type').get(deleteadm.read_a_deleteadm);
  //Admin Exchange Office//

  app.route('/AdminExchangeOffice').post(AdminExchangeOffice.create_a_AdminExchangeOffice);
  app.route('/secured/AdminExchangeOffice').post(AdminExchangeOffice.create_a_AdminExchangeOffice);

  //companyprofile admin details//
  app.route('/companyprofile').post(companyprofile.create_a_companyprofile);
  app.route('/secured/companyprofile').post(companyprofile.create_a_companyprofile);

  app.route('/companyprofile/:p_CP_id/:p_Active_YN').get(companyprofile.read_a_companyprofile);
  app.route('/secured/companyprofile/:p_CP_id/:p_Active_YN').get(companyprofile.read_a_companyprofile);
  //companyprofile Exchange Office//

  //adminDesignation admin details//
  app.route('/adminDesignation').post(adminDesignation.create_a_adminDesignation);
  app.route('/secured/adminDesignation').post(adminDesignation.create_a_adminDesignation);

  app.route('/adminDesignation/:p_Designation_Id/:p_Active_YN').get(adminDesignation.read_a_adminDesignation);
  app.route('/secured/adminDesignation/:p_Designation_Id/:p_Active_YN').get(adminDesignation.read_a_adminDesignation);
  //adminDesignation Exchange Office//


  //exchjfdtls details//
  app.route('/exchjfdtls').post(exchjfdtls.create_a_exchjfdtls);
  app.route('/secured/exchjfdtls').post(exchjfdtls.create_a_exchjfdtls);

  app.route('/exchjfdtls/:p_Jobfair_Id/:p_Ex_id/:p_Active_YN').get(exchjfdtls.read_a_exchjfdtls);
  app.route('/secured/exchjfdtls/:p_Jobfair_Id/:p_Ex_id/:p_Active_YN').get(exchjfdtls.read_a_exchjfdtls);

  app.route('/empexchjfdtls/:p_Jobfair_Id/:p_Ex_id/:p_Active_YN/:p_LMBY').get(exchjfdtls.read_a_empexchjfdtls);
  //exchjfdtls Exchange Office//


  //monthlytotal details//
  app.route('/monthlytotal').post(monthlytotal.create_a_monthlytotal);
  app.route('/secured/monthlytotal').post(monthlytotal.create_a_monthlytotal);

  app.route('/monthlytotal/:p_S_no/:p_Xyear').get(monthlytotal.read_a_monthlytotal);
  app.route('/secured/monthlytotal/:p_S_no/:p_Xyear').get(monthlytotal.create_a_monthlytotal);
  //monthlytotal Exchange Office//


  //yealytarget details//
  app.route('/yealytarget').post(yealytarget.create_a_yealytarget);
  app.route('/secured/yealytarget').post(yealytarget.create_a_yealytarget);

  app.route('/yealytarget/:p_Target_Id/:p_Active_YN').get(yealytarget.read_a_yealytarget);
  app.route('/secured/yealytarget/:p_Target_Id/:p_Active_YN').get(yealytarget.create_a_yealytarget);
  //yealytarget Exchange Office//


  //monthlyreporting details//
  app.route('/monthlyreporting').post(monthlyreporting.create_a_monthlyreporting);
  app.route('/secured/monthlyreporting').post(monthlyreporting.create_a_monthlyreporting);

  app.route('/monthlyreporting/:p_JF_Id/:p_Active_YN').get(monthlyreporting.read_a_monthlyreporting);
  app.route('/secured/monthlyreporting/:p_JF_Id/:p_Active_YN').get(monthlyreporting.create_a_monthlyreporting);
  //monthlyreporting Exchange Office//


  //ccexchjfdtls details//
  app.route('/ccexchjfdtls').post(ccexchjfdtls.create_a_ccexchjfdtls);
  app.route('/secured/ccexchjfdtls').post(ccexchjfdtls.create_a_ccexchjfdtls);

  app.route('/ccexchjfdtls/:p_CC_Id/:p_Active_YN').get(ccexchjfdtls.read_a_ccexchjfdtls);
  app.route('/secured/ccexchjfdtls/:p_CC_Id/:p_Active_YN').get(ccexchjfdtls.create_a_ccexchjfdtls);
  //ccexchjfdtls Exchange Office//


  //CCDetails details//
  app.route('/CCDetails').post(CCDetails.create_a_CCDetails);
  app.route('/secured/CCDetails').post(CCDetails.create_a_CCDetails);

  //ccregjobseeker details//
  app.route('/ccregjobseeker').post(ccregjobseeker.create_a_ccregjobseeker);
  app.route('/secured/ccregjobseeker').post(ccregjobseeker.create_a_ccregjobseeker);

  //selectdtls details//
  app.route('/selectdtls').post(selectdtls.create_a_selectdtls);
  app.route('/secured/selectdtls').post(selectdtls.create_a_selectdtls);

  //mstwlcategory details//
  app.route('/mstwlcategory').post(mstwlcategory.create_a_mstwlcategory);
  app.route('/secured/mstwlcategory').post(mstwlcategory.create_a_mstwlcategory);

  app.route('/mstwlcategory/:p_WL_Cat_Id/:p_Active_YN').get(mstwlcategory.read_a_mstwlcategory);
  app.route('/secured/mstwlcategory/:p_WL_Cat_Id/:p_Active_YN').get(mstwlcategory.read_a_mstwlcategory);
  //mstwlcategory Exchange Office//


  //wlurl details//
  app.route('/wlurl').post(wlurl.create_a_wlurl);
  app.route('/secured/wlurl').post(wlurl.create_a_wlurl);

  app.route('/wlurl/:p_WL_Id/:p_WL_Cat_Id/:p_Active_YN').get(wlurl.read_a_wlurl);
  app.route('/secured/wlurl/:p_WL_Id/:p_WL_Cat_Id/:p_Active_YN').get(wlurl.read_a_wlurl);
  //wlurl Exchange Office//


  //exchjfdtlsbyStatus details//
  app.route('/exchjfdtlsbyStatus/:p_Jobfair_Id/:p_Ex_id/:p_Active_YN/:p_Status').get(exchjfdtls.read_a_exchjfdtlsbyStatus);
  app.route('/secured/exchjfdtlsbyStatus/:p_Jobfair_Id/:p_Ex_id/:p_Active_YN/:p_Status').get(exchjfdtls.read_a_exchjfdtlsbyStatus);

  //adminresetPass details//
  app.route('/adminresetPass/:p_RegistrationId/:p_IdentificationNumber').get(adminresetPass.read_a_adminresetPass);
  app.route('/secured/adminresetPass/:p_RegistrationId/:p_IdentificationNumber').get(adminresetPass.read_a_adminresetPass);

  //fileuploaddtls details//
  app.route('/fileuploaddtls').post(fileuploaddtls.create_a_fileuploaddtls);
  app.route('/secured/fileuploaddtls').post(fileuploaddtls.create_a_fileuploaddtls);

  app.route('/fileuploaddtls/:p_File_ID').get(fileuploaddtls.read_a_fileuploaddtls);
  app.route('/secured/fileuploaddtls/:p_File_ID').get(fileuploaddtls.read_a_fileuploaddtls);
  //fileuploaddtls Exchange Office//


  //advcategory details//
  app.route('/advcategory').post(advcategory.create_a_advcategory);
  app.route('/secured/advcategory').post(advcategory.create_a_advcategory);

  app.route('/advcategory/:p_Adv_Cat_Id/:p_Active_YN').get(advcategory.read_a_advcategory);
  app.route('/secured/advcategory/:p_Adv_Cat_Id/:p_Active_YN').get(advcategory.read_a_advcategory);
  //advcategory Exchange Office//

  //GroupDiscussion details//
  app.route('/GroupDiscussion').post(GroupDiscussion.create_a_GroupDiscussion);
  app.route('/secured/GroupDiscussion').post(GroupDiscussion.create_a_GroupDiscussion);

  //advertisement details//
  app.route('/advertisement').post(advertisement.create_a_advertisement);
  app.route('/secured/advertisement').post(advertisement.create_a_advertisement);

  app.route('/advertisement/:p_Adv_Id/:p_Active_YN').get(advertisement.read_a_advertisement);
  app.route('/secured/advertisement/:p_Adv_Id/:p_Active_YN').get(advertisement.read_a_advertisement);
  //advcategory Exchange Office//


  //PhotoDetails details//
  app.route('/PhotoDetails').post(PhotoDetails.create_a_PhotoDetails);
  app.route('/secured/PhotoDetails').post(PhotoDetails.create_a_PhotoDetails);

  //PhotoUpload details//
  app.route('/PhotoUpload').post(PhotoDetails.create_a_PhotoUpload);
  app.route('/secured/PhotoUpload').post(PhotoDetails.create_a_PhotoUpload);

  // //PhotoVerify details//
  // app.route('/PhotoVerify').post(PhotoDetails.create_a_PhotoVerify);
  // app.route('/secured/PhotoVerify').post(PhotoDetails.create_a_PhotoVerify);


  //admexchmstnic details//
  app.route('/admexchmstnic/:p_NICCode/:p_Division_Code/:p_Active_YN').get(admexchmstnic.read_a_admexchmstnic);
  app.route('/secured/admexchmstnic/:p_NICCode/:p_Division_Code/:p_Active_YN').get(admexchmstnic.read_a_admexchmstnic);

  //AdminVerification details//
  app.route('/AdminVerification/:p_RegistraionNumber/:p_CandidateName/:p_RegDate/:p_DOB/:p_RegistrationMonth/:p_RegistrationYear').get(AdminVerification.read_a_AdminVerification);
  app.route('/secured/AdminVerification/:p_RegistraionNumber/:p_CandidateName/:p_RegDate/:p_DOB/:p_RegistrationMonth/:p_RegistrationYear').get(AdminVerification.read_a_AdminVerification);

  //RegistrationCancellation details//
  app.route('/RegistrationCancellation').post(RegistrationCancellation.create_a_RegistrationCancellation);
  app.route('/secured/RegistrationCancellation').post(RegistrationCancellation.create_a_RegistrationCancellation);

  //LIveRegisterReport details//
  app.route('/LIveRegisterReport/:p_RegMonth/:p_RegYear/:p_Gender/:p_Category').get(LIveRegisterReport.read_a_LIveRegisterReport);
  app.route('/secured/LIveRegisterReport/:p_RegMonth/:p_RegYear/:p_Gender/:p_Category').get(LIveRegisterReport.read_a_LIveRegisterReport);

  //QualififcationWiseDetail details//
  app.route('/QualififcationWiseDetail/:p_ExamPassed/:p_Subject_Group/:p_Ex_id').get(QualififcationWiseDetail.read_a_QualififcationWiseDetail);
  app.route('/secured/QualififcationWiseDetail/:p_ExamPassed/:p_Subject_Group/:p_Ex_id').get(QualififcationWiseDetail.read_a_QualififcationWiseDetail);

  //JSStatiscalReport details//
  app.route('/JSStatiscalReport/:p_FromDate/:p_ToDate').get(JSStatiscalReport.read_a_JSStatiscalReport);
  app.route('/secured/JSStatiscalReport/:p_FromDate/:p_ToDate').get(JSStatiscalReport.read_a_JSStatiscalReport);

  //admusermenu details//
  app.route('/admusermenu/:p_user_type_id').get(admusermenu.read_a_admusermenu);
  app.route('/secured/admusermenu/:p_user_type_id').get(admusermenu.read_a_admusermenu);

  //EmpRegister details//
  app.route('/EmpRegister').post(EmpRegister.create_a_EmpRegister);
  app.route('/secured/EmpRegister').post(EmpRegister.create_a_EmpRegister);

  app.route('/EmpRegbyEntryby/:p_EntryBy').get(EmpRegister.read_a_Empuser);

  //CategoryWiseSetPostCountN details//
  app.route('/CategoryWiseSetPostCountN').post(CategoryWiseSetPostCountN.create_a_CWCount);
  app.route('/secured/CategoryWiseSetPostCountN').post(CategoryWiseSetPostCountN.create_a_CWCount);

  //GetRptName details//
  app.route('/GetRptName/:p_RptID/:p_Module_Type/:p_Active_YN').get(GetRptName.read_a_GetRptName);
  app.route('/secured/GetRptName/:p_RptID/:p_Module_Type/:p_Active_YN').get(GetRptName.read_a_GetRptName);

  //GetOfficeReport details//
  app.route('/GetOfficeReport/:p_Rpt_Id').get(GetRptName.read_a_GetOfficeReport);
  app.route('/secured/GetOfficeReport/:p_Rpt_Id').get(GetRptName.read_a_GetOfficeReport);

  //GetCurrentJFDetailsN details//
  app.route('/GetCurrentJFDetailsN/:p_year/:p_month/:p_Ex_Id').get(GetCurrentJFDetailsN.read_a_GetCurrentJFDetailsN);
  app.route('/secured/GetCurrentJFDetailsN/:p_year/:p_month/:p_Ex_Id').get(GetCurrentJFDetailsN.read_a_GetCurrentJFDetailsN);

  //CounsellingByMonth details//
  app.route('/CounsellingByMonth/:p_MonthId/:p_year/:p_Ex_Id').get(jobseeker.read_a_CounsellingByMonth);
  app.route('/secured/CounsellingByMonth/:p_MonthId/:p_year/:p_Ex_Id').get(jobseeker.read_a_CounsellingByMonth);

  //CounsellingByMonthWise details//
  app.route('/CounsellingByMonthWise/:p_Frmyear/:p_Toyear/:p_Ex_Id').get(jobseeker.read_a_CounsellingByMonthWise);
  app.route('/secured/CounsellingByMonthWise/:p_Frmyear/:p_Toyear/:p_Ex_Id').get(jobseeker.read_a_CounsellingByMonthWise);

  //ExchNotWorking details//
  app.route('/ExchNotWorking/:p_day').get(ExchNotWorking.read_a_ExchNotWorking);
  app.route('/secured/ExchNotWorking/:p_day').get(ExchNotWorking.read_a_ExchNotWorking);

  //RenewalCurrentStatus//
  app.route('/RenewalCurrentStatus/:p_year').get(RenewalCurrentStatus.read_a_RenewalCurrentStatus);
  app.route('/secured/RenewalCurrentStatus/:p_year').get(RenewalCurrentStatus.read_a_RenewalCurrentStatus);

  //emp count//
  app.route('/regempcount/').get(regempcount.read_a_regempcount);
  app.route('/secured/regempcount/').get(regempcount.read_a_regempcount);

  //RegEmployerDetail details//
  app.route('/RegEmployerDetail/:p_Ex_id').get(RegEmployerDetail.read_a_RegEmployerDetail);
  app.route('/secured/RegEmployerDetail/:p_Ex_id').get(RegEmployerDetail.read_a_RegEmployerDetail);

  //employer registrations Route Begin
  app.route('/employerregistrations').post(employerregistrations.create_a_EmployerRegistration);
  app.route('/employerregistrations/:p_EmployerId/:p_IpAddress/:p_UserId').get(employerregistrations.read_a_EmployerRegistration);
  //employer registrations Route End


  //validateEmplogin Route Begin
  app.route('/validateemplogin/:p_UserId/:p_Password').get(validateemplogin.read_a_validateemplogin);

  // app.route('/EmpForgotPassword/:p_UserId').get(ValidateempLogin.read_a_EmpForgotPassword);
  // app.route('/secured/EmpForgotPassword/:p_UserId').get(ValidateempLogin.read_a_EmpForgotPassword);

  app.route('/EmpForgotPassword/:p_UserId').get(validateemplogin.read_a_EmpForgotPassword);
  app.route('/secured/EmpForgotPassword/:p_UserId').get(validateemplogin.read_a_EmpForgotPassword);

  //validateEmplogin Route End

  //JobPost Route B
  app.route('/JobPost').post(JobPost.create_a_JobPost);
  //JobPost Route End
  //Fetch Job Begin
  app.route('/Jobs/:p_Location/:p_Flag').get(FetchJob.read_a_FetchJob);
  //Fetch Job End

  //EmployerAdminVerification Begin
  app.route('/EmployerAdminVerification/:p_RegistraionNumber/:p_RegistrationMonth/:p_RegistrationYear/:p_Emp_Name/:p_Company_Type').get(EmployerAdminVerification.read_a_EmployerAdminVerification);
  //EmployerAdminVerification End


  //DeleteEmpDetail Begin
  app.route('/DeleteEmpDetail/:p_Emp_RegId').get(EmployerAdminVerification.read_a_DeleteEmpDetail);
  //DeleteEmpDetail End

  //EmployerRegDetail Begin
  app.route('/EmployerRegDetail/:p_Emp_RegId').get(employerregistrations.read_a_EmployerRegDetail);
  app.route('/secured/EmployerRegDetail/:p_Emp_RegId').get(employerregistrations.read_a_EmployerRegDetail);
  app.route('/EmployerRegDetailbyEmail/:p_Email').get(employerregistrations.read_a_EmployerRegDetailbyEmail);
  app.route('/Employer_ChangePasswordbyemail/:p_Emp_id').get(employerregistrations.read_a_EmployerChangePasswordbyemail);



  //EmployerRegDetail Route End


  //emp count//


  //ProEmpX6 details//
  app.route('/ProEmpX6').post(ProEmpX6.create_a_ProEmpX6);
  app.route('/secured/ProEmpX6').post(ProEmpX6.create_a_ProEmpX6);

  //CandidateJobAlert
  app.route('/secured/CandidateJobAlert/:p_EducationId/:p_DesignationId/:p_DistrictId').get(ProEmpX6.read_a_CandidateJobAlert);
  app.route('/CandidateJobAlert/:p_EducationId/:p_DesignationId/:p_DistrictId').get(ProEmpX6.read_a_CandidateJobAlert);

  //Emp by Exchange
  app.route('/EmpByExchange/:p_Ex_id').get(employerregistrations.read_a_EmployerByExchange);
  app.route('/secured/EmpByExchange/:p_Ex_id').get(employerregistrations.read_a_EmployerByExchange);
  app.route('/PlacementEmployer').get(employerregistrations.read_a_PlacementEmployer);
  //WorkingExchange details//
  app.route('/WorkingExchange/:p_flag/:p_Division_id/:p_Ex_id').get(WorkingExchange.read_a_WorkingExchange);
  app.route('/secured/WorkingExchange/:p_flag/:p_Division_id/:p_Ex_id').get(WorkingExchange.read_a_WorkingExchange);

  //MstDivision details//
  app.route('/MstDivision/:p_Division_id/:p_Active_YN').get(MstDivision.read_a_MstDivision);
  app.route('/secured/MstDivision/:p_Division_id/:p_Active_YN').get(MstDivision.read_a_MstDivision);

  //validateadmin details//
  app.route('/validateadmin/:p_Emp_Id/:p_Password').get(validateadmin.read_a_validateolexlogin);
  app.route('/secured/validateadmin/:p_Emp_Id/:p_Password').get(validateadmin.read_a_validateolexlogin);

  //AdmProUserUpdatePwd details//
  app.route('/AdmProUserUpdatePwd').post(AdmProUserUpdatePwd.create_a_AdmProUserUpdatePwd);
  app.route('/secured/AdmProUserUpdatePwd').post(AdmProUserUpdatePwd.create_a_AdmProUserUpdatePwd);

  //Lrstatus details//
  app.route('/Lrstatus').get(Lrstatus.read_a_Lrstatus);
  app.route('/secured/Lrstatus').get(Lrstatus.read_a_Lrstatus);

  //YearWiseLRStatus details//
  app.route('/YearWiseLRStatus/:P_year').get(YearWiseLRStatus.read_a_YearWiseLRStatus);
  app.route('/secured/YearWiseLRStatus/:P_year').get(YearWiseLRStatus.read_a_YearWiseLRStatus);

  //Duplicateregstatus details//
  app.route('/Duplicateregstatus').get(Duplicateregstatus.read_a_Duplicateregstatus);
  app.route('/secured/Duplicateregstatus').get(Duplicateregstatus.read_a_Duplicateregstatus);

  //Monthlyregstatus details//
  app.route('/Monthlyregstatus/:P_monthid/:p_year').get(Monthlyregstatus.read_a_Monthlyregstatus);
  app.route('/secured/Monthlyregstatus/:P_monthid/:p_year').get(Monthlyregstatus.read_a_Monthlyregstatus);

  app.route('/FetchMonthlyregstatus/:p_Ex_Id/:P_monthid/:p_year').get(Monthlyregstatus.read_a_FetchMonthlyregstatus);


  app.route('/MonthlyRegistrationDateWise/:p_Division_id/:p_Ex_Id/:p_MonthId/:p_year').get(Monthlyregstatus.read_a_MonthlyRegistrationDateWise);
  app.route('/secured/MonthlyRegistrationDateWise/:p_Division_id/:p_Ex_Id/:p_MonthId/:p_year').get(Monthlyregstatus.read_a_MonthlyRegistrationDateWise);

  //placementdetails details//
  app.route('/placementdetails').post(placementdetails.create_a_placementdetails);
  app.route('/secured/placementdetails').post(placementdetails.create_a_placementdetails);
  app.route('/verifyplacement_documents').post(placementdetails.create_a_verifyplacementdetails);
  app.route('/placementdetailsexcel').post(placementdetails.create_a_placementdetailsexcel);
  //placementdetails details//
  app.route('/placementdetails/:p_placement_deail_id/:p_category_id/:p_Candidate_district/:p_Month/:p_Year/:p_Gender/:p_Company_id/:p_Designation_Id/:p_Entry_By').get(placementdetails.read_a_placementdetails);
  app.route('/secured/placementdetails/:p_placement_deail_id/:p_category_id/:p_Candidate_district/:p_Month/:p_Year/:p_Gender:p_Company_id/:p_Designation_Id/:p_Entry_By').get(placementdetails.read_a_placementdetails);

  //HandicappedDocs details//
  app.route('/HandicappedDocs').post(HandicappedDocs.create_a_HandicappedDocs);
  app.route('/secured/HandicappedDocs').post(HandicappedDocs.create_a_HandicappedDocs);

  //USPGetSummary1 details//
  app.route('/USPGetSummary1').get(USPGetSummary1.read_a_USPGetSummary1);
  app.route('/secured/USPGetSummary1').get(USPGetSummary1.read_a_USPGetSummary1);
  //USPGetSummary1 details//
  app.route('/USPGetSummary2').get(USPGetSummary1.read_a_USPGetSummary2);
  app.route('/secured/USPGetSummary2').get(USPGetSummary1.read_a_USPGetSummary2);

  //candetail details//
  app.route('/candetail/:P_RegistrationId').get(candetail.read_a_candetail);
  app.route('/secured/candetail/:P_RegistrationId').get(candetail.read_a_candetail);

  //USPGetJobfairStatus details//
  app.route('/USPGetJobfairStatus').get(USPGetJobfairStatus.read_a_USPGetJobfairStatus);
  app.route('/secured/USPGetJobfairStatus').get(USPGetJobfairStatus.read_a_USPGetJobfairStatus);

  //GetJobFairdetails details//
  app.route('/GetJobFairdetails/:p_year/:p_month').get(GetJobFairdetails.read_a_GetJobFairdetails);
  app.route('/secured/GetJobFairdetails/:p_year/:p_month').get(GetJobFairdetails.read_a_GetJobFairdetails);

  //Summaryjsexchwise details//
  app.route('/Summaryjsexchwise').get(Summaryjsexchwise.read_a_Summaryjsexchwise);
  app.route('/secured/Summaryjsexchwise').get(Summaryjsexchwise.read_a_Summaryjsexchwise);

  //currentstatusrt details//
  app.route('/currentstatusrt/:p_Division_id/:p_Ex_id/:p_date').get(currentstatusrt.read_a_currentstatusrt);
  app.route('/secured/currentstatusrt/:p_Division_id/:p_Ex_id/:p_date').get(currentstatusrt.read_a_currentstatusrt);

  //Duplicatereg details//
  app.route('/Duplicatereg').get(Duplicatereg.read_a_Duplicatereg);
  app.route('/secured/Duplicatereg').get(Duplicatereg.read_a_Duplicatereg);

  //LRCurrentStatus details//
  app.route('/LRCurrentStatus/:p_FromDt/:p_ToDt').get(LRCurrentStatus.read_a_LRCurrentStatus);
  app.route('/secured/LRCurrentStatus/:p_FromDt/:p_ToDt').get(LRCurrentStatus.read_a_LRCurrentStatus);

  //ExchJSSearch details//
  app.route('/ExchJSSearch/:p_Ex_id/:p_Qualif_Level_id/:p_Qualif_id/:p_Sub_Group_id/:p_Flag').get(ExchJSSearch.read_a_ExchJSSearch);
  app.route('/secured/ExchJSSearch/:p_Ex_id/:p_Qualif_Level_id/:p_Qualif_id/:p_Sub_Group_id/:p_Flag').get(ExchJSSearch.read_a_ExchJSSearch);

  app.route('/exchangebydivision/:p_DivisionId').get(exchangebydivision.read_a_ExchangeByDivision);
  app.route('/secured/exchangebydivision/:p_DivisionId').get(exchangebydivision.read_a_ExchangeByDivision);
  //RptES11Entry details//
  app.route('/RptES11Entry').post(RptES11Entry.create_a_ES11Entry);
  app.route('/secured/RptES11Entry').post(RptES11Entry.create_a_ES11Entry);

  app.route('/ES11_Rpt/:p_Ex_id/:p_Rpt_Month/:p_Rpt_Year').get(RptES11Entry.read_a_RptESS11);
  app.route('/secured/ES11_Rpt/:p_Ex_id/:p_Rpt_Month/:p_Rpt_Year').get(RptES11Entry.read_a_RptESS11);

  app.route('/ES11_Entry_Rpt').post(RptES11Entry.create_a_RptES11Entry);
  app.route('/secured/ES11_Entry_Rpt').post(RptES11Entry.create_a_RptES11Entry);

  app.route('/ES11_Submit_Rpt').post(RptES11Entry.create_a_RptES11Submit);
  app.route('/getExchange_ES11Rpt/:p_Rpt_Month/:p_Rpt_Year').get(RptES11Entry.read_a_exchangeRptESS11);
 
  //esreport//
  app.route('/esverifyrpt').post(esverifyrpt.create_a_esverifyrpt);
  app.route('/secured/esverifyrpt').post(esverifyrpt.create_a_esverifyrpt);

  //RptRevert details//
  app.route('/RptRevert/:p_Exname/:p_Rpt_Month/:p_Rpt_Year/:p_RepID').get(RptRevert.read_a_RptRevert);
  app.route('/secured/RptRevert/:p_Exname/:p_Rpt_Month/:p_Rpt_Year/:p_RepID').get(RptRevert.read_a_RptRevert);

  //Status not send to Ho Report//
  //ExchJSSearch details//
  app.route('/rtpnotsendho/:p_FromDt/:p_ToDt/:p_Rpt_Month/:p_Rpt_Year/:p_RepID').get(rtpnotsendho.read_a_rtpnotsendho);
  app.route('/secured/rtpnotsendho/:p_FromDt/:p_ToDt/:p_Rpt_Month/:p_Rpt_Year/:p_RepID').get(rtpnotsendho.read_a_rtpnotsendho);

  //RptHOES11 details//
  app.route('/RptHOES11/:p_Ex_id/:p_Rpt_Month/:p_Rpt_Year').get(RptHOES11.read_a_RptHOES11);
  app.route('/secured/RptHOES11/:p_Ex_id/:p_Rpt_Month/:p_Rpt_Year').get(RptHOES11.read_a_RptHOES11);

  //RptHOExchStatu details//
  app.route('/RptHOExchStatu/:p_FromDt/:p_ToDt/:p_Rpt_Month/:p_Rpt_Year/:p_RepID').get(RptHOExchStatu.read_a_RptHOExchStatu);
  app.route('/secured/RptHOExchStatu/:p_FromDt/:p_ToDt/:p_Rpt_Month/:p_Rpt_Year/:p_RepID').get(RptHOExchStatu.read_a_RptHOExchStatu);

  //NotSendHOExchStatus details//
  app.route('/NotSendHOExchStatus/:p_FromDt/:p_ToDt/:p_Rpt_Month/:p_Rpt_Year/:p_RepID').get(RptHOExchStatu.read_a_NotSendHOExchStatus);
  app.route('/secured/NotSendHOExchStatus/:p_FromDt/:p_ToDt/:p_Rpt_Month/:p_Rpt_Year/:p_RepID').get(RptHOExchStatu.read_a_NotSendHOExchStatus);

  //LRtotaltoday details//
  app.route('/LRtotaltoday').get(LRtotaltoday.read_a_LRtotaltoday);
  app.route('/secured/LRtotaltoday').get(LRtotaltoday.read_a_LRtotaltoday);

  //Categorywisegenderdetail details//
  app.route('/Categorywisegenderdetail').get(Categorywisegenderdetail.read_a_Categorywisegenderdetail);
  app.route('/secured/Categorywisegenderdetail').get(Categorywisegenderdetail.read_a_Categorywisegenderdetail);

  //Religionwisegenderdetail details//
  app.route('/Religionwisegenderdetail').get(Religionwisegenderdetail.read_a_Religionwisegenderdetail);
  app.route('/secured/Religionwisegenderdetail').get(Religionwisegenderdetail.read_a_Religionwisegenderdetail);

  //Statewisegenderdetail details//
  app.route('/Statewisegenderdetail').get(Statewisegenderdetail.read_a_Statewisegenderdetail);
  app.route('/secured/Statewisegenderdetail').get(Statewisegenderdetail.read_a_Statewisegenderdetail);

  //Hcwisegenderdetail details//
  app.route('/Hcwisegenderdetail').get(Hcwisegenderdetail.read_a_Hcwisegenderdetail);
  app.route('/secured/Hcwisegenderdetail').get(Hcwisegenderdetail.read_a_Hcwisegenderdetail);
  //Search Jobs
  //app.route('/SearchJob/:p_Designation/:p_Placeofwork').get(SearchJob.read_a_SearchJobs);
  app.route('/SearchJob').post(SearchJob.create_a_SearchJobs);

  //DataBaseSearch//
  app.route('/DataBaseSearch').post(DataBaseSearch.create_a_DataBaseSearch);
  app.route('/secured/DataBaseSearch').post(DataBaseSearch.create_a_DataBaseSearch);

  app.route('/searchengineutilitylog').post(DataBaseSearch.create_a_searchengineutility_logs);
  app.route('/secured/searchengineutilitylog').post(DataBaseSearch.create_a_searchengineutility_logs);


  //Employer Change Password
  app.route('/EmpChangePassword').post(EmpChangePass.create_a_empChangepass);
  app.route('/secured/EmpChangePassword').post(EmpChangePass.create_a_empChangepass);

  //EmpDesiredSkill details//
  app.route('/EmpDesiredSkill/:p_Sector_Id/:p_Role').get(EmpDesiredSkill.read_a_EmpDesiredSkill);
  app.route('/secured/EmpDesiredSkill/:p_Sector_Id/:p_Role').get(EmpDesiredSkill.read_a_EmpDesiredSkill);
  // job_alert
  app.route('/job_alert').post(job_alert.create_a_job_alert);
  app.route('/secured/job_alert').post(job_alert.create_a_job_alert);

  //FeaturedJob details//
  app.route('/FeaturedJob/:p_Designation/:p_Placeofwork/').get(FeaturedJob.read_a_FeaturedJob);
  app.route('/secured/FeaturedJob/:p_Designation/:p_Placeofwork/').get(FeaturedJob.read_a_FeaturedJob);

  //TopEmployers details//
  app.route('/TopEmployers').get(TopEmployers.read_a_TopEmployers);
  app.route('/secured/TopEmployers').get(TopEmployers.read_a_TopEmployers);

  // Test
  app.route('/Test').post(Test.create_a_Test);
  app.route('/secured/Test').post(Test.create_a_Test);

  app.route('/Test/:p_userid').get(Test.read_a_Test);
  app.route('/secured/Test/:p_userid').get(Test.read_a_Test);

  app.route('/TestDelete/:p_userid/:p_firstname/:p_lastname/:p_address/:p_mobno/:p_Email/:p_Gender/:p_Flag').get(Test.read_a_TestDelete);

  //SearchJobById details//
  app.route('/SearchJobById/:p_JobId').get(SearchJobById.read_a_SearchJobById);
  app.route('/secured/SearchJobById/:p_JobId').get(SearchJobById.read_a_SearchJobById);

  // SavedAppliedjobs
  app.route('/SavedAppliedjobs').post(SavedAppliedjobs.create_a_SavedAppliedjobs);
  app.route('/secured/SavedAppliedjobs').post(SavedAppliedjobs.create_a_SavedAppliedjobs);

  app.route('/SavedAppliedjobs/:p_JobId/:p_JSRegNO/:p_Save/:p_Apply').get(SavedAppliedjobs.read_a_SavedAppliedjobs);
  app.route('/secured/SavedAppliedjobs/:p_JobId/:p_JSRegNO/:p_Save/:p_Apply').get(SavedAppliedjobs.read_a_SavedAppliedjobs);

  // jobfairregistration
  app.route('/jobfairregistration').post(jobfairregistration.create_a_jobfairregistration);
  app.route('/secured/jobfairregistration').post(jobfairregistration.create_a_jobfairregistration);

  app.route('/jobfairempreg').post(jobfairregistration.create_a_jobfairempreg);
  app.route('/secured/jobfairempreg').post(jobfairregistration.create_a_jobfairempreg);

  // jobseekerregistration
  app.route('/jobseekerregistration').post(jobseekerregistration.create_a_jobseekerregistration);
  app.route('/secured/jobseekerregistration').post(jobseekerregistration.create_a_jobseekerregistration);

  // savedjobs
  app.route('/savedjobs').post(savedjobs.create_a_savedjobs);
  app.route('/secured/savedjobs').post(savedjobs.create_a_savedjobs);

  //saveapplyCounter
  app.route('/saveapplyCounter/:p_jsREGNO').get(saveapplyCounter.read_a_saveapplyCounter);
  app.route('/secured/saveapplyCounter/:p_jsREGNO').get(saveapplyCounter.read_a_saveapplyCounter);

  //appliedjobbyregno
  app.route('/appliedjobbyregno/:p_jsREGNO').get(appliedjobbyregno.read_a_appliedjobbyregno);
  app.route('/secured/appliedjobbyregno/:p_jsREGNO').get(appliedjobbyregno.read_a_appliedjobbyregno);

  //savedjobbyregno
  app.route('/savedjobbyregno/:p_jsREGNO').get(savedjobbyregno.read_a_savedjobbyregno);
  app.route('/secured/savedjobbyregno/:p_jsREGNO').get(savedjobbyregno.read_a_savedjobbyregno);

  // newSearchJob
  app.route('/newSearchJob').post(newSearchJob.create_a_newSearchJob);
  app.route('/secured/newSearchJob').post(newSearchJob.create_a_newSearchJob);

  //SearchJobByIderg
  app.route('/SearchJobByIdereg/:p_JobId/:p_JSRegNO').get(SearchJobByIdereg.read_a_SearchJobByIdereg);
  app.route('/secured/SearchJobByIdereg/:p_JobId/:p_JSRegNO').get(SearchJobByIdereg.read_a_SearchJobByIdereg);
  // employerverification
  app.route('/employerverification').post(employerverification.create_a_employerverification);
  app.route('/secured/employerverification').post(employerverification.create_a_employerverification);

  //EmployerWithJobCount
  app.route('/EmployerWithJobCount/:p_Emp_Regno/:p_Adt_YN').get(EmployerWithJobCount.read_a_EmployerWithJobCount);
  app.route('/secured/EmployerWithJobCount/:p_Emp_Regno/:p_Adt_YN').get(EmployerWithJobCount.read_a_EmployerWithJobCount);

  //SearchCandidateId
  app.route('/SearchCandidateId').post(SearchCandidateId.create_a_SearchCandidateId);
  app.route('/secured/SearchCandidateId').post(SearchCandidateId.create_a_SearchCandidateId);
  //Vacancy
  app.route('/Vacancy/:p_Emp_Regno').get(Vacancy.read_a_Vacancy);
  app.route('/secured/Vacancy/:p_Emp_Regno').get(Vacancy.read_a_Vacancy);

  //SearchCandidateByJobId
  app.route('/SearchCandidateByJobId/:p_JobId').get(SearchCandidateByJobId.read_a_SearchCandidateByJobId);
  app.route('/secured/SearchCandidateByJobId/:p_JobId').get(SearchCandidateByJobId.read_a_SearchCandidateByJobId);

  //jobseekerreg
  app.route('/jobseekerreg/:p_Candidateid').get(jobseekerreg.read_a_jobseekerreg);
  app.route('/secured/jobseekerreg/:p_Candidateid').get(jobseekerreg.read_a_jobseekerreg);

  // UpdJobStatus
  app.route('/UpdJobStatus').post(UpdJobStatus.create_a_UpdJobStatus);
  app.route('/secured/UpdJobStatus').post(UpdJobStatus.create_a_UpdJobStatus);

  //indexnote
  app.route('/secured/indexnote').get(admexchmstnic.read_a_indexnote);
  app.route('/indexnote').get(admexchmstnic.read_a_indexnote);

  // indexnote
  app.route('/indexnote').post(admexchmstnic.create_a_indexnote);
  app.route('/secured/indexnote').post(admexchmstnic.create_a_indexnote);

  //JobSearch
  app.route('/secured/JobSearch/:p_DesignationId/:p_Qualif_id/:p_DistrictId/:p_VacancyType_id').get(SearchCandidateId.read_a_JobSearch);
  app.route('/JobSearch/:p_DesignationId/:p_Qualif_id/:p_DistrictId/:p_VacancyType_id').get(SearchCandidateId.read_a_JobSearch);

  //Jobseekerinfo
  app.route('/secured/Jobseekerinfo/:p_RegistrationNumber').get(Jobseekerinfo.read_a_Jobseekerinfo);
  app.route('/Jobseekerinfo/:p_RegistrationNumber').get(Jobseekerinfo.read_a_Jobseekerinfo);

  //EmployerLogo
  //app.route('/secured/EmployerLogo/:p_Emp_Regno/:p_Employer_Logo/:p_Flag').get(employerregistrations.read_a_EmployerLogo);
  //app.route('/EmployerLogo/:p_Emp_Regno/:p_Employer_Logo/:p_Flag').get(employerregistrations.read_a_EmployerLogo);
app.route('/EmployerLogo').post(employerregistrations.create_a_EmployerLogo);

  //InActiveEmp Begin
  app.route('/InActiveEmp/:P_Emp_Regno/:p_V_Status/:p_Type').get(EmployerAdminVerification.read_a_InActiveEmp);
  //InActiveEmp End

  app.route('/GSTFile').post(employerregistrations.create_a_GSTFile);

  //DivisionByState
  app.route('/secured/divisionbystate/:p_StateId').get(state.read_a_divisionbystate);
  app.route('/divisionbystate/:p_StateId').get(state.read_a_divisionbystate);

  //CandidateJobFair
  //BloodGroup Route Begin
  app.route('/BloodGroup').post(BloodGroup.create_a_BloodGroup);
  app.route('/secured/BloodGroup').post(BloodGroup.create_a_BloodGroup);
  //BloodGroup Route End
  //BloodGroup Route Begin
  app.route('/Nationality').post(Nationality.create_a_Nationality);
  app.route('/secured/Nationality').post(Nationality.create_a_Nationality);
  //BloodGroup Route End
  //BloodGroup Route Begin

  app.route('/ActivityMaster/:p_Type').get(ActivityMaster.read_a_ActivityMaster);
  app.route('/secured/ActivityMaster/:p_Type').get(ActivityMaster.read_a_ActivityMaster);

  //BloodGroup Route End
  //PreLocation Begin
  app.route('/preflocation').post(preflocation.create_a_PrefLocation);
  app.route('/secured/preflocation').post(preflocation.create_a_PrefLocation);
  app.route('/preflocation/:p_Location_id/:p_Active_YN').get(preflocation.read_a_PrefLocation);
  app.route('/secured/preflocation/:p_Location_id/:p_Active_YN').get(preflocation.read_a_PrefLocation);
  //PreLocation Route End

  //WalkIn
  app.route('/WalkIn').get(ProEmpX6.create_a_WalkIn);
  app.route('/secured/WalkIn').get(ProEmpX6.create_a_WalkIn);

  //Interestedcandidate
  app.route('/Interestedcandidatejf').post(Interestedcandidatejf.create_a_Interestedcandidatejf);
  app.route('/secured/Interestedcandidatejf').post(Interestedcandidatejf.create_a_Interestedcandidatejf);

  app.route('/Interestedcandidatejf/:p_jf_Id').get(Interestedcandidatejf.read_a_Interestedcandidatejf);
  app.route('/secured/Interestedcandidatejf/:p_jf_Id').get(Interestedcandidatejf.read_a_Interestedcandidatejf);

  //itijfexchange
  app.route('/itijfexchange').post(itijfexchange.create_a_itijfexchange);
  app.route('/secured/itijfexchange').post(itijfexchange.create_a_itijfexchange);

  app.route('/itijfempmapping').post(itijfexchange.create_a_itijfempmapping);
  app.route('/secured/itijfempmapping').post(itijfexchange.create_a_itijfempmapping);
  app.route('/itiEmployer').get(jobfairregistration.read_a_itiEmployer);
  app.route('/secured/itiEmployer').get(jobfairregistration.read_a_itiEmployer);

  app.route('/jobfairEmpRegbyId/:p_JobfairEmpRegId').get(jobfairregistration.read_a_jobfairEmpRegbyId);
  app.route('/secured/jobfairEmpRegbyId/:p_JobfairEmpRegId').get(jobfairregistration.read_a_jobfairEmpRegbyId);

  app.route('/hallticket/:p_CandidateId/:p_jf_id').get(itijfexchange.read_a_hallticket);
  app.route('/secured/hallticket/:p_CandidateId/:p_jf_id').get(itijfexchange.read_a_hallticket);
  app.route('/itijfexchange/:p_Ex_id').get(itijfexchange.read_a_itijfexchange);
  app.route('/secured/itijfexchange/:p_Ex_id').get(itijfexchange.read_a_itijfexchange);
  app.route('/iti_institute').get(itijfexchange.read_a_itiinstitute);
  app.route('/secured/iti_institute').get(itijfexchange.read_a_itiinstitute);

  //Itiinstitute
  app.route('/Itiinstitute').post(Itiinstitute.create_a_Itiinstitute);
  app.route('/secured/Itiinstitute').post(Itiinstitute.create_a_Itiinstitute);

  app.route('/ItiinstituteName/:p_District').get(Itiinstitute.read_a_Districtwiseitiinstitute);
  app.route('/secured/ItiinstituteName/:p_District').get(Itiinstitute.read_a_Districtwiseitiinstitute);

  app.route('/ZonewiseDistrict/:p_ZoneName').get(Itiinstitute.read_a_ZonewiseDistrict);
  app.route('/secured/ZonewiseDistrict/:p_ZoneName').get(Itiinstitute.read_a_ZonewiseDistrict);

  //SelectedCandJobFair
  app.route('/SelectedCandJobFair').post(SelectedCandJobFair.create_a_SelectedCandJobFair);
  app.route('/secured/SelectedCandJobFair').post(SelectedCandJobFair.create_a_SelectedCandJobFair);
  
  //SectorRole Route Begin
app.route('/SectorRolemapping').post(SectorRole.create_a_SectorRole);
  app.route('/insfunctionarea').post(SectorRole.create_a_Role);
  app.route('/employementSector').post(SectorRole.create_a_Sector);  
app.route('/getFunctionalArea').get(SectorRole.read_a_getFunctionalArea);

  //JobFairFiltersById
  app.route('/JobFairFiltersById/:p_jf_id').get(Itiinstitute.read_a_JobFairFiltersById);
  app.route('/secured/JobFairFiltersById/:p_jf_id').get(Itiinstitute.read_a_JobFairFiltersById);

  app.route('/emailconfig').get(emailconfig.read_a_emailconfig);
  app.route('/secured/emailconfig').get(emailconfig.read_a_emailconfig);

  // app.route('/trademaster').post(trademaster.create_a_trademaster);
  // app.route('/secured/trademaster').post(trademaster.create_a_trademaster);

  app.route('/trademaster').get(trademaster.read_a_trademaster);
  app.route('/secured/trademaster').get(trademaster.read_a_trademaster);

app.route('/faq/:p_Type/:p_Subtype').get(faq.read_a_faq);

  //RegDetailsCategoryWise
  // app.route('/RegDetailsCategoryWise/:p_Ex_id').get(RegDetailsCategoryWise.read_a_RegDetailsCategoryWise);
  // app.route('/secured/RegDetailsCategoryWise/:p_Ex_id').get(RegDetailsCategoryWise.read_a_RegDetailsCategoryWise);
  app.route('/SearchJobRecommended').post(newSearchJob.create_a_newSearchJobRecommended);
  app.route('/secured/SearchJobRecommended').post(newSearchJob.create_a_newSearchJobRecommended);



app.route('/oldjobseekerreg').post(jobseekerreg.create_a_oldjobseekerreg);
app.route('/secured/oldjobseekerreg').post(jobseekerreg.create_a_oldjobseekerreg);

app.route('/knowyourregnew/:p_FilterType/:p_FilterValue/:p_FirstName/:p_LastName/:p_Gender').get(knowyourregnew.read_a_knowyourregnew);
app.route('/secured/knowyourregnew/:p_FilterType/:p_FilterValue/:p_FirstName/:p_LastName/:p_Gender').get(knowyourregnew.read_a_knowyourregnew);


app.route('/JobSeekerProfileStatusOLEX/:p_CandidateId').get(knowyourregnew.read_a_JobSeekerProfileStatusOLEX);
app.route('/secured/JobSeekerProfileStatusOLEX/:p_CandidateId').get(knowyourregnew.read_a_JobSeekerProfileStatusOLEX);

app.route('/UpdDistrictOffice').post(districtoffice.create_a_UpdDistrictOffice);
app.route('/secured/UpdDistrictOffice').post(districtoffice.create_a_UpdDistrictOffice);

app.route('/DeleteJobseekerRegistration/:p_CandidateId').get(jobseekerregistration.read_a_DeleteJobseekerRegistration);
  app.route('/secured/DeleteJobseekerRegistration/:p_CandidateId').get(jobseekerregistration.read_a_DeleteJobseekerRegistration);
  //adminDesignation Exchange Office//


app.route('/updjobseekerregistrationlogin').post(knowyourregnew.create_a_updjobseekerregistrationlogin);
// app.route('/secured/updjobseekerregistrationlogin').post(knowyourregnew.create_a_updjobseekerregistrationlogin);

app.route('/crisp/:p_DistrictId').get(crisp.read_a_crisp);
 //app.route('/secured/crisp/:p_DistrictId').get(crisp.read_a_crisp);


 app.route('/crispYearMaster/:p_YearId').get(crisp.read_a_crispYearMaster);
 //app.route('/secured/crispYearMaster/:p_YearId').get(crisp.read_a_crispYearMaster);

 app.route('/crispDistrictWiseCandidateCount/:p_DistrictId').get(crisp.read_a_crispDistrictWiseCandidateCount);

 app.route('/crispDistrictWiseCandidateDetail/:p_DistrictId/:p_Year').get(crisp.read_a_crispDistrictWiseCandidateDetail);


 app.route('/CategoryWiseCandidateCount/:p_CategoryId').get(crisp.read_a_crispCategoryWiseCandidateCount);

 app.route('/CategoryWiseCandidateDetail/:p_CategoryId/:p_Year').get(crisp.read_a_crispCategoryWiseCandidateDetail);

 app.route('/ES12_Rpt/:p_Year/:p_Ex_Id').get(RptES12Report.read_a_ES12Annual);

 app.route('/ES13_Rpt/:p_Year/:p_Ex_Id').get(RptES13Report.read_a_ES13);

 app.route('/ES25P2Entry').post(es25p2.create_a_ES25Part2);

 app.route('/ES14Entry').post(es14.create_a_ES14Entry);

 app.route('/ES21Entry').post(RptES21Entry.create_a_ES21Entry);
 app.route('/ES21_Master').get(RptES21Entry.read_a_Fetch_ES21_Master);

 app.route('/CallCentreManager').get(districtoffice.read_CallCentreManager);

 app.route('/Counselling_ByMonthConsolidate/:p_FrmMonthId/:p_ToMonthId/:p_Frmyear/:p_Toyear/:p_Ex_Id').get(Counselling.read_a_Counselling_ByMonthConsolidate);

 app.route('/Counselling_ByMonthWise/:p_Frmyear/:p_Toyear/:p_Ex_Id').get(Counselling.read_a_CounsellingByMonthWise);

 app.route('/JFRep_ByMonthConsolidate/:p_FrmMonthId/:p_ToMonthId/:p_Frmyear/:p_Toyear/:p_ex_id').get(jobfairmonthlyreporting.read_a_JFRep_ByMonthConsolidate);

 app.route('/Edu_with_Gender').get(es14.read_a_Edu_with_Gender);

 app.route('/CallCentreManagerPlacementDetails/:p_Month/:p_Year/:p_Emp_Id/:p_Flag').get(districtoffice.read_CCallCentreManager_PlacementDetails);

 app.route('/CareerCounsel_Pro_Reporting').post(jobseeker.create_a_CareerCounsel_Pro_Reporting);

 app.route('/Transfer_location').post(personaldetails.create_a_Transfer_location);


 app.route('/olexusertype').get(olexuser.read_a_olexusertype);
 app.route('/secured/olexusertype').get(olexuser.read_a_olexusertype);


 app.route('/olexuser').post(olexuser.create_a_olexuser);
 app.route('/secured/olexuser').post(olexuser.create_a_olexuser);

 app.route('/olexuserpass').post(olexuser.create_a_pass_update);


 app.route('/olexuserProfile').get(olexuser.read_a_olexuser);

 app.route('/er1_form/:p_EmpId/:p_from_date/:p_to_date/:p_EntryFrom/:p_EntryBy').get(er1form.read_a_ER1Form);

 
 app.route('/er1_vacancy_filled/:p_EmpId').get(er1form.read_a_er1_vacancy_filled);

 app.route('/er1_vacancy_unfilled/:p_EmpId').get(er1form.read_a_er1_vacancy_unfilled);

 app.route('/er1_vacancy_yashaswi/:p_EmpId').get(er1form.read_a_er1_vacancy_yashaswi);

 app.route('/AgeWise_QualifName/:p_Ex_Id/:p_AsOnDate').get(Qualification.read_a_JS_Rpt_AgeWise_QualifName);

 app.route('/Rpt_AgeWise_QualifLevel/:p_Ex_Id/:p_AsOnDate').get(Qualification.read_a_JS_Rpt_AgeWise_QualifLevel);

 var categorywise = require('../controller/categorywisequalificationreportController');
 app.route('/CRISP_QualificationWiseCandidateDetail/:p_QualificationId/:p_Year').get(crisp.read_a_JSCRISP_QualificationWiseCandidateDetail);

 app.route('/CRISP_Fetch_QualificationWiseCandidateCount/:p_QualificationId').get(crisp.read_a_JSCRISP_QualificationWiseCandidateCount);

 app.route('/CRISP_Fetch_QualificationMaster/:p_QualificationId').get(crisp.read_a_JSCRISP_QualificationMaster);

 app.route('/CRISP_Fetch_DistrictWisePlacementDetail/:p_DistrictId/:p_Year').get(crisp.read_a_JSCRISP_DistrictWisePlacementDetail);

 app.route('/CI_Pro_JS_Rpt_CategoryWise_QualifName/:p_Ex_Id/:p_AsOnDate').get(categorywise.read_a_JS_Rpt_CategoryWise_QualifName);

 app.route('/CI_Pro_JS_Rpt_CategoryWise_QualifLevel/:p_Ex_Id/:p_AsOnDate').get(categorywise.read_a_JS_Rpt_CategoryWise_QualifLevel);

 app.route('/DeteteMenus/:p_User_Type_Id/:p_Menu_Id/:p_Sub_Menu_Id/:p_flag').get(rolemenumapping.read_a_deletemenus);

 app.route('/knowyourregnew').post(knowyourregnew.create_a_KnowYourRegistration_New);

 app.route('/fetch_latestNotification').get(latest.read_a_JSlatestNotification);
 app.route('/CRISP_Fetch_DistrictWisePlacementCount/:p_DistrictId').get(crisp.read_a_JSCRISP_DistrictWisePlacementCount);

// app.route('/fetch_latestNotification').get(latest.read_a_JSlatestNotification);

 app.route('/CRISP_Fetch_DistrictWiseJobFairCandidateCount/:p_DistrictId').get(crisp.read_a_JSCRISP_DistrictWiseJobFairCandidateCount);

 app.route('/CRISP_Fetch_DistrictWiseCareerCandidateCount/:p_DistrictId').get(crisp.read_a_JSCRISPDistrictWiseCareerCandidateCount);

 app.route('/CategoryMaster/:p_CategoryId').get(crisp.read_a_JSCategoryMaster);

 app.route('/AgeWiseCandidateCount').get(crisp.read_a_JSAgeWiseCandidateCount);

 app.route('/AgeWiseCandidateDetail/:p_AgeId/:p_Year').get(crisp.read_a_JSAgeWiseCandidateDetail);

 app.route('/savedsearch/:p_SearchId/:p_EmpId').get(searchcandidatereports.read_a_JSsavedsearch);

 app.route('/savedsearch').post(searchcandidatereports.create_a_savedsearch);

 app.route('/jobparticipant').post(SearchCandidateId.create_a_jobparticipant);

 app.route('/jobparticipant/:p_JobParticipantId/:p_SearchId').get(SearchCandidateId.read_a_JSjobparticipant);

 app.route('/SearchLogs').post(SearchLogs.create_a_SearchLogs);

 app.route('/TotalPlacementCount/:p_DistrictId').get(placementdetails.read_a_placementdetailsCount);
 
 app.route('/Fetch_ER1Rpt/:p_from_date/:p_to_date/:p_EntryFrom/:p_EntryBy').get(er1form.read_a_Fetch_ER1_Rpt);

 app.route('/ER1Rpt_COUNT/:p_Ex_id/:p_EntryBy').get(er1form.read_a_Fetch_ER1_RptCount);

 app.route('/Emp_Details/:p_Ex_id/:p_EntryBy').get(er1form.read_a_Fetch_Rpt_EmpDetail);

 app.route('/savedsearchCandidate').post(searchcandidatereports.create_a_CandidateSearch);

 var empfolders = require('../controller/EmpFoldersController');
 app.route('/Emp_Folders/:p_EmpId/:p_Active_YN').get(empfolders.read_a_EmpFolders);
 app.route('/InsupdEmpFolders').post(empfolders.create_a_EmpFolders);
 app.route('/DeleteEmpFolders/:p_folder_Id').get(empfolders.read_a_DeleteEmpFolders);

 app.route('/jobfairReport/:p_Ex_id/:p_Jobfair_FromDt/:p_Jobfair_ToDt/').get(jobfair.read_a_JobFairreport);

 app.route('/JobSeekerRegister').post(EmpRegister.create_a_JobSeekerRegister);
  app.route('/secured/JobSeekerRegister').post(EmpRegister.create_a_JobSeekerRegister);


 app.route('/olexdash/:p_districtId').get(Olexdash.read_a_olexdash);

 app.route('/exchangejobfairEmp').post(Exchangejobfair.create_a_exchangejobfair);
 app.route('/ExchangeWiseJF').post(Exchangejobfair.create_a_employerexchangejf);
 app.route('/ExjobfairReport/:p_Ex_id/:p_Jobfair_FromDt/:p_Jobfair_ToDt/').get(Exchangejobfair.read_a_Exjobfairreport);

 //renew routes
app.route('/registration_renew').post(registration.create_a_renewregistration);
app.route('/registration_renewforolduser').post(registration.create_a_renewregistrationforolduser);
app.route('/empjfReport').post(exchjfdtls.create_a_empjfReport);
app.post('/sendsms',(req,res)=>{
//console.log(req)
    Axios.post(`https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT?senderid=MPEMPL&username=DITMP-MPEMPL&secureKey=Empl*1107&smsservicetype=singlemsg&content=${req.body.message}&mobileno=${req.body.mobile}`)
  .then((response)=>{
    res.status(200).send({
      status:"success"
      })
  }).catch((err)=>{
    res.status(500).send({

      "data":"",
      "err":err
    })
  })
});
app.get('/getsendsms',(req,res)=>{
      Axios.post(`https://msdgweb.mgov.gov.in/esms/sendsmsrequest?senderid=MPEMPL&username=DITMP-MPEMPL&password=Empl*1107&smsservicetype=singlemsg&content=${req.query.Message}&mobileno=${req.query.Mobile}`)
    .then((response)=>{
      res.status(200).send({
        status:"success"
        })
    }).catch((err)=>{
      res.status(500).send({
  
        "data":"",
        "err":err
      })
    })
  })

  app.get('/getcricket',(req,res)=>{
    Axios.post(`https://msdgweb.mgov.gov.in/esms/sendsmsrequest?senderid=MPEMPL&username=DITMP-MPEMPL&password=Empl*1107&smsservicetype=singlemsg&content=${req.query.Message}&mobileno=${req.query.Mobile}`)
  .then((response)=>{
    res.status(200).send({
      status:"success"
      })
  }).catch((err)=>{
    res.status(500).send({

      "data":"",
      "err":err
    })
  })
})
app.post('/sentmail', function (req, res) {
    var msg = {
      html: req.body.Msg,
      createTextFromHtml: true,
      from: "noreply-mprojgar@mp.gov.in",
      to: req.body.To,
      subject: req.body.Subject,
      cc:req.body.CC?req.body.CC:'',
      bcc:req.body.BCC?req.body.BCC:''
    };
    transport.sendMail(msg, function (err) 
	{
console.log("response mail")
      if (err) {
console.log(err)
	res.status(500).send({

      "data":"",
      "err":err
    })
      }
else{
      return res.json({ "successMessage": "Credentials has been sent to your Email." });
}
    });
  });
app.post('/schedulezoommeet',(req,res)=>{
    var Zoom = require("zoomus")({
      key : "Hq5Y-YnRQC-mLQ4-9x3_Mg",
      secret : "nCE71q39XyQbnHJ1f0scUAzxamOs77WBs5xL"
    });
    console.log(req.body);
    var meeting = req.body;
  
  Zoom.meeting.create(meeting, function(res){
    console.log("meeting",meeting)
      if(res.error){
        console.log("error",res.error);
      } else {
        console.log("response",res);
      }
  });
  
  });
  app.post('/candidateregistration_ncs', bustHeaders, xmlparser(xmlOptions), (request, response) => {
   var p_xml_content=request.rawbody.split('')[1];
   console.log(p_xml_content);
    var sql = mysql.createConnection(credentials);
      sql.connect(function (err) {
      if (err) throw err;
      });
        
          sql.query("CALL InsUpd_Jobseeker_Registration_XML('"+p_xml_content+"')", function (err, res) {      
  
      if(err) {
              sql.end();
              console.log(err);
              //result(err, null);
          }
          else{
              sql.end();
              console.log(res);
              //result(null, res);
          }
      });   
    });
};



