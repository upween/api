'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var PersonalDetails = function PersonalDetails(personaldetails) {
    this.p_PersonalDetailsId = personaldetails.p_PersonalDetailsId;
    this.p_CandidateId = personaldetails.p_CandidateId;
    this.p_DateOfBirth = personaldetails.p_DateOfBirth;
    this.p_Resident = personaldetails.p_Resident;
    this.p_PermanentAddress = personaldetails.p_PermanentAddress;
    this.p_PresentAddress = personaldetails.p_PresentAddress;
    this.p_PinCode = personaldetails.p_PinCode;
    this.p_BloodGroup = personaldetails.p_BloodGroup;
    this.p_MaritalStatus = personaldetails.p_MaritalStatus;
    this.p_Category = personaldetails.p_Category;
    this.p_Religion = personaldetails.p_Religion;
    this.p_SubCaste = personaldetails.p_SubCaste;
    this.p_Nationality = personaldetails.p_Nationality;
    this.p_Language = personaldetails.p_Language;
    this.p_Proficiency = personaldetails.p_Proficiency;
    this.p_DifferentlyAbled = personaldetails.p_DifferentlyAbled;
    this.p_DifferentlyAbledType = personaldetails.p_DifferentlyAbledType;
    this.p_DiffAbledSubType = personaldetails.p_DiffAbledSubType;
    this.p_DisablityPriority = personaldetails.p_DisablityPriority;
    this.p_IpAddress = personaldetails.p_IpAddress;
    this.p_UserId = personaldetails.p_UserId;
    this.p_GreenCard = personaldetails.p_GreenCard;
    this.p_Area = personaldetails.p_Area;
    this.p_Block = personaldetails.p_Block;
    this.p_Panchayat = personaldetails.p_Panchayat;
    this.p_Village = personaldetails.p_Village;
    this.p_AreaOfInterest = personaldetails.p_AreaOfInterest;

    this.p_DistrictId = personaldetails.p_DistrictId;
    this.p_CityId = personaldetails.p_CityId;
    this.p_VillageId = personaldetails.p_VillageId;
    this.p_EntryBy = personaldetails.p_EntryBy;   
    this.p_Ip_Address = personaldetails.p_Ip_Address;
};
PersonalDetails.createPersonalDetails = function createUser(newPersonalDetails, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL InsUpd_PersonalDetailmaster('" + newPersonalDetails.p_PersonalDetailsId + "','" + newPersonalDetails.p_CandidateId + "','" + newPersonalDetails.p_DateOfBirth + "','" + newPersonalDetails.p_Resident + "','" + newPersonalDetails.p_PermanentAddress + "','" + newPersonalDetails.p_PresentAddress + "','" + newPersonalDetails.p_PinCode + "','" + newPersonalDetails.p_BloodGroup + "','" + newPersonalDetails.p_MaritalStatus + "','" + newPersonalDetails.p_Category + "','" + newPersonalDetails.p_Religion + "','" + newPersonalDetails.p_SubCaste + "','" + newPersonalDetails.p_Nationality + "','" + newPersonalDetails.p_Language + "','" + newPersonalDetails.p_Proficiency + "','" + newPersonalDetails.p_DifferentlyAbled + "','" + newPersonalDetails.p_DifferentlyAbledType + "','" + newPersonalDetails.p_DiffAbledSubType + "','" + newPersonalDetails.p_DisablityPriority + "','" + newPersonalDetails.p_IpAddress + "','" + newPersonalDetails.p_UserId + "','" + newPersonalDetails.p_GreenCard + "','" + newPersonalDetails.p_Area + "','" + newPersonalDetails.p_Block + "','" + newPersonalDetails.p_Panchayat + "','" + newPersonalDetails.p_Village + "','" + newPersonalDetails.p_AreaOfInterest + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};
PersonalDetails.getPersonalDetails = function createUser(fetch_PersonalDetails, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL fetch_personalDetailmaster(" + fetch_PersonalDetails.p_CandidateId + "," + fetch_PersonalDetails.p_IpAddress + "," + fetch_PersonalDetails.p_UserId + ")", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};



PersonalDetails.createTransfer_location = function createUser(Transfer_location, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
        if (err) throw err;
    });
    
        sql.query("CALL Transfer_location('" + Transfer_location.p_CandidateId + "','" + Transfer_location.p_DistrictId + "','" + Transfer_location.p_CityId + "','" + Transfer_location.p_VillageId + "','" + Transfer_location.p_EntryBy + "','" + Transfer_location.p_Ip_Address + "')", function (err, res) {
            if (err) {
            sql.end()
                result(err, null);
            } else {
            sql.end()
                result(null, res);
            }
        });
    };

module.exports = PersonalDetails;