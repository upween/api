
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES23Entry= function(ES23Entryform){
    this.p_Ex_Id  = ES23Entryform.p_Ex_Id ;
    this.p_FromDt =ES23Entryform.p_FromDt ;
    this.p_ToDt =ES23Entryform.p_ToDt ;
    this.p_Registration_M = ES23Entryform.p_Registration_M;
    this.p_PlacesJs_M = ES23Entryform.p_PlacesJs_M;
    this.p_Registration_Removed_M = ES23Entryform.p_Registration_Removed_M;
    this.p_LR_M = ES23Entryform.p_LR_M;
    this.p_Submissions_M = ES23Entryform.p_Submissions_M;
    this.p_LR_Prev_M = ES23Entryform.p_LR_Prev_M;

    this.p_Registration_C = ES23Entryform.p_Registration_C;
    this.p_PlacesJs_C = ES23Entryform.p_PlacesJs_C;
    this.p_Registration_Removed_C = ES23Entryform.p_Registration_Removed_C;
    this.p_LR_C = ES23Entryform.p_LR_C;
    this.p_Submissions_C = ES23Entryform.p_Submissions_C;
    this.p_LR_Prev_C = ES23Entryform.p_LR_Prev_C;

    this.p_Registration_S = ES23Entryform.p_Registration_S;
    this.p_PlacesJs_S = ES23Entryform.p_PlacesJs_S;
    this.p_Registration_Removed_S = ES23Entryform.p_Registration_Removed_S;
    this.p_LR_S = ES23Entryform.p_LR_S;
    this.p_Submissions_S = ES23Entryform.p_Submissions_S;
    this.p_LR_Prev_S = ES23Entryform.p_LR_Prev_S;

    this.p_Registration_B = ES23Entryform.p_Registration_B;
    this.p_PlacesJs_B = ES23Entryform.p_PlacesJs_B;
    this.p_Registration_Removed_B = ES23Entryform.p_Registration_Removed_B;
    this.p_LR_B = ES23Entryform.p_LR_B;
    this.p_Submissions_B = ES23Entryform.p_Submissions_B;
    this.p_LR_Prev_B = ES23Entryform.p_LR_Prev_B;
   
    this.p_Registration_Z = ES23Entryform.p_Registration_Z;
    this.p_PlacesJs_Z = ES23Entryform.p_PlacesJs_Z;
    this.p_Registration_Removed_Z = ES23Entryform.p_Registration_Removed_Z;
    this.p_LR_Z = ES23Entryform.p_LR_Z;
    this.p_Submissions_Z = ES23Entryform.p_Submissions_Z;
    this.p_LR_Prev_Z = ES23Entryform.p_LR_Prev_Z;

    this.p_Registration_Total = ES23Entryform.p_Registration_Total;
    this.p_PlacesJs_Total = ES23Entryform.p_PlacesJs_Total;
    this.p_Registration_Removed_Total = ES23Entryform.p_Registration_Removed_Total;
    this.p_LR_Total = ES23Entryform.p_LR_Total;
    this.p_Submissions_Total = ES23Entryform.p_Submissions_Total;
    this.p_LR_Prev_Total = ES23Entryform.p_LR_Prev_Total;
    this.p_Flag=ES23Entryform.p_Flag;
};




ES23Entry.createES23Entry= function createUser(ES23EntryPara, result) {  
 
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES23_Entry('"+ES23EntryPara.p_Ex_Id+"','"+ES23EntryPara.p_FromDt+"','"+ES23EntryPara.p_ToDt+"','"+ES23EntryPara.p_Registration_M+"','"+ES23EntryPara.p_PlacesJs_M+"','"+ES23EntryPara.p_Registration_Removed_M+"','"+ES23EntryPara.p_LR_M+"','"+ES23EntryPara.p_Submissions_M+"','"+ES23EntryPara.p_LR_Prev_M+"','"+ES23EntryPara.p_Registration_C+"','"+ES23EntryPara.p_PlacesJs_C+"','"+ES23EntryPara.p_Registration_Removed_C+"','"+ES23EntryPara.p_LR_C+"','"+ES23EntryPara.p_Submissions_C+"','"+ES23EntryPara.p_LR_Prev_C+"','"+ES23EntryPara.p_Registration_S+"','"+ES23EntryPara.p_PlacesJs_S+"','"+ES23EntryPara.p_Registration_Removed_S+"','"+ES23EntryPara.p_LR_S+"','"+ES23EntryPara.p_Submissions_S+"','"+ES23EntryPara.p_LR_Prev_S+"','"+ES23EntryPara.p_Registration_B+"','"+ES23EntryPara.p_PlacesJs_B+"','"+ES23EntryPara.p_Registration_Removed_B+"','"+ES23EntryPara.p_LR_B+"','"+ES23EntryPara.p_Submissions_B+"','"+ES23EntryPara.p_LR_Prev_B+"','"+ES23EntryPara.p_Registration_Z+"','"+ES23EntryPara.p_PlacesJs_Z+"','"+ES23EntryPara.p_Registration_Removed_Z+"','"+ES23EntryPara.p_LR_Z+"','"+ES23EntryPara.p_Submissions_Z+"','"+ES23EntryPara.p_LR_Prev_Z+"','"+ES23EntryPara.p_Registration_Total+"','"+ES23EntryPara.p_PlacesJs_Total+"','"+ES23EntryPara.p_Registration_Removed_Total+"','"+ES23EntryPara.p_LR_Total+"','"+ES23EntryPara.p_Submissions_Total+"','"+ES23EntryPara.p_LR_Prev_Total+"','"+ES23EntryPara.p_Flag+"')", function (err, res) {      
             if(err) {
                sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });           
};



module.exports= ES23Entry;