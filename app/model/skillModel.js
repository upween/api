'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Skill = function(skill){
    this.p_skillid = skill.p_skillid;
    this.p_skillname = skill.p_skillname;
    this.p_isactive = skill.p_isactive;
    this.p_IpAddress = skill.p_IpAddress;
    this.p_UserId = skill.p_UserId;
};
Skill.createSkill = function createUser(newSkill, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insupd_skillmaster('"+newSkill.p_skillid+"','"+newSkill.p_skillname+"','"+newSkill.p_isactive+"','"+newSkill.p_IpAddress+"','"+newSkill.p_UserId+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};
Skill.getSkill = function createUser(fetch_skill, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_skillmaster("+fetch_skill.p_skillid+","+fetch_skill.p_IpAddress+","+fetch_skill.p_IsActive+","+fetch_skill.p_UserId+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end() 
            result(null, res);
        }
    });   
};

module.exports= Skill;