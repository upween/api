'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var itijfexchange = function(itijfexchange){
    this.p_Jobfair_Id = itijfexchange.p_Jobfair_Id;
    this.p_Ex_id = itijfexchange.p_Ex_id;
    this.p_Jobfair_Title = itijfexchange.p_Jobfair_Title;
    this.p_Venue = itijfexchange.p_Venue;
    this.p_Jobfair_Details = itijfexchange.p_Jobfair_Details;
    this.p_Attachment_Filename = itijfexchange.p_Attachment_Filename;
    this.p_Date_of_Entry = itijfexchange.p_Date_of_Entry
    this.p_Jobfair_FromDt = itijfexchange.p_Jobfair_FromDt
    this.p_Jobfair_ToDt = itijfexchange.p_Jobfair_ToDt
    this.p_Jobfair_CreatedBy = itijfexchange.p_Jobfair_CreatedBy
    this.p_Status = itijfexchange.p_Status
    this.p_Verified_By = itijfexchange.p_Verified_By
    this.p_Verified_Dt = itijfexchange.p_Verified_Dt
    this.p_Reject_Reason = itijfexchange.p_Reject_Reason
    this.p_Active_YN = itijfexchange.p_Active_YN
    this.p_LMDT = itijfexchange.p_LMDT
    this.p_LMBY = itijfexchange.p_LMBY
    this.p_id = itijfexchange.p_id
    this.p_jf_id = itijfexchange.p_jf_id
    this.p_emp_id = itijfexchange.p_emp_id
    this.p_CandidateId=itijfexchange.p_CandidateId 
    this.p_Trade=itijfexchange.p_Trade
    this.p_Gender=itijfexchange.p_Gender
    this.p_percentage_req=itijfexchange.p_percentage_req
    this.p_Zone=itijfexchange.p_Zone
    this.p_District=itijfexchange.p_District
    this.p_passingoutyearfrom=itijfexchange.p_passingoutyearfrom
    this.passingoutyearto=itijfexchange.passingoutyearto
};
itijfexchange.createitijfexchange = function createUser(InsUpd_iti_jf_exchange, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_iti_jf_exchange('"+InsUpd_iti_jf_exchange.p_Jobfair_Id+"','"+InsUpd_iti_jf_exchange.p_Ex_id+"','"+InsUpd_iti_jf_exchange.p_Jobfair_Title+"','"+InsUpd_iti_jf_exchange.p_Venue+"','"+InsUpd_iti_jf_exchange.p_Jobfair_Details+"','"+InsUpd_iti_jf_exchange.p_Attachment_Filename+"','"+InsUpd_iti_jf_exchange.p_Date_of_Entry+"','"+InsUpd_iti_jf_exchange.p_Jobfair_FromDt+"','"+InsUpd_iti_jf_exchange.p_Jobfair_ToDt+"','"+InsUpd_iti_jf_exchange.p_Jobfair_CreatedBy+"','"+InsUpd_iti_jf_exchange.p_Status+"','"+InsUpd_iti_jf_exchange.p_Verified_By+"','"+InsUpd_iti_jf_exchange.p_Verified_Dt+"','"+InsUpd_iti_jf_exchange.p_Reject_Reason+"','"+InsUpd_iti_jf_exchange.p_Active_YN+"','"+InsUpd_iti_jf_exchange.p_LMDT+"','"+InsUpd_iti_jf_exchange.p_LMBY+"','"+InsUpd_iti_jf_exchange.p_Trade+"','"+InsUpd_iti_jf_exchange.p_passingoutyearfrom+"','"+InsUpd_iti_jf_exchange.passingoutyearto+"','"+InsUpd_iti_jf_exchange.p_Gender+"','"+InsUpd_iti_jf_exchange.p_percentage_req+"','"+InsUpd_iti_jf_exchange.p_Zone+"','"+InsUpd_iti_jf_exchange.p_District+"')", function (err, res) {      
        if(err) {
            //sql.end()
            sql.end();  
            result(err, null);
        }
        else{
            //sql.end()
            sql.end();  
            result(null, res);
        }
    });           
};

itijfexchange.createitijfempmapping = function createUser(InsUpd_iti_jf_emp_mapping, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_iti_jf_emp_mapping('"+InsUpd_iti_jf_emp_mapping.p_id+"','"+InsUpd_iti_jf_emp_mapping.p_jf_id+"','"+InsUpd_iti_jf_emp_mapping.p_emp_id+"')", function (err, res) {      
        if(err) {
            //sql.end()
            sql.end();  
            result(err, null);
        }
        else{
            //sql.end()
            sql.end();  
            result(null, res);
        }
    });           
};
itijfexchange.getitijfexchange= function createUser(Fetch_iti_jf_exchange, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_iti_jf_exchange("+Fetch_iti_jf_exchange.p_Ex_id+")", function (err, res) {             
        if(err) {
            //sql.end()   
            sql.end();  
            result(err, null);
        }
        else{
            //sql.end()
            sql.end();  
            result(null, res);
        }
    });   
};
itijfexchange.gethallticket= function createUser(hallticket, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_Hall_Ticket('"+hallticket.p_CandidateId+"','"+hallticket.p_jf_id+"')", function (err, res) {             
        if(err) {
            //sql.end()   
            sql.end();  
            result(err, null);
        }
        else{
            //sql.end()
            sql.end();  
            result(null, res);
        }
    });   
};
itijfexchange.getitijfexchange= function createUser(Fetch_iti_jf_exchange, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_iti_jf_exchange("+Fetch_iti_jf_exchange.p_Ex_id+")", function (err, res) {             
        if(err) {
            //sql.end()   
            sql.end();  
            result(err, null);
        }
        else{
            //sql.end()
            sql.end();  
            result(null, res);
        }
    });   
};
itijfexchange.getitinstitute= function createUser(itinstitute, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_iti_institute()", function (err, res) {             
        if(err) {
            //sql.end()   
            sql.end();  
            result(err, null);
        }
        else{
            //sql.end()
            sql.end();  
            result(null, res);
        }
    });   
};

module.exports= itijfexchange;



