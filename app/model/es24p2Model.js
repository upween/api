
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES24P2Entry= function(es24p2entryform){
    this.p_FromDate   = es24p2entryform.p_FromDate  ;
    this.p_ToDate    = es24p2entryform.p_ToDate   ;

    this.p_Ex_Id  = es24p2entryform.p_Ex_Id ;
    this.p_FromDt =es24p2entryform.p_FromDt ;
    this.p_ToDt =es24p2entryform.p_ToDt ;
    this.p_ES24_II_Id =es24p2entryform.p_ES24_II_Id;

    this.p_Outstanding_Prev_SC =es24p2entryform.p_Outstanding_Prev_SC ;    
    this.p_Outstanding_Prev_ST  =es24p2entryform.p_Outstanding_Prev_ST  ;     
    this.p_Outstanding_Prev_OBC =es24p2entryform.p_Outstanding_Prev_OBC ;

    this.p_Notified_SC =es24p2entryform.p_Notified_SC ;    
    this.p_Notified_ST  =es24p2entryform.p_Notified_ST ;      
    this.p_Notified_OBC =es24p2entryform.p_Notified_OBC ;

    this.p_Filled_SC =es24p2entryform.p_Filled_SC;     
    this.p_Filled_ST  =es24p2entryform.p_Filled_ST ;      
    this.p_Filled_OBC =es24p2entryform.p_Filled_OBC; 

    this.p_Cancelled_NA_SC =es24p2entryform.p_Cancelled_NA_SC;     
    this.p_Cancelled_NA_ST  =es24p2entryform.p_Cancelled_NA_ST ;      
    this.p_Cancelled_NA_OBC =es24p2entryform.p_Cancelled_NA_OBC; 

    this.p_Cancelled_Other_SC =es24p2entryform.p_Cancelled_Other_SC;     
    this.p_Cancelled_Other_ST  =es24p2entryform.p_Cancelled_Other_ST ;      
    this.p_Cancelled_Other_OBC =es24p2entryform.p_Cancelled_Other_OBC; 

    this.p_Outstanding_SC =es24p2entryform.p_Outstanding_SC;     
    this.p_Outstanding_ST  =es24p2entryform.p_Outstanding_ST ;      
    this.p_Outstanding_OBC =es24p2entryform.p_Outstanding_OBC; 

    this.p_Flag=es24p2entryform.p_Flag;
};


ES24P2Entry.getES24P2Rpt = function createUser(Es24P2, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_ES24ReportPart2('"+Es24P2.p_FromDate+"','"+Es24P2.p_ToDate+"',"+Es24P2.p_Ex_Id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

ES24P2Entry.createES24P2Entry= function createUser(Es24P2entryPara, result) {  
 
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES24_Part_II_ReserveVacancy('"+Es24P2entryPara.p_Ex_Id+"','"+Es24P2entryPara.p_FromDt+"','"+Es24P2entryPara.p_ToDt+"','"+Es24P2entryPara.p_ES24_II_Id+"','"+Es24P2entryPara.p_Outstanding_Prev_SC+"','"+Es24P2entryPara.p_Outstanding_Prev_ST+"','"+Es24P2entryPara.p_Outstanding_Prev_OBC+"','"+Es24P2entryPara.p_Notified_SC+"','"+Es24P2entryPara.p_Notified_ST+"','"+Es24P2entryPara.p_Notified_OBC+"','"+Es24P2entryPara.p_Filled_SC+"','"+Es24P2entryPara.p_Filled_ST+"','"+Es24P2entryPara.p_Filled_OBC+"','"+Es24P2entryPara.p_Cancelled_NA_SC+"','"+Es24P2entryPara.p_Cancelled_NA_ST+"','"+Es24P2entryPara.p_Cancelled_NA_OBC+"','"+Es24P2entryPara.p_Cancelled_Other_SC+"','"+Es24P2entryPara.p_Cancelled_Other_ST+"','"+Es24P2entryPara.p_Cancelled_Other_OBC+"','"+Es24P2entryPara.p_Outstanding_SC+"','"+Es24P2entryPara.p_Outstanding_ST+"','"+Es24P2entryPara.p_Outstanding_OBC+"','"+Es24P2entryPara.p_Flag+"')", function (err, res) {      
             if(err) {
                sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });           
};



module.exports= ES24P2Entry;