'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Typing = function(Typing){
    this.p_TypingStenoId = Typing.p_TypingStenoId;
    this.p_CandidateId = Typing.p_CandidateId;
    this.p_Language = Typing.p_Language;
    this.p_Type = Typing.p_Type;
    this.p_Institute = Typing.p_Institute;
    this.p_PassedYear = Typing.p_PassedYear;
    this.p_Speed = Typing.p_Speed;
    this.p_IpAddress = Typing.p_IpAddress;

};
Typing.createTyping = function createUser(newTyping, result) {  
 
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insUpd_TypingSteno_master('"+newTyping.p_TypingStenoId+"','"+newTyping.p_CandidateId+"','"+newTyping.p_Language+"','"+newTyping.p_Type+"','"+newTyping.p_Institute+"','"+newTyping.p_PassedYear+"','"+newTyping.p_Speed+"','"+newTyping.p_IpAddress+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};
Typing.getTyping = function createUser(fetch_TypingSteno_master, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_TypingSteno_master("+fetch_TypingSteno_master.p_TypingStenoId+","+fetch_TypingSteno_master.p_CandidateId+","+fetch_TypingSteno_master.p_IpAddress+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= Typing;