'use strict';
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var LicenseType = function LicenseType(_LicenseType) {
    this.L_Type_id = _LicenseType.L_Type_id;
    this.p_Active_YN = _LicenseType.p_Active_YN;
};

LicenseType.getLicenseType = function createUser(fetch_License_type, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL fetch_License_type(" + fetch_License_type.L_Type_id + "," + fetch_License_type.p_Active_YN + ")", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

module.exports = LicenseType;