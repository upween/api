'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');

var PhysicalDetails = function PhysicalDetails(physicaldetails) {
    this.p_PhysicalDetailId = physicaldetails.p_PhysicalDetailId;
    this.p_CandidateId = physicaldetails.p_CandidateId;
    this.p_Chest = physicaldetails.p_Chest;
    this.p_Height = physicaldetails.p_Height;
    this.p_Weight = physicaldetails.p_Weight;
    this.p_EyeSight = physicaldetails.p_EyeSight;
    this.p_IsActive = physicaldetails.p_IsActive;
    this.p_IpAddress = physicaldetails.p_IpAddress;
    this.p_UserId = physicaldetails.p_UserId;
    this.p_SportsName = physicaldetails.p_SportsName;
    this.p_SportsCertificate = physicaldetails.p_SportsCertificate;
    this.p_NCCCertificate = physicaldetails.p_NCCCertificate;
};
PhysicalDetails.createPhysicalDetails = function createUser(newPhysicalDetails, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL InsUpd_PhysicalOtherdtail('" + newPhysicalDetails.p_PhysicalDetailId + "','" + newPhysicalDetails.p_CandidateId + "','" + newPhysicalDetails.p_Chest + "','" + newPhysicalDetails.p_Height + "','" + newPhysicalDetails.p_Weight + "','" + newPhysicalDetails.p_EyeSight + "','" + newPhysicalDetails.p_IsActive + "','" + newPhysicalDetails.p_IpAddress + "','" + newPhysicalDetails.p_UserId + "','" + newPhysicalDetails.p_SportsName + "','" + newPhysicalDetails.p_SportsCertificate + "','" + newPhysicalDetails.p_NCCCertificate + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};
PhysicalDetails.getPhysicalDetails = function createUser(fetch_physicalOtherDetail, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL fetch_physicalOtherDetail(" + fetch_physicalOtherDetail.p_CandidateId + "," + fetch_physicalOtherDetail.p_IpAddress + "," + fetch_physicalOtherDetail.p_UserId + ")", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

module.exports = PhysicalDetails;