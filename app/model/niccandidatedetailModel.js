
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var NICCandidateDetails = function(niccandidatedetails){
    this.p_DistrictId = niccandidatedetails.p_DistrictId;
 
    
};

NICCandidateDetails.getdistrictwisecandidateCount= function createUser(candidatecount, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_DistrictWiseCandidateCount("+candidatecount.p_DistrictId+")", function (err, res) {                
          if(err) {
            
              sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};
NICCandidateDetails.getdistrictwisecandidateDetail= function createUser(candidatedetail, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_DistrictWiseCandidateDetail("+candidatedetail.p_DistrictId+")", function (err, res) {                
          if(err) {
            
              sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};
module.exports= NICCandidateDetails;