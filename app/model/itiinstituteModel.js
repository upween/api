'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Itiinstitute = function(Itiinstitute){
    this.p_Id = Itiinstitute.p_Id;
    this.p_ZoneName = Itiinstitute.p_ZoneName;
    this.p_District = Itiinstitute.p_District;
    this.p_Institute_Name = Itiinstitute.p_Institute_Name;
    this.p_Block = Itiinstitute.p_Block;  
    this.p_Tehsil = Itiinstitute.p_Tehsil;
    this.p_Vidhan_Sabha = Itiinstitute.p_Vidhan_Sabha;
    this.p_jf_id = Itiinstitute.p_jf_id;

   
};


Itiinstitute.createItiinstitute = function createUser(insUpd_Itiinstitute, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Ins_upd_iti_institute('"+insUpd_Itiinstitute.p_Id+"','"+insUpd_Itiinstitute.p_ZoneName+"','"+insUpd_Itiinstitute.p_District+"','"+insUpd_Itiinstitute.p_Institute_Name+"','"+insUpd_Itiinstitute.p_Block+"','"+insUpd_Itiinstitute.p_Tehsil+"','"+insUpd_Itiinstitute.p_Vidhan_Sabha+"',)", function (err, res) {      
        if(err) {
            //end.sql()
           sql.end();
            result(err, null);
        }
        else{
           // end.sql()
            sql.end();
            result(null, res);
        }
    });           
};


        
Itiinstitute.getDistrictwiseitiinstitute = function createUser(fetch_Districtwise_itiinstitute,result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL `fetch_Districtwise_itiinstitute`("+fetch_Districtwise_itiinstitute.p_District+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
           sql.end(); 
            result(null, res);
        }
    });   
};

Itiinstitute.getJobFairFiltersById = function createUser(Fetch_JobFairFiltersById, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_JobFairFiltersById('"+Fetch_JobFairFiltersById.p_jf_id+"')", function (err, res) {             
        if(err) {
            sql.end();  
            result(err, null);
        }
        else{
            sql.end();  
            result(null, res);
        }
    });   
};

Itiinstitute.getZonewiseDistrict = function createUser(fetch_Zonewise_District, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_Zonewise_District("+fetch_Zonewise_District.p_ZoneName+")", function (err, res) {             
        if(err) {
            sql.end();  
            result(err, null);
        }
        else{
            sql.end();  
            result(null, res);
        }
    });   
};


  

module.exports= Itiinstitute;