'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES12Rpt = function(ES12RptForm){
    this.p_Year   = ES12RptForm.p_Year  
    this.p_Ex_Id  = ES12RptForm.p_Ex_Id 
     
    this.p_Ex_id=ES12RptForm.p_Ex_id;          
    this.p_FromDt=ES12RptForm.p_FromDt;          
    this.p_ToDt=ES12RptForm.p_ToDt;           
    this.p_NCO=ES12RptForm.p_NCO;           
    this.p_Women_LR=ES12RptForm.p_Women_LR;           
    this.p_SC_LR=ES12RptForm.p_SC_LR;          
    this.p_ST_LR=ES12RptForm.p_ST_LR;          
    this.p_OBC_LR=ES12RptForm.p_OBC_LR;          
    this.p_Disabled_LR=ES12RptForm.p_Disabled_LR;          
    this.p_Total_LR=ES12RptForm.p_Total_LR;
           
    this.p_Women_Notify=ES12RptForm.p_Women_Notify;           
    this.p_SC_Notify=ES12RptForm.p_SC_Notify;          
    this.p_ST_Notify=ES12RptForm.p_ST_Notify;          
    this.p_OBC_Notify=ES12RptForm.p_OBC_Notify;          
    this.p_Disabled_Notify=ES12RptForm.p_Disabled_Notify;          
    this.p_Total_Notify=ES12RptForm.p_Total_Notify;

    this.p_Women_Filled=ES12RptForm.p_Women_Filled;           
    this.p_SC_Filled=ES12RptForm.p_SC_Filled;          
    this.p_ST_Filled=ES12RptForm.p_ST_Filled;          
    this.p_OBC_Filled=ES12RptForm.p_OBC_Filled;          
    this.p_Disabled_Filled=ES12RptForm.p_Disabled_Filled;          
    this.p_Total_Filled=ES12RptForm.p_Total_Filled;
   
    this.p_Women_Cancelled=ES12RptForm.p_Women_Cancelled;           
    this.p_SC_Cancelled=ES12RptForm.p_SC_Cancelled;          
    this.p_ST_Cancelled=ES12RptForm.p_ST_Cancelled;          
    this.p_OBC_Cancelled=ES12RptForm.p_OBC_Cancelled;          
    this.p_Disabled_Cancelled=ES12RptForm.p_Disabled_Cancelled;          
    this.p_Total_Cancelled=ES12RptForm.p_Total_Cancelled;

    this.p_Women_Outstanding=ES12RptForm.p_Women_Outstanding;           
    this.p_SC_Outstanding=ES12RptForm.p_SC_Outstanding;          
    this.p_ST_Outstanding=ES12RptForm.p_ST_Outstanding;          
    this.p_OBC_Outstanding=ES12RptForm.p_OBC_Outstanding;          
    this.p_Disabled_Outstanding=ES12RptForm.p_Disabled_Outstanding;          
    this.p_Total_Outstanding=ES12RptForm.p_Total_Outstanding;

    this.p_Verify_YN=ES12RptForm.p_Verify_YN;         
    this.p_Flag=ES12RptForm.p_Flag;        
    this.p_ES12_Id=ES12RptForm.p_ES12_Id;

    this.p_NCO_Code=ES12RptForm.p_NCO_Code;
};




ES12Rpt.getES12Annual = function createUser(Fetch_ES12Annual , result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_ES12Annual ("+Fetch_ES12Annual .p_Year +","+Fetch_ES12Annual .p_Ex_Id  +")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};
ES12Rpt.getNCOCodeavail= function createUser(NCOCodeavail , result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Adm_Pro_NCO_CheckAvailable("+NCOCodeavail.p_NCO_Code +")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};
ES12Rpt.createES12Annual= function createUser(ES12AnnualEntry, result) {  
 
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES12_Entry('"+ES12AnnualEntry.p_Ex_id+"','"+ES12AnnualEntry.p_FromDt+"','"+ES12AnnualEntry.p_ToDt+"',"+ES12AnnualEntry.p_NCO +",'"+ES12AnnualEntry.p_Women_LR+"','"+ES12AnnualEntry.p_SC_LR+"','"+ES12AnnualEntry.p_ST_LR+"','"+ES12AnnualEntry.p_OBC_LR +"','"+ES12AnnualEntry.p_Disabled_LR +"','"+ES12AnnualEntry.p_Total_LR +"','"+ES12AnnualEntry.p_Women_Notify+"','"+ES12AnnualEntry.p_SC_Notify+"','"+ES12AnnualEntry.p_ST_Notify+"','"+ES12AnnualEntry.p_OBC_Notify +"','"+ES12AnnualEntry.p_Disabled_Notify +"','"+ES12AnnualEntry.p_Total_Notify +"','"+ES12AnnualEntry.p_Women_Filled+"','"+ES12AnnualEntry.p_SC_Filled+"','"+ES12AnnualEntry.p_ST_Filled+"','"+ES12AnnualEntry.p_OBC_Filled +"','"+ES12AnnualEntry.p_Disabled_Filled +"','"+ES12AnnualEntry.p_Total_Filled +"','"+ES12AnnualEntry.p_Women_Cancelled+"','"+ES12AnnualEntry.p_SC_Cancelled+"','"+ES12AnnualEntry.p_ST_Cancelled+"','"+ES12AnnualEntry.p_OBC_Cancelled +"','"+ES12AnnualEntry.p_Disabled_Cancelled +"','"+ES12AnnualEntry.p_Total_Cancelled +"','"+ES12AnnualEntry.p_Women_Outstanding+"','"+ES12AnnualEntry.p_SC_Outstanding+"','"+ES12AnnualEntry.p_ST_Outstanding+"','"+ES12AnnualEntry.p_OBC_Outstanding +"','"+ES12AnnualEntry.p_Disabled_Outstanding +"','"+ES12AnnualEntry.p_Total_Outstanding +"','"+ES12AnnualEntry.p_Verify_YN+"','"+ES12AnnualEntry.p_Flag+"','"+ES12AnnualEntry.p_ES12_Id+"')", function (err, res) {      
             if(err) {
                sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });           
};
module.exports= ES12Rpt ;
