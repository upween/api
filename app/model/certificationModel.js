'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Certification = function (certification) {
    this.p_CertificationId = certification.p_CertificationId;
    this.p_TrainingProgramName = certification.p_TrainingProgramName;
    this.p_TrainingCenter = certification.p_TrainingCenter;
    this.p_Duration = certification.p_Duration;
    this.p_Scheme = certification.p_Scheme;
    this.p_SkillSet = certification.p_SkillSet;
    this.p_JobPrefrences = certification.p_JobPrefrences;
    this.p_CertificationYear = certification.p_CertificationYear;
    this.p_IsActive = certification.p_IsActive;
    this.p_CandidateId = certification.p_CandidateId;
};
Certification.createCertification = function createUser(newCertification, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_CertificationMaster('" + newCertification.p_CertificationId + "','" + newCertification.p_TrainingProgramName + "','" + newCertification.p_TrainingCenter + "','" + newCertification.p_Duration + "','" + newCertification.p_Scheme + "','" + newCertification.p_SkillSet + "','"+ newCertification.p_JobPrefrences +"','" + newCertification.p_CertificationYear + "','" + newCertification.p_IsActive + "','" + newCertification.p_CandidateId + "')", function (err, res) {
        if (err) {
            //end.sql()
sql.end();
            result(err, null);
        }
        else {
            //end.sql()
sql.end();
            result(null, res);
        }
    });
};
Certification.getCertification = function createUser(fetch_certification, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Certificationmaster(" + fetch_certification.p_CertificationId + ","+fetch_certification.p_CandidateId+"," + fetch_certification.p_IpAddress + "," + fetch_certification.p_IsActive + "," + fetch_certification.p_UserId + ")", function (err, res) {
        if (err) {
            sql.end();
            result(err, null);
        }
        else {
            sql.end();
            result(null, res);
        }
    });
};


module.exports = Certification;