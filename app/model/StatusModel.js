'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Status = function(Status){
    this.p_Status_id = Status.p_Status_id;
    this.p_Active_YN = Status.p_Active_YN;
    
};

Status.getStatus = function createUser(fetch_Status_master, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Status_master("+fetch_Status_master.p_Status_id+","+fetch_Status_master.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= Status;