'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var TypingInstitute = function(TypingInstitute){
    this.p_TS_Institute_id = TypingInstitute.p_TS_Institute_id;
    this.p_Active_YN = TypingInstitute.p_Active_YN;
    
};

TypingInstitute.getTypingInstitute = function createUser(fetch_Typing_institute, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Typing_institute("+fetch_Typing_institute.p_TS_Institute_id+","+fetch_Typing_institute.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= TypingInstitute;