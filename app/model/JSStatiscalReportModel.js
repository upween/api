'use strict';

'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var JSStatiscalReport = function JSStatiscalReport(_JSStatiscalReport) {
    this.p_FromDate = _JSStatiscalReport.p_FromDate;
    this.p_ToDate = _JSStatiscalReport.p_ToDate;
    this.p_Ex_id = _JSStatiscalReport.p_Ex_id;
};

JSStatiscalReport.getJSStatiscalReport = function createUser(JSStatiscalReport, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL Fetch_JS_StatiscalReport(" + JSStatiscalReport.p_FromDate + "," + JSStatiscalReport.p_ToDate + "," + JSStatiscalReport.p_Ex_id + ")", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};
module.exports = JSStatiscalReport;