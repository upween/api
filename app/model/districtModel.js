'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var District = function(district){
    this.p_DistrictId = district.p_DistrictId;
    this.p_DistrictName = district.p_DistrictName;
    this.p_StateId = district.p_StateId;
    this.p_IsActive = district.p_IsActive;
    this.p_IpAddress = district.p_IpAddress;
    this.p_UserId = district.p_UserId;
};
District.createDistrict = function createUser(newDistrict, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insupd_districtmaster('"+newDistrict.p_DistrictId+"','"+newDistrict.p_DistrictName+"','"+newDistrict.p_StateId+"','"+newDistrict.p_IsActive+"','"+newDistrict.p_IpAddress+"','"+newDistrict.p_UserId+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};
District.getDistrict= function createUser(fetch_District, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_districtbystate("+fetch_District.p_StateId+","+fetch_District.p_IpAddress+","+fetch_District.p_IsActive+","+fetch_District.p_UserId+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

module.exports= District;