'use strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var EmpSubUser = function (empsubuser) {
        this.p_SubUserId = empsubuser.p_SubUserId;
        this.p_Emp_Id = empsubuser.p_Emp_Id;
        this.p_Name = empsubuser.p_Name;
        this.p_EmailId = empsubuser.p_EmailId;
        this.p_Mobile = empsubuser.p_Mobile;
        this.p_RoleId = empsubuser.p_RoleId;
        this.p_Reporting = empsubuser.p_Reporting;
        this.p_IsActive = empsubuser.p_IsActive;
        this.p_EntryBy = empsubuser.p_EntryBy;
        this.p_IpAddress = empsubuser.p_IpAddress;
        this.p_Password = empsubuser.p_Password;
};



EmpSubUser.createSubUser = function createUser(newSubUser, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL Insupd_EmployerSubUser('"+newSubUser.p_SubUserId+"','"+newSubUser.p_Emp_Id+"','"+newSubUser.p_Name+"','"+newSubUser.p_EmailId+"','"+newSubUser.p_Mobile+"','"+newSubUser.p_RoleId+"','"+newSubUser.p_Reporting+"','"+newSubUser.p_IsActive+"','"+newSubUser.p_EntryBy+"','"+newSubUser.p_IpAddress+"','"+newSubUser.p_Password+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };

            EmpSubUser.getSubUser = function createUser(SubUser, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL fetch_employersubuser('"+SubUser.p_Emp_Id+"','"+SubUser.p_RoleId+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };

            EmpSubUser.getDeleteSubUser = function createUser(DeleteSubUser, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL Delete_employer_subuserdetails('"+DeleteSubUser.p_SubUserId+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };


module.exports = EmpSubUser;