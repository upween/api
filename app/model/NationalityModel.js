'use strict';
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Nationality = function Nationality(_Nationality) {
    this.p_Nationality_id = _Nationality.p_Nationality_id;
    this.p_Active_YN = _Nationality.p_Active_YN;
    this.p_Nationality_name = _Nationality.p_Nationality_name;
};

Nationality.getNationality = function createUser(fetch_Nationality_master, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL fetch_Nationality_master(" + fetch_Nationality_master.p_Nationality_id + "," + fetch_Nationality_master.p_Active_YN + ")", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};
Nationality.createNationality = function createUser(newNationality, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL InsUpd_nationality_master('" + newNationality.p_Nationality_id + "','" + newNationality.p_Nationality_name + "','" + newNationality.p_Active_YN + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

module.exports = Nationality;