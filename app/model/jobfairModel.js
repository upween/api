'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');

var Jobfair = function Jobfair(jobfair) {
    this.p_JobFairId = jobfair.p_JobFairId;
    this.p_Head = jobfair.p_Head;
    this.p_Details = jobfair.p_Details;
    this.p_Links = jobfair.p_Links;
    this.p_FromDate = jobfair.p_FromDate;
    this.p_ToDate = jobfair.p_ToDate;
    this.p_IpAddress = jobfair.p_IpAddress;
    this.p_EducationId = jobfair.p_EducationId;
    this.p_DesignationId = jobfair.p_DesignationId;
    this.p_DistrictId = jobfair.p_DistrictId;
    this.p_Gender = jobfair.p_Gender;
    this.p_MinAge = jobfair.p_MinAge;
    this.p_MaxAge = jobfair.p_MaxAge;
    this.p_MinSalary = jobfair.p_MinSalary;
    this.p_MaxSalary = jobfair.p_MaxSalary;
    this.p_Limit=jobfair.p_Limit;

this.p_JobfairParticipantId=jobfair.p_JobfairParticipantId;
this.p_Jobfair_Id=jobfair.p_Jobfair_Id;
this.p_CandidateId=jobfair.p_CandidateId;
this.p_Selection_Status=jobfair.p_Selection_Status;
this.p_EntryBy=jobfair.p_EntryBy;
this.p_IpAddress=jobfair.p_IpAddress;
this.p_From=jobfair.p_From;
this.p_To=jobfair.p_To;
this.p_PrimarySelection=jobfair.p_PrimarySelection;
this.p_EmpId=jobfair.p_EmpId;
};
Jobfair.createJobfair = function createUser(newJobfair, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL InsUpd_JobFairmaster('" + newJobfair.p_JobFairId + "','" + newJobfair.p_Head + "','" + newJobfair.p_Details + "','" + newJobfair.p_Links + "','" + newJobfair.p_FromDate + "','" + newJobfair.p_ToDate + "','" + newJobfair.p_IpAddress + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};
Jobfair.createJobfairParticipant = function createUser(Jobfairparameter, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
        if (err) throw err;
    });
    
        sql.query("CALL Ins_jobfair_participant('" + Jobfairparameter.p_JobfairParticipantId + "','" + Jobfairparameter.p_Jobfair_Id + "','" + Jobfairparameter.p_CandidateId+ "','" + Jobfairparameter.p_Selection_Status+ "','" + Jobfairparameter.p_EntryBy + "','" + Jobfairparameter.p_IpAddress + "','"+Jobfairparameter.p_EmpId+"')", function (err, res) {
            if (err) {
            sql.end()
                result(err, null);
            } else {
            sql.end()
                result(null, res);
            }
        });
    };
Jobfair.getJobfair = function createUser(fetch_Jobfair, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL fetch_JobFairmaster(" + fetch_Jobfair.p_FromDate + "," + fetch_Jobfair.p_ToDate + "," + fetch_Jobfair.p_IpAddress + ")", function (err, res) {
        if (err) {
sql.end();
            result(err, null);
        } else {
sql.end();
            result(null, res);
        }
    });
};

Jobfair.getLatestJobfair = function getLatestJobfair(result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});
    
sql.query("CALL fetch_JOBFAIRS()", function (err, res) {

        if (err) {
			sql.end();
            //console.log("error: ", err);
            result(null, err);
        } else {
            //console.log('Office : ', res);  
sql.end();
            result(null, res);
        }
    });
};

Jobfair.createCandidateJobFair = function createUser(fetch_CandidateJobFair, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL fetch_CandidateJobFair('"+fetch_CandidateJobFair.p_EducationId +"','"+fetch_CandidateJobFair.p_DesignationId +"','"+ fetch_CandidateJobFair.p_DistrictId+"','"+fetch_CandidateJobFair.p_Gender+"','"+fetch_CandidateJobFair.p_MinAge+"','"+fetch_CandidateJobFair.p_MaxAge+"','"+fetch_CandidateJobFair.p_MinSalary+"','"+fetch_CandidateJobFair.p_MaxSalary+"','"+fetch_CandidateJobFair.p_Limit+"','"+fetch_CandidateJobFair.p_Jobfair_Id+"','"+fetch_CandidateJobFair.p_From+"','"+fetch_CandidateJobFair.p_To+"')", function (err, res) {
        if (err) {
sql.end();
            result(err, null);
        } else {
sql.end();
            result(null, res);
        }
    });
};
Jobfair.getJobFairMonthlyRpt = function createUser(fetch_JobFairRpt, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
        if (err) throw err;
    });
    
        sql.query("CALL JobFairMonthlyReporting(" + fetch_JobFairRpt.p_Month  + "," + fetch_JobFairRpt.p_Year  + "," + fetch_JobFairRpt.p_Ex_Id  + ")", function (err, res) {
            if (err) {
    sql.end();
                result(err, null);
            } else {
    sql.end();
                result(null, res);
            }
        });
    };
    Jobfair.getJobfairParticipant = function createUser(Jobfair, result) {
        var sql = mysql.createConnection(credentials);
        sql.connect(function (err) {
            if (err) throw err;
        });
        
            sql.query("CALL fetch_Jobfair_Participant("+Jobfair.p_Jobfair_Id+","+Jobfair.p_Selection_Status+")", function (err, res) {
                if (err) {
        sql.end();
                    result(err, null);
                } else {
        sql.end();
                    result(null, res);
                }
            });
        };
        Jobfair.updateParticipantJobFair = function createUser(JobFair, result) {
            var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
                if (err) throw err;
            });
            
                sql.query("CALL upd_selectionstatus('"+JobFair.p_Jobfair_Id+"','"+JobFair.p_CandidateId+"','"+JobFair.p_Selection_Status+"','"+JobFair.p_EmpId+"')", function (err, res) {
                    if (err) {
            sql.end();
                        result(err, null);
                    } else {
            sql.end();
                        result(null, res);
                    }
                });
            };

            Jobfair.getjobfairreport= function createUser(jobfair_Report, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL jobfair_Report("+jobfair_Report.p_Ex_id+","+jobfair_Report.p_Jobfair_FromDt+","+jobfair_Report.p_Jobfair_ToDt+")", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };
            
module.exports = Jobfair;