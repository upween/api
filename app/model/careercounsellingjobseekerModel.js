'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var JobSeeker = function(JobSeeker){
    this.p_RegistrationNumber = JobSeeker.p_RegistrationNumber;
    this.p_MonthId = JobSeeker.p_MonthId;
    this.p_year = JobSeeker.p_year;
    this.p_Ex_Id = JobSeeker.p_Ex_Id;
    this.p_Frmyear = JobSeeker.p_Frmyear;
    this.p_Toyear = JobSeeker.p_Toyear;
    this.p_Ex_Id = JobSeeker.p_Ex_Id;
    this.p_Month=JobSeeker.p_Month;
    this.p_Year =JobSeeker.p_Year ;

   
    this.p_Flag =JobSeeker.p_Flag ;
    this.p_Xmonth =JobSeeker.p_Xmonth ;
    this.p_Xyear =JobSeeker.p_Xyear ;
    this.p_Tran_Id =JobSeeker.p_Tran_Id ;
    this.p_Total_Counseling =JobSeeker.p_Total_Counseling ;
    this.p_Total_Candidates =JobSeeker.p_Total_Candidates ;
    this.p_Female_Candidates =JobSeeker.p_Female_Candidates ;
    this.p_SC_Candidates =JobSeeker.p_SC_Candidates ;
    this.p_ST_Candidates =JobSeeker.p_ST_Candidates ;
    this.p_OBC_Candidates =JobSeeker.p_OBC_Candidates ;
    this.p_PH_Candidates =JobSeeker.p_PH_Candidates ;
    this.p_Minority_Candidates =JobSeeker.p_Minority_Candidates ;
    this.p_Remark =JobSeeker.p_Remark ;
    this.p_UR_Candidates =JobSeeker.p_UR_Candidates ;
    this.p_Client_IP =JobSeeker.p_Client_IP ;
    this.p_LM_BY =JobSeeker.p_LM_BY ;
    this.p_Status =JobSeeker.p_Status ;

    
};

JobSeeker.getJobSeeker = function createUser(fetch_JobSeeker, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL CareerCounsellingJobseeker("+fetch_JobSeeker.p_RegistrationNumber+")", function (err, res) {             
        if(err) {
            //end.sql()
            sql.end();
            result(err, null);
        }
        else{
            //end.sql()
            sql.end();
            result(null, res);
        }
    });   
};


JobSeeker.getCounsellingByMonth = function createUser(CI_Pro_Counselling_ByMonth, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL CI_Pro_Counselling_ByMonth("+CI_Pro_Counselling_ByMonth.p_MonthId+","+CI_Pro_Counselling_ByMonth.p_year+","+CI_Pro_Counselling_ByMonth.p_Ex_Id+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};


JobSeeker.getCounsellingByMonthWise = function createUser(CI_Pro_Counselling_ByMonthWise, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL CI_Pro_Counselling_ByMonthWise("+CI_Pro_Counselling_ByMonthWise.p_Frmyear+","+CI_Pro_Counselling_ByMonthWise.p_Toyear+","+CI_Pro_Counselling_ByMonthWise.p_Ex_Id+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};
JobSeeker.getCounsellingMonthlyReporting = function createUser(CI_Pro_Counselling_ByMonth, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL CareerCounselling_Monthly_Reporting("+CI_Pro_Counselling_ByMonth.p_Month+","+CI_Pro_Counselling_ByMonth.p_Year +","+CI_Pro_Counselling_ByMonth.p_Ex_Id+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};



JobSeeker.createCareerCounsel_Pro_Reporting = function createUser(CareerCounsel_Pro_Reporting, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL CI_CareerCounsel_Pro_Reporting('"+CareerCounsel_Pro_Reporting.p_Flag+"','"+CareerCounsel_Pro_Reporting.p_Xmonth +"','"+CareerCounsel_Pro_Reporting.p_Xyear+"','"+CareerCounsel_Pro_Reporting.p_Ex_Id+"','"+CareerCounsel_Pro_Reporting.p_Tran_Id+"','"+CareerCounsel_Pro_Reporting.p_Total_Counseling+"','"+CareerCounsel_Pro_Reporting.p_Total_Candidates+"','"+CareerCounsel_Pro_Reporting.p_Female_Candidates+"','"+CareerCounsel_Pro_Reporting.p_SC_Candidates+"','"+CareerCounsel_Pro_Reporting.p_ST_Candidates+"','"+CareerCounsel_Pro_Reporting.p_OBC_Candidates+"','"+CareerCounsel_Pro_Reporting.p_PH_Candidates+"','"+CareerCounsel_Pro_Reporting.p_Minority_Candidates+"','"+CareerCounsel_Pro_Reporting.p_Remark+"','"+CareerCounsel_Pro_Reporting.p_UR_Candidates+"','"+CareerCounsel_Pro_Reporting.p_Client_IP+"','"+CareerCounsel_Pro_Reporting.p_LM_BY+"','"+CareerCounsel_Pro_Reporting.p_Status+"')", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};



module.exports= JobSeeker;

