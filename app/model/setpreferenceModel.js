'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');


var SetPreference = function(setpreference){
    this.p_CandidateId = setpreference.p_CandidateId;
    this.p_Qualification = setpreference.p_Qualification;
    this.p_Location = setpreference.p_Location;
    this.p_Industry = setpreference.p_Industry;
    this.p_Title = setpreference.p_Title;
    this.p_Type = setpreference.p_Type;
};
SetPreference.createSetPreference= function createUser(newSetPreference, result) {  
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_jobseeker_preferences("+newSetPreference.p_CandidateId+",'"+newSetPreference.p_Qualification+"','"+newSetPreference.p_Location+"','"+newSetPreference.p_Industry+"','"+newSetPreference.p_Title+"','"+newSetPreference.p_Type+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null,res);
        }
    });           
};
SetPreference.getSetPreference= function createUser(SetPreference, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });

    sql.query("CALL Fetch_jobseerker_preferences("+SetPreference.p_CandidateId+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });   
};
module.exports= SetPreference;