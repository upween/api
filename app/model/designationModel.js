'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Designation = function(designation){
    this.p_designationid = designation.p_designationid;
    this.p_designationname = designation.p_designationname;
    this.p_DepartmentId = designation.p_DepartmentId;
    this.p_isactive = designation.p_isactive;
    this.p_IpAddress = designation.p_IpAddress;
    this.p_UserId = designation.p_UserId;
};
Designation.createDesignation = function createUser(newDesignation, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insupd_designationmaster('"+newDesignation.p_designationid+"','"+newDesignation.p_designationname+"','"+newDesignation.p_DepartmentId+"','"+newDesignation.p_isactive+"','"+newDesignation.p_IpAddress+"','"+newDesignation.p_UserId+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};
Designation.getDesignation= function createUser(fetch_Designation, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_designationmaster("+fetch_Designation.p_DepartmentId+","+fetch_Designation.p_IpAddress+","+fetch_Designation.p_isactive+","+fetch_Designation.p_UserId+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};
Designation.getLatestDesignation = function getLatestDesignation(result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Latest9Designation()", function (err, res) {

            if(err) {
                sql.end();
                result(null, err);
            }
            else{
              //console.log('Office : ', res);  
              sql.end();
             result(null, res);
            }
        });   
};


Designation.getDesignationJob= function createUser(fetch_8Designation, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_8Designation("+fetch_8Designation.p_DesignationId+","+fetch_8Designation.p_IsActive+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

module.exports= Designation;



