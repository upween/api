'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var SavedAppliedjobs = function(SavedAppliedjobs){
    this.p_Id = SavedAppliedjobs.p_Id;
    this.p_JobId = SavedAppliedjobs.p_JobId;
    this.p_JSRegNo = SavedAppliedjobs.p_JSRegNo;
    this.p_Save = SavedAppliedjobs.p_Save;
    this.p_Apply = SavedAppliedjobs.p_Apply;
};
SavedAppliedjobs.createSavedAppliedjobs = function createUser(newSavedAppliedjobs, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insupd_Saved_appliedjobs('"+newSavedAppliedjobs.p_Id+"','"+newSavedAppliedjobs.p_JobId+"','"+newSavedAppliedjobs.p_JSRegNo+"','"+newSavedAppliedjobs.p_Save+"','"+newSavedAppliedjobs.p_Apply+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};

SavedAppliedjobs.getSavedAppliedjobs = function createUser(fetchSavedAppliedjobs, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Saved_appliedjobs('"+fetchSavedAppliedjobs.p_JobId+"','"+fetchSavedAppliedjobs.p_JSRegNO+"','"+fetchSavedAppliedjobs.p_Save+"','"+fetchSavedAppliedjobs.p_Apply+"')", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= SavedAppliedjobs;