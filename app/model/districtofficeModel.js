'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var DistrictOffice = function(districtoffice){
    this.p_DistrictId = districtoffice.p_DistrictId;
    this.p_DivisionCallCentreId = districtoffice.p_DivisionCallCentreId;
    this.p_DivisionCallCentre = districtoffice.p_DivisionCallCentre;
    this.p_Administration = districtoffice.p_Administration;
    this.p_DistrictsCovered = districtoffice.p_DistrictsCovered;
    this.p_CentreManager = districtoffice.p_CentreManager;
    this.p_Contact = districtoffice.p_Contact;
    this.p_EMailId = districtoffice.p_EMailId;
    this.p_Address = districtoffice.p_Address;

    this.p_Month = districtoffice.p_Month;
    this.p_Year = districtoffice.p_Year;
    this.p_Emp_Id = districtoffice.p_Emp_Id;
    this.p_Flag = districtoffice.p_Flag;

};

DistrictOffice.getDistrictOffice = function getDistrictOffice(result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Districtexchangemaster()", function (err, res) {

            if(err) {
                sql.end();
                result(null, err);
            }
            else{
              //console.log('Office : ', res);  
              sql.end();
             result(null, res);
            }
        });   
};
DistrictOffice.getDitrictOfficeDetail = function createUser(districtoffice_detail, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL DistrictOfficeDetail("+districtoffice_detail.p_DistrictId+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

DistrictOffice.createUpdDistrictOffice = function createUser(UpdDistrictOffice, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Upddistrict_employment_exchangemaster('"+UpdDistrictOffice.p_DivisionCallCentreId+"','"+UpdDistrictOffice.p_DivisionCallCentre+"','"+UpdDistrictOffice.p_Administration+"','"+UpdDistrictOffice.p_DistrictsCovered+"','"+UpdDistrictOffice.p_CentreManager+"','"+UpdDistrictOffice.p_Contact+"','"+UpdDistrictOffice.p_EMailId+"','"+UpdDistrictOffice.p_Address+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

DistrictOffice.getCallCentreManager = function createUser(CallCentreManager, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_CallCentreManager()", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

DistrictOffice.getCCallCentreManager_PlacementDetails = function createUser(PlacementDetails, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_CallCentreManager_PlacementDetails('"+PlacementDetails.p_Month+"','"+PlacementDetails.p_Year+"','"+PlacementDetails.p_Emp_Id+"','"+PlacementDetails.p_Flag+"')", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};


module.exports= DistrictOffice;

