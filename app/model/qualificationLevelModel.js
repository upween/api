'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Qualification = function(Qualification){
    this.p_Qualif_Level_id = Qualification.p_Qualif_Level_id;
    this.p_Qualif_Level_name = Qualification.p_Qualif_Level_name;
    this.p_Qualif_Level_name_H = Qualification.p_Qualif_Level_name_H;
    this.p_Qualif_desc = Qualification.p_Qualif_desc;
    this.p_QualifLevel_order = Qualification.p_QualifLevel_order;
    this.p_Active_YN = Qualification.p_Active_YN;
    
};

Qualification.getQualification= function createUser(fetch_qualification_level, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});

    sql.query("CALL fetch_qualification_level("+fetch_qualification_level.p_Qualif_Level_id+","+fetch_qualification_level.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

Qualification.createQualification = function createUser(InsUpd_adm_js_mst_qualification_level, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
        if (err) throw err;
    });
    sql.query("CALL InsUpd_adm_js_mst_qualification_level('"+InsUpd_adm_js_mst_qualification_level.p_Qualif_Level_id+"','"+InsUpd_adm_js_mst_qualification_level.p_Qualif_Level_name+"','"+InsUpd_adm_js_mst_qualification_level.p_Qualif_Level_name_H+"','"+InsUpd_adm_js_mst_qualification_level.p_Qualif_desc+"','"+InsUpd_adm_js_mst_qualification_level.p_QualifLevel_order+"','"+InsUpd_adm_js_mst_qualification_level.p_Active_YN+"')", function (err, res) {      
        if(err) {
            //end.sql()
           sql.end();
            result(err, null);
        }
        else{
            //end.sql()
            sql.end();
            result(null, res);
        }
    });           
}

Qualification.getJS_Rpt_AgeWise_QualifLevel= function createUser(JS_Rpt_AgeWise_QualifLevel, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});

    sql.query("CALL CI_Pro_JS_Rpt_AgeWise_QualifLevel('"+JS_Rpt_AgeWise_QualifLevel.p_Ex_Id +"','"+JS_Rpt_AgeWise_QualifLevel.p_AsOnDate+"')", function (err, res) {             
        if(err) {   
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= Qualification;