'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Role = function(role){
    this.p_RoleId = role.p_RoleId;
    this.p_RoleName = role.p_RoleName;
    this.p_IsActive = role.p_IsActive;
    this.p_IpAddress = role.p_IpAddress;
    this.p_UserId = role.p_UserId;
};
Role.createRole = function createUser(newRole, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insupd_rolemaster('"+newRole.p_RoleId+"','"+newRole.p_RoleName+"','"+newRole.p_IsActive+"','"+newRole.p_IpAddress+"','"+newRole.p_UserId+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};
Role.getRole = function createUser(fetch_Role, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_rolemaster('"+fetch_Role.p_RoleId+"','"+fetch_Role.p_IpAddress+"','"+fetch_Role.p_IsActive+"','"+fetch_Role.p_UserId+"')", function (err, res) {             
        if(err) {
            sql.end()  
            result(err, null);
        }
        else{
            sql.end() 
            result(null, res);
        }
    });   
};


module.exports= Role;