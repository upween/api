
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var DataBaseSearch= function(DataBaseSearch){
    this.p_Gender = DataBaseSearch.p_Gender;
    this.p_Category_id = DataBaseSearch.p_Category_id;
    this.p_NoOFCandidates = DataBaseSearch.p_NoOFCandidates;
    this.p_Division_id = DataBaseSearch.p_Division_id;
    this.p_District_id = DataBaseSearch.p_District_id;
    this.p_Ex_Id = DataBaseSearch.p_Ex_Id;

    this.p_Qualif_YN = DataBaseSearch.p_Qualif_YN;
    this.p_Qualif_Level_id = DataBaseSearch.p_Qualif_Level_id;
    this.p_Qualif_id = DataBaseSearch.p_Qualif_id;
    this.p_Sub_Group_id = DataBaseSearch.p_Sub_Group_id;
    this.p_Q_Division_id = DataBaseSearch.p_Q_Division_id;
    this.p_Q_PassingYear = DataBaseSearch.p_Q_PassingYear;

    this.p_Criteria_Age_YN = DataBaseSearch.p_Criteria_Age_YN;
    this.p_minAge = DataBaseSearch.p_minAge;
    this.p_maxAge = DataBaseSearch.p_maxAge;
    this.p_Asondate = DataBaseSearch.p_Asondate;

    this.p_Criteria_TS_YN = DataBaseSearch.p_Criteria_TS_YN;
    this.p_Lang_id = DataBaseSearch.p_Lang_id;
    this.p_Type_id = DataBaseSearch.p_Type_id;
    this.p_Speed = DataBaseSearch.p_Speed;
    this.p_TS_Pass_Year = DataBaseSearch.p_TS_Pass_Year;

    this.p_Criteria_Reg_YN = DataBaseSearch.p_Criteria_Reg_YN;
    this.p_Fromdate = DataBaseSearch.p_Fromdate;
    this.p_Tomdate = DataBaseSearch.p_Tomdate;

    this.p_NCO_YN = DataBaseSearch.p_NCO_YN;
    this.p_NCO_m = DataBaseSearch.p_NCO_m;
    this.p_NCO_1 = DataBaseSearch.p_NCO_1;
    this.p_NCO_2 = DataBaseSearch.p_NCO_2;

    this.p_ph_YN = DataBaseSearch.p_ph_YN;
    this.p_HC_Category_id = DataBaseSearch.p_HC_Category_id;
    this.p_ph_all = DataBaseSearch.p_ph_all;

    this.p_City_id= DataBaseSearch.p_City_id;
    this.p_Location_id = DataBaseSearch.p_Location_id;
    this.p_FunctionalArea_id = DataBaseSearch.p_FunctionalArea_id;
    this.p_IpAddress = DataBaseSearch.p_IpAddress;
    this.p_OlexUserId = DataBaseSearch.p_OlexUserId;
    this.p_Entrydate = DataBaseSearch.p_Entrydate;
    this.p_CandidateId = DataBaseSearch.p_CandidateId;
    this.p_LogId = DataBaseSearch.p_LogId;
    this.p_Limit = DataBaseSearch.p_Limit;

};
DataBaseSearch.createDataBaseSearch= function createUser(DataBaseSearch, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
   
    sql.query("CALL DataBaseSearchEngine('"+DataBaseSearch.p_Gender+"','"+DataBaseSearch.p_Category_id+"','"+DataBaseSearch.p_NoOFCandidates+"','"+DataBaseSearch.p_Division_id+"','"+DataBaseSearch.p_District_id+"','"+DataBaseSearch.p_Ex_Id+"','"+DataBaseSearch.p_Qualif_YN+"','"+DataBaseSearch.p_Qualif_Level_id+"','"+DataBaseSearch.p_Qualif_id+"','"+DataBaseSearch.p_Sub_Group_id+"','"+DataBaseSearch.p_Q_Division_id+"','"+DataBaseSearch.p_Q_PassingYear+"','"+DataBaseSearch.p_Criteria_Age_YN+"','"+DataBaseSearch.p_minAge+"','"+DataBaseSearch.p_maxAge+"','"+DataBaseSearch.p_Asondate+"','"+DataBaseSearch.p_Criteria_TS_YN+"','"+DataBaseSearch.p_Lang_id+"','"+DataBaseSearch.p_Type_id+"','"+DataBaseSearch.p_Speed+"','"+DataBaseSearch.p_TS_Pass_Year+"','"+DataBaseSearch.p_Criteria_Reg_YN+"','"+DataBaseSearch.p_Fromdate+"','"+DataBaseSearch.p_Tomdate+"','"+DataBaseSearch.p_NCO_YN+"','"+DataBaseSearch.p_NCO_m+"','"+DataBaseSearch.p_NCO_1+"','"+DataBaseSearch.p_NCO_2+"','"+DataBaseSearch.p_ph_YN+"','"+DataBaseSearch.p_HC_Category_id+"','"+DataBaseSearch.p_ph_all+"','"+DataBaseSearch.p_City_id+"','"+DataBaseSearch.p_Location_id+"','"+DataBaseSearch.p_FunctionalArea_id+"','"+DataBaseSearch.p_OlexUserId+"','"+DataBaseSearch.p_Limit+"')", function (err, res) {      
             if(err) {
                sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

DataBaseSearch.createsearchengineutility_logs= function createUser(searchengineutilitylog, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
   
    sql.query("CALL InsUpd_searchengineutility_logs('"+searchengineutilitylog.p_LogId+"','"+searchengineutilitylog.p_CandidateId+"','"+searchengineutilitylog.p_Entrydate+"','"+searchengineutilitylog.p_OlexUserId+"','"+searchengineutilitylog.p_IpAddress+"')", function (err, res) {      
             if(err) {
                sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

module.exports= DataBaseSearch;