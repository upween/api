'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var CompanyEmails = function(CompanyEmails){   
    this.p_CompanyId = CompanyEmails.p_CompanyId;
    this.p_CandidateId = CompanyEmails.p_CandidateId;
    this.p_Name = CompanyEmails.p_Name;
    this.p_EmailId = CompanyEmails.p_EmailId;
    this.p_PhoneNo = CompanyEmails.p_PhoneNo;
    this.p_Subject = CompanyEmails.p_Subject;
    this.p_Message = CompanyEmails.p_Message;
    this.p_IpAddress = CompanyEmails.p_IpAddress;
    this.p_UserId = CompanyEmails.p_UserId;
};
CompanyEmails.createCompanyEmails = function createUser(insUpd_CompanyMails, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insUpd_CompanyMails('"+insUpd_CompanyMails.p_CompanyId+"','"+insUpd_CompanyMails.p_CandidateId+"','"+insUpd_CompanyMails.p_Name+"','"+insUpd_CompanyMails.p_EmailId+"','"+insUpd_CompanyMails.p_PhoneNo+"','"+insUpd_CompanyMails.p_Subject+"','"+insUpd_CompanyMails.p_Message+"','"+insUpd_CompanyMails.p_IpAddress+"','"+insUpd_CompanyMails.p_UserId+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};
CompanyEmails.getCompanyEmails = function createUser(fetch_CompanyMails, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_CompanyMails("+fetch_CompanyMails.p_CompanyId+","+fetch_CompanyMails.p_CandidateId+","+fetch_CompanyMails.p_IpAddress+","+fetch_CompanyMails.p_UserId+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

module.exports= CompanyEmails;