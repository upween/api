'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Registration = function(registration){
    this.p_CandidateId = registration.p_CandidateId;
    this.p_FirstName = registration.p_FirstName;
    this.p_MiddleName = registration.p_MiddleName;
    this.p_LastName = registration.p_LastName;
    this.p_FatherName = registration.p_FatherName;
    this.p_Gender = registration.p_Gender;
    this.p_VillageId = registration.p_VillageId;
    this.p_DistrictId = registration.p_DistrictId;
    this.p_CityId = registration.p_CityId;
    this.p_UniqueIdentification = registration.p_UniqueIdentification;
    this.p_IdentificationNumber = registration.p_IdentificationNumber;
    this.p_MobileNumber = registration.p_MobileNumber;
    this.p_EmailId = registration.p_EmailId;
    this.p_UserName= registration.p_UserName;
    this.p_Password = registration.p_Password;
    this.p_IpAddress = registration.p_IpAddress;
    this.p_jobalert_email  = registration.p_jobalert_email ;
    this.p_jobalert_sms  = registration.p_jobalert_sms ;
    this.p_Ex_id  = registration.p_Ex_id ;
    this.p_RegistrationId=registration.p_RegistrationId;
    this.p_Aadhar   = registration.p_Aadhar;
    this.p_DOB   = registration.p_DOB;
    this.p_Flag =registration.p_Flag;
    this.p_StateId   = registration.p_StateId;
    this.p_NonMP_District =registration.p_NonMP_District;
};
Registration.createRegistration = function createUser(newRegistration, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insupd_jobseeker_registration('"+newRegistration.p_CandidateId+"','"+newRegistration.p_FirstName+"','"+newRegistration.p_MiddleName+"','"+newRegistration.p_LastName+"','"+newRegistration.p_FatherName+"','"+newRegistration.p_Gender+"','"+newRegistration.p_VillageId+"','"+newRegistration.p_DistrictId+"','"+newRegistration.p_CityId+"','"+newRegistration.p_UniqueIdentification+"','"+newRegistration.p_IdentificationNumber+"','"+newRegistration.p_MobileNumber+"','"+newRegistration.p_EmailId+"','"+newRegistration.p_UserName+"','"+newRegistration.p_Password+"','"+newRegistration.p_IpAddress+"','"+newRegistration.p_jobalert_email+"','"+newRegistration.p_jobalert_sms+"','"+newRegistration.p_StateId+"','"+newRegistration.p_NonMP_District+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};
Registration.getRegistration= function createUser(fetch_Registration, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_jobseeker_registration("+fetch_Registration.p_CandidateId+","+fetch_Registration.p_IpAddress+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};
Registration.getKnowRegistration= function createUser(fetch_KnowRegistration, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
  
    sql.query("CALL KnowYourRegistration("+fetch_KnowRegistration.p_FirstName+","+fetch_KnowRegistration.p_LastName+","+fetch_KnowRegistration.p_Gender+","+fetch_KnowRegistration.p_Aadhar+","+fetch_KnowRegistration.p_DOB+","+fetch_KnowRegistration.p_Flag+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};
Registration.getRegistrationCard= function createUser(fetch_RegistrationCard, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_Jobseeker_RegistrationCard('"+fetch_RegistrationCard.p_CandidateId+"','"+fetch_RegistrationCard.p_IpAddress+"')", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end() 
            result(null, res);
        }
    });   
};
Registration.createRegistrationrenew = function createUser(renewRegistration, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_jobseeker_registration_renew('"+renewRegistration.p_CandidateId+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};
Registration.createrenewforolduser = function createUser(renewRegistrationforolduser, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_jobseeker_registration_renew_new('"+renewRegistrationforolduser.p_RegistrationId+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};




module.exports= Registration;