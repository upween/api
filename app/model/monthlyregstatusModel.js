'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var Monthlyregstatus = function Monthlyregstatus(_Monthlyregstatus) {
    this.P_monthid = _Monthlyregstatus.P_monthid;
    this.p_year = _Monthlyregstatus.p_year;
    this.p_Division_id = _Monthlyregstatus.p_Division_id;
    this.p_Ex_Id = _Monthlyregstatus.p_Ex_Id;
    this.p_MonthId = _Monthlyregstatus.p_MonthId;
};

Monthlyregstatus.getMonthlyregstatus = function createUser(Monthlyregstatus, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL monthly_reg_status(" + Monthlyregstatus.P_monthid + "," + Monthlyregstatus.p_year + ")", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

Monthlyregstatus.getMonthlyRegistrationDateWise = function createUser(MonthlyRegDateWise, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL MonthlyRegistrationDateWise('"+ MonthlyRegDateWise.p_Division_id +"','"+ MonthlyRegDateWise.p_Ex_Id +"','"+ MonthlyRegDateWise.p_MonthId +"','"+ MonthlyRegDateWise.p_year +"')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};


Monthlyregstatus.getFetchMonthlyregstatus = function createUser(Monthlyregstatus, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
        if (err) throw err;
    });
    
        sql.query("CALL fetch_monthly_reg_status(" + Monthlyregstatus.p_Ex_Id + "," + Monthlyregstatus.P_monthid + "," + Monthlyregstatus.p_year + ")", function (err, res) {
            if (err) {
            sql.end()
                result(err, null);
            } else {
            sql.end()
                result(null, res);
            }
        });
    };
    

module.exports = Monthlyregstatus;