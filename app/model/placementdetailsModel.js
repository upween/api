'use strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var placementdetails = function placementDetails(_placementdetails) {
    this.p_placement_deail_id = _placementdetails.p_placement_deail_id;
    this.p_Position = _placementdetails.p_Position;
    this.p_candidate_name = _placementdetails.p_candidate_name;
    this.p_reg_no = _placementdetails.p_reg_no;
    this.p_company_name = _placementdetails.p_company_name;
    this.p_joining_date = _placementdetails.p_joining_date;
    this.p_ctc = _placementdetails.p_ctc;
    this.p_job_location = _placementdetails.p_job_location;
    this.p_candidate_address = _placementdetails.p_candidate_address;
    this.p_category_id = _placementdetails.p_category_id;
    this.p_candidate_district = _placementdetails.p_candidate_district;
    this.p_mobile_number = _placementdetails.p_mobile_number;
    this.p_address_proof = _placementdetails.p_address_proof;
    this.p_unique_id_no = _placementdetails.p_unique_id_no;
    this.p_appointment_letter_collected = _placementdetails.p_appointment_letter_collected;
    this.p_attach_letter = _placementdetails.p_attach_letter;
    this.p_salaryslip_first = _placementdetails.p_salaryslip_first;
    this.p_ss_first_attach = _placementdetails.p_ss_first_attach;
    this.p_salaryslip_sec = _placementdetails.p_salaryslip_sec;
    this.p_ss_sec_attach = _placementdetails.p_ss_sec_attach;
    this.p_salaryslip_third = _placementdetails.p_salaryslip_third;
    this.p_ss_third_attach = _placementdetails.p_ss_third_attach;
    this.p_Recruiter = _placementdetails.p_Recruiter;
    this.p_candidate_undertaking = _placementdetails.p_candidate_undertaking;
    this.p_Email = _placementdetails.p_Email;
    this.p_con_number = _placementdetails.p_con_number;
    this.p_con_per_name = _placementdetails.p_con_per_name;
    this.p_Gender = _placementdetails.p_Gender;
    this.p_Month = _placementdetails.p_Month;
    this.p_Year = _placementdetails.p_Year;
    this.p_can_udertaking = _placementdetails.p_can_udertaking;
    this.p_Company_id = _placementdetails.p_Company_id;
    this.p_Designation_Id = _placementdetails.p_Designation_Id;
    this.p_Entry_By = _placementdetails.p_Entry_By;
    this.p_Entered_Ip = _placementdetails.p_Entered_Ip;
    this.p_documentType  = _placementdetails.p_documentType ;
    this.p_DistrictId  = _placementdetails.p_DistrictId ;

};
placementdetails.createplacementdetails = function createUser(InsUpd_placement_details, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL InsUpd_placement_details('" + InsUpd_placement_details.p_placement_deail_id + "','" + InsUpd_placement_details.p_Position + "','" + InsUpd_placement_details.p_candidate_name + "','" + InsUpd_placement_details.p_reg_no + "','" + InsUpd_placement_details.p_company_name + "','" + InsUpd_placement_details.p_joining_date + "','" + InsUpd_placement_details.p_ctc + "','" + InsUpd_placement_details.p_job_location + "','" + InsUpd_placement_details.p_candidate_address + "','" + InsUpd_placement_details.p_category_id + "','" + InsUpd_placement_details.p_candidate_district + "','" + InsUpd_placement_details.p_mobile_number + "','" + InsUpd_placement_details.p_address_proof + "','" + InsUpd_placement_details.p_unique_id_no + "','" + InsUpd_placement_details.p_appointment_letter_collected + "','" + InsUpd_placement_details.p_attach_letter + "','" + InsUpd_placement_details.p_salaryslip_first + "','" + InsUpd_placement_details.p_ss_first_attach + "','" + InsUpd_placement_details.p_salaryslip_sec + "','" + InsUpd_placement_details.p_ss_sec_attach + "','" + InsUpd_placement_details.p_salaryslip_third + "','" + InsUpd_placement_details.p_ss_third_attach + "','" + InsUpd_placement_details.p_Recruiter + "','" + InsUpd_placement_details.p_candidate_undertaking + "','" + InsUpd_placement_details.p_Email + "','" + InsUpd_placement_details.p_con_number + "','" + InsUpd_placement_details.p_con_per_name + "','" + InsUpd_placement_details.p_Gender + "','" + InsUpd_placement_details.p_can_udertaking + "','"+InsUpd_placement_details.p_Entry_By+"','"+InsUpd_placement_details.p_Entered_Ip+"')", function (err, res) {
        if (err) {
            //end.sql()
	    sql.end()
            result(err, null);
        } else {
            //end.sql()
	    sql.end()
            result(null, res);
        }
    });
};

placementdetails.getplacementdetails = function createUser(placementdetails, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL fetch_placement_details(" + placementdetails.p_placement_deail_id + "," + placementdetails.p_category_id + "," + placementdetails.p_Candidate_district + "," + placementdetails.p_Month + "," + placementdetails.p_Year + "," + placementdetails.p_Gender + "," + placementdetails.p_Company_id + "," + placementdetails.p_Designation_Id + ","+placementdetails.p_Entry_By+")", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};
placementdetails.createverifyplacementdetails = function createUser(verifyplacementdetails, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
        if (err) throw err;
    });
    
        sql.query("CALL verify_placementdocuments('" + verifyplacementdetails.p_placement_deail_id + "','" + verifyplacementdetails.p_documentType + "')", function (err, res) {
            if (err) {
                //end.sql()
            sql.end()
                result(err, null);
            } else {
                //end.sql()
            sql.end()
                result(null, res);
            }
        });
    };
    placementdetails.createplacementdetailsexcel = function createUser(InsUpd_placement_details, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL InsUpd_placement_details_excel('" + InsUpd_placement_details.p_placement_deail_id + "','" + InsUpd_placement_details.p_Position + "','" + InsUpd_placement_details.p_candidate_name + "','" + InsUpd_placement_details.p_reg_no + "','" + InsUpd_placement_details.p_company_name + "','" + InsUpd_placement_details.p_joining_date + "','" + InsUpd_placement_details.p_ctc + "','" + InsUpd_placement_details.p_job_location + "','" + InsUpd_placement_details.p_candidate_address + "','" + InsUpd_placement_details.p_category_id + "','" + InsUpd_placement_details.p_candidate_district + "','" + InsUpd_placement_details.p_mobile_number + "','" + InsUpd_placement_details.p_address_proof + "','" + InsUpd_placement_details.p_unique_id_no + "','" + InsUpd_placement_details.p_appointment_letter_collected + "','" + InsUpd_placement_details.p_attach_letter + "','" + InsUpd_placement_details.p_salaryslip_first + "','" + InsUpd_placement_details.p_ss_first_attach + "','" + InsUpd_placement_details.p_salaryslip_sec + "','" + InsUpd_placement_details.p_ss_sec_attach + "','" + InsUpd_placement_details.p_salaryslip_third + "','" + InsUpd_placement_details.p_ss_third_attach + "','" + InsUpd_placement_details.p_Recruiter + "','" + InsUpd_placement_details.p_candidate_undertaking + "','" + InsUpd_placement_details.p_Email + "','" + InsUpd_placement_details.p_con_number + "','" + InsUpd_placement_details.p_con_per_name + "','" + InsUpd_placement_details.p_Gender + "','" + InsUpd_placement_details.p_can_udertaking + "','"+InsUpd_placement_details.p_Entry_By+"','"+InsUpd_placement_details.p_Entered_Ip+"')", function (err, res) {
        if (err) {
            //end.sql()
	    sql.end()
            result(err, null);
        } else {
            //end.sql()
	    sql.end()
            result(null, res);
        }
    });
};


placementdetails.getplacementdetailsCount = function createUser(placementdetails, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
        if (err) throw err;
    });
    
        sql.query("CALL Fetch_DistrictWisePlacementReport(" + placementdetails.p_DistrictId + ")", function (err, res) {
            if (err) {
            sql.end()
                result(err, null);
            } else {
            sql.end()
                result(null, res);
            }
        });
    };

module.exports = placementdetails;