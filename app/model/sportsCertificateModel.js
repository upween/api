'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var SportsCertificate = function(SportsCertificate){
    this.p_S_Certificate_id = SportsCertificate.p_S_Certificate_id;
    this.p_Active_YN = SportsCertificate.p_Active_YN;
    
};

SportsCertificate.getSportsCertificate= function createUser(fetch_Sportscertificate_master, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Sportscertificate_master("+fetch_Sportscertificate_master.p_S_Certificate_id+","+fetch_Sportscertificate_master.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= SportsCertificate;