'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var SportsName = function(SportsName){
    this.p_S_id = SportsName.p_S_id;
    this.p_Active_YN = SportsName.p_Active_YN;
    
};

SportsName.getSportsName= function createUser(fetch_Sports_master, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Sports_master("+fetch_Sports_master.p_S_id+","+fetch_Sports_master.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });   
};

module.exports= SportsName;