'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var sector = function(sector){
    this.p_Sector_Id = sector.p_Sector_Id
    this.p_Active_YN = sector.p_Active_YN;
    
};

sector.getsector = function createUser(fetch_sector_master, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_sector_master("+fetch_sector_master.p_Sector_Id+","+fetch_sector_master.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= sector;