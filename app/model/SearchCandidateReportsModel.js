'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var searchcandidatereports = function(searchcandidatereports){
    this.p_SearchId  = searchcandidatereports.p_SearchId 
    this.p_EmpId  = searchcandidatereports.p_EmpId 
    this.p_Education = searchcandidatereports.p_Education 
    this.p_Designation  = searchcandidatereports.p_Designation
    this.p_District=searchcandidatereports.p_District
    this.p_Gender=searchcandidatereports.p_Gender    
    this.p_MinAge=searchcandidatereports.p_MinAge 
    this.p_MaxAge=searchcandidatereports.p_MaxAge 
    this.p_MinSalary=searchcandidatereports.p_MinSalary 
    this.p_MaxSalary=searchcandidatereports.p_MaxSalary 
    this.p_IpAddress=searchcandidatereports.p_IpAddress 
    this.p_EducationId = searchcandidatereports.p_EducationId  
    this.p_DesignationId= searchcandidatereports.p_DesignationId
    this.p_DistrictId = searchcandidatereports.p_DistrictId
    this.p_Limit = searchcandidatereports.p_Limit
   
    
     
    
};
searchcandidatereports.getJSsavedsearch= function createUser(JSsavedsearch, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});

    sql.query("CALL fetch_savedsearch('"+JSsavedsearch.p_SearchId +"','"+JSsavedsearch.p_EmpId+"')", function (err, res) {
        if(err) {   
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};
searchcandidatereports.createsavedsearch = function createUser(savedsearch, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_savedSearch('"+savedsearch.p_SearchId+"','"+savedsearch.p_EmpId+"','"+savedsearch.p_Education+"','"+savedsearch.p_Designation+"','"+savedsearch.p_District+"','"+savedsearch.p_Gender+"','"+savedsearch.p_MinAge+"','"+savedsearch.p_MaxAge+"','"+savedsearch.p_MinSalary+"','"+savedsearch.p_MaxSalary+"','"+savedsearch.p_IpAddress+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

searchcandidatereports.createCandidateSearch = function createUser(fetch_CandidateJobFair, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
        if (err) throw err;
    });
    
        sql.query("CALL fetch_emp_search_candidate('"+fetch_CandidateJobFair.p_EducationId +"','"+fetch_CandidateJobFair.p_DesignationId +"','"+ fetch_CandidateJobFair.p_DistrictId+"','"+fetch_CandidateJobFair.p_Gender+"','"+fetch_CandidateJobFair.p_MinAge+"','"+fetch_CandidateJobFair.p_MaxAge+"','"+fetch_CandidateJobFair.p_MinSalary+"','"+fetch_CandidateJobFair.p_MaxSalary+"','"+fetch_CandidateJobFair.p_Limit+"','"+fetch_CandidateJobFair.p_EmpId+"')", function (err, res) {
            if (err) {
    sql.end();
                result(err, null);
            } else {
    sql.end();
                result(null, res);
            }
        });
    };

module.exports= searchcandidatereports ;