'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Counselling = function(counselling){
    this.p_CandidateName  = counselling.p_CandidateName;
    this.p_DateOfBirth  = counselling.p_DateOfBirth ;
    this.p_Address  = counselling.p_Address ;
    this.p_ContactNumber  = counselling.p_ContactNumber ;
    this.p_MaritalStatus  = counselling.p_MaritalStatus ;
    this.p_Category   = counselling.p_Category ;
    this.p_EducationalQualification   = counselling.p_EducationalQualification  ;
    this.p_Training    = counselling.p_Training  ;
    this.p_Experience    = counselling.p_Experience  ;
    this.p_Problem    = counselling.p_Problem  ;
    this.p_Advice     = counselling.p_Advice   ;
    this.p_HighestScore_1     = counselling.p_HighestScore_1   ;
    this.p_HighestScore_2     = counselling.p_HighestScore_2   ;
    this.p_Difficulty_1    = counselling.p_Difficulty_1  ;
    this.p_Difficulty_2     = counselling.p_Difficulty_2   ;
    this.p_Health     = counselling.p_Health   ;
    this.p_Temperament_1     = counselling.p_Temperament_1   ;
    this.p_Temperament_2      = counselling.p_Temperament_2    ;
    this.p_Temperament_3      = counselling.p_Temperament_3    ;
    this.p_Temperament_4      = counselling.p_Temperament_4    ;
    this.p_Interest_1_1      = counselling.p_Interest_1_1    ;
    this.p_Interest_1_2       = counselling.p_Interest_1_2     ;
    this.p_Interest_2_1       = counselling.p_Interest_2_1     ;
    this.p_Interest_2_2       = counselling.p_Interest_2_2    ;
    this.p_Interest_3       = counselling.p_Interest_3    ;
    this.p_Interest_4        = counselling.p_Interest_4     ;
    this.p_Interest_5_1        = counselling.p_Interest_5_1     ;
    this.p_Interest_5_2        = counselling.p_Interest_5_2     ;
    this.p_Problem_1         = counselling.p_Problem_1      ;
    this.p_Problem_2         = counselling.p_Problem_2     ;
    this.p_Problem_3_1         = counselling.p_Problem_3_1      ;
  this.p_Problem_3_2         = counselling.p_Problem_3_2      ;
  this.p_Problem_4_1         = counselling.p_Problem_4_1      ;
  this.p_Problem_4_2         = counselling.p_Problem_4_2      ;
  this.p_CandidateId          = counselling.p_CandidateId       ;
  this.p_Relation           = counselling.p_Relation        ;
  this.p_Age           = counselling.p_Age        ;
  this.p_Education           = counselling.p_Education        ;
  this.p_Profession           = counselling.p_Profession        ;
  this.p_Salary           = counselling.p_Salary        ;
  this.p_Year            = counselling.p_Year         ;
  this.p_Subject             = counselling.p_Subject          ;
  this.p_Percentage              = counselling.p_Percentage           ;
  this.p_Division               = counselling.p_Division            ;
  this.p_Month              = counselling.p_Month           ;
  this.p_Year               = counselling.p_Year            ;
  this.p_EnteredOn =counselling.p_EnteredOn ;
  this.p_EnteredIP =counselling.p_EnteredIP ;

  this.p_Ex_Id =counselling.p_Ex_Id ;
  this.p_Toyear =counselling.p_Toyear ;
  this.p_Frmyear =counselling.p_Frmyear ;  
  this.p_ToMonthId =counselling.p_ToMonthId ;
  this.p_FrmMonthId =counselling.p_FrmMonthId ;
  this.p_CC_Id=counselling.p_CC_Id
};


Counselling.createCounselling= function createUser(counselling, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Ins_counselling_registration('"+counselling.p_CandidateName+"','"+counselling.p_DateOfBirth+"','"+counselling.p_Address+"','"+counselling.p_ContactNumber+"','"+counselling.p_MaritalStatus+"','"+counselling.p_Category+"','"+counselling.p_EducationalQualification +"','"+counselling.p_Training +"','"+counselling.p_Experience +"','"+counselling.p_Problem +"','"+counselling.p_Advice +"','"+counselling.p_HighestScore_1 +"','"+counselling.p_HighestScore_2 +"','"+counselling.p_Difficulty_1 +"','"+counselling.p_Difficulty_2+"','"+counselling.p_Health +"','"+counselling.p_Temperament_1 +"','"+counselling.p_Temperament_2 +"','"+counselling.p_Temperament_3 +"','"+counselling.p_Temperament_4  +"','"+counselling.p_Interest_1_1  +"','"+counselling.p_Interest_1_2  +"','"+counselling.p_Interest_2_1  +"','"+counselling.p_Interest_2_2  +"','"+counselling.p_Interest_3  +"','"+counselling.p_Interest_4  +"','"+counselling.p_Interest_5_1  +"','"+counselling.p_Interest_5_2  +"','"+counselling.p_Problem_1  +"','"+counselling.p_Problem_2  +"','"+counselling.p_Problem_3_1  +"','"+counselling.p_Problem_3_2  +"','"+counselling.p_Problem_4_1  +"','"+counselling.p_Problem_4_2 +"','"+counselling.p_EnteredOn +"','"+counselling.p_EnteredIP +"','"+counselling.p_CC_Id +"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

Counselling.createCounsellingfamily= function createUser(counselling, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Ins_counselling_family('"+counselling.p_CandidateId +"','"+counselling.p_Relation +"','"+counselling.p_Age +"','"+counselling.p_Education +"','"+counselling.p_Profession +"','"+counselling.p_Salary+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

Counselling.createCounsellingeducation= function createUser(counselling, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Ins_counselling_education('"+counselling.p_CandidateId +"','"+counselling.p_Education  +"','"+counselling.p_Year  +"','"+counselling.p_Subject  +"','"+counselling.p_Percentage  +"','"+counselling.p_Division +"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};
Counselling.getCounselling= function createUser(counselling, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_counselling_registration('"+counselling.p_Month +"','"+counselling.p_Year  +"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};


Counselling.getCounselling_ByMonthConsolidate= function createUser(Counselling_ByMonthConsolidate, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Counselling_ByMonthConsolidate('"+Counselling_ByMonthConsolidate.p_FrmMonthId+"','"+Counselling_ByMonthConsolidate.p_ToMonthId+"','"+Counselling_ByMonthConsolidate.p_Frmyear+"','"+Counselling_ByMonthConsolidate.p_Toyear+"','"+Counselling_ByMonthConsolidate.p_Ex_Id+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};


Counselling.getCounsellingByMonthWise= function createUser(CounsellingByMonthWise, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Counselling_ByMonthWise('"+CounsellingByMonthWise.p_Frmyear+"','"+CounsellingByMonthWise.p_Toyear+"','"+CounsellingByMonthWise.p_Ex_Id+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

module.exports= Counselling;