
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES25P1Entry= function(es25Part1entry){
    
    this.p_Ex_Id  = es25Part1entry.p_Ex_Id;
    this.p_FromDt    = es25Part1entry.p_FromDt;
    this.p_ToDt     = es25Part1entry.p_ToDt;

    this.p_Registration_Blind= es25Part1entry.p_Registration_Blind;
    this.p_PlacesJs_CG_Blind = es25Part1entry.p_PlacesJs_CG_Blind;
    this.p_PlacesJs_UT_Blind= es25Part1entry.p_PlacesJs_UT_Blind ;
    this.p_PlacesJs_SG_Blind   = es25Part1entry.p_PlacesJs_SG_Blind ;
    this.p_PlacesJs_QG_Blind     = es25Part1entry.p_PlacesJs_QG_Blind ;
    this.p_PlacesJs_LB_Blind      = es25Part1entry.p_PlacesJs_LB_Blind ;
    this.p_PlacesJs_Private_Blind = es25Part1entry.p_PlacesJs_Private_Blind ;
    this.p_PlacesJs_Total_Blind  = es25Part1entry.p_PlacesJs_Total_Blind ;
    this.p_Registration_Removed_Blind = es25Part1entry.p_Registration_Removed_Blind  ;
    this.p_LR_Blind = es25Part1entry.p_LR_Blind;
    this.p_Submissions_Blind     = es25Part1entry.p_Submissions_Blind ;
    this.p_LR_Prev_1_Blind      = es25Part1entry.p_LR_Prev_1_Blind ;

    this.p_Registration_Deaf = es25Part1entry.p_Registration_Deaf ;
    this.p_PlacesJs_CG_Deaf  = es25Part1entry.p_PlacesJs_CG_Deaf ;
    this.p_PlacesJs_UT_Deaf = es25Part1entry.p_PlacesJs_UT_Deaf  ;
    this.p_PlacesJs_SG_Deaf    = es25Part1entry.p_PlacesJs_SG_Deaf  ;
    this.p_PlacesJs_QG_Deaf      = es25Part1entry.p_PlacesJs_QG_Deaf  ;
    this.p_PlacesJs_LB_Deaf       = es25Part1entry.p_PlacesJs_LB_Deaf  ;
    this.p_PlacesJs_Private_Deaf  = es25Part1entry.p_PlacesJs_Private_Deaf  ;
    this.p_PlacesJs_Total_Deaf   = es25Part1entry.p_PlacesJs_Total_Deaf  ;
    this.p_Registration_Removed_Deaf  = es25Part1entry.p_Registration_Removed_Deaf   ;
    this.p_LR_Deaf   = es25Part1entry.p_LR_Deaf   ;
    this.p_Submissions_Deaf    = es25Part1entry.p_Submissions_Deaf   ;
    this.p_LR_Prev_1_Deaf   = es25Part1entry.p_LR_Prev_1_Deaf    ;

    this.p_Registration_Orthopaedics  = es25Part1entry.p_Registration_Orthopaedics  ;
    this.p_PlacesJs_CG_Orthopaedics   = es25Part1entry.p_PlacesJs_CG_Orthopaedics  ;
    this.p_PlacesJs_UT_Orthopaedics  = es25Part1entry.p_PlacesJs_UT_Orthopaedics   ;
    this.p_PlacesJs_SG_Orthopaedics     = es25Part1entry.p_PlacesJs_SG_Orthopaedics   ;
    this.p_PlacesJs_QG_Orthopaedics       = es25Part1entry.p_PlacesJs_QG_Orthopaedics   ;
    this.p_PlacesJs_LB_Orthopaedics        = es25Part1entry.p_PlacesJs_LB_Orthopaedics   ;
    this.p_PlacesJs_Private_Orthopaedics   = es25Part1entry.p_PlacesJs_Private_Orthopaedics   ;
    this.p_PlacesJs_Total_Orthopaedics    = es25Part1entry.p_PlacesJs_Total_Orthopaedics   ;
    this.p_Registration_Removed_Orthopaedics   = es25Part1entry.p_Registration_Removed_Orthopaedics    ;
    this.p_LR_Orthopaedics    = es25Part1entry.p_LR_Orthopaedics    ;
    this.p_Submissions_Orthopaedics     = es25Part1entry.p_Submissions_Orthopaedics    ;
    this.p_LR_Prev_1_Orthopaedics    = es25Part1entry.p_LR_Prev_1_Orthopaedics     ;

    this.p_Registration_Respiratory   = es25Part1entry.p_Registration_Respiratory   ;
    this.p_PlacesJs_CG_Respiratory   = es25Part1entry.p_PlacesJs_CG_Respiratory  ;
    this.p_PlacesJs_UT_Respiratory  = es25Part1entry.p_PlacesJs_UT_Respiratory   ;
    this.p_PlacesJs_SG_Respiratory    = es25Part1entry.p_PlacesJs_SG_Respiratory  ;
    this.p_PlacesJs_QG_Respiratory      = es25Part1entry.p_PlacesJs_QG_Respiratory  ;
    this.p_PlacesJs_LB_Respiratory       = es25Part1entry.p_PlacesJs_LB_Respiratory  ;
    this.p_PlacesJs_Private_Respiratory  = es25Part1entry.p_PlacesJs_Private_Respiratory  ;
    this.p_PlacesJs_Total_Respiratory   = es25Part1entry.p_PlacesJs_Total_Respiratory  ;
    this.p_Registration_Removed_Respiratory  = es25Part1entry.p_Registration_Removed_Respiratory   ;
    this.p_LR_Respiratory   = es25Part1entry.p_LR_Respiratory   ;
    this.p_Submissions_Respiratory    = es25Part1entry.p_Submissions_Respiratory   ;
    this.p_LR_Prev_1_Respiratory   = es25Part1entry.p_LR_Prev_1_Respiratory    ;

    this.p_Registration_Leprosy   = es25Part1entry.p_Registration_Leprosy ;
    this.p_PlacesJs_CG_Leprosy   = es25Part1entry.p_PlacesJs_CG_Leprosy;
    this.p_PlacesJs_UT_Leprosy  = es25Part1entry.p_PlacesJs_UT_Leprosy ;
    this.p_PlacesJs_SG_Leprosy    = es25Part1entry.p_PlacesJs_SG_Leprosy;
    this.p_PlacesJs_QG_Leprosy      = es25Part1entry.p_PlacesJs_QG_Leprosy;
    this.p_PlacesJs_LB_Leprosy       = es25Part1entry.p_PlacesJs_LB_Leprosy;
    this.p_PlacesJs_Private_Leprosy  = es25Part1entry.p_PlacesJs_Private_Leprosy;
    this.p_PlacesJs_Total_Leprosy   = es25Part1entry.p_PlacesJs_Total_Leprosy;
    this.p_Registration_Removed_Leprosy  = es25Part1entry.p_Registration_Removed_Leprosy ;
    this.p_LR_Leprosy   = es25Part1entry.p_LR_Leprosy ;
    this.p_Submissions_Leprosy    = es25Part1entry.p_Submissions_Leprosy ;
    this.p_LR_Prev_1_Leprosy   = es25Part1entry.p_LR_Prev_1_Leprosy  ;

    this.p_Registration_Total   = es25Part1entry.p_Registration_Total ;
    this.p_PlacesJs_CG_Total   = es25Part1entry.p_PlacesJs_CG_Total;
    this.p_PlacesJs_UT_Total  = es25Part1entry.p_PlacesJs_UT_Total ;
    this.p_PlacesJs_SG_Total    = es25Part1entry.p_PlacesJs_SG_Total;
    this.p_PlacesJs_QG_Total      = es25Part1entry.p_PlacesJs_QG_Total;
    this.p_PlacesJs_LB_Total       = es25Part1entry.p_PlacesJs_LB_Total;
    this.p_PlacesJs_Private_Total  = es25Part1entry.p_PlacesJs_Private_Total;
    this.p_PlacesJs_Total_Total   = es25Part1entry.p_PlacesJs_Total_Total;
    this.p_Registration_Removed_Total  = es25Part1entry.p_Registration_Removed_Total ;
    this.p_LR_Total   = es25Part1entry.p_LR_Total ;
    this.p_Submissions_Total    = es25Part1entry.p_Submissions_Total ;
    this.p_LR_Prev_1_Total   = es25Part1entry.p_LR_Prev_1_Total  ;

     this.p_Registration_W   = es25Part1entry.p_Registration_W ;
    this.p_PlacesJs_CG_W   = es25Part1entry.p_PlacesJs_CG_W;
    this.p_PlacesJs_UT_W  = es25Part1entry.p_PlacesJs_UT_W ;
    this.p_PlacesJs_SG_W    = es25Part1entry.p_PlacesJs_SG_W;
    this.p_PlacesJs_QG_W      = es25Part1entry.p_PlacesJs_QG_W;
    this.p_PlacesJs_LB_W       = es25Part1entry.p_PlacesJs_LB_W;
    this.p_PlacesJs_Private_W  = es25Part1entry.p_PlacesJs_Private_W;
    this.p_PlacesJs_Total_W   = es25Part1entry.p_PlacesJs_Total_W;
    this.p_Registration_Removed_W  = es25Part1entry.p_Registration_Removed_W ;
    this.p_LR_W   = es25Part1entry.p_LR_W ;
    this.p_Submissions_W    = es25Part1entry.p_Submissions_W ;
    this.p_LR_Prev_1_W   = es25Part1entry.p_LR_Prev_1_W  ;

    this.p_Flag =es25Part1entry.p_Flag ;
};


ES25P1Entry.getES25P1Rpt = function createUser(Es25P1, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_ES25Part1('"+Es25P1.p_FromDt+"','"+Es25P1.p_ToDt+"',"+Es25P1.p_Ex_Id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};
ES25P1Entry.createES25P1Rpt= function createUser(Es25P1entryPara, result) {  
 
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES25_Part_I_Entry('"+Es25P1entryPara.p_Ex_Id+"','"+Es25P1entryPara.p_FromDt+"','"+Es25P1entryPara.p_ToDt+"','"+Es25P1entryPara.p_Registration_Blind+"','"+Es25P1entryPara.p_PlacesJs_CG_Blind+"','"+Es25P1entryPara.p_PlacesJs_UT_Blind+"','"+Es25P1entryPara.p_PlacesJs_SG_Blind+"','"+Es25P1entryPara.p_PlacesJs_QG_Blind+"','"+Es25P1entryPara.p_PlacesJs_LB_Blind+"','"+Es25P1entryPara.p_PlacesJs_Private_Blind+"','"+Es25P1entryPara.p_PlacesJs_Total_Blind+"','"+Es25P1entryPara.p_Registration_Removed_Blind+"','"+Es25P1entryPara.p_LR_Blind+"','"+Es25P1entryPara.p_Submissions_Blind+"','"+Es25P1entryPara.p_LR_Prev_1_Blind+"','"+Es25P1entryPara.p_Registration_Deaf+"','"+Es25P1entryPara.p_PlacesJs_CG_Deaf+"','"+Es25P1entryPara.p_PlacesJs_UT_Deaf+"','"+Es25P1entryPara.p_PlacesJs_SG_Deaf+"','"+Es25P1entryPara.p_PlacesJs_QG_Deaf+"','"+Es25P1entryPara.p_PlacesJs_LB_Deaf+"','"+Es25P1entryPara.p_PlacesJs_Private_Deaf+"','"+Es25P1entryPara.p_PlacesJs_Total_Deaf+"','"+Es25P1entryPara.p_Registration_Removed_Deaf+"','"+Es25P1entryPara.p_LR_Deaf+"','"+Es25P1entryPara.p_Submissions_Deaf+"','"+Es25P1entryPara.p_LR_Prev_1_Deaf+"','"+Es25P1entryPara.p_Registration_Orthopaedics+"','"+Es25P1entryPara.p_PlacesJs_CG_Orthopaedics+"','"+Es25P1entryPara.p_PlacesJs_UT_Orthopaedics+"','"+Es25P1entryPara.p_PlacesJs_SG_Orthopaedics+"','"+Es25P1entryPara.p_PlacesJs_QG_Orthopaedics+"','"+Es25P1entryPara.p_PlacesJs_LB_Orthopaedics+"','"+Es25P1entryPara.p_PlacesJs_Private_Orthopaedics+"','"+Es25P1entryPara.p_PlacesJs_Total_Orthopaedics+"','"+Es25P1entryPara.p_Registration_Removed_Orthopaedics+"','"+Es25P1entryPara.p_LR_Orthopaedics+"','"+Es25P1entryPara.p_Submissions_Orthopaedics+"','"+Es25P1entryPara.p_LR_Prev_1_Orthopaedics+"','"+Es25P1entryPara.p_Registration_Respiratory+"','"+Es25P1entryPara.p_PlacesJs_CG_Respiratory+"','"+Es25P1entryPara.p_PlacesJs_UT_Respiratory+"','"+Es25P1entryPara.p_PlacesJs_SG_Respiratory+"','"+Es25P1entryPara.p_PlacesJs_QG_Respiratory+"','"+Es25P1entryPara.p_PlacesJs_LB_Respiratory+"','"+Es25P1entryPara.p_PlacesJs_Private_Respiratory+"','"+Es25P1entryPara.p_PlacesJs_Total_Respiratory+"','"+Es25P1entryPara.p_Registration_Removed_Respiratory +"','"+Es25P1entryPara.p_LR_Respiratory +"','"+Es25P1entryPara.p_Submissions_Respiratory +"','"+Es25P1entryPara.p_LR_Prev_1_Respiratory +"','"+Es25P1entryPara.p_Registration_Leprosy +"','"+Es25P1entryPara.p_PlacesJs_CG_Leprosy +"','"+Es25P1entryPara.p_PlacesJs_UT_Leprosy+"','"+Es25P1entryPara.p_PlacesJs_SG_Leprosy+"','"+Es25P1entryPara.p_PlacesJs_QG_Leprosy+"','"+Es25P1entryPara.p_PlacesJs_LB_Leprosy+"','"+Es25P1entryPara.p_PlacesJs_Private_Leprosy+"','"+Es25P1entryPara.p_PlacesJs_Total_Leprosy+"','"+Es25P1entryPara.p_Registration_Removed_Leprosy+"','"+Es25P1entryPara.p_LR_Leprosy+"','"+Es25P1entryPara.p_Submissions_Leprosy+"','"+Es25P1entryPara.p_LR_Prev_1_Leprosy+"','"+Es25P1entryPara.p_Registration_Total +"','"+Es25P1entryPara.p_PlacesJs_CG_Total +"','"+Es25P1entryPara.p_PlacesJs_UT_Total+"','"+Es25P1entryPara.p_PlacesJs_SG_Total+"','"+Es25P1entryPara.p_PlacesJs_QG_Total+"','"+Es25P1entryPara.p_PlacesJs_LB_Total+"','"+Es25P1entryPara.p_PlacesJs_Private_Total+"','"+Es25P1entryPara.p_PlacesJs_Total_Total+"','"+Es25P1entryPara.p_Registration_Removed_Total+"','"+Es25P1entryPara.p_LR_Total+"','"+Es25P1entryPara.p_Submissions_Total+"','"+Es25P1entryPara.p_LR_Prev_1_Total+"','"+Es25P1entryPara.p_Registration_W +"','"+Es25P1entryPara.p_PlacesJs_CG_W +"','"+Es25P1entryPara.p_PlacesJs_UT_W+"','"+Es25P1entryPara.p_PlacesJs_SG_W+"','"+Es25P1entryPara.p_PlacesJs_QG_W+"','"+Es25P1entryPara.p_PlacesJs_LB_W+"','"+Es25P1entryPara.p_PlacesJs_Private_W+"','"+Es25P1entryPara.p_PlacesJs_Total_W+"','"+Es25P1entryPara.p_Registration_Removed_W+"','"+Es25P1entryPara.p_LR_W+"','"+Es25P1entryPara.p_Submissions_W+"','"+Es25P1entryPara.p_LR_Prev_1_W+"','"+Es25P1entryPara.p_Flag+"')", function (err, res) {      
             if(err) {
                sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });           
};


module.exports= ES25P1Entry;