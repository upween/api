'use strict';

'user strict';

var sql = require('./db.js');

var DataBaseSearch = function DataBaseSearch(_DataBaseSearch) {
    this.p_Gender = _DataBaseSearch.p_Gender;
    this.p_Category_id = _DataBaseSearch.p_Category_id;
    this.p_NoOFCandidates = _DataBaseSearch.p_NoOFCandidates;
    this.p_Division_id = _DataBaseSearch.p_Division_id;
    this.p_District_id = _DataBaseSearch.p_District_id;
    this.p_Qualif_Level_id = _DataBaseSearch.p_Qualif_Level_id;
    this.p_Qualif_id = _DataBaseSearch.p_Qualif_id;
    this.p_Sub_Group_id = _DataBaseSearch.p_Sub_Group_id;
    this.p_Q_Division_id = _DataBaseSearch.p_Q_Division_id;
    this.p_Q_PassingYear = _DataBaseSearch.p_Q_PassingYear;
    this.p_Criteria_Age_YN = _DataBaseSearch.p_Criteria_Age_YN;
    this.p_minAge = _DataBaseSearch.p_minAge;
    this.p_maxAge = _DataBaseSearch.p_maxAge;
    this.p_Asondate = _DataBaseSearch.p_Asondate;
    this.p_Criteria_TS_YN = _DataBaseSearch.p_Criteria_TS_YN;
    this.p_Lang_id = _DataBaseSearch.p_Lang_id;
    this.p_Type_id = _DataBaseSearch.p_Type_id;
    this.p_Speed = _DataBaseSearch.p_Speed;
    this.p_TS_Pass_Year = _DataBaseSearch.p_TS_Pass_Year;
    this.p_Criteria_Reg_YN = _DataBaseSearch.p_Criteria_Reg_YN;
    this.p_Fromdate = _DataBaseSearch.p_Fromdate;
    this.p_Tomdate = _DataBaseSearch.p_Tomdate;
    this.p_Ex_Id = _DataBaseSearch.p_Ex_Id;
    this.p_NCO_YN = _DataBaseSearch.p_NCO_YN;
    this.p_NCO_m = _DataBaseSearch.p_NCO_m;
    this.p_NCO_1 = _DataBaseSearch.p_NCO_1;
    this.p_NCO_2 = _DataBaseSearch.p_NCO_2;
    this.p_Qualif_YN = _DataBaseSearch.p_Qualif_YN;
    this.p_ph_YN = _DataBaseSearch.p_ph_YN;
    this.p_HC_Category_id = _DataBaseSearch.p_HC_Category_id;
    this.p_ph_all = _DataBaseSearch.p_ph_all;
    this.p_City_id = _DataBaseSearch.p_City_id;
    this.p_Location_id = _DataBaseSearch.p_Location_id;
    this.p_FunctionalArea_id = _DataBaseSearch.p_FunctionalArea_id;
};
DataBaseSearch.createDataBaseSearch = function createUser(DataBaseSearch, result) {

    sql.query("CALL DataBaseSearchEngine('" + DataBaseSearch.p_Gender + "','" + DataBaseSearch.p_Category_id + "','" + DataBaseSearch.p_NoOFCandidates + "','" + DataBaseSearch.p_Division_id + "','" + DataBaseSearch.p_District_id + "','" + DataBaseSearch.p_Qualif_Level_id + "','" + DataBaseSearch.p_Qualif_id + "','" + DataBaseSearch.p_Sub_Group_id + "','" + DataBaseSearch.p_Q_Division_id + "','" + DataBaseSearch.p_Q_PassingYear + "','" + DataBaseSearch.p_Criteria_Age_YN + "','" + DataBaseSearch.p_minAge + "','" + DataBaseSearch.p_maxAge + "','" + DataBaseSearch.p_Asondate + "','" + DataBaseSearch.p_Criteria_TS_YN + "','" + DataBaseSearch.p_Lang_id + "','" + DataBaseSearch.p_Type_id + "','" + DataBaseSearch.p_Speed + "','" + DataBaseSearch.p_TS_Pass_Year + "','" + DataBaseSearch.p_Criteria_Reg_YN + "','" + DataBaseSearch.p_Fromdate + "','" + DataBaseSearch.p_Tomdate + "','" + DataBaseSearch.p_Ex_Id + "','" + DataBaseSearch.p_NCO_YN + "','" + DataBaseSearch.p_NCO_m + "','" + DataBaseSearch.p_NCO_1 + "','" + DataBaseSearch.p_NCO_2 + "','" + DataBaseSearch.p_Qualif_YN + "','" + DataBaseSearch.p_ph_YN + "','" + DataBaseSearch.p_HC_Category_id + "','" + DataBaseSearch.p_ph_all + "','" + DataBaseSearch.p_City_id + "','" + DataBaseSearch.p_Location_id + "','" + DataBaseSearch.p_FunctionalArea_id + "')", function (err, res) {
        if (err) {

            result(err, null);
        } else {

            result(null, res);
        }
    });
};

module.exports = DataBaseSearch;