'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var EmpRegister = function(EmpRegister){
    this.p_JobFair_ID = EmpRegister.p_JobFair_ID;
    this.p_Emp_Regno = EmpRegister.p_Emp_Regno;
    this.p_Flag = EmpRegister.p_Flag;
    this.p_EntryBy=EmpRegister.p_EntryBy;
    this.p_VacancyCount=EmpRegister.p_VacancyCount;
    this.p_Jobfair_Id=EmpRegister.p_Jobfair_Id;
    this.p_RegistrationId=EmpRegister.p_RegistrationId;
};
EmpRegister.createEmpRegister = function createUser(CI_Pro_JF_Emp_Register, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_JF_Emp_Register('"+CI_Pro_JF_Emp_Register.p_JobFair_ID+"','"+CI_Pro_JF_Emp_Register.p_Emp_Regno+"','"+CI_Pro_JF_Emp_Register.p_Flag+"','"+CI_Pro_JF_Emp_Register.p_VacancyCount+"')", function (err, res) {      
        if(err) {
            
            sql.end();
           
            result(err, null);
        }
        else{
           // end.sql()
           sql.end();
            
            result(null, res);
        }
    });           
};
EmpRegister.getEmpRegbyentry = function createUser(Emp_Register, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_employerbyuser('"+Emp_Register.p_EntryBy+"')", function (err, res) {      
        if(err) {
            
            sql.end();
           
            result(err, null);
        }
        else{
           // end.sql()
           sql.end();
            
            result(null, res);
        }
    });           
};

EmpRegister.createJobSeekerRegister = function createUser(Jobseeker_Register, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_JF_Jobseeker_Register('"+Jobseeker_Register.p_Jobfair_Id+"','"+Jobseeker_Register.p_RegistrationId+"','"+Jobseeker_Register.p_Flag+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
           sql.end();
            result(null, res);
        }
    });           
};
module.exports= EmpRegister;
