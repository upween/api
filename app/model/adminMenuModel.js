'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var AdminMenu = function(AdminMenu){
    this.p_Menu_Id = AdminMenu.p_Menu_Id;
    this.p_Menu_Name = AdminMenu.p_Menu_Name;
    this.p_Menu_Description = AdminMenu.p_Menu_Description;
    this.p_URL = AdminMenu.p_URL;
    this.p_Active_YN = AdminMenu.p_Active_YN;
    this.p_Sequence = AdminMenu.p_Sequence;
    this.p_open_for = AdminMenu.p_open_for;
    this.p_parent_id= AdminMenu.p_parent_id;
    
};

AdminMenu.getAdminMenu= function createUser(fetch_adm_user_mst_menu, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_adm_user_mst_menu("+fetch_adm_user_mst_menu.p_Menu_Id+","+fetch_adm_user_mst_menu.p_Active_YN+")", function (err, res) {             
        if(err) {
            //end.sql()
            sql.end();
            result(err, null);
        }
        else{
            //end.sql()
            sql.end();
            result(null, res);
        }
    });   
};

AdminMenu.createAdminMenu = function createUser(insUpd_adm_user_mst_menu, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insUpd_adm_user_mst_menu('"+insUpd_adm_user_mst_menu.p_Menu_Id+"','"+insUpd_adm_user_mst_menu.p_Menu_Name+"','"+insUpd_adm_user_mst_menu.p_Menu_Description+"','"+insUpd_adm_user_mst_menu.p_URL+"','"+insUpd_adm_user_mst_menu.p_Active_YN+"','"+insUpd_adm_user_mst_menu.p_Sequence+"','"+insUpd_adm_user_mst_menu.p_open_for+"','"+insUpd_adm_user_mst_menu.p_parent_id+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

module.exports= AdminMenu;