
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES11Entry= function(es11entry){
    this.p_Ex_id = es11entry.p_Ex_id;
    this.p_Rpt_Month = es11entry.p_Rpt_Month;
    this.p_Rpt_Year = es11entry.p_Rpt_Year;
    this.p_Prev_LR_M = es11entry.p_Prev_LR_M;
    this.p_ReceiveRegistration_M = es11entry.p_ReceiveRegistration_M;
    this.p_FreshRegistration_M = es11entry.p_FreshRegistration_M;
    this.p_ReRegistration_M = es11entry.p_ReRegistration_M;
    this.p_Total_3_M = es11entry.p_Total_3_M;
    this.p_PlacedRegistration_M = es11entry.p_PlacedRegistration_M;
    this.p_RemovedRegistration_M = es11entry.p_RemovedRegistration_M;
    this.p_TransferRegistration_M = es11entry.p_TransferRegistration_M;
    this.p_Total_7_M = es11entry.p_Total_7_M;
    this.p_LR_M = es11entry.p_LR_M;
    this.p_Rural_LR_M = es11entry.p_Rural_LR_M;
    this.p_Submissions_M = es11entry.p_Submissions_M;
    this.p_NoOfNotify_M = es11entry.p_NoOfNotify_M;
    this.p_Prev_LR_F = es11entry.p_Prev_LR_F;
    this.p_ReceiveRegistration_F = es11entry.p_ReceiveRegistration_F;
    this.p_FreshRegistration_F = es11entry.p_FreshRegistration_F;
    this.p_ReRegistration_F = es11entry.p_ReRegistration_F;
    this.p_Total_3_F = es11entry.p_Total_3_F;
    this.p_PlacedRegistration_F = es11entry.p_PlacedRegistration_F;
    this.p_RemovedRegistration_F = es11entry.p_RemovedRegistration_F;
    this.p_TransferRegistration_F = es11entry.p_TransferRegistration_F;
    this.p_Total_7_F = es11entry.p_Total_7_F;
    this.p_LR_F = es11entry.p_LR_F;
    this.p_Rural_LR_F = es11entry.p_Rural_LR_F;
    this.p_Submissions_F = es11entry.p_Submissions_F;
    this.p_NoOfNotify_F = es11entry.p_NoOfNotify_F;
    this.p_Total_PrevLR = es11entry.p_Total_PrevLR;
    this.p_ReceiveRegistration = es11entry.p_ReceiveRegistration;
    this.p_FreshRegistration = es11entry.p_FreshRegistration;
    this.p_ReRegistration= es11entry.p_ReRegistration;
    this.p_Total_3 = es11entry.p_Total_3;
    this.p_PlacedRegistration = es11entry.p_PlacedRegistration;
    this.p_RemovedRegistration = es11entry.p_RemovedRegistration;
    this.p_TransferRegistration = es11entry.p_TransferRegistration;
    this.p_Total_7 = es11entry.p_Total_7;
    this.p_LR = es11entry.p_LR;
    this.p_Rural_LR = es11entry.p_Rural_LR;
    this.p_TotalSubmission = es11entry.p_TotalSubmission;
    this.p_TotalNotify = es11entry.p_TotalNotify;
    this.p_Verify_YN = es11entry.p_Verify_YN;
    this.p_User_Type_Id  = es11entry.p_User_Type_Id ;
   // this.p_Flag = es11entry.p_Flag;
};
ES11Entry.createES11Entry= function createUser(ES11, result) { 
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    }); 
 
    sql.query("CALL CI_Pro_Rpt_ES11_Entry('"+ES11.p_Ex_id+"','"+ES11.p_Rpt_Month+"','"+ES11.p_Rpt_Year+"','"+ES11.p_Prev_LR_M+"','"+ES11.p_ReceiveRegistration_M+"','"+ES11.p_FreshRegistration_M+"','"+ES11.p_ReRegistration_M+"','"+ES11.p_Total_3_M+"','"+ES11.p_PlacedRegistration_M+"','"+ES11.p_RemovedRegistration_M+"','"+ES11.p_TransferRegistration_M+"','"+ES11.p_Total_7_M+"','"+ES11.p_LR_M+"','"+ES11.p_Rural_LR_M+"','"+ES11.p_Submissions_M+"','"+ES11.p_NoOfNotify_M+"','"+ES11.p_Prev_LR_F+"','"+ES11.p_ReceiveRegistration_F+"','"+ES11.p_FreshRegistration_F+"','"+ES11.p_ReRegistration_F+"','"+ES11.p_Total_3_F+"','"+ES11.p_PlacedRegistration_F+"','"+ES11.p_RemovedRegistration_F+"','"+ES11.p_TransferRegistration_F+"','"+ES11.p_Total_7_F+"','"+ES11.p_LR_F+"','"+ES11.p_Rural_LR_F+"','"+ES11.p_Submissions_F+"','"+ES11.p_NoOfNotify_F+"','"+ES11.p_ReceiveRegistration+"','"+ES11.p_FreshRegistration+"','"+ES11.p_ReRegistration+"','"+ES11.p_Total_3+"','"+ES11.p_PlacedRegistration+"','"+ES11.p_RemovedRegistration+"','"+ES11.p_TransferRegistration+"','"+ES11.p_Total_7+"','"+ES11.p_LR+"','"+ES11.p_Rural_LR+"','"+ES11.p_TotalSubmission+"','"+ES11.p_TotalNotify+"','"+ES11.p_Total_PrevLR+"','"+ES11.p_Flag+"')", function (err, res) {      
             if(err) {
                sql.end()
            result(err, null);
        }
        else{
            sql.end() 
            result(null, res);
        }
    });           
};



ES11Entry.createRptES11Entry= function createUser(RptES11, result) {  
 
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_ES11_Rpt('"+RptES11.p_Ex_id+"','"+RptES11.p_Rpt_Month+"','"+RptES11.p_Rpt_Year+"','"+RptES11.p_Prev_LR_M+"','"+RptES11.p_ReceiveRegistration_M+"','"+RptES11.p_FreshRegistration_M+"','"+RptES11.p_ReRegistration_M+"','"+RptES11.p_Total_3_M+"','"+RptES11.p_PlacedRegistration_M+"','"+RptES11.p_RemovedRegistration_M+"','"+RptES11.p_TransferRegistration_M+"','"+RptES11.p_Total_7_M+"','"+RptES11.p_LR_M+"','"+RptES11.p_Rural_LR_M+"','"+RptES11.p_Submissions_M+"','"+RptES11.p_NoOfNotify_M+"','"+RptES11.p_Prev_LR_F+"','"+RptES11.p_ReceiveRegistration_F+"','"+RptES11.p_FreshRegistration_F+"','"+RptES11.p_ReRegistration_F+"','"+RptES11.p_Total_3_F+"','"+RptES11.p_PlacedRegistration_F+"','"+RptES11.p_RemovedRegistration_F+"','"+RptES11.p_TransferRegistration_F+"','"+RptES11.p_Total_7_F+"','"+RptES11.p_LR_F+"','"+RptES11.p_Rural_LR_F+"','"+RptES11.p_Submissions_F+"','"+RptES11.p_NoOfNotify_F+"','"+RptES11.p_Total_PrevLR+"','"+RptES11.p_ReceiveRegistration+"','"+RptES11.p_FreshRegistration+"','"+RptES11.p_ReRegistration+"','"+RptES11.p_Total_3+"','"+RptES11.p_PlacedRegistration+"','"+RptES11.p_RemovedRegistration+"','"+RptES11.p_TransferRegistration+"','"+RptES11.p_Total_7+"','"+RptES11.p_LR+"','"+RptES11.p_Rural_LR+"','"+RptES11.p_TotalSubmission+"','"+RptES11.p_TotalNotify+"','"+RptES11.p_Verify_YN+"')", function (err, res) {      
             if(err) {
                sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });           
};

ES11Entry.getRptESS11 = function createUser(Es11, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Feth_ES11_Rpt("+Es11.p_Ex_id+","+Es11.p_Rpt_Month+","+Es11.p_Rpt_Year+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

ES11Entry.createRptES11Submit= function createUser(RptES11Submit, result) {  
 
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_ES11_Rpt_Submit('"+RptES11Submit.p_Ex_id+"','"+RptES11Submit.p_Rpt_Month+"','"+RptES11Submit.p_Rpt_Year+"','"+RptES11Submit.p_User_Type_Id +"')", function (err, res) {      
             if(err) {
                sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });           
};
ES11Entry.getExchangeforRptESS11 = function createUser(Es11, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_ES11_Rpt_Submit("+Es11.p_Rpt_Month+","+Es11.p_Rpt_Year+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= ES11Entry;