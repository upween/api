'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Itskill = function(itskill){
    this.p_ITSkillId = itskill.p_ITSkillId;
    this.p_CandidateId = itskill.p_CandidateId;
    this.p_ITSkills = itskill.p_ITSkills;
    this.p_Version = itskill.p_Version;
    this.p_LastUsed = itskill.p_LastUsed;
    this.p_ExperienceInYear = itskill.p_ExperienceInYear;
    this.p_ExperienceInMonth = itskill.p_ExperienceInMonth;
    this.p_IpAddress = itskill.p_IpAddress;
    this.p_UserId = itskill.p_UserId;
};
Itskill.createItskill = function createUser(newItskill, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_ITSkillmaster('"+newItskill.p_ITSkillId+"','"+newItskill.p_CandidateId+"','"+newItskill.p_ITSkills+"','"+newItskill.p_Version+"','"+newItskill.p_LastUsed+"','"+newItskill.p_ExperienceInYear+"','"+newItskill.p_ExperienceInMonth+"','"+newItskill.p_IpAddress+"','"+newItskill.p_UserId+"')", function (err, res) {      
        if(err) {
            sql.end();  
            result(err, null);
        }
        else{
            sql.end();  
            result(null, res);
        }
    });           
};
Itskill.getItskill = function createUser(fetch_Itskill, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_ITSkillmaster("+fetch_Itskill.p_CandidateId+","+fetch_Itskill.p_IpAddress+","+fetch_Itskill.p_UserId+")", function (err, res) {             
        if(err) {
            sql.end();  
            result(err, null);
        }
        else{
            sql.end();  
            result(null, res);
        }
    });   
};

module.exports= Itskill;