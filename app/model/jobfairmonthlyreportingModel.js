'use strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var JobFairReporting = function JobFairReporting(JobFairReportingform) {
  this.p_Flag=JobFairReportingform.p_Flag;                             
this.p_Xmonth=JobFairReportingform.p_Xmonth;                             
this.p_Xyear=JobFairReportingform.p_Xyear;                             
this.p_Ex_Id=JobFairReportingform.p_Ex_Id;                             
this.p_JF_Id=JobFairReportingform.p_JF_Id;                             
this.p_JF_Date=JobFairReportingform.p_JF_Date;                            
this.p_Company_Name=JobFairReportingform.p_Company_Name;                             
this.p_Post_Name=JobFairReportingform.p_Post_Name;                            
this.p_Selected_Candidates=JobFairReportingform.p_Selected_Candidates;                             
this.p_Female_Candidates=JobFairReportingform.p_Female_Candidates;                            
this.p_SC_Candidates=JobFairReportingform.p_SC_Candidates;                            
this.p_ST_Candidates=JobFairReportingform.p_ST_Candidates;                            
this.p_OBC_Candidates=JobFairReportingform.p_OBC_Candidates;                            
this.p_PH_Candidates=JobFairReportingform.p_PH_Candidates;                            
this.p_Minority_Candidates=JobFairReportingform.p_Minority_Candidates;                             
this.p_Remark=JobFairReportingform.p_Remark;                      
this.p_UR_Candidates=JobFairReportingform.p_UR_Candidates;                          
this.p_Client_IP=JobFairReportingform.p_Client_IP;                            
this.p_LM_BY=JobFairReportingform.p_LM_BY;            
this.p_Status=JobFairReportingform.p_Status;

this.p_FrmMonthId=JobFairReportingform.p_FrmMonthId;
this.p_ToMonthId=JobFairReportingform.p_ToMonthId;
this.p_Frmyear=JobFairReportingform.p_Frmyear;
this.p_Toyear=JobFairReportingform.p_Toyear;
this.p_ex_id=JobFairReportingform.p_ex_id;
};
JobFairReporting.createJobFairReporting = function createUser(newJobFairReporting, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL CI_JF_Pro_Reporting('" + newJobFairReporting.p_Flag + "','" + newJobFairReporting.p_Xmonth + "','" + newJobFairReporting.p_Xyear + "','" + newJobFairReporting.p_Ex_Id + "','" + newJobFairReporting.p_JF_Id + "','" + newJobFairReporting.p_JF_Date + "','" + newJobFairReporting.p_Company_Name + "','" + newJobFairReporting.p_Post_Name + "','" + newJobFairReporting.p_Selected_Candidates + "','" + newJobFairReporting.p_Female_Candidates + "','" + newJobFairReporting.p_SC_Candidates + "','" + newJobFairReporting.p_ST_Candidates + "','" + newJobFairReporting.p_OBC_Candidates + "','" + newJobFairReporting.p_PH_Candidates + "','" + newJobFairReporting.p_Minority_Candidates + "','"+newJobFairReporting.p_Remark+"','"+newJobFairReporting.p_UR_Candidates+"','"+newJobFairReporting.p_Client_IP+"','"+newJobFairReporting.p_LM_BY+"','"+newJobFairReporting.p_Status+"')", function (err, res) {
        if (err) {
sql.end()
            result(err, null);
        } else {
sql.end()
            result(null, res);
        }
    });
};



JobFairReporting.getJFRep_ByMonthConsolidate= function createUser(JFRep_ByMonthConsolidate, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_JFRep_ByMonthConsolidate('"+JFRep_ByMonthConsolidate.p_FrmMonthId+"','"+JFRep_ByMonthConsolidate.p_ToMonthId+"','"+JFRep_ByMonthConsolidate.p_Frmyear+"','"+JFRep_ByMonthConsolidate.p_Toyear+"','"+JFRep_ByMonthConsolidate.p_ex_id+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};





module.exports = JobFairReporting;