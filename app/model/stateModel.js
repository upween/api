'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var State = function(state){
    this.p_StateId = state.p_StateId;
    this.p_StateName = state.p_StateName;
    this.p_CountryId = state.p_CountryId;
    this.p_IsActive = state.p_IsActive;
    this.p_IpAddress = state.p_IpAddress;
    this.p_UserId = state.p_UserId;
};
State.createState = function createUser(newState, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insUpd_Statemaster('"+newState.p_StateId+"','"+newState.p_StateName+"','"+newState.p_CountryId+"','"+newState.p_IsActive+"','"+newState.p_IpAddress+"',"+newState.p_UserId+")", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end() 
            result(null, res);
        }
    });           
};
State.getState = function createUser(fetch_State, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Statemaster("+fetch_State.p_StateId+","+fetch_State.p_IpAddress+","+fetch_State.p_IsActive+","+fetch_State.p_UserId+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });   
};
State.getDivisionByState = function createUser(fetch_DivisionByState, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_DivisionByState("+fetch_DivisionByState.p_StateId+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end() 
            result(null, res);
        }
    });   
};



module.exports= State;