'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var JobPost = function JobPost(_JobPost) {
    this.p_JobId = _JobPost.p_JobId;
    this.p_JobTitle = _JobPost.p_JobTitle;
    this.p_JobDescription = _JobPost.p_JobDescription;
    this.p_Keywords = _JobPost.p_Keywords;
    this.p_WorkExpMin = _JobPost.p_WorkExpMin;
    this.p_WorkExpMax = _JobPost.p_WorkExpMax;
    this.p_AnnualCTCCur = _JobPost.p_AnnualCTCCur;
    this.p_AnnualCTCMin = _JobPost.p_AnnualCTCMin;
    this.p_AnnualCTCMax = _JobPost.p_AnnualCTCMax;
    this.p_OtherSalDet = _JobPost.p_OtherSalDet;
    this.p_NoOfVacancy = _JobPost.p_NoOfVacancy;
    this.p_Location = _JobPost.p_Location;
    this.p_Industry = _JobPost.p_Industry;
    this.p_FunctionalArea = _JobPost.p_FunctionalArea;
    this.p_EmploymentDetails = _JobPost.p_EmploymentDetails;
    this.p_UGQualifications = _JobPost.p_UGQualifications;
    this.p_PGQualifications = _JobPost.p_PGQualifications;
    this.p_PHDQualification = _JobPost.p_PHDQualification;
    this.p_ReponsesOn = _JobPost.p_ReponsesOn;
    this.p_ReferenceCode = _JobPost.p_ReferenceCode;
    this.p_Email = _JobPost.p_Email;
    this.p_Name = _JobPost.p_Name;
    this.p_About = _JobPost.p_About;
    this.p_Website = _JobPost.p_Website;
    this.p_ContactPerson = _JobPost.p_ContactPerson;
    this.p_ContactNumber = _JobPost.p_ContactNumber;
    this.p_Address = _JobPost.p_Address;
    this.p_RefreshJob = _JobPost.p_RefreshJob;
    this.p_IsActive = _JobPost.p_IsActive;
};
JobPost.createJobPost = function createUser(newJobPost, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL InsUpd_Job_Posting('" + newJobPost.p_JobId + "','" + newJobPost.p_JobTitle + "','" + newJobPost.p_JobDescription + "','" + newJobPost.p_Keywords + "','" + newJobPost.p_WorkExpMin + "','" + newJobPost.p_WorkExpMax + "','" + newJobPost.p_AnnualCTCCur + "','" + newJobPost.p_AnnualCTCMin + "','" + newJobPost.p_AnnualCTCMax + "','" + newJobPost.p_OtherSalDet + "','" + newJobPost.p_NoOfVacancy + "','" + newJobPost.p_Location + "','" + newJobPost.p_Industry + "','" + newJobPost.p_FunctionalArea + "','" + newJobPost.p_EmploymentDetails + "','" + newJobPost.p_UGQualifications + "','" + newJobPost.p_PGQualifications + "','" + newJobPost.p_PHDQualification + "','" + newJobPost.p_ReponsesOn + "','" + newJobPost.p_ReferenceCode + "','" + newJobPost.p_Email + "','" + newJobPost.p_Name + "','" + newJobPost.p_About + "','" + newJobPost.p_Website + "','" + newJobPost.p_ContactPerson + "','" + newJobPost.p_ContactNumber + "','" + newJobPost.p_Address + "','" + newJobPost.p_RefreshJob + "','" + newJobPost.p_IsActive + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

module.exports = JobPost;