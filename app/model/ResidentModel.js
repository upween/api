'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Resident = function(Resident){
    this.p_Resident_id = Resident.p_Resident_id;
    this.p_Active_YN = Resident.p_Active_YN;
    
};

Resident.getResident = function createUser(fetch_Resident_master, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Resident_master("+fetch_Resident_master.p_Resident_id+","+fetch_Resident_master.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= Resident;