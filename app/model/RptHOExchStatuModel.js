
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var RptHOExchStatu = function(RptHOExchStatu){
    this.p_FromDt = RptHOExchStatu.p_FromDt;
    this.p_ToDt = RptHOExchStatu.p_ToDt;
    this.p_Rpt_Month = RptHOExchStatu.p_Rpt_Month;
    this.p_Rpt_Year = RptHOExchStatu.p_Rpt_Year;
    this.p_RepID = RptHOExchStatu.p_RepID;

    
};

RptHOExchStatu.getRptHOExchStatu= function createUser(RptHOExchStatu, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_HO_ExchStatus('"+RptHOExchStatu.p_FromDt+"','"+RptHOExchStatu.p_ToDt+"',"+RptHOExchStatu.p_Rpt_Month+","+RptHOExchStatu.p_Rpt_Year+","+RptHOExchStatu.p_RepID+")", function (err, res) {                
          if(err) {
              //sql.end()
              sql.end()
            result(err, null);
        }
        else{
            sql.end()
            //sql.end()
            result(null, res);
        }
    });   
};

RptHOExchStatu.getNotSendHOExchStatus= function createUser(NotSendHOExchStatus, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    })
    sql.query("CALL CI_Pro_Rpt_NotSend_HO_ExchStatus("+NotSendHOExchStatus.p_FromDt+","+NotSendHOExchStatus.p_ToDt+","+NotSendHOExchStatus.p_Rpt_Month+","+NotSendHOExchStatus.p_Rpt_Year+","+NotSendHOExchStatus.p_RepID+")", function (err, res) {                
          if(err) {
              //sql.end()
              sql.end()
            result(err, null);
        }
        else{
            sql.end()
            //sql.end()
            result(null, res);
        }
    });   
};
module.exports= RptHOExchStatu;