
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES24P1Entry= function(es24p1entryform){
    this.p_FromDate   = es24p1entryform.p_FromDate  ;
    this.p_ToDate    = es24p1entryform.p_ToDate   ;
    this.p_Ex_Id  = es24p1entryform.p_Ex_Id ;
    this.p_FromDt=es24p1entryform.p_FromDt;
    this.p_ToDt=es24p1entryform.p_ToDt;

    this.p_Registration_SC=es24p1entryform.p_Registration_SC; 
    this.p_PlacesJs_CG_SC =es24p1entryform.p_PlacesJs_CG_SC;  
    this.p_PlacesJs_UT_SC =es24p1entryform.p_PlacesJs_UT_SC; 
    this.p_PlacesJs_SG_SC =es24p1entryform.p_PlacesJs_SG_SC;
    this.p_PlacesJs_QG_SC =es24p1entryform.p_PlacesJs_QG_SC;
    this.p_PlacesJs_LB_SC =es24p1entryform.p_PlacesJs_LB_SC;
    this.p_PlacesJs_Private_SC=es24p1entryform.p_PlacesJs_Private_SC  
    this.p_PlacesJs_Total_SC =es24p1entryform.p_PlacesJs_Total_SC;
    this.p_Registration_Removed_SC =es24p1entryform.p_Registration_Removed_SC
    this.p_LR_SC=es24p1entryform.p_LR_SC;
    this.p_Submissions_SC=es24p1entryform.p_Submissions_SC;
    this.p_LR_Prev_1_SC=es24p1entryform.p_LR_Prev_1_SC;

    this.p_Registration_ST=es24p1entryform.p_Registration_ST; 
    this.p_PlacesJs_CG_ST =es24p1entryform.p_PlacesJs_CG_ST;  
    this.p_PlacesJs_UT_ST =es24p1entryform.p_PlacesJs_UT_ST; 
    this.p_PlacesJs_SG_ST =es24p1entryform.p_PlacesJs_SG_ST;
    this.p_PlacesJs_QG_ST =es24p1entryform.p_PlacesJs_QG_ST;
    this.p_PlacesJs_LB_ST =es24p1entryform.p_PlacesJs_LB_ST;
    this.p_PlacesJs_Private_ST=es24p1entryform.p_PlacesJs_Private_ST  
    this.p_PlacesJs_Total_ST =es24p1entryform.p_PlacesJs_Total_ST;
    this.p_Registration_Removed_ST =es24p1entryform.p_Registration_Removed_ST
    this.p_LR_ST=es24p1entryform.p_LR_ST;
    this.p_Submissions_ST=es24p1entryform.p_Submissions_ST;
    this.p_LR_Prev_1_ST=es24p1entryform.p_LR_Prev_1_ST;

    this.p_Registration_OBC=es24p1entryform.p_Registration_OBC; 
    this.p_PlacesJs_CG_OBC =es24p1entryform.p_PlacesJs_CG_OBC;  
    this.p_PlacesJs_UT_OBC =es24p1entryform.p_PlacesJs_UT_OBC; 
    this.p_PlacesJs_SG_OBC =es24p1entryform.p_PlacesJs_SG_OBC;
    this.p_PlacesJs_QG_OBC =es24p1entryform.p_PlacesJs_QG_OBC;
    this.p_PlacesJs_LB_OBC =es24p1entryform.p_PlacesJs_LB_OBC;
    this.p_PlacesJs_Private_OBC=es24p1entryform.p_PlacesJs_Private_OBC  
    this.p_PlacesJs_Total_OBC =es24p1entryform.p_PlacesJs_Total_OBC;
    this.p_Registration_Removed_OBC =es24p1entryform.p_Registration_Removed_OBC
    this.p_LR_OBC=es24p1entryform.p_LR_OBC;
    this.p_Submissions_OBC=es24p1entryform.p_Submissions_OBC;
    this.p_LR_Prev_1_OBC=es24p1entryform.p_LR_Prev_1_OBC;

    this.p_Flag =es24p1entryform.p_Flag ;
};


ES24P1Entry.getES24P1Rpt = function createUser(Es24P1, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_ES24ReportPart1('"+Es24P1.p_FromDate+"','"+Es24P1.p_ToDate+"',"+Es24P1.p_Ex_Id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

ES24P1Entry.createES24P1Entry= function createUser(Es24P1entryPara, result) {  
 
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES24_Part_I_Entry('"+Es24P1entryPara.p_Ex_Id+"','"+Es24P1entryPara.p_FromDt+"','"+Es24P1entryPara.p_ToDt+"','"+Es24P1entryPara.p_Registration_SC+"','"+Es24P1entryPara.p_PlacesJs_CG_SC+"','"+Es24P1entryPara.p_PlacesJs_UT_SC+"','"+Es24P1entryPara.p_PlacesJs_SG_SC+"','"+Es24P1entryPara.p_PlacesJs_QG_SC+"','"+Es24P1entryPara.p_PlacesJs_LB_SC+"','"+Es24P1entryPara.p_PlacesJs_Private_SC+"','"+Es24P1entryPara.p_PlacesJs_Total_SC+"','"+Es24P1entryPara.p_Registration_Removed_SC+"','"+Es24P1entryPara.p_LR_SC+"','"+Es24P1entryPara.p_Submissions_SC+"','"+Es24P1entryPara.p_LR_Prev_1_SC+"','"+Es24P1entryPara.p_Registration_ST+"','"+Es24P1entryPara.p_PlacesJs_CG_ST+"','"+Es24P1entryPara.p_PlacesJs_UT_ST+"','"+Es24P1entryPara.p_PlacesJs_SG_ST+"','"+Es24P1entryPara.p_PlacesJs_QG_ST+"','"+Es24P1entryPara.p_PlacesJs_LB_ST+"','"+Es24P1entryPara.p_PlacesJs_Private_ST+"','"+Es24P1entryPara.p_PlacesJs_Total_ST+"','"+Es24P1entryPara.p_Registration_Removed_ST+"','"+Es24P1entryPara.p_LR_ST+"','"+Es24P1entryPara.p_Submissions_ST+"','"+Es24P1entryPara.p_LR_Prev_1_ST+"','"+Es24P1entryPara.p_Registration_OBC+"','"+Es24P1entryPara.p_PlacesJs_CG_OBC+"','"+Es24P1entryPara.p_PlacesJs_UT_OBC+"','"+Es24P1entryPara.p_PlacesJs_SG_OBC+"','"+Es24P1entryPara.p_PlacesJs_QG_OBC+"','"+Es24P1entryPara.p_PlacesJs_LB_OBC+"','"+Es24P1entryPara.p_PlacesJs_Private_OBC+"','"+Es24P1entryPara.p_PlacesJs_Total_OBC+"','"+Es24P1entryPara.p_Registration_Removed_OBC+"','"+Es24P1entryPara.p_LR_OBC+"','"+Es24P1entryPara.p_Submissions_OBC+"','"+Es24P1entryPara.p_LR_Prev_1_OBC+"','"+Es24P1entryPara.p_Flag+"')", function (err, res) {      
             if(err) {
                sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });           
};


module.exports= ES24P1Entry;