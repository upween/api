'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Exchangejobfair = function(exchangejobfair){
    this.p_EmpId = exchangejobfair.p_EmpId ;
    this.p_JobFair_Id = exchangejobfair.p_JobFair_Id ;
    this.p_EmployerName = exchangejobfair.p_EmployerName ;
    this.p_EntryBy = exchangejobfair.p_EntryBy ;
    this.p_Ex_id  = exchangejobfair.p_Ex_id ;
    this.p_NoOfEmployers  = exchangejobfair.p_NoOfEmployers ;
    this.p_NoOfVacancies  = exchangejobfair.p_NoOfVacancies ;
    this.p_NoOfCandidates_Selected   = exchangejobfair.p_NoOfCandidates_Selected ;
    this.p_NoOfCandidates_Primary    = exchangejobfair.p_NoOfCandidates_Primary ;
    this.p_NoOfCandidates_Offerd     = exchangejobfair.p_NoOfCandidates_Offerd ;
    this.p_Jobfair_FromDt = exchangejobfair.p_Jobfair_FromDt ;
    this.p_Jobfair_ToDt = exchangejobfair.p_Jobfair_ToDt ;

    this.p_Sector = exchangejobfair.p_Sector ;
    this.p_Vacancy = exchangejobfair.p_Vacancy ;
    this.p_DistrictId = exchangejobfair.p_DistrictId ;
    this.p_Salary = exchangejobfair.p_Salary ;

};
Exchangejobfair.createexchangejobfair = function createUser(exchangejobfair, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });  
    sql.query("CALL Insupd_JobfairEmployerRegister("+exchangejobfair.p_EmpId+",'"+exchangejobfair.p_JobFair_Id+"','"+exchangejobfair.p_EmployerName+"','"+exchangejobfair.p_EntryBy+"','"+exchangejobfair.p_Sector+"','"+exchangejobfair.p_Vacancy+"','"+exchangejobfair.p_DistrictId+"','"+exchangejobfair.p_Salary+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null,res);
        }
    });           
};

Exchangejobfair.createemployerexchangejf = function createUser(employerexchangejf , result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });  
    sql.query("CALL InsUpd_ExchangeWise_JF("+employerexchangejf.p_Ex_id +",'"+employerexchangejf.p_JobFair_Id +"','"+employerexchangejf.p_NoOfEmployers +"','"+employerexchangejf.p_NoOfVacancies +"','"+employerexchangejf.p_NoOfCandidates_Selected +"','"+employerexchangejf.p_NoOfCandidates_Primary +"','"+employerexchangejf.p_NoOfCandidates_Offerd+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null,res);
        }
    });           
};


Exchangejobfair.getexjobfairreport= function createUser(JSsavedsearch, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});

    sql.query("CALL fetch_exchange_jobfair_Report('"+JSsavedsearch.p_Ex_id +"','"+JSsavedsearch.p_Jobfair_FromDt+"','"+JSsavedsearch.p_Jobfair_ToDt+"')", function (err, res) {
        if(err) {   
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= Exchangejobfair;