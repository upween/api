'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var monthlyreporting = function monthlyreporting(_monthlyreporting) {
    this.p_JF_Id = _monthlyreporting.p_JF_Id;
    this.p_Ex_Id = _monthlyreporting.p_Ex_Id;
    this.p_JF_Date = _monthlyreporting.p_JF_Date;
    this.p_Company_Name = _monthlyreporting.p_Company_Name;
    this.p_Post_Name = _monthlyreporting.p_Post_Name;
    this.p_Selected_Candidates = _monthlyreporting.p_Selected_Candidates;
    this.p_Female_Candidates = _monthlyreporting.p_Female_Candidates;
    this.p_SC_Candidates = _monthlyreporting.p_SC_Candidates;
    this.p_ST_Candidates = _monthlyreporting.p_ST_Candidates;
    this.p_OBC_Candidates = _monthlyreporting.p_OBC_Candidates;
    this.p_PH_Candidates = _monthlyreporting.p_PH_Candidates;
    this.p_Minority_Candidates = _monthlyreporting.p_Minority_Candidates;
    this.p_UR_Candidates = _monthlyreporting.p_UR_Candidates;
    this.p_Remark = _monthlyreporting.p_Remark;
    this.p_Active_YN = _monthlyreporting.p_Active_YN;
    this.p_Verify_YN = _monthlyreporting.p_Verify_YN;
    this.p_Client_IP = _monthlyreporting.p_Client_IP;
    this.p_LM_DT = _monthlyreporting.p_LM_DT;
    this.p_LM_BY = _monthlyreporting.p_LM_BY;
    this.p_Status = _monthlyreporting.p_Status;
    this.p_XMonth = _monthlyreporting.p_XMonth;
    this.p_XYear = _monthlyreporting.p_XYear;
};
monthlyreporting.createmonthlyreporting = function createUser(InsUpd_ci_jf_monthlyreporting, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL InsUpd_ci_jf_monthlyreporting('" + InsUpd_ci_jf_monthlyreporting.p_JF_Id + "','" + InsUpd_ci_jf_monthlyreporting.p_Ex_Id + "','" + InsUpd_ci_jf_monthlyreporting.p_JF_Date + "','" + InsUpd_ci_jf_monthlyreporting.p_Company_Name + "','" + InsUpd_ci_jf_monthlyreporting.p_Post_Name + "','" + InsUpd_ci_jf_monthlyreporting.p_Selected_Candidates + "','" + InsUpd_ci_jf_monthlyreporting.p_Female_Candidates + "','" + InsUpd_ci_jf_monthlyreporting.p_SC_Candidates + "','" + InsUpd_ci_jf_monthlyreporting.p_ST_Candidates + "','" + InsUpd_ci_jf_monthlyreporting.p_OBC_Candidates + "','" + InsUpd_ci_jf_monthlyreporting.p_PH_Candidates + "','" + InsUpd_ci_jf_monthlyreporting.p_Minority_Candidates + "','" + InsUpd_ci_jf_monthlyreporting.p_UR_Candidates + "','" + InsUpd_ci_jf_monthlyreporting.p_Remark + "','" + InsUpd_ci_jf_monthlyreporting.p_Active_YN + "','" + InsUpd_ci_jf_monthlyreporting.p_Verify_YN + "','" + InsUpd_ci_jf_monthlyreporting.p_Client_IP + "','" + InsUpd_ci_jf_monthlyreporting.p_LM_DT + "','" + InsUpd_ci_jf_monthlyreporting.p_LM_BY + "','" + InsUpd_ci_jf_monthlyreporting.p_Status + "','" + InsUpd_ci_jf_monthlyreporting.p_XMonth + "','" + InsUpd_ci_jf_monthlyreporting.p_XYear + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};
monthlyreporting.getmonthlyreporting = function createUser(monthlyreporting, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});
    
var id = "";
    sql.query("CALL Fetch_ci_jf_monthlyreporting('" + monthlyreporting.p_JF_Id + "','" + monthlyreporting.p_Active_YN + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

module.exports = monthlyreporting;