'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var ProEmpX6 = function ProEmpX6(_ProEmpX) {
    this.p_Postid = _ProEmpX.p_Postid;
    this.p_Emp_Regno = _ProEmpX.p_Emp_Regno;
    this.p_Ex_id = _ProEmpX.p_Ex_id;
    this.p_Designation = _ProEmpX.p_Designation;
    this.p_JobProfile = _ProEmpX.p_JobProfile;
    this.p_minAge = _ProEmpX.p_minAge;
    this.p_maxAge = _ProEmpX.p_maxAge;
    this.p_TotalVacancy = _ProEmpX.p_TotalVacancy;
    this.p_VacancyType_id = _ProEmpX.p_VacancyType_id;
    this.p_PayandAllowances = _ProEmpX.p_PayandAllowances;
    this.p_Placeofwork = _ProEmpX.p_Placeofwork;
    this.p_ProbableDt = _ProEmpX.p_ProbableDt;
    this.p_Lastdateofapply = _ProEmpX.p_Lastdateofapply;
    this.p_InterviewDt = _ProEmpX.p_InterviewDt;
    this.p_InterviewTime = _ProEmpX.p_InterviewTime;
    this.p_InterviewPlace = _ProEmpX.p_InterviewPlace;
    this.p_CandidateList = _ProEmpX.p_CandidateList;
    this.p_Otherinfo = _ProEmpX.p_Otherinfo;
    this.p_ContactPerson = _ProEmpX.p_ContactPerson;
    this.p_ContactNo = _ProEmpX.p_ContactNo;
    this.p_Adt_YN = _ProEmpX.p_Adt_YN;
    this.p_Adt_Start = _ProEmpX.p_Adt_Start;
    this.p_Adt_End = _ProEmpX.p_Adt_End;
    this.p_Qualification_essential = _ProEmpX.p_Qualification_essential;
    this.p_Desirable = _ProEmpX.p_Desirable;
    this.p_Gender = _ProEmpX.p_Gender;
    this.p_ExchEmp_Type_Id = _ProEmpX.p_ExchEmp_Type_Id;
    this.p_Flag = _ProEmpX.p_Flag;
    this.p_AsOnDate = _ProEmpX.p_AsOnDate;
    this.p_Emp_id = _ProEmpX.p_Emp_id;
    this.p_RoleId = _ProEmpX.p_RoleId;
    this.p_SectorId = _ProEmpX.p_SectorId;

    this.p_EducationId = _ProEmpX.p_EducationId;
    this.p_DesignationId = _ProEmpX.p_DesignationId;
    this.p_DistrictId = _ProEmpX.p_DistrictId;
};
ProEmpX6.createProEmpX6 = function createUser(CI_Pro_Emp_X6, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL CI_Pro_Emp_X6('" + CI_Pro_Emp_X6.p_Postid + "','" + CI_Pro_Emp_X6.p_Emp_Regno + "','" + CI_Pro_Emp_X6.p_Ex_id + "','" + CI_Pro_Emp_X6.p_Designation + "','" + CI_Pro_Emp_X6.p_JobProfile + "','" + CI_Pro_Emp_X6.p_minAge + "','" + CI_Pro_Emp_X6.p_maxAge + "','" + CI_Pro_Emp_X6.p_TotalVacancy + "','" + CI_Pro_Emp_X6.p_VacancyType_id + "','" + CI_Pro_Emp_X6.p_PayandAllowances + "','" + CI_Pro_Emp_X6.p_Placeofwork + "','" + CI_Pro_Emp_X6.p_ProbableDt + "','" + CI_Pro_Emp_X6.p_Lastdateofapply + "','" + CI_Pro_Emp_X6.p_InterviewDt + "','" + CI_Pro_Emp_X6.p_InterviewTime + "','" + CI_Pro_Emp_X6.p_InterviewPlace + "','" + CI_Pro_Emp_X6.p_CandidateList + "','" + CI_Pro_Emp_X6.p_Otherinfo + "','" + CI_Pro_Emp_X6.p_ContactPerson + "','" + CI_Pro_Emp_X6.p_ContactNo + "','" + CI_Pro_Emp_X6.p_Adt_YN + "','" + CI_Pro_Emp_X6.p_Adt_Start + "','" + CI_Pro_Emp_X6.p_Adt_End + "','" + CI_Pro_Emp_X6.p_Qualification_essential + "','" + CI_Pro_Emp_X6.p_Desirable + "','" + CI_Pro_Emp_X6.p_Gender + "','" + CI_Pro_Emp_X6.p_ExchEmp_Type_Id + "','" + CI_Pro_Emp_X6.p_Flag + "','" + CI_Pro_Emp_X6.p_Xge_ReceiveBy + "','" + CI_Pro_Emp_X6.p_AsOnDate + "','" + CI_Pro_Emp_X6.p_Emp_id + "','" + CI_Pro_Emp_X6.p_RoleId + "','" + CI_Pro_Emp_X6.p_SectorId + "')", function (err, res) {
        if (err) {
            //end.sql()
	    sql.end()
            result(err, null);
        } else {
            //end.sql()
	    sql.end()
            result(null, res);
        }
    });
};

ProEmpX6.getCandidateJobAlert = function createUser(fetch_CandidateJobAlert, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL fetch_CandidateJobAlert(" + fetch_CandidateJobAlert.p_EducationId + "," + fetch_CandidateJobAlert.p_DesignationId + "," + fetch_CandidateJobAlert.p_DistrictId + ")", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

ProEmpX6.createWalkIn = function createUser(fetch_WalkIn, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL fetch_WalkIn()", function (err, res) {
        if (err) {
            //end.sql()
	    sql.end()
            result(err, null);
        } else {
            //end.sql()
	    sql.end()
            result(null, res);
        }
    });
};

module.exports = ProEmpX6;