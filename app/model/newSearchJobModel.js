'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var newSearchJob = function newSearchJob(_newSearchJob) {
    this.p_Designation = _newSearchJob.p_Designation;
    this.p_Placeofwork = _newSearchJob.p_Placeofwork;
    this.p_JSRegNO = _newSearchJob.p_JSRegNO;
    this.p_Qualification = _newSearchJob.p_Qualification;
    this.p_Sector = _newSearchJob.p_Sector;
};
newSearchJob.createnewSearchJob = function createUser(SearchJob_new, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL SearchJob_new('" + SearchJob_new.p_Designation + "','" + SearchJob_new.p_Placeofwork + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};
newSearchJob.createnewSearchJobRecommended = function createUser(SearchJob_Recommended, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL SearchJob_Recommended('" + SearchJob_Recommended.p_Sector + "','" + SearchJob_Recommended.p_Qualification + "','" + SearchJob_Recommended.p_Placeofwork + "','" + SearchJob_Recommended.p_Designation + "','" + SearchJob_Recommended.p_JSRegNO + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

module.exports = newSearchJob;