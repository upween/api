'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var RoleMenuMapping = function(rolemenumapping){
    this.p_RoleMenu_Id = rolemenumapping.p_RoleMenu_Id;
    this.p_User_Type_Id = rolemenumapping.p_User_Type_Id;
    this.p_Menu_Id = rolemenumapping.p_Menu_Id;
    this.p_Active_YN = rolemenumapping.p_Active_YN;
    this.p_Sub_Menu_Id=rolemenumapping.p_Sub_Menu_Id;
    this.p_Flag=rolemenumapping.p_Flag;
};
RoleMenuMapping.createRoleMenuMapping= function createUser(newRoleMenuMapping, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insupd_rolemenumapping('"+newRoleMenuMapping.p_RoleMenu_Id+"','"+newRoleMenuMapping.p_User_Type_Id+"','"+newRoleMenuMapping.p_Menu_Id+"','"+newRoleMenuMapping.p_Active_YN+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};
RoleMenuMapping.createRoleSubMenuMapping= function createUser(newRoleSubMenuMapping, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insupd_rolesubmenumapping('"+newRoleSubMenuMapping.p_User_Type_Id+"','"+newRoleSubMenuMapping.p_Menu_Id+"','"+newRoleSubMenuMapping.p_Sub_Menu_Id+"','"+newRoleSubMenuMapping.p_Active_YN+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};
RoleMenuMapping.getRoleMenuMapping = function createUser(fetch_RoleMenuMapping, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_rolemenu_mapping('"+fetch_RoleMenuMapping.p_User_Type_Id+"')", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};
RoleMenuMapping.getRoleSubMenuMapping = function createUser(fetch_RoleSubMenuMapping, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_rolesubmenu_mapping('"+fetch_RoleSubMenuMapping.p_User_Type_Id+"','"+fetch_RoleSubMenuMapping.p_Menu_Id+"')", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

RoleMenuMapping.getDeleteMenus = function createUser(Delete_menu_and_submenu, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Delete_menu_and_submenu('"+Delete_menu_and_submenu.p_User_Type_Id+"','"+Delete_menu_and_submenu.p_Menu_Id+"','"+Delete_menu_and_submenu.p_Sub_Menu_Id+"','"+Delete_menu_and_submenu.p_flag+"')", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};


module.exports= RoleMenuMapping;