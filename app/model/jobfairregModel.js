'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var JobFairReg = function JobFairReg(jobfairreg) {
    this.p_JobFairRegistrationId = jobfairreg.p_JobFairRegistrationId;
    this.p_FirstName = jobfairreg.p_FirstName;
    this.p_MiddleName = jobfairreg.p_MiddleName;
    this.p_LastName = jobfairreg.p_LastName;
    this.p_Age = jobfairreg.p_Age;
    this.p_PhoneNumber = jobfairreg.p_PhoneNumber;
    this.p_EmailId = jobfairreg.p_EmailId;
    this.p_Qualification = jobfairreg.p_Qualification;
    this.p_Fresher = jobfairreg.p_Fresher;
    this.p_CurrentOrganisation = jobfairreg.p_CurrentOrganisation;
    this.p_CurrentDesignation = jobfairreg.p_CurrentDesignation;
    this.p_Role = jobfairreg.p_Role;
    this.p_Salary = jobfairreg.p_Salary;
    this.p_JobLocation = jobfairreg.p_JobLocation;
    this.p_Expectation = jobfairreg.p_Expectation;
    this.p_Resident = jobfairreg.p_Resident;
    this.p_JobFairId = jobfairreg.p_JobFairId;
    this.p_Fields = jobfairreg.p_Fields;
};
JobFairReg.createJobFairReg = function createUser(newJobFairReg, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL InsUpd_JobFairReg('" + newJobFairReg.p_JobFairRegistrationId + "','" + newJobFairReg.p_FirstName + "','" + newJobFairReg.p_MiddleName + "','" + newJobFairReg.p_LastName + "','" + newJobFairReg.p_Age + "','" + newJobFairReg.p_PhoneNumber + "','" + newJobFairReg.p_EmailId + "','" + newJobFairReg.p_Qualification + "','" + newJobFairReg.p_Fresher + "','" + newJobFairReg.p_CurrentOrganisation + "','" + newJobFairReg.p_CurrentDesignation + "','" + newJobFairReg.p_Role + "','" + newJobFairReg.p_Salary + "','" + newJobFairReg.p_JobLocation + "','" + newJobFairReg.p_Expectation + "','" + newJobFairReg.p_Resident + "','" + newJobFairReg.p_JobFairId + "','" + newJobFairReg.p_Fields + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

module.exports = JobFairReg;