'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var HCsubCategory = function(HCsubCategory){
    this.p_HC_SubCategory_id = HCsubCategory.p_HC_SubCategory_id;
    this.p_Active_YN = HCsubCategory.p_Active_YN;
    this.p_HC_Category_id = HCsubCategory.p_HC_Category_id;
    this.p_HC_Category_Description = HCsubCategory.p_HC_Category_Description;
    this.p_LMBY = HCsubCategory.p_LMBY;
    this.p_Client_IP = HCsubCategory.p_Client_IP;
    
};

HCsubCategory.getHCsubCategory= function createUser(fetch_HC_subcategory_master, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_HC_subcategory_master("+fetch_HC_subcategory_master.p_HC_SubCategory_id+","+fetch_HC_subcategory_master.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end();  
            result(err, null);
        }
        else{
            sql.end();  
            result(null, res);
        }
    });   
};

HCsubCategory.createHCsubCategory = function createUser(InsUpd_HC_subcategory_master, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_HC_subcategory_master('"+InsUpd_HC_subcategory_master.p_HC_SubCategory_id+"','"+InsUpd_HC_subcategory_master.p_HC_Category_id+"','"+InsUpd_HC_subcategory_master.p_HC_Category_Description+"','"+InsUpd_HC_subcategory_master.p_LMBY+"','"+InsUpd_HC_subcategory_master.p_Client_IP+"','"+InsUpd_HC_subcategory_master.p_Active_YN+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};


module.exports= HCsubCategory;