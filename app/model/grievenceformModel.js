'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Grievenceform = function(grievenceform){
    this.p_GrievenceId = grievenceform.p_GrievenceId;
    this.p_RegisteredBy = grievenceform.p_RegisteredBy;
    this.p_RegistrationId = grievenceform.p_RegistrationId;
    this.p_Name = grievenceform.p_Name;
    this.p_EmailId = grievenceform.p_EmailId;
    this.p_ContactNumber = grievenceform.p_ContactNumber;
    this.p_DistrictOfficeId = grievenceform.p_DistrictOfficeId;
    this.p_Department = grievenceform.p_Department;
    this.p_TypeOfGrievence = grievenceform.p_TypeOfGrievence;
    this.p_GrievenceDetail = grievenceform.p_GrievenceDetail;
    this.p_DocumentAttached = grievenceform.p_DocumentAttached;
    this.p_UserId = grievenceform.p_UserId;
    this.p_IpAddress = grievenceform.p_IpAddress;
  
};
Grievenceform.createGrievenceform = function createUser(newGrievenceform, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_GrievenceForm('"+newGrievenceform.p_GrievenceId+"','"+newGrievenceform.p_RegisteredBy+"','"+newGrievenceform.p_RegistrationId+"','"+newGrievenceform.p_Name+"','"+newGrievenceform.p_EmailId+"','"+newGrievenceform.p_ContactNumber+"','"+newGrievenceform.p_DistrictOfficeId+"','"+newGrievenceform.p_Department+"','"+newGrievenceform.p_TypeOfGrievence+"','"+newGrievenceform.p_GrievenceDetail+"','"+newGrievenceform.p_DocumentAttached+"','"+newGrievenceform.p_UserId+"','"+newGrievenceform.p_IpAddress+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};
Grievenceform.getGrievenceform = function createUser(fetch_Grievenceform, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_GrievenceForm("+fetch_Grievenceform.p_GrievenceId+","+fetch_Grievenceform.p_RegisteredBy+","+fetch_Grievenceform.p_IpAddress+","+fetch_Grievenceform.p_UserId+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

module.exports= Grievenceform;