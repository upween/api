'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Employement = function(employement){
    this.p_EmploymentId = employement.p_EmploymentId;
    this.p_CandidateId = employement.p_CandidateId;
    this.p_DesignationId = employement.p_DesignationId;
    this.p_Organisation = employement.p_Organisation;
    this.p_IsCurrentCompany = employement.p_IsCurrentCompany;
    this.p_StartedWorkinYear = employement.p_StartedWorkinYear;
    this.p_StartedWorkinMonth = employement.p_StartedWorkinMonth;
    this.p_WorkedTillinYear = employement.p_WorkedTillinYear;
    this.p_WorkedTillinMonth = employement.p_WorkedTillinMonth;
    this.p_SalaryinLacs = employement.p_SalaryinLacs;
    this.p_SalaryinThousand = employement.p_SalaryinThousand;
    this.p_ProfileDescription = employement.p_ProfileDescription;
    this.p_NoticePeriod = employement.p_NoticePeriod;
   
};
Employement.createEmployement = function createUser(newEmployement, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_Employmentmaster('"+newEmployement.p_EmploymentId+"','"+newEmployement.p_CandidateId+"','"+newEmployement.p_DesignationId+"','"+newEmployement.p_Organisation+"','"+newEmployement.p_IsCurrentCompany+"','"+newEmployement.p_StartedWorkinYear+"','"+newEmployement.p_StartedWorkinMonth+"','"+newEmployement.p_WorkedTillinYear+"','"+newEmployement.p_WorkedTillinMonth+"','"+newEmployement.p_SalaryinLacs+"','"+newEmployement.p_SalaryinThousand+"','"+newEmployement.p_ProfileDescription+"','"+newEmployement.p_NoticePeriod+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};
Employement.getEmployement = function createUser(fetch_Employement, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_Employmentmaster("+fetch_Employement.p_CandidateId+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

module.exports= Employement;