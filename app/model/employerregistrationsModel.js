'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var EmployerRegistration = function(EmployerRegistration){
    this.p_Emp_Regno = EmployerRegistration.p_Emp_Regno;
    this.p_Emp_RegId = EmployerRegistration.p_Emp_RegId;
    this.p_Ex_id = EmployerRegistration.p_Ex_id;
    this.p_Emp_RegDt = EmployerRegistration.p_Emp_RegDt;
    this.p_CompName = EmployerRegistration.p_CompName;
    this.p_NICCode = EmployerRegistration.p_NICCode;
    this.p_CompanyProfile = EmployerRegistration.p_CompanyProfile;
    this.p_Company_type_id = EmployerRegistration.p_Company_type_id;
    this.p_V_Status = EmployerRegistration.p_V_Status;
    this.p_HO_YN = EmployerRegistration.p_HO_YN;
    this.p_TotalEmployee = EmployerRegistration.p_TotalEmployee;
    this.p_VerifyDt = EmployerRegistration.p_VerifyDt;
    this.p_CancelDt = EmployerRegistration.p_CancelDt;
    this.p_Contact_Person = EmployerRegistration.p_Contact_Person;
    this.p_Contact_Person_Desig = EmployerRegistration.p_Contact_Person_Desig;
    this.p_Address = EmployerRegistration.p_Address;
    this.p_City = EmployerRegistration.p_City;
    this.p_District_id = EmployerRegistration.p_District_id;
    this.p_District_Name = EmployerRegistration.p_District_Name;
    this.p_State_ID = EmployerRegistration.p_State_ID;
    this.p_Pincode = EmployerRegistration.p_Pincode;
    this.p_ContactNo = EmployerRegistration.p_ContactNo;
    this.p_StdCode = EmployerRegistration.p_StdCode;
    this.p_Email = EmployerRegistration.p_Email;
    this.p_fax = EmployerRegistration.p_fax;
    this.p_URL = EmployerRegistration.p_URL;
    this.p_Business_Type    = EmployerRegistration.p_Business_Type;
    this.p_E_Userid = EmployerRegistration.p_E_Userid;
    this.p_PanNo = EmployerRegistration.p_PanNo;
    this.p_CorrespondenceAdd = EmployerRegistration.p_CorrespondenceAdd;
    this.p_ContactNo_M = EmployerRegistration.p_ContactNo_M;
    this.p_Sector_Id = EmployerRegistration.p_Sector_Id; 
    this.p_Gst_Number=EmployerRegistration.p_Gst_Number ;
this.p_Password=EmployerRegistration.p_Password;
this.p_Employer_Logo = EmployerRegistration.p_Employer_Logo; 
    this.p_Flag=EmployerRegistration.p_Flag ;	
    this.P_GST_File =EmployerRegistration.P_GST_File  ;	
    this.P_PAN_File =EmployerRegistration.P_PAN_File ;
    this.p_EntryBy=EmployerRegistration.p_EntryBy;
};
EmployerRegistration.createEmployerRegistration = function createUser(newEmployerRegistration, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_emp_registration('"+newEmployerRegistration.p_Emp_Regno+"','"+newEmployerRegistration.p_Emp_RegId+"','"+newEmployerRegistration.p_Ex_id+"','"+newEmployerRegistration.p_Emp_RegDt+"','"+newEmployerRegistration.p_CompName+"','"+newEmployerRegistration.p_NICCode+"','"+newEmployerRegistration.p_CompanyProfile+"','"+newEmployerRegistration.p_Company_type_id+"','"+newEmployerRegistration.p_V_Status+"','"+newEmployerRegistration.p_HO_YN+"','"+newEmployerRegistration.p_TotalEmployee+"','"+newEmployerRegistration.p_VerifyDt+"','"+newEmployerRegistration.p_CancelDt+"','"+newEmployerRegistration.p_Contact_Person+"','"+newEmployerRegistration.p_Contact_Person_Desig+"','"+newEmployerRegistration.p_Address+"','"+newEmployerRegistration.p_City+"','"+newEmployerRegistration.p_District_id+"','"+newEmployerRegistration.p_District_Name+"','"+newEmployerRegistration.p_State_ID+"','"+newEmployerRegistration.p_Pincode+"','"+newEmployerRegistration.p_ContactNo+"','"+newEmployerRegistration.p_StdCode+"','"+newEmployerRegistration.p_Email+"','"+newEmployerRegistration.p_fax+"','"+newEmployerRegistration.p_URL+"','"+newEmployerRegistration.p_Business_Type+"','"+newEmployerRegistration.p_E_Userid+"','"+newEmployerRegistration.p_PanNo+"','"+newEmployerRegistration.p_CorrespondenceAdd+"','"+newEmployerRegistration.p_ContactNo_M+"','"+newEmployerRegistration.p_Sector_Id+"','"+newEmployerRegistration.p_Gst_Number +"','"+newEmployerRegistration.p_Password+"','"+newEmployerRegistration.P_GST_File+"','"+newEmployerRegistration.P_PAN_File +"','"+newEmployerRegistration.p_EntryBy+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

EmployerRegistration.createGSTFile = function createUser(GST_File, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Update_GST_File('"+GST_File.p_Emp_Regno+"','"+GST_File.P_GST_File+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

EmployerRegistration.getEmployerRegistration= function createUser(fetch_EmployerRegistration, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_employer_registrations("+fetch_EmployerRegistration.p_Emp_Regno+","+fetch_EmployerRegistration.p_StdCode+","+fetch_EmployerRegistration.p_Email+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

EmployerRegistration.getEmployerRegDetail= function createUser(Fetch_EmployerRegDetail, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_EmployerRegDetail("+Fetch_EmployerRegDetail.p_Emp_RegId+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};
EmployerRegistration.getEmployerByExchange= function createUser(EmployerByExchange, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_EmployerByExchange("+EmployerByExchange.p_Ex_id+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};
EmployerRegistration.createEmployerLogo= function createUser(EmployerLogo, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Employer_Logo('"+EmployerLogo.p_Emp_Regno+"','"+EmployerLogo.p_Employer_Logo+"','"+EmployerLogo.p_Flag+"')", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
            
        }
    });   
};
EmployerRegistration.getEmployerRegDetailbyEmail= function createUser(EmployerRegDetailbyEmail, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_EmployerRegDetailbyEmail("+EmployerRegDetailbyEmail.p_Email+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};
EmployerRegistration.getEmployerChangePasswordbyemail= function createUser(EmployerChangePasswordbyemail, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Employer_ChangePasswordbyemail("+EmployerChangePasswordbyemail.p_Emp_id+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};
EmployerRegistration.getEmployerPlacement= function createUser(EmployerPlacement, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_PlacementEmployer()", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};
module.exports= EmployerRegistration;