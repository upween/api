'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES13Rpt = function(ES13RptForm){
this.p_Year   = ES13RptForm.p_Year  
this.p_Ex_Id  = ES13RptForm.p_Ex_Id 
this.p_FromDt=ES13RptForm.p_FromDt; 
this.p_ToDt=ES13RptForm.p_ToDt;  
this.p_Outstanding_Prev_CG=ES13RptForm.p_Outstanding_Prev_CG;    
this.p_Outstanding_Prev_UT=ES13RptForm.p_Outstanding_Prev_UT;  
this.p_Outstanding_Prev_SG=ES13RptForm.p_Outstanding_Prev_SG;  
this.p_Outstanding_Prev_Quasi_CG=ES13RptForm.p_Outstanding_Prev_Quasi_CG;  
this.p_Outstanding_Prev_Quasi_SG=ES13RptForm.p_Outstanding_Prev_Quasi_SG;  
this.p_Outstanding_Prev_LB=ES13RptForm.p_Outstanding_Prev_LB;  
this.p_Outstanding_Prev_Private_Act=ES13RptForm.p_Outstanding_Prev_Private_Act;  
this.p_Outstanding_Prev_Private_NonAct=ES13RptForm.p_Outstanding_Prev_Private_NonAct;  
this.p_Total_Prev_Outstanding=ES13RptForm.p_Total_Prev_Outstanding;  
this.p_Transfer_Receive_CG=ES13RptForm.p_Transfer_Receive_CG;  
this.p_Transfer_Receive_UT=ES13RptForm.p_Transfer_Receive_UT;  
this.p_Transfer_Receive_SG=ES13RptForm.p_Transfer_Receive_SG;  
this.p_Transfer_Receive_Quasi_CG=ES13RptForm.p_Transfer_Receive_Quasi_CG;  
this.p_Transfer_Receive_Quasi_SG=ES13RptForm.p_Transfer_Receive_Quasi_SG;  
this.p_Transfer_Receive_LB=ES13RptForm.p_Transfer_Receive_LB;  
this.p_Transfer_Receive_Private_Act=ES13RptForm.p_Transfer_Receive_Private_Act;  
this.p_Transfer_Receive_Private_NonAct=ES13RptForm.p_Transfer_Receive_Private_NonAct;  
this.p_Total_Transfer_Receive=ES13RptForm.p_Total_Transfer_Receive;  
this.p_Notified_CG=ES13RptForm.p_Notified_CG;  
this.p_Notified_UT=ES13RptForm.p_Notified_UT;  
this.p_Notified_SG=ES13RptForm.p_Notified_SG;  
this.p_Notified_Quasi_CG=ES13RptForm.p_Notified_Quasi_CG;  
this.p_Notified_Quasi_SG=ES13RptForm.p_Notified_Quasi_SG;  
this.p_Notified_LB=ES13RptForm.p_Notified_LB;  
this.p_Notified_Private_Act=ES13RptForm.p_Notified_Private_Act;  
this.p_Notified_Private_NonAct=ES13RptForm.p_Notified_Private_NonAct;  
this.p_Total_Notified=ES13RptForm.p_Total_Notified;  
this.p_Total_1_1a_2_CG=ES13RptForm.p_Total_1_1a_2_CG;  
this.p_Total_1_1a_2_UT=ES13RptForm.p_Total_1_1a_2_UT;  
this.p_Total_1_1a_2_SG=ES13RptForm.p_Total_1_1a_2_SG;  
this.p_Total_1_1a_2_Quasi_CG=ES13RptForm.p_Total_1_1a_2_Quasi_CG;  
this.p_Total_1_1a_2_Quasi_SG=ES13RptForm.p_Total_1_1a_2_Quasi_SG;  
this.p_Total_1_1a_2_LB=ES13RptForm.p_Total_1_1a_2_LB;  
this.p_Total_1_1a_2_Private_Act=ES13RptForm.p_Total_1_1a_2_Private_Act;  
this.p_Total_1_1a_2_Private_NonAct=ES13RptForm.p_Total_1_1a_2_Private_NonAct;  
this.p_Total_Total_1_1a_2=ES13RptForm.p_Total_Total_1_1a_2;  
this.p_Filled_CG=ES13RptForm.p_Filled_CG;  
this.p_Filled_UT=ES13RptForm.p_Filled_UT;  
this.p_Filled_SG=ES13RptForm.p_Filled_SG;  
this.p_Filled_Quasi_CG=ES13RptForm.p_Filled_Quasi_CG;  
this.p_Filled_Quasi_SG=ES13RptForm.p_Filled_Quasi_SG;  
this.p_Filled_LB=ES13RptForm.p_Filled_LB;  
this.p_Filled_Private_Act=ES13RptForm.p_Filled_Private_Act;  
this.p_Filled_Private_NonAct=ES13RptForm.p_Filled_Private_NonAct;  
this.p_Total_Filled=ES13RptForm.p_Total_Filled;  
this.p_Cancelled_CG=ES13RptForm.p_Cancelled_CG;  
this.p_Cancelled_UT=ES13RptForm.p_Cancelled_UT;  
this.p_Cancelled_SG=ES13RptForm.p_Cancelled_SG;  
this.p_Cancelled_Quasi_CG=ES13RptForm.p_Cancelled_Quasi_CG;  
this.p_Cancelled_Quasi_SG=ES13RptForm.p_Cancelled_Quasi_SG;  
this.p_Cancelled_LB=ES13RptForm.p_Cancelled_LB;  
this.p_Cancelled_Private_Act=ES13RptForm.p_Cancelled_Private_Act;  
this.p_Cancelled_Private_NonAct=ES13RptForm.p_Cancelled_Private_NonAct;  
this.p_Total_Cancelled=ES13RptForm.p_Total_Cancelled;  
this.p_Transfer_CG=ES13RptForm.p_Transfer_CG;  
this.p_Transfer_UT=ES13RptForm.p_Transfer_UT;  
this.p_Transfer_SG=ES13RptForm.p_Transfer_SG;  
this.p_Transfer_Quasi_CG=ES13RptForm.p_Transfer_Quasi_CG;  
this.p_Transfer_Quasi_SG=ES13RptForm.p_Transfer_Quasi_SG;  
this.p_Transfer_LB=ES13RptForm.p_Transfer_LB;  
this.p_Transfer_Private_Act=ES13RptForm.p_Transfer_Private_Act;  
this.p_Transfer_Private_NonAct=ES13RptForm.p_Transfer_Private_NonAct;  
this.p_Total_Transfer=ES13RptForm.p_Total_Transfer;  
this.p_Outstanding_CG=ES13RptForm.p_Outstanding_CG;  
this.p_Outstanding_UT=ES13RptForm.p_Outstanding_UT;  
this.p_Outstanding_SG=ES13RptForm.p_Outstanding_SG;  
this.p_Outstanding_Quasi_CG=ES13RptForm.p_Outstanding_Quasi_CG;  
this.p_Outstanding_Quasi_SG=ES13RptForm.p_Outstanding_Quasi_SG;  
this.p_Outstanding_LB=ES13RptForm.p_Outstanding_LB;  
this.p_Outstanding_Private_Act=ES13RptForm.p_Outstanding_Private_Act;  
this.p_Outstanding_Private_NonAct=ES13RptForm.p_Outstanding_Private_NonAct;  
this.p_Total_Outstanding=ES13RptForm.p_Total_Outstanding;  
this.p_Total_4_5_5a_6_CG=ES13RptForm.p_Total_4_5_5a_6_CG;  
this.p_Total_4_5_5a_6_UT=ES13RptForm.p_Total_4_5_5a_6_UT;  
this.p_Total_4_5_5a_6_SG=ES13RptForm.p_Total_4_5_5a_6_SG;  
this.p_Total_4_5_5a_6_Quasi_CG=ES13RptForm.p_Total_4_5_5a_6_Quasi_CG;  
this.p_Total_4_5_5a_6_Quasi_SG=ES13RptForm.p_Total_4_5_5a_6_Quasi_SG;  
this.p_Total_4_5_5a_6_LB=ES13RptForm.p_Total_4_5_5a_6_LB;  
this.p_Total_4_5_5a_6_Private_Act=ES13RptForm.p_Total_4_5_5a_6_Private_Act;  
this.p_Total_4_5_5a_6_Private_NonAct=ES13RptForm.p_Total_4_5_5a_6_Private_NonAct;  
this.p_Total_Total_4_5_5a_6=ES13RptForm.p_Total_Total_4_5_5a_6;  
this.p_Notified_NotSubmitted_CG=ES13RptForm.p_Notified_NotSubmitted_CG;  
this.p_Notified_NotSubmitted_UT=ES13RptForm.p_Notified_NotSubmitted_UT;  
this.p_Notified_NotSubmitted_SG=ES13RptForm.p_Notified_NotSubmitted_SG;  
this.p_Notified_NotSubmitted_Quasi_CG=ES13RptForm.p_Notified_NotSubmitted_Quasi_CG;  
this.p_Notified_NotSubmitted_Quasi_SG=ES13RptForm.p_Notified_NotSubmitted_Quasi_SG;  
this.p_Notified_NotSubmitted_LB=ES13RptForm.p_Notified_NotSubmitted_LB;  
this.p_Notified_NotSubmitted_Private_Act=ES13RptForm.p_Notified_NotSubmitted_Private_Act;  
this.p_Notified_NotSubmitted_Private_NonAct=ES13RptForm.p_Notified_NotSubmitted_Private_NonAct;  
this.p_Total_Notified_NotSubmitted=ES13RptForm.p_Total_Notified_NotSubmitted;  
this.p_Filled_ByOtherExch_CG=ES13RptForm.p_Filled_ByOtherExch_CG;  
this.p_Filled_ByOtherExch_UT=ES13RptForm.p_Filled_ByOtherExch_UT;  
this.p_Filled_ByOtherExch_SG=ES13RptForm.p_Filled_ByOtherExch_SG;  
this.p_Filled_ByOtherExch_Quasi_CG=ES13RptForm.p_Filled_ByOtherExch_Quasi_CG;  
this.p_Filled_ByOtherExch_Quasi_SG=ES13RptForm.p_Filled_ByOtherExch_Quasi_SG;  
this.p_Filled_ByOtherExch_LB=ES13RptForm.p_Filled_ByOtherExch_LB;  
this.p_Filled_ByOtherExch_Private_Act=ES13RptForm.p_Filled_ByOtherExch_Private_Act;  
this.p_Filled_ByOtherExch_Private_NonAct=ES13RptForm.p_Filled_ByOtherExch_Private_NonAct;  
this.p_Total_Filled_ByOtherExch=ES13RptForm.p_Total_Filled_ByOtherExch;  
this.p_Filled_ForOtherExch_CG=ES13RptForm.p_Filled_ForOtherExch_CG;  
this.p_Filled_ForOtherExch_UT=ES13RptForm.p_Filled_ForOtherExch_UT;  
this.p_Filled_ForOtherExch_SG=ES13RptForm.p_Filled_ForOtherExch_SG;  
this.p_Filled_ForOtherExch_Quasi_CG=ES13RptForm.p_Filled_ForOtherExch_Quasi_CG;  
this.p_Filled_ForOtherExch_Quasi_SG=ES13RptForm.p_Filled_ForOtherExch_Quasi_SG;  
this.p_Filled_ForOtherExch_LB=ES13RptForm.p_Filled_ForOtherExch_LB;  
this.p_Filled_ForOtherExch_Private_Act=ES13RptForm.p_Filled_ForOtherExch_Private_Act;  
this.p_Filled_ForOtherExch_Private_NonAct=ES13RptForm.p_Filled_ForOtherExch_Private_NonAct;  
this.p_Total_Filled_ForOtherExch=ES13RptForm.p_Total_Filled_ForOtherExch;  
this.p_EmpUseExch_CG=ES13RptForm.p_EmpUseExch_CG;  
this.p_EmpUseExch_UT=ES13RptForm.p_EmpUseExch_UT;  
this.p_EmpUseExch_SG=ES13RptForm.p_EmpUseExch_SG;  
this.p_EmpUseExch_Quasi_CG=ES13RptForm.p_EmpUseExch_Quasi_CG;  
this.p_EmpUseExch_Quasi_SG=ES13RptForm.p_EmpUseExch_Quasi_SG;  
this.p_EmpUseExch_LB=ES13RptForm.p_EmpUseExch_LB;  
this.p_EmpUseExch_Private_Act=ES13RptForm.p_EmpUseExch_Private_Act;  
this.p_EmpUseExch_Private_NonAct=ES13RptForm.p_EmpUseExch_Private_NonAct;  
this.p_Total_EmpUseExch=ES13RptForm.p_Total_EmpUseExch;   
 this.p_Flag=ES13RptForm.p_Flag;
};



ES13Rpt.getES13 = function createUser(Fetch_ES13 , result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_ES13 ("+Fetch_ES13 .p_Year +","+Fetch_ES13 .p_Ex_Id  +")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};
ES13Rpt.createES13Rpt= function createUser(ES13RptPara, result) {  
 
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES13_Entry('"+ES13RptPara.p_Ex_Id+"','"+ES13RptPara.p_FromDt+"','"+ES13RptPara.p_ToDt+"','"+ES13RptPara.p_Outstanding_Prev_CG+"','"+ES13RptPara.p_Outstanding_Prev_CG+"','"+ES13RptPara.p_Outstanding_Prev_SG+"','"+ES13RptPara.p_Outstanding_Prev_Quasi_CG+"','"+ES13RptPara.p_Outstanding_Prev_Quasi_SG+"','"+ES13RptPara.p_Outstanding_Prev_LB+"','"+ES13RptPara.p_Outstanding_Prev_Private_Act+"','"+ES13RptPara.p_Outstanding_Prev_Private_NonAct+"','"+ES13RptPara.p_Total_Prev_Outstanding+"','"+ES13RptPara.p_Transfer_Receive_CG+"','"+ES13RptPara.p_Transfer_Receive_CG+"','"+ES13RptPara.p_Transfer_Receive_SG+"','"+ES13RptPara.p_Transfer_Receive_Quasi_CG+"','"+ES13RptPara.p_Transfer_Receive_Quasi_SG+"','"+ES13RptPara.p_Transfer_Receive_LB+"','"+ES13RptPara.p_Transfer_Receive_Private_Act+"','"+ES13RptPara.p_Transfer_Receive_Private_NonAct+"','"+ES13RptPara.p_Total_Transfer_Receive+"','"+ES13RptPara.p_Notified_CG+"','"+ES13RptPara.p_Notified_CG+"','"+ES13RptPara.p_Notified_SG+"','"+ES13RptPara.p_Notified_Quasi_CG+"','"+ES13RptPara.p_Notified_Quasi_SG+"','"+ES13RptPara.p_Notified_LB+"','"+ES13RptPara.p_Notified_Private_Act+"','"+ES13RptPara.p_Notified_Private_NonAct+"','"+ES13RptPara.p_Total_Notified+"','"+ES13RptPara.p_Total_1_1a_2_CG+"','"+ES13RptPara.p_Total_1_1a_2_CG+"','"+ES13RptPara.p_Total_1_1a_2_SG+"','"+ES13RptPara.p_Total_1_1a_2_Quasi_CG+"','"+ES13RptPara.p_Total_1_1a_2_Quasi_SG+"','"+ES13RptPara.p_Total_1_1a_2_LB+"','"+ES13RptPara.p_Total_1_1a_2_Private_Act+"','"+ES13RptPara.p_Total_1_1a_2_Private_NonAct+"','"+ES13RptPara.p_Total_Total_1_1a_2+"','"+ES13RptPara.p_Filled_CG+"','"+ES13RptPara.p_Filled_CG+"','"+ES13RptPara.p_Filled_SG+"','"+ES13RptPara.p_Filled_Quasi_CG+"','"+ES13RptPara.p_Filled_Quasi_SG+"','"+ES13RptPara.p_Filled_LB+"','"+ES13RptPara.p_Filled_Private_Act+"','"+ES13RptPara.p_Filled_Private_NonAct+"','"+ES13RptPara.p_Total_Filled+"','"+ES13RptPara.p_Cancelled_CG+"','"+ES13RptPara.p_Cancelled_CG+"','"+ES13RptPara.p_Cancelled_SG+"','"+ES13RptPara.p_Cancelled_Quasi_CG+"','"+ES13RptPara.p_Cancelled_Quasi_SG+"','"+ES13RptPara.p_Cancelled_LB+"','"+ES13RptPara.p_Cancelled_Private_Act+"','"+ES13RptPara.p_Cancelled_Private_NonAct+"','"+ES13RptPara.p_Total_Cancelled+"','"+ES13RptPara.p_Transfer_CG+"','"+ES13RptPara.p_Transfer_CG+"','"+ES13RptPara.p_Transfer_SG+"','"+ES13RptPara.p_Transfer_Quasi_CG+"','"+ES13RptPara.p_Transfer_Quasi_SG+"','"+ES13RptPara.p_Transfer_LB+"','"+ES13RptPara.p_Transfer_Private_Act+"','"+ES13RptPara.p_Transfer_Private_NonAct+"','"+ES13RptPara.p_Total_Transfer+"','"+ES13RptPara.p_Outstanding_CG+"','"+ES13RptPara.p_Outstanding_CG+"','"+ES13RptPara.p_Outstanding_SG+"','"+ES13RptPara.p_Outstanding_Quasi_CG+"','"+ES13RptPara.p_Outstanding_Quasi_SG+"','"+ES13RptPara.p_Outstanding_LB+"','"+ES13RptPara.p_Outstanding_Private_Act+"','"+ES13RptPara.p_Outstanding_Private_NonAct+"','"+ES13RptPara.p_Total_Outstanding+"','"+ES13RptPara.p_Total_4_5_5a_6_CG+"','"+ES13RptPara.p_Total_4_5_5a_6_CG+"','"+ES13RptPara.p_Total_4_5_5a_6_SG+"','"+ES13RptPara.p_Total_4_5_5a_6_Quasi_CG+"','"+ES13RptPara.p_Total_4_5_5a_6_Quasi_SG+"','"+ES13RptPara.p_Total_4_5_5a_6_LB+"','"+ES13RptPara.p_Total_4_5_5a_6_Private_Act+"','"+ES13RptPara.p_Total_4_5_5a_6_Private_NonAct+"','"+ES13RptPara.p_Total_Total_4_5_5a_6+"','"+ES13RptPara.p_Notified_NotSubmitted_CG+"','"+ES13RptPara.p_Notified_NotSubmitted_CG+"','"+ES13RptPara.p_Notified_NotSubmitted_SG+"','"+ES13RptPara.p_Notified_NotSubmitted_Quasi_CG+"','"+ES13RptPara.p_Notified_NotSubmitted_Quasi_SG+"','"+ES13RptPara.p_Notified_NotSubmitted_LB+"','"+ES13RptPara.p_Notified_NotSubmitted_Private_Act+"','"+ES13RptPara.p_Notified_NotSubmitted_Private_NonAct+"','"+ES13RptPara.p_Total_Notified_NotSubmitted+"','"+ES13RptPara.p_Filled_ByOtherExch_CG+"','"+ES13RptPara.p_Filled_ByOtherExch_CG+"','"+ES13RptPara.p_Filled_ByOtherExch_SG+"','"+ES13RptPara.p_Filled_ByOtherExch_Quasi_CG+"','"+ES13RptPara.p_Filled_ByOtherExch_Quasi_SG+"','"+ES13RptPara.p_Filled_ByOtherExch_LB+"','"+ES13RptPara.p_Filled_ByOtherExch_Private_Act+"','"+ES13RptPara.p_Filled_ByOtherExch_Private_NonAct+"','"+ES13RptPara.p_Total_Filled_ByOtherExch+"','"+ES13RptPara.p_Filled_ForOtherExch_CG+"','"+ES13RptPara.p_Filled_ForOtherExch_CG+"','"+ES13RptPara.p_Filled_ForOtherExch_SG+"','"+ES13RptPara.p_Filled_ForOtherExch_Quasi_CG+"','"+ES13RptPara.p_Filled_ForOtherExch_Quasi_SG+"','"+ES13RptPara.p_Filled_ForOtherExch_LB+"','"+ES13RptPara.p_Filled_ForOtherExch_Private_Act+"','"+ES13RptPara.p_Filled_ForOtherExch_Private_NonAct+"','"+ES13RptPara.p_Total_Filled_ForOtherExch+"','"+ES13RptPara.p_EmpUseExch_CG+"','"+ES13RptPara.p_EmpUseExch_CG+"','"+ES13RptPara.p_EmpUseExch_SG+"','"+ES13RptPara.p_EmpUseExch_Quasi_CG+"','"+ES13RptPara.p_EmpUseExch_Quasi_SG+"','"+ES13RptPara.p_EmpUseExch_LB+"','"+ES13RptPara.p_EmpUseExch_Private_Act+"','"+ES13RptPara.p_EmpUseExch_Private_NonAct+"','"+ES13RptPara.p_Total_EmpUseExch+"','"+ES13RptPara.p_Flag+"')", function (err, res) {      
             if(err) {
                sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });           
};
module.exports= ES13Rpt ;
