'use strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var ER1Form = function (er1form) {
    this.p_Name = er1form.p_Name;
    this.p_Address = er1form.p_Address;
    this.p_OfficeType = er1form.p_OfficeType;
    this.p_PreQuaterMen = er1form.p_PreQuaterMen;
    this.p_PreQuaterWomen = er1form.p_PreQuaterWomen;
    this.p_PreQuaterTotal = er1form.p_PreQuaterTotal;
    this.p_UndQuaterMen = er1form.p_UndQuaterMen;
    this.p_UndQuaterWomen = er1form.p_UndQuaterWomen;
    this.p_UndQuaterTotal = er1form.p_UndQuaterTotal;
    this.p_IncDecQuater = er1form.p_IncDecQuater;
    this.p_Reason = er1form.p_Reason;
    this.p_Date = er1form.p_Date;
    this.p_Signature = er1form.p_Signature;
    this.p_Place = er1form.p_Place;
    this.p_EnteredOn = er1form.p_EnteredOn;
    this.p_EnteredIP = er1form.p_EnteredIP;
    this.p_EmpId = er1form.p_EmpId;
    this.p_Occurred = er1form.p_Occurred;
    this.p_Local = er1form.p_Local;
    this.p_Central = er1form.p_Central;
    this.p_Filled = er1form.p_Filled;
    this.p_Source = er1form.p_Source;
    this.p_Designation = er1form.p_Designation;
    this.p_Qualification = er1form.p_Qualification;
    this.p_Experience = er1form.p_Experience;
    this.p_ExperienceNot = er1form.p_ExperienceNot;
    this.p_Position = er1form.p_Position;
    this.p_JobDescription = er1form.p_JobDescription;
    this.p_Salary = er1form.p_Salary;
    this.p_NoOfReq = er1form.p_NoOfReq;
    this.p_JobLocation = er1form.p_JobLocation;
    this.p_BuisnessNature = er1form.p_BuisnessNature;
    this.p_HR_name = er1form.p_HR_name;
    this.p_Contactnumber = er1form.p_Contactnumber;
    this.p_EmailId = er1form.p_EmailId;
    this.p_Month = er1form.p_Month;
    this.p_Year = er1form.p_Year;
    this.p_EmployerId = er1form.p_EmployerId;
    this.p_EntryFrom = er1form.p_EntryFrom;
    this.p_EntryBy = er1form.p_EntryBy;
    this.p_p_Ex_id = er1form.p_p_Ex_id
};
ER1Form.createer1form = function createUser(Ins_er1_form, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
        if (err) throw err;
    });
    
        sql.query("CALL Ins_er1_form('" + Ins_er1_form.p_Name + "','" + Ins_er1_form.p_Address + "','" + Ins_er1_form.p_OfficeType + "','" + Ins_er1_form.p_PreQuaterMen + "','" + Ins_er1_form.p_PreQuaterWomen + "','" + Ins_er1_form.p_PreQuaterTotal + "','" + Ins_er1_form.p_UndQuaterMen + "','" + Ins_er1_form.p_UndQuaterWomen + "','" + Ins_er1_form.p_UndQuaterTotal + "','"+Ins_er1_form.p_IncDecQuater+"','" + Ins_er1_form.p_Reason + "','" + Ins_er1_form.p_Date + "','" + Ins_er1_form.p_Signature + "','" + Ins_er1_form.p_Place + "','" + Ins_er1_form.p_EnteredOn + "','"+Ins_er1_form.p_EnteredIP+"','"+Ins_er1_form.p_BuisnessNature+"','"+Ins_er1_form.p_HR_name+"','"+Ins_er1_form.p_Contactnumber+"','"+Ins_er1_form.p_EmailId+"','"+Ins_er1_form.p_Month+"','"+Ins_er1_form.p_Year+"','"+Ins_er1_form.p_EmployerId+"','"+Ins_er1_form.p_EntryFrom+"','"+Ins_er1_form.p_EntryBy+"')", function (err, res) {
            if (err) {
                //end.sql()
            sql.end()
                result(err, null);
            } else {
                //end.sql()
            sql.end()
                result(null, res);
            }
        });
    };
ER1Form.createer1formvacancy = function createUser(Ins_er1_vacancyform, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
        if (err) throw err;
    });
    
        sql.query("CALL Ins_er1_vacancy_filled('" + Ins_er1_vacancyform.p_EmpId + "','" + Ins_er1_vacancyform.p_Occurred + "','" + Ins_er1_vacancyform.p_Local + "','" + Ins_er1_vacancyform.p_Central + "','" + Ins_er1_vacancyform.p_Filled + "','" + Ins_er1_vacancyform.p_Source + "')", function (err, res) {
            if (err) {
                //end.sql()
            sql.end()
                result(err, null);
            } else {
                //end.sql()
            sql.end()
                result(null, res);
            }
        });
    };
    ER1Form.createer1formvacancyunfilled = function createUser(Ins_er1_vacancyformunfilled, result) {
        var sql = mysql.createConnection(credentials);
        sql.connect(function (err) {
            if (err) throw err;
        });
        
            sql.query("CALL Ins_er1_vacancy_unfilled('" + Ins_er1_vacancyformunfilled.p_EmpId + "','" + Ins_er1_vacancyformunfilled.p_Designation + "','" + Ins_er1_vacancyformunfilled.p_Qualification + "','" + Ins_er1_vacancyformunfilled.p_Experience + "','" + Ins_er1_vacancyformunfilled.p_ExperienceNot + "')", function (err, res) {
                if (err) {
                    //end.sql()
                sql.end()
                    result(err, null);
                } else {
                    //end.sql()
                sql.end()
                    result(null, res);
                }
            });
        };
        ER1Form.createer1formvacancyunyashaswi = function createUser(Ins_er1_vacancyformyashaswi, result) {
            var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
                if (err) throw err;
            });
            
                sql.query("CALL Ins_er1_vacancy_yashaswi('" + Ins_er1_vacancyformyashaswi.p_EmpId + "','" + Ins_er1_vacancyformyashaswi.p_Position + "','" + Ins_er1_vacancyformyashaswi.p_JobDescription + "','" + Ins_er1_vacancyformyashaswi.p_Salary + "','" + Ins_er1_vacancyformyashaswi.p_NoOfReq + "','"+Ins_er1_vacancyformyashaswi.p_JobLocation+"')", function (err, res) {
                    if (err) {
                        //end.sql()
                    sql.end()
                        result(err, null);
                    } else {
                        //end.sql()
                    sql.end()
                        result(null, res);
                    }
                });
            };



            ER1Form.getER1Form = function createUser(fetch_ER1Form, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL fetch_er1_form_rpt('"+fetch_ER1Form.p_EmpId+"','"+fetch_ER1Form.p_from_date+"','"+fetch_ER1Form.p_to_date+"','"+fetch_ER1Form.p_EntryFrom+"','"+fetch_ER1Form.p_EntryBy+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };


            ER1Form.geter1_vacancy_filled = function createUser(er1_vacancy_filled, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL fetch_er1_vacancy_filled('"+er1_vacancy_filled.p_EmpId+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };



            ER1Form.geter1_vacancy_unfilled = function createUser(er1_vacancy_unfilled, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL fetch_er1_vacancy_unfilled('"+er1_vacancy_unfilled.p_EmpId+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };



            ER1Form.geter1_vacancy_yashaswi = function createUser(er1_vacancy_yashaswi, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL fetch_er1_vacancy_yashaswi('"+er1_vacancy_yashaswi.p_EmpId+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };

            ER1Form.getER1FormByEmp = function createUser(fetch_ER1Form, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL fetch_er1_rptbyEmp('"+fetch_ER1Form.p_Year+"','"+fetch_ER1Form.p_Month+"','"+fetch_ER1Form.p_EmployerId+"','"+fetch_ER1Form.p_EntryFrom+"','"+fetch_ER1Form.p_EntryBy+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };




            ER1Form.getFetch_ER1_Rpt = function createUser(ER1RPT, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL Fetch_ER1_Rpt('"+ER1RPT.p_from_date+"','"+ER1RPT.p_to_date+"','"+ER1RPT.p_EntryFrom+"','"+ER1RPT.p_EntryBy+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };

            ER1Form.getFetch_ER1_RptCount = function createUser(ER1RPT, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL Fetch_Rpt_EmpCount('"+ER1RPT.p_Ex_id+"','"+ER1RPT.p_EntryBy+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };


            ER1Form.getFetch_Rpt_EmpDetail = function createUser(ER1RPT, result) {
                var sql = mysql.createConnection(credentials);
            sql.connect(function (err) {
            if (err) throw err;
            });
                sql.query("CALL Fetch_Rpt_EmpDetail('"+ER1RPT.p_Ex_id+"','"+ER1RPT.p_EntryBy+"')", function (err, res) {             
                    if(err) {
                        sql.end();
                        result(err, null);
                    }
                    else{
                        sql.end();
                        result(null, res);
                    }
                });   
            };

module.exports = ER1Form;