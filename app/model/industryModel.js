'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Industry = function (industry) {
    this.p_IndustryId = industry.p_IndustryId;
    this.p_IndustryName = industry.p_IndustryName;
    this.p_IsActive = industry.p_IsActive;
    this.p_IpAddress = industry.p_IpAddress;
    this.p_UserId = industry.p_UserId;
};
Industry.createIndustry = function createUser(newIndustry, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_industry_master(" + newIndustry.p_IndustryId + ",'" + newIndustry.p_IndustryName + "'," + newIndustry.p_IsActive + ",'" + newIndustry.p_IpAddress + "'," + newIndustry.p_UserId + ")", function (err, res) {
        if (err) {
            sql.end();  
            result(err, null);
        }
        else {

            sql.end();  
            result(null, res);
        }
    });
};
Industry.getIndustry = function createUser(fetch_industry, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_industrymaster(" + fetch_industry.p_IndustryId + "," + fetch_industry.p_IpAddress + "," + fetch_industry.p_IsActive + "," + fetch_industry.p_UserId + ")", function (err, res) {
        if (err) {
            sql.end();  //console.log("error: ", err);
            result(err, null);
        }
        else {
            sql.end();
            result(null, res);
        }
    });
};


module.exports = Industry;