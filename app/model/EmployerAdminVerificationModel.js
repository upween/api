'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var EmployerAdminVerification = function(EmployerAdminVerification){
    this.p_RegistraionNumber = EmployerAdminVerification.p_RegistraionNumber;
    this.p_RegistrationMonth = EmployerAdminVerification.p_RegistrationMonth;
    this.p_RegistrationYear = EmployerAdminVerification.p_RegistrationYear;
    this.p_Emp_Name = EmployerAdminVerification.p_Emp_Name;
    this.p_Company_Type = EmployerAdminVerification.p_Company_Type;
    this.p_Emp_RegId = EmployerAdminVerification.p_Emp_RegId;

    this.P_Emp_Regno = EmployerAdminVerification.P_Emp_Regno;
    this.p_V_Status = EmployerAdminVerification.p_V_Status;
    this.p_Type = EmployerAdminVerification.p_Type;

};

EmployerAdminVerification.getEmployerAdminVerification= function createUser(Fetch_Employer_AdminVerification, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_Employer_AdminVerification("+Fetch_Employer_AdminVerification.p_RegistraionNumber+","+Fetch_Employer_AdminVerification.p_RegistrationMonth+","+Fetch_Employer_AdminVerification.p_RegistrationYear+","+Fetch_Employer_AdminVerification.p_Emp_Name+","+Fetch_Employer_AdminVerification.p_Company_Type+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

EmployerAdminVerification.getDeleteEmpDetail= function createUser(Delete_Emp_Detail, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Delete_Emp_Detail("+Delete_Emp_Detail.p_Emp_RegId+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

EmployerAdminVerification.getInActiveEmp= function createUser(Fetch_InActiveEmp, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Fetch_InActiveEmp("+Fetch_InActiveEmp.P_Emp_Regno+","+Fetch_InActiveEmp.p_V_Status+","+Fetch_InActiveEmp.p_Type+")", function (err, res) {             
        if(err) {
            sql.end(); 
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};


module.exports= EmployerAdminVerification;