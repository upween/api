'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var subjectnew = function(subjectnew){
    this.p_Sub_id = subjectnew.p_Sub_id;
    this.p_Sub_Group_id = subjectnew.p_Sub_Group_id;
    this.p_Qualif_id = subjectnew.p_Qualif_id;
    this.p_Active_YN = subjectnew.p_Active_YN;
    
};

subjectnew.getsubjectnew = function createUser(fetch_subject, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_subject("+fetch_subject.p_Sub_id+","+fetch_subject.p_Sub_Group_id+","+fetch_subject.p_Qualif_id+","+fetch_subject.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end() 
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= subjectnew;