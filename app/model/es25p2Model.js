
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES25P2Entry= function(es25P2entry){
    this.p_FromDt    = es25P2entry.p_FromDt   ;
    this.p_ToDt     = es25P2entry.p_ToDt    ;
    this.p_Ex_Id  = es25P2entry.p_Ex_Id ;
   
    this.p_Outstanding_Prev_CG = es25P2entry.p_Outstanding_Prev_CG ;    
    this.p_Outstanding_Prev_UT = es25P2entry.p_Outstanding_Prev_UT ;    
    this.p_Outstanding_Prev_SG = es25P2entry.p_Outstanding_Prev_SG ;    
    this.p_Outstanding_Prev_Quasi_CG = es25P2entry.p_Outstanding_Prev_Quasi_CG ;    
    this.p_Outstanding_Prev_Quasi_SG = es25P2entry.p_Outstanding_Prev_Quasi_SG ;    
    this.p_Outstanding_Prev_LB = es25P2entry.p_Outstanding_Prev_LB ;    
    this.p_Outstanding_Prev_Private = es25P2entry.p_Outstanding_Prev_Private ;    
    this.p_Outstanding_Prev_Total = es25P2entry.p_Outstanding_Prev_Total ;    
    this.p_Notified_CG = es25P2entry.p_Notified_CG ;    
    this.p_Notified_UT = es25P2entry.p_Notified_UT ;    
    this.p_Notified_SG = es25P2entry.p_Notified_SG ;    
    this.p_Notified_Quasi_CG = es25P2entry.p_Notified_Quasi_CG ;    
    this.p_Notified_Quasi_SG = es25P2entry.p_Notified_Quasi_SG ;    
    this.p_Notified_LB = es25P2entry.p_Notified_LB ;    
    this.p_Notified_Private = es25P2entry.p_Notified_Private ;    
    this.p_Notified_Total = es25P2entry.p_Notified_Total ;    
    this.p_Filled_CG = es25P2entry.p_Filled_CG ;    
    this.p_Filled_UT = es25P2entry.p_Filled_UT ;    
    this.p_Filled_SG = es25P2entry.p_Filled_SG ;    
    this.p_Filled_Quasi_CG = es25P2entry.p_Filled_Quasi_CG ;    
    this.p_Filled_Quasi_SG = es25P2entry.p_Filled_Quasi_SG ;    
    this.p_Filled_LB = es25P2entry.p_Filled_LB ;    
    this.p_Filled_Private = es25P2entry.p_Filled_Private ;    
    this.p_Filled_Total = es25P2entry.p_Filled_Total ;    
    this.p_Cancelled_NA_CG = es25P2entry.p_Cancelled_NA_CG ;    
    this.p_Cancelled_NA_UT = es25P2entry.p_Cancelled_NA_UT ;    
    this.p_Cancelled_NA_SG = es25P2entry.p_Cancelled_NA_SG ;    
    this.p_Cancelled_NA_Quasi_CG = es25P2entry.p_Cancelled_NA_Quasi_CG ;    
    this.p_Cancelled_NA_Quasi_SG = es25P2entry.p_Cancelled_NA_Quasi_SG ;    
    this.p_Cancelled_NA_LB = es25P2entry.p_Cancelled_NA_LB ;    
    this.p_Cancelled_NA_Private = es25P2entry.p_Cancelled_NA_Private ;    
    this.p_Cancelled_NA_Total = es25P2entry.p_Cancelled_NA_Total ;    
    this.p_Cancelled_Other_CG = es25P2entry.p_Cancelled_Other_CG ;    
    this.p_Cancelled_Other_UT = es25P2entry.p_Cancelled_Other_UT ;    
    this.p_Cancelled_Other_SG = es25P2entry.p_Cancelled_Other_SG ;    
    this.p_Cancelled_Other_Quasi_CG = es25P2entry.p_Cancelled_Other_Quasi_CG ;   
    this.p_Cancelled_Other_Quasi_SG = es25P2entry.p_Cancelled_Other_Quasi_SG ;    
    this.p_Cancelled_Other_LB = es25P2entry.p_Cancelled_Other_LB ;    
    this.p_Cancelled_Other_Private = es25P2entry.p_Cancelled_Other_Private ;    
    this.p_Cancelled_Other_Total = es25P2entry.p_Cancelled_Other_Total ;    
    this.p_Outstanding_CG = es25P2entry.p_Outstanding_CG ;    
    this.p_Outstanding_UT = es25P2entry.p_Outstanding_UT ;    
    this.p_Outstanding_SG = es25P2entry.p_Outstanding_SG ;    
    this.p_Outstanding_Quasi_CG = es25P2entry.p_Outstanding_Quasi_CG ;    
    this.p_Outstanding_Quasi_SG = es25P2entry.p_Outstanding_Quasi_SG ;    
    this.p_Outstanding_LB = es25P2entry.p_Outstanding_LB ;    
    this.p_Outstanding_Private = es25P2entry.p_Outstanding_Private ;    
    this.p_Outstanding_Total = es25P2entry.p_Outstanding_Total ;      
    this.p_Flag = es25P2entry.p_Flag ;  

};


ES25P2Entry.getES25P2Rpt = function createUser(Es25P2, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_ES25Part2('"+Es25P2.p_FromDt+"','"+Es25P2.p_ToDt+"',"+Es25P2.p_Ex_Id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};



ES25P2Entry.createES25Part2 = function createUser(newES25Part2, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES25_Part_II_Entry('"+newES25Part2.p_Ex_Id+"','"+newES25Part2.p_FromDt+"','"+newES25Part2.p_ToDt+"','"+newES25Part2.p_Outstanding_Prev_CG+"','"+newES25Part2.p_Outstanding_Prev_UT+"','"+newES25Part2.p_Outstanding_Prev_SG+"','"+newES25Part2.p_Outstanding_Prev_Quasi_CG+"','"+newES25Part2.p_Outstanding_Prev_Quasi_SG+"','"+newES25Part2.p_Outstanding_Prev_LB+"','"+newES25Part2.p_Outstanding_Prev_Private+"','"+newES25Part2.p_Outstanding_Prev_Total+"','"+newES25Part2.p_Notified_CG+"','"+newES25Part2.p_Notified_UT+"','"+newES25Part2.p_Notified_SG+"','"+newES25Part2.p_Notified_Quasi_CG+"','"+newES25Part2.p_Notified_Quasi_SG+"','"+newES25Part2.p_Notified_LB+"','"+newES25Part2.p_Notified_Private+"','"+newES25Part2.p_Notified_Total+"','"+newES25Part2.p_Filled_CG+"','"+newES25Part2.p_Filled_UT+"','"+newES25Part2.p_Filled_SG+"','"+newES25Part2.p_Filled_Quasi_CG+"','"+newES25Part2.p_Filled_Quasi_SG+"','"+newES25Part2.p_Filled_LB+"','"+newES25Part2.p_Filled_Private+"','"+newES25Part2.p_Filled_Total+"','"+newES25Part2.p_Cancelled_NA_CG+"','"+newES25Part2.p_Cancelled_NA_UT+"','"+newES25Part2.p_Cancelled_NA_SG+"','"+newES25Part2.p_Cancelled_NA_Quasi_CG+"','"+newES25Part2.p_Cancelled_NA_Quasi_SG+"','"+newES25Part2.p_Cancelled_NA_LB+"','"+newES25Part2.p_Cancelled_NA_Private+"','"+newES25Part2.p_Cancelled_NA_Total+"','"+newES25Part2.p_Cancelled_Other_CG+"','"+newES25Part2.p_Cancelled_Other_UT+"','"+newES25Part2.p_Cancelled_Other_SG+"','"+newES25Part2.p_Cancelled_Other_Quasi_CG+"','"+newES25Part2.p_Cancelled_Other_Quasi_SG+"','"+newES25Part2.p_Cancelled_Other_LB+"','"+newES25Part2.p_Cancelled_Other_Private+"','"+newES25Part2.p_Cancelled_Other_Total+"','"+newES25Part2.p_Outstanding_CG+"','"+newES25Part2.p_Outstanding_UT+"','"+newES25Part2.p_Outstanding_SG+"','"+newES25Part2.p_Outstanding_Quasi_CG+"','"+newES25Part2.p_Outstanding_Quasi_SG+"','"+newES25Part2.p_Outstanding_LB+"','"+newES25Part2.p_Outstanding_Private+"','"+newES25Part2.p_Outstanding_Total+"','"+newES25Part2.p_Flag+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};

module.exports= ES25P2Entry;