'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var CategoryWiseSetPostCountN = function(CategoryWiseSetPostCountN){
    this.p_PostId = CategoryWiseSetPostCountN.p_PostId;
    this.p_Jobfair_Id = CategoryWiseSetPostCountN.p_Jobfair_Id;
    this.p_Ex_Id = CategoryWiseSetPostCountN.p_Ex_Id;
    this.p_PostName = CategoryWiseSetPostCountN.p_PostName;
    this.p_NoOfVacancy = CategoryWiseSetPostCountN.p_NoOfVacancy;
    this.p_E_Userid = CategoryWiseSetPostCountN.p_E_Userid;
    this.p_PayAllowance = CategoryWiseSetPostCountN.p_PayAllowance;
    this.p_PlaceOfwork = CategoryWiseSetPostCountN.p_PlaceOfwork;
    this.p_Gender = CategoryWiseSetPostCountN.p_Gender;
    this.p_MinAge = CategoryWiseSetPostCountN.p_MinAge;
    this.p_MaxAge = CategoryWiseSetPostCountN.p_MaxAge;
    this.p_Date = CategoryWiseSetPostCountN.p_Date;
    this.p_EQualification = CategoryWiseSetPostCountN.p_EQualification;
    this.p_Dqualification = CategoryWiseSetPostCountN.p_Dqualification;
    this.p_Remark = CategoryWiseSetPostCountN.p_Remark;
    this.p_Year = CategoryWiseSetPostCountN.p_Year;
    this.p_Month = CategoryWiseSetPostCountN.p_Month;
    this.p_flag = CategoryWiseSetPostCountN.p_flag;
    
};
CategoryWiseSetPostCountN.createCategoryWiseSetPostCountN = function createUser(CI_JF_CategoryWiseSetPostCountN, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_JF_CategoryWiseSetPostCountN('"+CI_JF_CategoryWiseSetPostCountN.p_PostId+"','"+CI_JF_CategoryWiseSetPostCountN.p_Jobfair_Id+"','"+CI_JF_CategoryWiseSetPostCountN.p_Ex_Id+"','"+CI_JF_CategoryWiseSetPostCountN.p_PostName+"','"+CI_JF_CategoryWiseSetPostCountN.p_NoOfVacancy+"','"+CI_JF_CategoryWiseSetPostCountN.p_E_Userid+"','"+CI_JF_CategoryWiseSetPostCountN.p_PayAllowance+"','"+CI_JF_CategoryWiseSetPostCountN.p_PlaceOfwork+"','"+CI_JF_CategoryWiseSetPostCountN.p_Gender+"','"+CI_JF_CategoryWiseSetPostCountN.p_MinAge+"','"+CI_JF_CategoryWiseSetPostCountN.p_MaxAge+"','"+CI_JF_CategoryWiseSetPostCountN.p_Date+"','"+CI_JF_CategoryWiseSetPostCountN.p_EQualification+"','"+CI_JF_CategoryWiseSetPostCountN.p_Dqualification+"','"+CI_JF_CategoryWiseSetPostCountN.p_Remark+"','"+CI_JF_CategoryWiseSetPostCountN.p_Year+"','"+CI_JF_CategoryWiseSetPostCountN.p_Month+"','"+CI_JF_CategoryWiseSetPostCountN.p_flag+"')", function (err, res) {      
        if(err) {
            sql.end();
           
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};
module.exports= CategoryWiseSetPostCountN;
