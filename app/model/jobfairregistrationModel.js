'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var jobfairregistration = function jobfairregistration(_jobfairregistration) {
    this.p_job_fair_id = _jobfairregistration.p_job_fair_id;
    this.p_registration_no = _jobfairregistration.p_registration_no;
    this.p_table_id = _jobfairregistration.p_table_id;
    this.p_JobfairEmpRegId = _jobfairregistration.p_JobfairEmpRegId;
    this.p_JobfairId = _jobfairregistration.p_JobfairId;
    this.p_Employer = _jobfairregistration.p_Employer;
    this.p_Designation = _jobfairregistration.p_Designation;
    this.p_Qualification = _jobfairregistration.p_Qualification;
    this.p_Location = _jobfairregistration.p_Location;
    this.p_Trade = _jobfairregistration.p_Trade;
    this.p_MinAge = _jobfairregistration.p_MinAge;
    this.p_MaxAge = _jobfairregistration.p_MaxAge;
    this.p_TotalVacancy = _jobfairregistration.p_TotalVacancy;
    this.p_Min_Salary = _jobfairregistration.p_Min_Salary;
    this.p_Max_Salary = _jobfairregistration.p_Max_Salary;
    this.p_AttachDoc = _jobfairregistration.p_AttachDoc;
    this.p_InsertedBy = _jobfairregistration.p_InsertedBy;
    this.p_Company_HR_Name = _jobfairregistration.p_Company_HR_Name;
    this.p_gender = _jobfairregistration.p_gender;
    this.p_e_mail = _jobfairregistration.p_e_mail;
    this.p_mobile_no = _jobfairregistration.p_mobile_no;
    this.p_Discription = _jobfairregistration.p_Discription;
    this.p_Percentage = _jobfairregistration.p_Percentage;
    this.p_jf_id = _jobfairregistration.p_jf_id;
};
jobfairregistration.createjobfairregistration = function createUser(insupd_job_fair_registration, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL insupd_job_fair_registration('" + insupd_job_fair_registration.p_job_fair_id + "','" + insupd_job_fair_registration.p_registration_no + "','" + insupd_job_fair_registration.p_table_id + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

jobfairregistration.createjobfairempreg = function createUser(InsUpd_jobfair_emp_reg, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL InsUpd_jobfair_emp_reg('" + InsUpd_jobfair_emp_reg.p_JobfairEmpRegId + "','" + InsUpd_jobfair_emp_reg.p_JobfairId + "','" + InsUpd_jobfair_emp_reg.p_Employer + "','" + InsUpd_jobfair_emp_reg.p_Company_HR_Name + "','" + InsUpd_jobfair_emp_reg.p_Designation + "','" + InsUpd_jobfair_emp_reg.p_Qualification + "','" + InsUpd_jobfair_emp_reg.p_Location + "','" + InsUpd_jobfair_emp_reg.p_Trade + "','" + InsUpd_jobfair_emp_reg.p_MinAge + "','" + InsUpd_jobfair_emp_reg.p_MaxAge + "','" + InsUpd_jobfair_emp_reg.p_TotalVacancy + "','" + InsUpd_jobfair_emp_reg.p_Min_Salary + "','" + InsUpd_jobfair_emp_reg.p_Max_Salary + "','" + InsUpd_jobfair_emp_reg.p_AttachDoc + "','" + InsUpd_jobfair_emp_reg.p_InsertedBy + "','" + InsUpd_jobfair_emp_reg.p_gender + "','" + InsUpd_jobfair_emp_reg.p_e_mail + "','" + InsUpd_jobfair_emp_reg.p_mobile_no + "','" + InsUpd_jobfair_emp_reg.p_Discription + "','" + InsUpd_jobfair_emp_reg.p_Percentage + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};
jobfairregistration.getITIEmployer = function createUser(fetch_ITIEmployer, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL Fetch_jobfair_emp_reg()", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

jobfairregistration.getjobfairEmpRegbyId = function createUser(JobfairEmpReg, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL Fetch_jobfairEmpReg_Id('" + JobfairEmpReg.p_JobfairEmpRegId + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

jobfairregistration.getJobfairEmployer = function createUser(JobfairEmployer, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL Fetch_jobfair_emp(" + JobfairEmployer.p_jf_id + ")", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

module.exports = jobfairregistration;