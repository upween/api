'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var CounsellingSummary = function(counsellingsummary){
    this.p_CandidateId  = counsellingsummary.p_CandidateId;
    this.p_SummName  = counsellingsummary.p_SummName ;
    this.p_SummAddress  = counsellingsummary.p_SummAddress ;
    this.p_Summ_1  = counsellingsummary.p_Summ_1 ;
    this.p_Summ_2_1  = counsellingsummary.p_Summ_2_1 ;
    this.p_Summ_2_2   = counsellingsummary.p_Summ_2_2 ;
    this.p_Summ_2_3   = counsellingsummary.p_Summ_2_3  ;
    this.p_Summ_2_4    = counsellingsummary.p_Summ_2_4  ;
    this.p_SummDate    = counsellingsummary.p_SummDate  ;
    this.p_SummConsName    = counsellingsummary.p_SummConsName  ;
    this.p_SummConsComment     = counsellingsummary.p_SummConsComment   ;
    this.p_SummSpecComment     = counsellingsummary.p_SummSpecComment   ;
    this.p_SpecDate     = counsellingsummary.p_SpecDate   ;
    this.p_SpecName    = counsellingsummary.p_SpecName  ;
    this.p_CaseNo     = counsellingsummary.p_CaseNo   ;
    this.p_CaseDate     = counsellingsummary.p_CaseDate   ;
    this.p_Name     = counsellingsummary.p_Name   ;
    this.p_Age      = counsellingsummary.p_Age    ;
    this.p_EducationalQualification      = counsellingsummary.p_EducationalQualification    ;
    this.p_SpecRemark      = counsellingsummary.p_SpecRemark    ;
    this.p_SpecialRemark      = counsellingsummary.p_SpecialRemark    ;
  
};


CounsellingSummary.createCounsellingsummary= function createUser(counselling, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Ins_counselling_summary('"+counselling.p_CandidateId+"','"+counselling.p_SummName+"','"+counselling.p_SummAddress+"','"+counselling.p_Summ_1+"','"+counselling.p_Summ_2_1+"','"+counselling.p_Summ_2_2+"','"+counselling.p_Summ_2_3 +"','"+counselling.p_Summ_2_4 +"','"+counselling.p_SummDate +"','"+counselling.p_SummConsName +"','"+counselling.p_SummConsComment +"','"+counselling.p_SummSpecComment +"','"+counselling.p_SpecDate +"','"+counselling.p_SpecName +"','"+counselling.p_CaseNo+"','"+counselling.p_CaseDate +"','"+counselling.p_Name +"','"+counselling.p_Age +"','"+counselling.p_EducationalQualification +"','"+counselling.p_ConsRemark  +"','"+counselling.p_SpecRemark  +"','"+counselling.p_SpecialRemark  +"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};


module.exports= CounsellingSummary;