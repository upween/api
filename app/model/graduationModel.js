'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var Graduation = function(graduation){
    this.p_GraduationId = graduation.p_GraduationId;
    this.p_CandidateId = graduation.p_CandidateId;
    this.p_EducationId = graduation.p_EducationId;
    this.p_CourseId = graduation.p_CourseId;
    this.p_SpecializationId = graduation.p_SpecializationId;
    this.p_UniversityId = graduation.p_UniversityId;
    this.p_CourseTypeId = graduation.p_CourseTypeId;
    this.p_PassingOutYearId = graduation.p_PassingOutYearId;
    this.p_GradingSystemId = graduation.p_GradingSystemId;
    this.p_Percentage = graduation.p_Percentage;
    this.p_Division = graduation.p_Division;
    this.p_SubjectId = graduation.p_SubjectId;
    this.p_IpAddress = graduation.p_IpAddress;
};
Graduation.createGraduation = function createUser(newGraduation, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_Graduationmaster('"+newGraduation.p_GraduationId+"','"+newGraduation.p_CandidateId+"','"+newGraduation.p_EducationId+"','"+newGraduation.p_CourseId+"','"+newGraduation.p_SpecializationId+"','"+newGraduation.p_UniversityId+"','"+newGraduation.p_CourseTypeId+"','"+newGraduation.p_PassingOutYearId+"','"+newGraduation.p_GradingSystemId+"','"+newGraduation.p_Percentage+"','"+newGraduation.p_Division+"','"+newGraduation.p_SubjectId+"','"+newGraduation.p_IpAddress+"')", function (err, res) {      
        if(err) {
            sql.end();  
            result(err, null);
        }
        else{
            sql.end();  
            result(null, res);
        }
    });           
};
Graduation.getGraduation = function createUser(fetch_Graduation, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_graduation_master("+fetch_Graduation.p_CandidateId+","+fetch_Graduation.p_GraduationId+","+fetch_Graduation.p_IpAddress+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

module.exports= Graduation;