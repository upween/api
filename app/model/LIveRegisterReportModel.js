'use strict';

'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var LIveRegisterReport = function LIveRegisterReport(_LIveRegisterReport) {
    this.p_RegMonth = _LIveRegisterReport.p_RegMonth;
    this.p_RegYear = _LIveRegisterReport.p_RegYear;
    this.p_Gender = _LIveRegisterReport.p_Gender;
    this.p_Category = _LIveRegisterReport.p_Category;
    this.p_Ex_id = _LIveRegisterReport.p_Ex_id;
};

LIveRegisterReport.getLIveRegisterReport = function createUser(LIveRegisterReport, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL Jobseeker_LIveRegister_Report(" + LIveRegisterReport.p_RegMonth + "," + LIveRegisterReport.p_RegYear + "," + LIveRegisterReport.p_Gender + "," + LIveRegisterReport.p_Category + "," + LIveRegisterReport.p_Ex_id + ")", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};
module.exports = LIveRegisterReport;