'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var subjectgroup = function(subjectgroup){
    this.p_Sub_Group_id = subjectgroup.p_Sub_Group_id;
    this.p_Qualif_id = subjectgroup.p_Qualif_id;
    this.p_Active_YN = subjectgroup.p_Active_YN;
    
};

subjectgroup.getsubjectgroup = function createUser(fetch_subject_group, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_subject_group("+fetch_subject_group.p_Sub_Group_id+","+fetch_subject_group.p_Qualif_id+","+fetch_subject_group.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end() 
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= subjectgroup;