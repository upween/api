'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES22Rpt = function(ES22RptForm){
    this.p_Ex_id= ES22RptForm.p_Ex_id;
    this.p_ToDt= ES22RptForm.p_ToDt; 
    this.p_FromDt=ES22RptForm.p_FromDt;
    this.p_NCO_Code=ES22RptForm.p_NCO_Code;
    this.p_TraineeReg=ES22RptForm.p_TraineeReg;
    this.p_TraineePlacement=ES22RptForm.p_TraineePlacement;
    this.p_TraineeLR=ES22RptForm.p_TraineeLR;
    this.p_ApprenticeReg=ES22RptForm.p_ApprenticeReg;
    this.p_ApprenticePlace=ES22RptForm.p_ApprenticePlace;
    this.p_ApprenticeLR=ES22RptForm.p_ApprenticeLR;
    this.p_Verify_YN=ES22RptForm.p_Verify_YN;
    this.p_flag=ES22RptForm.p_flag;            
   
};




ES22Rpt.getES22Annual = function createUser(ES22Annual , result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES22Entry ("+ES22Annual.p_Ex_id +",'"+ES22Annual.p_ToDt  +"','"+ES22Annual.p_flag+"')", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

ES22Rpt.createES22Rpt = function createUser(newES22Rpt, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES22Insert('"+newES22Rpt.p_Ex_id+"','"+newES22Rpt.p_FromDt+"','"+newES22Rpt.p_ToDt+"','"+newES22Rpt.p_NCO_Code+"','"+newES22Rpt.p_TraineeReg+"','"+newES22Rpt.p_TraineePlacement+"','"+newES22Rpt.p_TraineeLR+"','"+newES22Rpt.p_ApprenticeReg+"','"+newES22Rpt.p_ApprenticePlace+"','"+newES22Rpt.p_ApprenticeLR+"','"+newES22Rpt.p_Verify_YN+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};
ES22Rpt.updateES22Rpt = function createUser(newES22Rpt, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES22update('"+newES22Rpt.p_Ex_id+"','"+newES22Rpt.p_FromDt+"','"+newES22Rpt.p_ToDt+"','"+newES22Rpt.p_NCO_Code+"','"+newES22Rpt.p_TraineeReg+"','"+newES22Rpt.p_TraineePlacement+"','"+newES22Rpt.p_TraineeLR+"','"+newES22Rpt.p_ApprenticeReg+"','"+newES22Rpt.p_ApprenticePlace+"','"+newES22Rpt.p_ApprenticeLR+"','"+newES22Rpt.p_Verify_YN+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};
module.exports= ES22Rpt ;
