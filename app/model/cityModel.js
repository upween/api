'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var City = function(city){
    this.p_CityId = city.p_CityId;
    this.p_CityName = city.p_CityName;
    this.p_DistrictId = city.p_DistrictId;
    this.p_IsActive = city.p_IsActive;
    this.p_IpAddress = city.p_IpAddress;
    this.p_UserId = city.p_UserId;
};
City.createCity = function createUser(newCity, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insupd_citymaster('"+newCity.p_CityId+"','"+newCity.p_CityName+"',"+newCity.p_DistrictId+",'"+newCity.p_IsActive+"','"+newCity.p_IpAddress+"','"+newCity.p_UserId+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            //end.sql()
            sql.end();
            result(null, res);
        }
    });           
};
City.getCity = function createUser(fetch_City, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_citybydistrict("+fetch_City.p_DistrictId+","+fetch_City.p_IpAddress+","+fetch_City.p_IsActive+","+fetch_City.p_UserId+")", function (err, res) {             
        if(err) {
            sql.end();
            
            result(err, null);
        }
        else{
            //end.sql()
            sql.end();
            result(null, res);
        }
    });   
};
City.getsomeCity = function createUser(fetch_City, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_8City("+fetch_City.p_DistrictId+","+fetch_City.p_IpAddress+")", function (err, res) {             
        if(err) {
            //end.sql()
            sql.end();
            result(err, null);
        }
        else{
            //end.sql()
            sql.end();
            result(null, res);
        }
    });   
};
module.exports= City;