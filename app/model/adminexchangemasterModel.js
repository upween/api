'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var AdminExchangeOffice = function(AdminExchangeOffice){
    this.p_Ex_id = AdminExchangeOffice.p_Ex_id;
    this.p_Ex_Type_id = AdminExchangeOffice.p_Ex_Type_id;
    this.p_District_id = AdminExchangeOffice.p_District_id;
    this.p_Ex_name = AdminExchangeOffice.p_Ex_name;
    this.p_Ex_NameH = AdminExchangeOffice.p_Ex_NameH;
    this.p_Emp_id = AdminExchangeOffice.p_Emp_id;
    this.p_Address = AdminExchangeOffice.p_Address;
    this.p_AddressH = AdminExchangeOffice.p_AddressH;
    this.p_Email_id = AdminExchangeOffice.p_Email_id;
    this.p_STDCode = AdminExchangeOffice.p_STDCode;
    this.p_Ophone1 = AdminExchangeOffice.p_Ophone1;
    this.p_Ophone2 = AdminExchangeOffice.p_Ophone2;
    this.p_Mobile_no = AdminExchangeOffice.p_Mobile_no;
    this.p_LMBY = AdminExchangeOffice.p_LMBY;
    this.p_Client_IP = AdminExchangeOffice.p_Client_IP;
    this.p_Active_YN = AdminExchangeOffice.p_Active_YN;
    this.p_s_no = AdminExchangeOffice.p_s_no;
    this.p_open_YN = AdminExchangeOffice.p_open_YN;
   
};


AdminExchangeOffice.createAdminExchangeOffice = function createUser(insUpd_adm_user_mst_exchangeoffice, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_adm_exch_mst_office('"+insUpd_adm_user_mst_exchangeoffice.p_Ex_id+"','"+insUpd_adm_user_mst_exchangeoffice.p_Ex_Type_id+"','"+insUpd_adm_user_mst_exchangeoffice.p_District_id+"','"+insUpd_adm_user_mst_exchangeoffice.p_Ex_name+"','"+insUpd_adm_user_mst_exchangeoffice.p_Ex_NameH+"','"+insUpd_adm_user_mst_exchangeoffice.p_Emp_id+"','"+insUpd_adm_user_mst_exchangeoffice.p_Address+"','"+insUpd_adm_user_mst_exchangeoffice.p_AddressH+"','"+insUpd_adm_user_mst_exchangeoffice.p_Email_id+"','"+insUpd_adm_user_mst_exchangeoffice.p_STDCode+"','"+insUpd_adm_user_mst_exchangeoffice.p_Ophone1+"','"+insUpd_adm_user_mst_exchangeoffice.p_Ophone2+"','"+insUpd_adm_user_mst_exchangeoffice.p_Mobile_no+"','"+insUpd_adm_user_mst_exchangeoffice.p_LMBY+"','"+insUpd_adm_user_mst_exchangeoffice.p_Client_IP+"','"+insUpd_adm_user_mst_exchangeoffice.p_Active_YN+"','"+insUpd_adm_user_mst_exchangeoffice.p_s_no+"','"+insUpd_adm_user_mst_exchangeoffice.p_open_YN+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            
            result(null, res);
        }
    });           
};

module.exports= AdminExchangeOffice;