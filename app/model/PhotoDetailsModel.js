'use strict';
'user strict';

var mysql = require('mysql');
var credentials = require('./db.js');


var PhotoDetails = function PhotoDetails(_PhotoDetails) {
    this.p_PG_Id = _PhotoDetails.p_PG_Id;
    this.p_PG_Cat_Id = _PhotoDetails.p_PG_Cat_Id;
    this.p_CC_Title = _PhotoDetails.p_CC_Title;
    this.p_Description = _PhotoDetails.p_Description;
    this.p_Verify_Status = _PhotoDetails.p_Verify_Status;
    this.p_Verify_By = _PhotoDetails.p_Verify_By;
    this.p_Closing_Date = _PhotoDetails.p_Closing_Date;
    this.p_Verify_Date = _PhotoDetails.p_Verify_Date;
    this.p_EX_id = _PhotoDetails.p_EX_id;
    this.p_DateofEvent = _PhotoDetails.p_DateofEvent;
    this.p_LMBY = _PhotoDetails.p_LMBY;
    this.p_Active_YN = _PhotoDetails.p_Active_YN;
    this.p_Flag = _PhotoDetails.p_Flag;
    this.p_DisplayAlways = _PhotoDetails.p_DisplayAlways;
    this.p_Image_Id = _PhotoDetails.p_Image_Id;
    this.p_Image_Name = _PhotoDetails.p_Image_Name;
    this.p_Thumb_Image_Name = _PhotoDetails.p_Thumb_Image_Name;
};
PhotoDetails.createPhotoDetails = function createUser(CI_Pro_PG_PhotoDetails, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL CI_Pro_PG_PhotoDetails(" + CI_Pro_PG_PhotoDetails.p_PG_Id + ",'" + CI_Pro_PG_PhotoDetails.p_PG_Cat_Id + "','" + CI_Pro_PG_PhotoDetails.p_CC_Title + "','" + CI_Pro_PG_PhotoDetails.p_Description + "','" + CI_Pro_PG_PhotoDetails.p_Verify_Status + "','" + CI_Pro_PG_PhotoDetails.p_Verify_By + "','" + CI_Pro_PG_PhotoDetails.p_Closing_Date + "','" + CI_Pro_PG_PhotoDetails.p_Verify_Date + "','" + CI_Pro_PG_PhotoDetails.p_EX_id + "','" + CI_Pro_PG_PhotoDetails.p_DateofEvent + "','" + CI_Pro_PG_PhotoDetails.p_LMBY + "','" + CI_Pro_PG_PhotoDetails.p_Active_YN + "','" + CI_Pro_PG_PhotoDetails.p_Flag + "','" + CI_Pro_PG_PhotoDetails.p_DisplayAlways + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

PhotoDetails.createPhotoUpload = function createUser(CI_Pro_PG_PhotoUpload, result) {
var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
    if (err) throw err;
});

    sql.query("CALL CI_Pro_PG_PhotoUpload(" + CI_Pro_PG_PhotoUpload.p_Image_Id + ",'" + CI_Pro_PG_PhotoUpload.p_PG_Id + "','" + CI_Pro_PG_PhotoUpload.p_Description + "','" + CI_Pro_PG_PhotoUpload.p_Image_Name + "','" + CI_Pro_PG_PhotoUpload.p_Thumb_Image_Name + "','" + CI_Pro_PG_PhotoUpload.p_Active_YN + "','" + CI_Pro_PG_PhotoUpload.p_Flag + "')", function (err, res) {
        if (err) {
	    sql.end()
            result(err, null);
        } else {
	    sql.end()
            result(null, res);
        }
    });
};

// PhotoDetails.createPhotoVerify = function createUser(CI_Pro_PG_PhotoVerify, result) {  

//     sql.query("CALL CI_Pro_PG_PhotoVerify('"+CI_Pro_PG_PhotoVerify.p_PG_Cat_Id+"','"+CI_Pro_PG_PhotoVerify.p_Image_Id+"','"+CI_Pro_PG_PhotoVerify.p_PG_Id+"','"+CI_Pro_PG_PhotoVerify.p_Image_Name+"','"+CI_Pro_PG_PhotoVerify.p_Verify_Status+"','"+CI_Pro_PG_PhotoVerify.p_Verify_By+"','"+CI_Pro_PG_PhotoVerify.p_Active_YN+"','"+CI_Pro_PG_PhotoVerify.p_Flag+"')", function (err, res) {      
//         if(err) {
//	    sql.end()
//             result(err, null);
//         }
//         else{
//	    sql.end()
//             result(null, res);
//         }.


//     });           
// };

module.exports = PhotoDetails;