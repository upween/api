'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var knowyourregnew = function(knowyourregnew){
    this.p_FilterType  = knowyourregnew.p_FilterType 
    this.p_FilterValue  = knowyourregnew.p_FilterValue ;
    this.p_CandidateId  = knowyourregnew.p_CandidateId ;
    this.p_FirstName   = knowyourregnew.p_FirstName  
    this.p_MiddleName   = knowyourregnew.p_MiddleName  ;
    this.p_LastName   = knowyourregnew.p_LastName  ;
    this.p_FatherName   = knowyourregnew.p_FatherName  
    this.p_DateOfBirth   = knowyourregnew.p_DateOfBirth ;
    this.p_Aadhar   = knowyourregnew.p_Aadhar  ;
    this.p_MobileNumber   = knowyourregnew.p_MobileNumber  ;
    this.p_EmailId    = knowyourregnew.p_EmailId   ;
    this.p_Gender    = knowyourregnew.p_Gender   ;
     
    
};

knowyourregnew.getknowyourregnew = function createUser(KnowYourRegistration_New, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });

    sql.query("CALL KnowYourRegistration_New("+KnowYourRegistration_New.p_FilterType+","+KnowYourRegistration_New.p_FilterValue+","+KnowYourRegistration_New.p_FirstName+","+KnowYourRegistration_New.p_LastName+","+KnowYourRegistration_New.p_Gender+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};


knowyourregnew.getJobSeekerProfileStatusOLEX = function createUser(Fetch_JobSeeker_ProfileStatus_OLEX , result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_JobSeeker_ProfileStatus_OLEX ("+Fetch_JobSeeker_ProfileStatus_OLEX .p_CandidateId +")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

knowyourregnew.createupdjobseekerregistrationlogin = function createUser(upd_jobseeker_registration_login, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL upd_jobseeker_registration_login('"+upd_jobseeker_registration_login.p_CandidateId +"','"+upd_jobseeker_registration_login.p_FirstName +"','"+upd_jobseeker_registration_login.p_MiddleName +"','"+upd_jobseeker_registration_login.p_LastName +"','"+upd_jobseeker_registration_login.p_FatherName +"','"+upd_jobseeker_registration_login.p_DateOfBirth+"','"+upd_jobseeker_registration_login.p_Aadhar +"','"+upd_jobseeker_registration_login.p_MobileNumber+"','"+upd_jobseeker_registration_login.p_EmailId +"')", function (err, res) {      
        if(err) {
            //end.sql()
            sql.end()
            result(err, null);
        }
        else{
            //end.sql()
            sql.end()
            result(null, res);
        }
    });           
}



knowyourregnew.createKnowYourRegistration_New = function createUser(KnowYourRegistration_New, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL KnowYourRegistration_New('"+KnowYourRegistration_New.p_FilterType+"','"+KnowYourRegistration_New.p_FilterValue+"','"+KnowYourRegistration_New.p_FirstName+"','"+KnowYourRegistration_New.p_LastName+"','"+KnowYourRegistration_New.p_Gender+"')", function (err, res) {      
        if(err) {
            //end.sql()
            sql.end()
            result(err, null);
        }
        else{
            //end.sql()
            sql.end()
            result(null, res);
        }
    });           
}

module.exports= knowyourregnew;