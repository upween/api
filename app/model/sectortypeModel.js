'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var sectortype = function(sectortype){
    this.p_S_Id = sectortype.p_S_Id;
    this.p_Active_YN = sectortype.p_Active_YN;
    this.p_Des_cription = sectortype.p_Des_cription;
    
};

sectortype.getsectortype = function createUser(fetch_sectortype_master, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_sectortype_master("+fetch_sectortype_master.p_S_Id+","+fetch_sectortype_master.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

sectortype.createsectortype = function createUser(InsUpd_sectortype, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_sectortype('"+InsUpd_sectortype.p_S_Id+"','"+InsUpd_sectortype.p_Des_cription+"','"+InsUpd_sectortype.p_Active_YN+"')", function (err, res) {      
        if(err) {
            //end.sql()
            sql.end()
            result(err, null);
        }
        else{
            //end.sql()
            sql.end()
            result(null, res);
        }
    });           
}

module.exports= sectortype;