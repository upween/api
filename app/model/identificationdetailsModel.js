'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var IdentificationDetails = function(identificationdetails){
    this.p_IdentificationId = identificationdetails.p_IdentificationId;
    this.p_CandidateId = identificationdetails.p_CandidateId;
    this.p_IdentificationType = identificationdetails.p_IdentificationType;
    this.p_IdentificationNumber = identificationdetails.p_IdentificationNumber;
    this.p_Date = identificationdetails.p_Date;
    this.p_Upto = identificationdetails.p_Upto;

    this.p_IssuingAuthority = identificationdetails.p_IssuingAuthority;
    this.p_PassingOutYearId = identificationdetails.p_PassingOutYearId;
    this.p_IpAddress = identificationdetails.p_IpAddress;
    this.p_LicenseType=identificationdetails.p_LicenseType;

};
IdentificationDetails.createIdentificationDetails = function createUser(newIdentificationDetails, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insUpd_identification_details('"+newIdentificationDetails.p_IdentificationId+"','"+newIdentificationDetails.p_CandidateId+"','"+newIdentificationDetails.p_IdentificationType+"','"+newIdentificationDetails.p_IdentificationNumber+"','"+newIdentificationDetails.p_Date+"','"+newIdentificationDetails.p_Upto+"','"+newIdentificationDetails.p_IssuingAuthority+"','"+newIdentificationDetails.p_IpAddress+"','"+newIdentificationDetails.p_LicenseType+"')", function (err, res) {      
        if(err) {
            sql.end();  
            result(err, null);
        }
        else{
            sql.end();  
            result(null, res);
        }
    });           
};
IdentificationDetails.getIdentificationDetails = function createUser(fetch_IdentificationDetails, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_identification_details("+fetch_IdentificationDetails.p_IdentificationId+","+fetch_IdentificationDetails.p_CandidateId+","+fetch_IdentificationDetails.p_IpAddress+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

module.exports= IdentificationDetails;