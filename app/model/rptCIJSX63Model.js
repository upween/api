'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var RptCIJSX63 = function(RptCIJSX63){
    this.p_Ex_Id = RptCIJSX63.p_Ex_Id;
    this.p_From = RptCIJSX63.p_From;
     this.p_To = RptCIJSX63.p_To;
    this.p_Category = RptCIJSX63.p_Category;
    this.p_Gender = RptCIJSX63.p_Gender;
    this.p_DifferentlyAbled = RptCIJSX63.p_DifferentlyAbled;
    this.p_Resident = RptCIJSX63.p_Resident;
    this.p_Area = RptCIJSX63.p_Area;
    

 };

RptCIJSX63.getRptCIJSX63 = function createUser(parameter,result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `temp_rpt_ci_js_x-63`()", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

RptCIJSX63.getReportX63 = function createUser(Report_X63,result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `Report_X63`("+Report_X63.p_Ex_Id+","+Report_X63.p_From+","+Report_X63.p_To+","+Report_X63.p_Category+","+Report_X63.p_Gender+","+Report_X63.p_DifferentlyAbled+","+Report_X63.p_Resident+","+Report_X63.p_Area+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports = RptCIJSX63;