'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var AdminSubMenu = function(AdminSubMenu){
    this.p_Sub_Menu_Id = AdminSubMenu.p_Sub_Menu_Id;
    this.p_Menu_Id = AdminSubMenu.p_Menu_Id;
    this.p_Sub_Menu_Name = AdminSubMenu.p_Sub_Menu_Name;
    this.p_URL = AdminSubMenu.p_URL;
    this.p_Sub_Menu_Description = AdminSubMenu.p_Sub_Menu_Description;
    this.p_Active_YN = AdminSubMenu.p_Active_YN;
    this.p_Sequence = AdminSubMenu.p_Sequence;
    this.p_open_for = AdminSubMenu.p_open_for;
};

AdminSubMenu.getAdminSubMenu= function createUser(fetch_Submenu, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_Submenu("+fetch_Submenu.p_Sub_Menu_Id+","+fetch_Submenu.p_Menu_Id+","+fetch_Submenu.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            
            result(null, res);
        }
    });   
};

AdminSubMenu.createAdminSubMenu = function createUser(InsUpd_adm_user_mst_submenu, result) {  
   var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL InsUpd_adm_user_mst_submenu('"+InsUpd_adm_user_mst_submenu.p_Sub_Menu_Id+"','"+InsUpd_adm_user_mst_submenu.p_Menu_Id+"','"+InsUpd_adm_user_mst_submenu.p_Sub_Menu_Name+"','"+InsUpd_adm_user_mst_submenu.p_URL+"','"+InsUpd_adm_user_mst_submenu.p_Sub_Menu_Description+"','"+InsUpd_adm_user_mst_submenu.p_Active_YN+"','"+InsUpd_adm_user_mst_submenu.p_Sequence+"','"+InsUpd_adm_user_mst_submenu.p_open_for+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};

module.exports= AdminSubMenu;