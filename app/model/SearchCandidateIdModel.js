'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var SearchCandidateId = function(searchCandidateId){
    this.p_Experience = searchCandidateId.p_Experience;
    this.p_DesignationId = searchCandidateId.p_DesignationId;
    this.p_Qualif_id = searchCandidateId.p_Qualif_id;
    this.p_Sub_Group_id = searchCandidateId.p_Sub_Group_id;
    this.p_DistrictId = searchCandidateId.p_DistrictId;
    this.p_SkillId = searchCandidateId.p_SkillId;
    this.p_VacancyType_id = searchCandidateId.p_VacancyType_id;
   this.p_JobParticipantId = searchCandidateId.p_JobParticipantId;
   this.p_SearchId = searchCandidateId.p_SearchId;
   this.p_CandidateId = searchCandidateId.p_CandidateId;
   this.p_Email = searchCandidateId.p_Email;
   this.p_Sms = searchCandidateId.p_Sms;
   this.p_SelectionStatus = searchCandidateId.p_SelectionStatus;
   this.p_EntryBy = searchCandidateId.p_EntryBy;
   this.p_IpAddress = searchCandidateId.p_IpAddress;

    
    
};

SearchCandidateId.createSearchCandidateId = function createUser(parameters, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
      
        sql.query("CALL SearchCandidateId('"+parameters.p_Experience +"','"+parameters.p_DesignationId +"','"+parameters.p_Qualif_id +"','"+parameters.p_Sub_Group_id +"','"+parameters.p_DistrictId +"','"+parameters.p_SkillId+"')", function (err, res) {      

    if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};


SearchCandidateId.getJobSearch = function createUser(JobSearch, result) {
    
    sql.query("CALL Candidate_JobSearch("+JobSearch.p_DesignationId+","+JobSearch.p_Qualif_id+","+JobSearch.p_DistrictId+","+JobSearch.p_VacancyType_id+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};
SearchCandidateId.createjobparticipant = function createUser(jobparticipant, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_jobparticipant('"+jobparticipant.p_JobParticipantId+"','"+jobparticipant.p_SearchId+"','"+jobparticipant.p_CandidateId+"','"+jobparticipant.p_Email+"','"+jobparticipant.p_Sms+"','"+jobparticipant.p_SelectionStatus+"','"+jobparticipant.p_EntryBy+"','"+jobparticipant.p_IpAddress+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};
SearchCandidateId.getJSjobparticipant= function createUser(JSjobparticipant, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});

    sql.query("CALL fetch_jobparticipant('"+JSjobparticipant.p_JobParticipantId +"','"+JSjobparticipant.p_SearchId+"')", function (err, res) {
        if(err) {   
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};


module.exports= SearchCandidateId;