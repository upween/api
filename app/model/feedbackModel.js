'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');
var FeedBack = function(feedback){
    this.p_FeedBackId = feedback.p_FeedBackId;
    this.p_UserName = feedback.p_UserName;
    this.p_ContactNumber = feedback.p_ContactNumber;
    this.p_EmailId = feedback.p_EmailId;
    this.p_Location = feedback.p_Location;
    this.p_Comments = feedback.p_Comments;
    this.p_Documents = feedback.p_Documents;
    this.p_FeedBackArea = feedback.p_FeedBackArea;
    this.p_Type = feedback.p_Type;
    this.p_UserId=feedback.p_UserId;
    this.p_IpAddress=feedback.p_IpAddress
    
};
FeedBack.createFeedBack = function createUser(newFeedBack, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insUpd_FeedBack('"+newFeedBack.p_FeedBackId+"','"+newFeedBack.p_UserName+"','"+newFeedBack.p_ContactNumber+"','"+newFeedBack.p_EmailId+"','"+newFeedBack.p_Location+"','"+newFeedBack.p_Comments+"','"+newFeedBack.p_Documents+"','"+newFeedBack.p_FeedBackArea+"','"+newFeedBack.p_UserId+"','"+newFeedBack.p_IpAddress+"')", function (err, res) {      
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};
FeedBack.getFeedBack= function createUser(fetch_FeedBack, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL fetch_feedBack("+fetch_FeedBack.p_FeedBackId+")", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};

FeedBack.getFeedbackUser= function createUser(Check_UserAvailability, result) {
    var sql = mysql.createConnection(credentials);
sql.connect(function (err) {
if (err) throw err;
});
    sql.query("CALL Check_UserAvailability('"+Check_UserAvailability.p_UserName+"','"+Check_UserAvailability.p_Type+"')", function (err, res) {             
        if(err) {
            sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });   
};
module.exports= FeedBack;