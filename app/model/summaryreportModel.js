'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Summaryreport = function(Summaryreport){  
    this.p_fromdate = Summaryreport.p_fromdate;
    this.p_todate = Summaryreport.p_todate; 
    this.p_Ex_id = Summaryreport.p_Ex_id;
    this.p_Fromdt =Summaryreport.p_Fromdt;
    this.p_Todt = Summaryreport.p_Todt;

    this.p_Month = Summaryreport.p_Month; 
    this.p_year = Summaryreport.p_year;
    this.p_qualificationid =Summaryreport.p_qualificationid;
 };

 Summaryreport.getSummaryreport = function createUser(parameter,result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `temp_summaryreport`()", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

Summaryreport.getSummaryreportCategoryWise = function createUser(CategoryWise,result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `SummaryReport_CategoryWise`("+CategoryWise.p_fromdate+","+CategoryWise.p_todate+","+CategoryWise.p_Ex_id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end() 
            result(null, res);
        }
    });   
};

Summaryreport.getSummaryreportDifferentlyAbled = function createUser(DifferentlyAbled ,result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `CI_Pro_Rpt_Summary_PHWise`("+DifferentlyAbled.p_Fromdt+","+DifferentlyAbled.p_Todt+","+DifferentlyAbled.p_Ex_id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end() 
            result(null, res);
        }
    });   
};

Summaryreport.getSummaryreportAreaWise = function createUser(AreaWise ,result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `SummaryReport_AreaWise`("+AreaWise.p_fromdate+","+AreaWise.p_todate+","+AreaWise.p_Ex_id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

Summaryreport.getSummaryreportReligionWise = function createUser(ReligionWise ,result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `CI_Pro_Rpt_Summary_ReligionWise`("+ReligionWise.p_Fromdt+","+ReligionWise.p_Todt+","+ReligionWise.p_Ex_id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end() 
            result(null, res);
        }
    });   
};

Summaryreport.getITIQualificationWiseSummaryReport = function createUser(QualificationWise ,result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `ITI_QualificationWise_SummaryReport`("+QualificationWise.p_Month+","+QualificationWise.p_year+","+QualificationWise.p_Ex_id+","+QualificationWise.p_qualificationid+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

Summaryreport.getConsolidateSummaryReport = function createUser(ConsolidateSummaryReport ,result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `Consolidate_Summary_Report`("+ConsolidateSummaryReport.p_fromdate+","+ConsolidateSummaryReport.p_todate+","+ConsolidateSummaryReport.p_Ex_id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

Summaryreport.getDetailedSummaryReport = function createUser(DetailedSummaryReport ,result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `Detailed_Summary_Report_category`("+DetailedSummaryReport.p_fromdate+","+DetailedSummaryReport.p_todate+","+DetailedSummaryReport.p_Ex_id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()  
            result(null, res);
        }
    });  
    

};

Summaryreport.getDetailedSummaryReportReligion = function createUser(DetailedSummaryReportReligion ,result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `Detailed_Summary_Report_Religion`("+DetailedSummaryReportReligion.p_fromdate+","+DetailedSummaryReportReligion.p_todate+","+DetailedSummaryReportReligion.p_Ex_id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });  
    

};

Summaryreport.getDetailedSummaryReportAreaWise = function createUser(DetailedSummaryReportAreaWise ,result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `Detailed_Summary_Report_AreaWise`("+DetailedSummaryReportAreaWise.p_fromdate+","+DetailedSummaryReportAreaWise.p_todate+","+DetailedSummaryReportAreaWise.p_Ex_id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });  
    

};

Summaryreport.getDetailedSummaryReportPHWise = function createUser(DetailedSummaryReportPHWise ,result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL `Detailed_Summary_Report_PHWise`("+DetailedSummaryReportPHWise.p_fromdate+","+DetailedSummaryReportPHWise.p_todate+","+DetailedSummaryReportPHWise.p_Ex_id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });  
    

};
module.exports = Summaryreport;