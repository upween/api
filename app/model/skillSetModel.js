'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var SkillSet = function(SkillSet){
    this.p_ESID = SkillSet.p_ESID;
    this.p_Active_YN = SkillSet.p_Active_YN;
    
};

SkillSet.getSkillSet = function createUser(fetch_Employment_sector_SkillSet, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Employment_sector_SkillSet("+fetch_Employment_sector_SkillSet.p_ESID+","+fetch_Employment_sector_SkillSet.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= SkillSet;