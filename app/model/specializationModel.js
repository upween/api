'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Specialization = function(specialization){
    this.p_specializationid = specialization.p_specializationid;
    this.p_specializationname = specialization.p_specializationname;
    this.p_isactive = specialization.p_isactive;
    this.p_courseid = specialization.p_courseid;
    this.p_IpAddress = specialization.p_IpAddress;
    this.p_UserId = specialization.p_UserId;
};
Specialization.createSpecialization = function createUser(newSpecialization, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL insupd_specializationmaster('"+newSpecialization.p_specializationid+"','"+newSpecialization.p_specializationname+"','"+newSpecialization.p_isactive+"','"+newSpecialization.p_courseid+"','"+newSpecialization.p_IpAddress+"','"+newSpecialization.p_UserId+"')", function (err, res) {      
        if(err) {
           sql.end();
            result(err, null);
        }
        else{
            sql.end();
            result(null, res);
        }
    });           
};
Specialization.getSpecialization = function createUser(fetch_Specialization, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    
    sql.query("CALL fetch_specializationmaster("+fetch_Specialization.p_specializationid+","+fetch_Specialization.p_IpAddress+","+fetch_Specialization.p_isactive+","+fetch_Specialization.p_courseid+","+fetch_Specialization.p_UserId+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= Specialization;