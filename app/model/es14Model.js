
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES14Entry= function(es14entry){
    this.p_Year  = es14entry.p_Year ;
    this.p_Ex_Id  = es14entry.p_Ex_Id ;
   
    this.p_ToDt  = es14entry.p_ToDt ;
    this.p_Edu_id  = es14entry.p_Edu_id ;
    this.p_Upto19  = es14entry.p_Upto19 ;
    this.p_Age_20_29  = es14entry.p_Age_20_29 ;
    this.p_Age_30_39  = es14entry.p_Age_30_39 ;
    this.p_Age_40_49  = es14entry.p_Age_40_49 ;
    this.p_Age_50_59  = es14entry.p_Age_50_59 ;
    this.p_Age_60_above  = es14entry.p_Age_60_above ;
    this.p_Flag  = es14entry.p_Flag ;

};


ES14Entry.getRptES14 = function createUser(Es14, result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_ES14Report("+Es14.p_Year+","+Es14.p_Ex_Id+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};



ES14Entry.createES14Entry = function createUser(newES14Entry, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES14Entry('"+newES14Entry.p_Ex_Id+"','"+newES14Entry.p_ToDt+"','"+newES14Entry.p_Edu_id+"','"+newES14Entry.p_Upto19+"','"+newES14Entry.p_Age_20_29+"','"+newES14Entry.p_Age_30_39+"','"+newES14Entry.p_Age_40_49+"','"+newES14Entry.p_Age_50_59+"','"+newES14Entry.p_Age_60_above+"','"+newES14Entry.p_Flag+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};



ES14Entry.getEdu_with_Gender = function createUser(Edu_with_Gender,result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_Edu_with_Gender()", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};


module.exports= ES14Entry;