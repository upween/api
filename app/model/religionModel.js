'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Religion = function(Religion){
    this.p_Religion_id = Religion.p_Religion_id;
    this.p_Active_YN = Religion.p_Active_YN;
    this.p_Religion_Name = Religion.p_Religion_Name;
    this.p_Religion_Name_H = Religion.p_Religion_Name_H;
    
};

Religion.getReligion= function createUser(fetch_Religion_master, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_Religion_master("+fetch_Religion_master.p_Religion_id+","+fetch_Religion_master.p_Active_YN+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

Religion.createReligion = function createUser(InsUpd_Religion_master, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_Religion_master('"+InsUpd_Religion_master.p_Religion_id+"','"+InsUpd_Religion_master.p_Religion_Name+"','"+InsUpd_Religion_master.p_Religion_Name_H+"','"+InsUpd_Religion_master.p_Active_YN+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};

module.exports= Religion;