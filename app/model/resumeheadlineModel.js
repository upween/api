'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var Resumeheadline = function(resumeheadline){
    this.p_DetailId = resumeheadline.p_DetailId;
    this.p_Detail = resumeheadline.p_Detail;
    this.p_CandidateId = resumeheadline.p_CandidateId;
    this.p_IpAddress = resumeheadline.p_IpAddress;
    this.p_UserId = resumeheadline.p_UserId;
};
Resumeheadline.createResumeheadline = function createUser(newResumeheadline, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL InsUpd_ResumeHeadLine('"+newResumeheadline.p_DetailId+"','"+newResumeheadline.p_Detail+"','"+newResumeheadline.p_CandidateId+"','"+newResumeheadline.p_IpAddress+"','"+newResumeheadline.p_UserId+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};
Resumeheadline.getResumeheadline = function createUser(fetch_Resumeheadline, result) {
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL fetch_ResumeHeadLine("+fetch_Resumeheadline.p_CandidateId+","+fetch_Resumeheadline.p_IpAddress+","+fetch_Resumeheadline.p_UserId+")", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};

module.exports= Resumeheadline;