
'user strict';
var mysql = require('mysql');
var credentials = require('./db.js');

var ES21Entry= function(es21entry){
    this.p_Ex_id  = es21entry.p_Ex_id ;
    this.p_ToDt  = es21entry.p_ToDt ;
    this.p_Edu21_id  = es21entry.p_Edu21_id ;
    this.p_Total_Regd  = es21entry.p_Total_Regd ;
    this.p_Total_Placement  = es21entry.p_Total_Placement ;
    this.p_Total_LR  = es21entry.p_Total_LR ;
    this.p_W_Regd  = es21entry.p_W_Regd ;
    this.p_W_Placement  = es21entry.p_W_Placement ;
    this.p_W_LR  = es21entry.p_W_LR ;
    this.p_SC_Regd  = es21entry.p_SC_Regd ;
    this.p_SC_Placement  = es21entry.p_SC_Placement ;
    this.p_SC_LR  = es21entry.p_SC_LR ;
    this.p_ST_Regd  = es21entry.p_ST_Regd ;
    this.p_ST_Placement  = es21entry.p_ST_Placement ;
    this.p_ST_LR  = es21entry.p_ST_LR ;
    this.p_OBC_Regd  = es21entry.p_OBC_Regd ;
    this.p_OBC_Placement  = es21entry.p_OBC_Placement ;
    this.p_OBC_LR  = es21entry.p_OBC_LR ;
    this.p_Flag  = es21entry.p_Flag ;

};




ES21Entry.createES21Entry = function createUser(newES21Entry, result) {  
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL CI_Pro_Rpt_ES21_Entry('"+newES21Entry.p_Ex_id+"','"+newES21Entry.p_ToDt+"','"+newES21Entry.p_Edu21_id+"','"+newES21Entry.p_Total_Regd+"','"+newES21Entry.p_Total_Placement+"','"+newES21Entry.p_Total_LR+"','"+newES21Entry.p_W_Regd+"','"+newES21Entry.p_W_Placement+"','"+newES21Entry.p_W_LR+"','"+newES21Entry.p_SC_Regd+"','"+newES21Entry.p_SC_Placement+"','"+newES21Entry.p_SC_LR+"','"+newES21Entry.p_ST_Regd+"','"+newES21Entry.p_ST_Placement+"','"+newES21Entry.p_ST_LR+"','"+newES21Entry.p_OBC_Regd+"','"+newES21Entry.p_OBC_Placement+"','"+newES21Entry.p_OBC_LR+"','"+newES21Entry.p_Flag+"')", function (err, res) {      
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });           
};



ES21Entry.getFetch_ES21_Master = function createUser(Fetch_ES21_Master,result) {
    
    var sql = mysql.createConnection(credentials);
    sql.connect(function (err) {
    if (err) throw err;
    });
    sql.query("CALL Fetch_ES21_Master()", function (err, res) {             
        if(err) {
            sql.end()
            result(err, null);
        }
        else{
            sql.end()
            result(null, res);
        }
    });   
};


module.exports= ES21Entry;