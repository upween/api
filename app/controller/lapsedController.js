'use strict';

var Lapsed = require('../model/lapsedModel.js');

exports.read_a_Lapsed = function (req, res) {
  Lapsed.getLapsed(req.params, function (err, Lapsed) {
    if (err) {
      res.send(err);
    } else {
      res.json(Lapsed);
    }
  });
};