
'use strict';

var niccandidatedetail = require('../model/niccandidatedetailModel.js');

exports.read_a_candidatecount = function (req, res) {
  
  niccandidatedetail.getdistrictwisecandidateCount(req.params, function (err, count) {
    if (err) {
      res.send(err);
    } else {
      res.json(count);
    }
  });
};
exports.read_a_candidatedetails = function (req, res) {
  
    niccandidatedetail.getdistrictwisecandidateDetail(req.params, function (err, detail) {
      if (err) {
        res.send(err);
      } else {
        res.json(detail);
      }
    });
  };
  