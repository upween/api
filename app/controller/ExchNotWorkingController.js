'use strict';

var ExchNotWorking = require('../model/ExchNotWorkingModel.js');

exports.read_a_ExchNotWorking = function (req, res) {

  var Adm_Pro_Exchange_NotWorking = req.params;
  ExchNotWorking.getExchNotWorking(req.params, function (err, ExchNotWorking) {
    if (err) {
      res.send(err);
    } else {
      res.json(ExchNotWorking);
    }
  });
};