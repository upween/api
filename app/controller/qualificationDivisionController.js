'use strict';

var QualificationDivision = require('../model/qualificationDivisionModel.js');

exports.read_a_QualificationDivision = function (req, res) {
  var read_a_QualificationDivision = req.params;
  QualificationDivision.getQualificationDivision(req.params, function (err, QualificationDivision) {
    if (err) {
      res.send(err);
    } else {
      res.json(QualificationDivision);
    }
  });
};
//DMPL//