'use strict';

var ProfileSummary = require('../model/ProfileSummaryModel.js');

exports.create_a_profileSummary = function (req, res) {
  var new_profileSummary = new ProfileSummary(req.body);

  ProfileSummary.createProfileSummary(new_profileSummary, function (err, profileSummary) {
    if (err) {
      res.send(err);
    } else {
      res.json(profileSummary);
    }
  });
};
exports.read_a_profileSummary = function (req, res) {
  var fetch_ProfileSummary = req.params;
  ProfileSummary.getProfileSummary(req.params, function (err, profileSummary) {
    if (err) {
      res.send(err);
    } else {
      res.json(profileSummary);
    }
  });
};