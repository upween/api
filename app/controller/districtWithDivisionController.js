'use strict';

var DistrictWithDivision = require('../model/districtWithDivisionModel.js');

exports.read_a_DistrictWithDivision = function (req, res) {
  DistrictWithDivision.getDistrictWithDivision(req.params, function (err, DistrictWithDivision) {
    if (err) {
      res.send(err);
    } else {
      res.json(DistrictWithDivision);
    }
  });
};