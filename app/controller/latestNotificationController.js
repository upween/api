'use strict';
var latest  = require('../model/latestNotificationModel.js');


exports.read_a_JSlatestNotification = function (req, res) {
    var fetch_latestNotification = req.params;
    latest.getJSlatestNotification(req.params, function (err,latestNotification) {
      if (err) {
        res.send(err);
      } else {
        res.json(latestNotification);
      }
    });
  };
