


  'use strict';
  var SectorRole = require('../model/SectorRoleModel.js');
  
  

exports.create_a_Role = function(req, res) {
  var new_role = new SectorRole(req.body);
 
  SectorRole.createRole(new_role, function(err, role) {
      if (err){
        res.send(err);
      }
      else{
      res.json(role);
      
      }
    });
  
};
exports.create_a_Sector = function(req, res) {
  var new_Sector = new SectorRole(req.body);
 
  SectorRole.createSector(new_Sector, function(err, sector) {
      if (err){
        res.send(err);
      }
      else{
      res.json(sector);
      
      }
    });
  
};
  
  exports.create_a_SectorRole = function(req, res) {
    var new_sectorrole = new SectorRole(req.body);
   
    SectorRole.createSectorRole(new_sectorrole, function(err, sectorrole) {
        if (err){
          res.send(err);
        }
        else{
        res.json(sectorrole);
        
        }
      });
    
  };
 
exports.read_a_getFunctionalArea = function(req, res) {
  var fetch_functionarea_Master= req.params;
  SectorRole.getgetFunctionalArea(req.params, function(err, getFunctionalArea) {
    if (err){
      res.send(err);
    }
    else{
      res.json(getFunctionalArea);
    }
  });
};

    
  