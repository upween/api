'use strict';

var yealytarget = require('../model/yealytargetModel.js');

exports.create_a_yealytarget = function (req, res) {
  var InsUpd_ci_jf_jobfair_yealytarget = new yealytarget(req.body);

  yealytarget.createyealtarget(InsUpd_ci_jf_jobfair_yealytarget, function (err, yealytarget) {
    if (err) {
      res.send(err);
    } else {
      res.json(yealytarget);
    }
  });
};
exports.read_a_yealytarget = function (req, res) {

  var Fetch_ci_jf_jobfair_yealytarget = req.params;
  yealytarget.getyealytarget(req.params, function (err, yealytarget) {
    if (err) {
      res.send(err);
    } else {
      res.json(yealytarget);
    }
  });
};