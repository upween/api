'use strict';

var Itskill = require('../model/itskillModel.js');

exports.create_a_itskill = function (req, res) {
  var new_itskill = new Itskill(req.body);

  Itskill.createItskill(new_itskill, function (err, itskill) {
    if (err) {
      res.send(err);
    } else {
      res.json(itskill);
    }
  });
};
exports.read_a_itskill = function (req, res) {
  var fetch_itskill = req.params;
  Itskill.getItskill(req.params, function (err, itskill) {
    if (err) {
      res.send(err);
    } else {
      res.json(itskill);
    }
  });
};