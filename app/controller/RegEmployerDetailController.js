'use strict';

var RegEmployerDetail = require('../model/RegEmployerDetailModel.js');

exports.read_a_RegEmployerDetail = function (req, res) {

  var Rpt_USP_RegEmployerDetail = req.params;
  RegEmployerDetail.getRegEmployerDetail(req.params, function (err, RegEmployerDetail) {
    if (err) {
      res.send(err);
    } else {
      res.json(RegEmployerDetail);
    }
  });
};