'use strict';

var Employement = require('../model/employementModel.js');

exports.create_a_employement = function (req, res) {
  var new_employement = new Employement(req.body);

  Employement.createEmployement(new_employement, function (err, employement) {
    if (err) {
      res.send(err);
    } else {
      res.json(employement);
    }
  });
};
exports.read_a_employement = function (req, res) {
  var fetch_employement = req.params;
  Employement.getEmployement(req.params, function (err, employement) {
    if (err) {
      res.send(err);
    } else {
      res.json(employement);
    }
  });
};