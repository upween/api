'use strict';
var EmployerRegistration = require('../model/employerregistrationsModel.js');

exports.create_a_EmployerRegistration = function(req, res) {
  var new_EmployerRegistration = new EmployerRegistration(req.body);
 
  EmployerRegistration.createEmployerRegistration(new_EmployerRegistration, function(err, EmployerRegistration) {
      if (err){
        res.status(200).send(err);
      }
      else{
        res.status(200).send(EmployerRegistration);
      }
    });
};

exports.create_a_GSTFile = function(req, res) {
  var GST_File = new EmployerRegistration(req.body);
 
  EmployerRegistration.createGSTFile(GST_File, function(err, EmployerRegistration) {
      if (err){
        res.status(200).send(err);
      }
      else{
        res.status(200).send(EmployerRegistration);
      }
    });
};

exports.read_a_EmployerRegistration = function(req, res) {
  var fetch_EmployerRegistration = req.params;
  EmployerRegistration.getEmployerRegistration(req.params, function(err, EmployerRegistration) {
    if (err){
      res.send(err);
    }
    else{
    res.json(EmployerRegistration);
    }
  });
};

exports.read_a_EmployerRegDetail = function(req, res) {
  var Fetch_EmployerRegDetail = req.params;
  EmployerRegistration.getEmployerRegDetail(req.params, function(err, EmployerRegDetail) {
    if (err){
      res.send(err);
    }
    else{
    res.json(EmployerRegDetail);
    }
  });
};
exports.read_a_EmployerByExchange = function(req, res) {
  var EmployerByExchange = req.params;
  EmployerRegistration.getEmployerByExchange(req.params, function(err, EmployerByExchange) {
    if (err){
      res.send(err);
    }
    else{
    res.json(EmployerByExchange);
    }
  });
};
exports.create_a_EmployerLogo = function(req, res) {
  
  var EmployerLogo = new EmployerRegistration(req.body);
 
  EmployerRegistration.createEmployerLogo(EmployerLogo, function(err, EmployerLogo) {
    if (err){
      res.send(err);
    }
    else{
    res.json(EmployerLogo);
    }
  });
};
exports.read_a_EmployerRegDetailbyEmail = function(req, res) {
  var Fetch_EmployerRegDetailbyEmail = req.params;
  EmployerRegistration.getEmployerRegDetailbyEmail(req.params, function(err, EmployerRegDetailbyEmail) {
    if (err){
      res.send(err);
    }
    else{
    res.json(EmployerRegDetailbyEmail);
    }
  });
};
exports.read_a_EmployerChangePasswordbyemail = function(req, res) {
  var Employer_ChangePasswordbyemail = req.params;
  EmployerRegistration.getEmployerChangePasswordbyemail(req.params, function(err, EmployerChangePasswordbyemail) {
    if (err){
      res.send(err);
    }
    else{
    res.json(EmployerChangePasswordbyemail);
    }
  });
};
exports.read_a_PlacementEmployer = function(req, res) {
  EmployerRegistration.getEmployerPlacement(req.params, function(err, EmployerPlacement) {
    if (err){
      res.send(err);
    }
    else{
    res.json(EmployerPlacement);
    }
  });
};