'use strict';

var companytype = require('../model/companyTypeModel.js');

exports.read_a_companytype = function (req, res) {
  var fetch_companytype_master = req.params;
  companytype.getcompanytype(req.params, function (err, companytype) {
    if (err) {
      res.send(err);
    } else {
      res.json(companytype);
    }
  });
};

exports.create_a_companytype = function (req, res) {
  var InsUpd_companytype_master = new companytype(req.body);

  companytype.createcompanytype(InsUpd_companytype_master, function (err, companytype) {
    if (err) {
      res.send(err);
    } else {
      res.json(companytype);
    }
  });
};