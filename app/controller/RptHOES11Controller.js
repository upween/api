'use strict';

var RptHOES11 = require('../model/RptHOES11Model.js');

exports.read_a_RptHOES11 = function (req, res) {

  var CI_Pro_Rpt_HO_ES11 = req.params;
  RptHOES11.getRptHOES11(req.params, function (err, RptHOES11) {
    if (err) {
      res.send(err);
    } else {
      res.json(RptHOES11);
    }
  });
};