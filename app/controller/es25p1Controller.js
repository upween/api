'use strict';

var ES25P1Entry = require('../model/es25p1Model.js');


exports.read_a_es25P1Rpt = function (req, res) {
  var ES25P1Entrypara = req.params;
  ES25P1Entry.getES25P1Rpt(ES25P1Entrypara, function (err, es25p1) {
    if (err) {
      res.send(err);
    } else {
      res.json(es25p1);
    }
  });
};

exports.create_a_es25P1RptEntry = function (req, res) {
  var es25P1RptEntry = new ES25P1Entry(req.body);

  ES25P1Entry.createES25P1Rpt(es25P1RptEntry, function (err, es25p1) {
    if (err) {
      res.send(err);
    } else {
      res.json(es25p1);
    }
  });
};