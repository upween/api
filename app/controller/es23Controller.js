'use strict';

var ES23Entry = require('../model/es23Model.js');




exports.create_a_es23RptEntry = function (req, res) {
  var es23RptEntry = new ES23Entry(req.body);

  ES23Entry.createES23Entry(es23RptEntry, function (err, es23) {
    if (err) {
      res.send(err);
    } else {
      res.json(es23);
    }
  });
};