'use strict';

var AdminUserType = require('../model/adminUserTypeModel.js');

exports.read_a_AdminUserType = function (req, res) {
  var fetch_adm_user_mst_usertype = req.params;
  AdminUserType.getAdminUserType(req.params, function (err, AdminUserType) {
    if (err) {
      res.send(err);
    } else {
      res.json(AdminUserType);
    }
  });
};