'use strict';

var SportsName = require('../model/sportsNameModel.js');

exports.read_a_SportsName = function (req, res) {
  var read_a_SportsName = req.params;
  SportsName.getSportsName(req.params, function (err, SportsName) {
    if (err) {
      res.send(err);
    } else {
      res.json(SportsName);
    }
  });
};