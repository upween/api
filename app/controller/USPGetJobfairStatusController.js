'use strict';

var USPGetJobfairStatus = require('../model/USPGetJobfairStatusModel.js');

exports.read_a_USPGetJobfairStatus = function (req, res) {

  var DashBoard_USP_GetJobfairStatus = req.params;
  USPGetJobfairStatus.getUSPGetJobfairStatus(req.params, function (err, USPGetJobfairStatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(USPGetJobfairStatus);
    }
  });
};