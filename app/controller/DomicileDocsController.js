'use strict';

var DomicileDocs = require('../model/DomicileDocsModel.js');

exports.create_a_DomicileDocs = function (req, res) {
  var new_DomicileDocs = new DomicileDocs(req.body);

  DomicileDocs.createDomicileDocs(new_DomicileDocs, function (err, DomicileDocs) {
    if (err) {
      res.send(err);
    } else {
      res.json(DomicileDocs);
    }
  });
};