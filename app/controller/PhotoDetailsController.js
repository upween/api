'use strict';

var PhotoDetails = require('../model/PhotoDetailsModel.js');

exports.create_a_PhotoDetails = function (req, res) {
  var CI_Pro_PG_PhotoDetails = new PhotoDetails(req.body);

  PhotoDetails.createPhotoDetails(CI_Pro_PG_PhotoDetails, function (err, PhotoDetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(PhotoDetails);
    }
  });
};

exports.create_a_PhotoUpload = function (req, res) {
  var CI_Pro_PG_PhotoUpload = new PhotoDetails(req.body);

  PhotoDetails.createPhotoUpload(CI_Pro_PG_PhotoUpload, function (err, PhotoDetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(PhotoDetails);
    }
  });
};

//   exports.create_a_PhotoVerify = function(req, res) {
//     var CI_Pro_PG_PhotoVerify= new PhotoVerify(req.body);

//     PhotoDetails.createPhotoVerify(CI_Pro_PG_PhotoVerify, function(err, PhotoDetails) {
//         if (err)
//           res.send(err);
//         res.json(PhotoDetails);
//       });

//   };