'use strict';

var City = require('../model/8CityModel.js');

exports.read_a_city8 = function (req, res) {
  var fetch_feedfetch_City = req.params;
  City.getCity(req.params, function (err, city8) {
    if (err) {
      res.send(err);
    } else {
      res.json(city8);
    }
  });
};