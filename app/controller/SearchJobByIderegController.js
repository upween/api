'use strict';

var SearchJobByIdereg = require('../model/SearchJobByIdregModel.js');

exports.read_a_SearchJobByIdereg = function (req, res) {

  var SearchJobId = req.params;
  SearchJobByIdereg.getSearchJobByIdereg(req.params, function (err, SearchJobByIdereg) {
    if (err) {
      res.send(err);
    } else {
      res.json(SearchJobByIdereg);
    }
  });
};