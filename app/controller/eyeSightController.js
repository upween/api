'use strict';

var EyeSight = require('../model/eyeSightModel.js');

exports.read_a_EyeSight = function (req, res) {
  var read_a_EyeSight = req.params;
  EyeSight.getEyeSight(req.params, function (err, EyeSight) {
    if (err) {
      res.send(err);
    } else {
      res.json(EyeSight);
    }
  });
};