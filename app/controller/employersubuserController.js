'use strict';

var EmpSubUser= require('../model/employersubuserModel.js');

exports.create_a_empsubuser = function (req, res) {
  var insupdEmpSubUser= new EmpSubUser(req.body);

  EmpSubUser.createSubUser(insupdEmpSubUser, function (err, subuser) {
    if (err) {
      res.send(err);
    } else {
      res.json(subuser);
    }
  });
};
exports.read_a_empsubuser = function (req, res) {
  
    EmpSubUser.getSubUser(req.params, function (err, subuser) {
      if (err) {
        res.send(err);
      } else {
        res.json(subuser);
      }
    });
  };

  exports.read_a_DeleteSubUser = function (req, res) {
    var DeleteSubUser = req.params;  
    EmpSubUser.getDeleteSubUser(req.params, function (err, DeleteSubUser) {
      if (err) {
        res.send(err);
      } else {
        res.json(DeleteSubUser);
      }
    });
  };