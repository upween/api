
'use strict';

var ActivityMaster = require('../model/activitymasterModel.js');

exports.read_a_ActivityMaster = function (req, res) {
  var fetch_ActivityMaster = req.params;
  ActivityMaster.getActivityMaster(req.params, function (err, ActivityMaster) {
    if (err) {
      res.send(err);
    } else {
      res.json(ActivityMaster);
    }
  });
};
exports.create_a_ActivityMaster = function (req, res) {
  var new_ActivityMaster = new ActivityMaster(req.body);

  ActivityMaster.createActivityMaster(new_ActivityMaster, function (err, ActivityMaster) {
    if (err) {
      res.send(err);
    } else {
      res.json(ActivityMaster);
    }
  });
};