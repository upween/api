'use strict';

var ES14Entry = require('../model/es14Model.js');


exports.read_a_es14Rpt = function (req, res) {
  var ES14Entrypara = req.params;
  ES14Entry.getRptES14(ES14Entrypara, function (err, es14) {
    if (err) {
      res.send(err);
    } else {
      res.json(es14);
    }
  });
};



exports.create_a_ES14Entry = function (req, res) {
  var CI_Pro_Rpt_ES14Entry = new ES14Entry(req.body);

  ES14Entry.createES14Entry(CI_Pro_Rpt_ES14Entry, function (err, ES14Entry) {
    if (err) {
      res.send(err);
    } else {
      res.json(ES14Entry);
    }
  });
};



exports.read_a_Edu_with_Gender = function (req, res) {
  var Fetch_Edu_with_Gender = req.params;
  ES14Entry.getEdu_with_Gender(Fetch_Edu_with_Gender, function (err, Edu_with_Gender) {
    if (err) {
      res.send(err);
    } else {
      res.json(Edu_with_Gender);
    }
  });
};
