
'use strict';

var Status = require('../model/StatusModel.js');

exports.read_a_Status = function (req, res) {
  var fetch_Status_master = req.params;
  Status.getStatus(req.params, function (err, Status) {
    if (err) {
      res.send(err);
    } else {
      res.json(Status);
    }
  });
};