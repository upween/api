'use strict';

var jobseekerreg = require('../model/jobseekerregModel.js');

exports.read_a_jobseekerreg = function (req, res) {

  var fetch_jobseeker_reg = req.params;
  jobseekerreg.getjobseekerreg(req.params, function (err, jobseekerreg) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobseekerreg);
    }
  });
};

exports.create_a_oldjobseekerreg = function(req, res) {
  var Upd_Old_jobseeker_registration = new jobseekerreg(req.body);
 
  jobseekerreg.createoldjobseekerreg(Upd_Old_jobseeker_registration, function(err, oldjobseekerreg) {
      if (err){
        res.send(err);
      }
      else{
      res.json(oldjobseekerreg);
      }
    });
  
};



  
