'use strict';

var Hcwisegenderdetail = require('../model/hcwisegenderdetailModel.js');

exports.read_a_Hcwisegenderdetail = function (req, res) {
  Hcwisegenderdetail.getHcwisegenderdetail(req.params, function (err, Hcwisegenderdetail) {
    if (err) {
      res.send(err);
    } else {
      res.json(Hcwisegenderdetail);
    }
  });
};