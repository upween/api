'use strict';

var ProEmpX6 = require('../model/ProEmpX6Model.js');

exports.create_a_ProEmpX6 = function (req, res) {
  var CI_Pro_Emp_X6 = new ProEmpX6(req.body);

  ProEmpX6.createProEmpX6(CI_Pro_Emp_X6, function (err, ProEmpX6) {
    if (err) {
      res.send(err);
    } else {
      res.json(ProEmpX6);
    }
  });
};

exports.read_a_CandidateJobAlert = function (req, res) {
  var fetch_CandidateJobAlert = req.params;
  ProEmpX6.getCandidateJobAlert(req.params, function (err, CandidateJobAlert) {
    if (err) {
      res.send(err);
    } else {
      res.json(CandidateJobAlert);
    }
  });
};

exports.create_a_WalkIn = function (req, res) {
  var fetch_WalkIn = new ProEmpX6(req.body);

  ProEmpX6.createWalkIn(fetch_WalkIn, function (err, WalkIn) {
    if (err) {
      res.send(err);
    } else {
      res.json(WalkIn);
    }
  });
};