'use strict';

var NCS = require('../model/ncsModel.js');

exports.create_a_ncscandidatedata = function (req, res) {
  var registration = new NCS(req.body);

  NCS.createjobseekerregistration(registration, function (err, candidatedata) {
    if (err) {
      res.send(err);
    } else {
      res.json(candidatedata);
    }
  });
};