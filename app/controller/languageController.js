
'use strict';

var Language = require('../model/languageModel.js');

exports.create_a_Language = function (req, res) {
  var new_Language = new Language(req.body);

  Language.createLanguage(new_Language, function (err, Language) {
    if (err) {
      res.send(err);
    } else {
      res.json(Language);
    }
  });
};
exports.read_a_Language = function (req, res) {
  var fetch_Language_master = req.params;
  Language.getLanguage(req.params, function (err, Language) {
    if (err) {

      res.send(err);
    } else {
      res.json(Language);
    }
  });
};