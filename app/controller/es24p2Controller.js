'use strict';

var ES24P2Entry = require('../model/es24p2Model.js');


exports.read_a_es24P2Rpt = function (req, res) {
  var ES24P2Entrypara = req.params;
  ES24P2Entry.getES24P2Rpt(ES24P2Entrypara, function (err, es24P2) {
    if (err) {
      res.send(err);
    } else {
      res.json(es24P2);
    }
  });
};
exports.create_a_es24p2RptEntry = function (req, res) {
  var es24RptEntry = new ES24P2Entry(req.body);

  ES24P2Entry.createES24P2Entry(es24RptEntry, function (err, es24) {
    if (err) {
      res.send(err);
    } else {
      res.json(es24);
    }
  });
};