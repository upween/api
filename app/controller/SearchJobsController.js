
'use strict';

var SearchJobs = require('../model/SearchJobsModel.js');

exports.create_a_SearchJobs = function (req, res) {
  var new_searchjobs = new SearchJobs(req.body);

  SearchJobs.createSearchJob(new_searchjobs, function (err, searchjobs) {
    if (err) {
      res.send(err);
    } else {
      res.json(searchjobs);
      //console.log(searchjobs);
    }
  });
};