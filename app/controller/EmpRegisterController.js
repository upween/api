'use strict';

var EmpRegister = require('../model/EmpRegisterModel.js');

exports.create_a_EmpRegister = function (req, res) {
  var CI_Pro_JF_Emp_Register = new EmpRegister(req.body);

  EmpRegister.createEmpRegister(CI_Pro_JF_Emp_Register, function (err, EmpRegister) {
    if (err) {
      res.send(err);
    } else {
      res.json(EmpRegister);
    }
  });
};

exports.read_a_Empuser = function (req, res) {
  var CI_Pro_JF_Emp_Register = new EmpRegister(req.body);

  EmpRegister.getEmpRegbyentry(req.params, function (err, EmpRegister) {
    if (err) {
      res.send(err);
    } else {
      res.json(EmpRegister);
    }
  });
};

exports.create_a_JobSeekerRegister = function (req, res) {
  var CI_Pro_JF_Jobseeker_Register = new EmpRegister(req.body);

  EmpRegister.createJobSeekerRegister(CI_Pro_JF_Jobseeker_Register, function (err, JobseekerRegister) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobseekerRegister);
    }
  });
};
