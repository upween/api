'use strict';

var CWCount = require('../model/CategoryWiseSetPostCountNModel.js');

exports.create_a_CWCount = function (req, res) {
  var CWCountEmp = new CWCount(req.body);

  CWCount.createCategoryWiseSetPostCountN(CWCountEmp, function (err, CWCount) {
    if (err) {
      res.send(err);
    } else {
      res.json(CWCount);
    }
  });
};