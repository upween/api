'use strict';

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _config = require('../../config.json');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : { default: obj };
}

var ValidateempLogin = require('../model/validateemployerloginModel.js');

exports.read_a_validateemplogin = function (req, res) {
  var fetch_validateemplogin = req.params;
  ValidateempLogin.getValidateEmpLogin(req.params, function (err, result) {

    if (err) {
      res.send(err);
    } else {
      var error = false;
      var userDetails = null;
      //console.log(result)
      if (result.length) {
        if (result[0].length) {
          userDetails = result[0][0];
          if (userDetails.ReturnValue == 2) {
            error = true;
          }
        } else {
          error = true;
        }
      } else {
        error = true;
      }
      if (error) {
        res.status(200).send({
          error: true,
          result: {
            userDetails: userDetails
          }
        });
      } else {
        var tokenObject = JSON.parse(JSON.stringify(userDetails));
        var refreshToken = _jsonwebtoken2.default.sign(tokenObject, _config2.default.refreshToken.secret, {
          expiresIn: _config2.default.refreshToken.expiresIn
        });
        var token = _jsonwebtoken2.default.sign(tokenObject, _config2.default.token.secret, {
          expiresIn: _config2.default.token.expiresIn
        });
        res.status(200).send({
          error: false,
          result: {
            userDetails: userDetails,
            token: token,
            refreshToken: refreshToken
          }
        });
      }
    }
  });
};

// exports.read_a_EmpForgotPassword = function(req, res) {
//   var validate_Emp_ForgotPassword= req.params;
//   ValidateempLogin.getEmpForgotPassword(req.params, function(err, EmpForgotPassword) {
//     if (err){
//       res.send(err);
//     }
//     else{
//     res.json(EmpForgotPassword);
//     }
//   });
// };

exports.read_a_EmpForgotPassword = function (req, res) {

  var validate_Emp_ForgotPassword = req.params;
  ValidateempLogin.getEmpForgotPassword(req.params, function (err, EmpForgotPassword) {
    if (err) {
      res.send(err);
    } else {
      res.json(EmpForgotPassword);
    }
  });
};