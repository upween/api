'use strict';

var State = require('../model/stateModel.js');

exports.create_a_state = function (req, res) {
  var new_state = new State(req.body);

  State.createState(new_state, function (err, state) {
    if (err) {
      res.send(err);
    } else {
      res.json(state);
    }
  });
};
exports.read_a_state = function (req, res) {
  var fetch_state = req.params;
  State.getState(req.params, function (err, state) {
    if (err) {
      res.send(err);
    } else {
      res.json(state);
    }
  });
};
exports.read_a_divisionbystate = function (req, res) {
  var fetch_state = req.params;
  State.getDivisionByState(req.params, function (err, state) {
    if (err) {
      res.send(err);
    } else {
      res.json(state);
    }
  });
};