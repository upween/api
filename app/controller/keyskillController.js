'use strict';

var Keyskill = require('../model/keyskillModel.js');

exports.create_a_keyskill = function (req, res) {
  var new_keyskill = new Keyskill(req.body);

  Keyskill.createKeyskill(new_keyskill, function (err, keyskill) {
    if (err) {
      res.send(err);
    } else {
      res.json(keyskill);
    }
  });
};
exports.read_a_keyskill = function (req, res) {

  var fetch_keyskill = req.params;
  Keyskill.getKeyskill(req.params, function (err, keyskill) {
    if (err) {
      res.send(err);
    } else {
      res.json(keyskill);
    }
  });
};
exports.read_delete_keyskill = function (req, res) {

  var delete_keyskill = req.params;
  Keyskill.getDeleteSKill(req.params, function (err, keyskill) {
    if (err) {
      res.send(err);
    } else {
      res.json(keyskill);
    }
  });
};