'use strict';

var Industry = require('../model/industryModel.js');

exports.create_a_industry = function (req, res) {
  var new_industry = new Industry(req.body);

  Industry.createIndustry(new_industry, function (err, industry) {
    if (err) {
      res.send(err);
    } else {
      res.json(industry);
    }
  });
};
exports.read_a_industry = function (req, res) {
  var fetch_industry = req.params;
  Industry.getIndustry(req.params, function (err, industry) {
    if (err) {
      res.send(err);
    } else {
      res.json(industry);
    }
  });
};