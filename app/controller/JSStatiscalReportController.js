'use strict';

var JSStatiscalReport = require('../model/JSStatiscalReportModel.js');

exports.read_a_JSStatiscalReport = function (req, res) {

  var Fetch_JS_StatiscalReport = req.params;
  JSStatiscalReport.getJSStatiscalReport(req.params, function (err, JSStatiscalReport) {
    if (err) {
      res.send(err);
    } else {
      res.json(JSStatiscalReport);
    }
  });
};