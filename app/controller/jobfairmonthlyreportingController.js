

'use strict';

var JobFairReporting = require('../model/jobfairmonthlyreportingModel.js');

exports.create_a_JobFairReporting = function (req, res) {
  var new_jobfair = new JobFairReporting(req.body);

  JobFairReporting.createJobFairReporting(new_jobfair, function (err, jobfair) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobfair);
    }
  });
};



exports.read_a_JFRep_ByMonthConsolidate = function (req, res) {
  var CI_Pro_JFRep_ByMonthConsolidate = req.params;
  JobFairReporting.getJFRep_ByMonthConsolidate(req.params, function (err, JFRep_ByMonthConsolidate) {
    if (err) {
      res.send(err);
    } else {
      res.json(JFRep_ByMonthConsolidate);
    }
  });
};