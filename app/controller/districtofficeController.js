'use strict';

var Districtoffice = require('../model/districtofficeModel.js');

exports.read_a_districtoffice = function (req, res) {
  Districtoffice.getDistrictOffice(function (err, districtoffice) {

    //console.log('controller')
    if (err) {
      res.send(err);
    } else {
      //console.log('res', districtoffice);
      res.send(districtoffice);
    }
  });
};
exports.read_DitrictOfficeDetail = function (req, res) {
  var fetch_districtoffice_detail = req.params;
  Districtoffice.getDitrictOfficeDetail(req.params, function (err, districtoffice) {
    if (err) {
      res.send(err);
    } else {
      res.json(districtoffice);
    }
  });
};

exports.create_a_UpdDistrictOffice = function(req, res) {
  var updDistrictOfficecallcenter = new Districtoffice(req.body);
 
  Districtoffice.createUpdDistrictOffice(updDistrictOfficecallcenter, function(err, UpdDistrictOffice) {
      if (err){
        res.send(err);
      }
      else{
      res.json(UpdDistrictOffice);
      }
    });
  
};


exports.read_CallCentreManager = function (req, res) {
  var Fetch_CallCentreManager = req.params;
  Districtoffice.getCallCentreManager(req.params, function (err, CallCentreManager) {
    if (err) {
      res.send(err);
    } else {
      res.json(CallCentreManager);
    }
  });
};

exports.read_CCallCentreManager_PlacementDetails = function (req, res) {
  var Fetch_CallCentreManager_PlacementDetails = req.params;
  Districtoffice.getCCallCentreManager_PlacementDetails(req.params, function (err, CallCentreManager_PlacementDetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(CallCentreManager_PlacementDetails);
    }
  });
};