'use strict';

var LRCurrentStatus = require('../model/LRCurrentStatusModel.js');

exports.read_a_LRCurrentStatus = function (req, res) {

  var CI_Pro_Rpt_getLRCurrentStatus = req.params;
  LRCurrentStatus.getLRCurrentStatus(req.params, function (err, LRCurrentStatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(LRCurrentStatus);
    }
  });
};