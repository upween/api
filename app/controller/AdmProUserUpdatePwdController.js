'use strict';

var AdmProUserUpdatePwd = require('../model/AdmProUserUpdatePwdModel.js');

exports.create_a_AdmProUserUpdatePwd = function (req, res) {
  var Adm_Pro_User_UpdatePwd = new AdmProUserUpdatePwd(req.body);

  AdmProUserUpdatePwd.createAdmProUserUpdatePwd(Adm_Pro_User_UpdatePwd, function (err, AdmProUserUpdatePwd) {
    if (err) {
      res.send(err);
    } else {
      res.json(AdmProUserUpdatePwd);
    }
  });
};