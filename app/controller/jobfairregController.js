'use strict';

var JobFairReg = require('../model/jobfairregModel.js');

exports.create_a_jobfairreg = function (req, res) {
  var new_jobfairreg = new JobFairReg(req.body);

  JobFairReg.createJobFairReg(new_jobfairreg, function (err, jobfairreg) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobfairreg);
    }
  });
};