'use strict';

var placementdetails = require('../model/placementdetailsModel.js');

exports.create_a_placementdetails = function (req, res) {
  var InsUpd_placement_details = new placementdetails(req.body);

  placementdetails.createplacementdetails(InsUpd_placement_details, function (err, Placement) {
    if (err) {
      res.send(err);
    } else {
      res.json(Placement);
    }
  });
};

exports.read_a_placementdetails = function (req, res) {

  var fetch_placement_details = req.params;
  placementdetails.getplacementdetails(req.params, function (err, placementdetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(placementdetails);
    }
  });
};
exports.create_a_verifyplacementdetails = function (req, res) {
  var Verify_placement_details = new placementdetails(req.body);

  placementdetails.createverifyplacementdetails(Verify_placement_details, function (err, Placementverify) {
    if (err) {
      res.send(err);
    } else {
      res.json(Placementverify);
    }
  });
};
exports.create_a_placementdetailsexcel = function (req, res) {
  var InsUpd_placement_detailsexcel = new placementdetails(req.body);

  placementdetails.createplacementdetailsexcel(InsUpd_placement_detailsexcel, function (err, Placement) {
    if (err) {
      res.send(err);
    } else {
      res.json(Placement);
    }
  });
};


exports.read_a_placementdetailsCount = function (req, res) {

  var Fetch_DistrictWisePlacementReport = req.params;
  placementdetails.getplacementdetailsCount(req.params, function (err, placementdetailsCount) {
    if (err) {
      res.send(err);
    } else {
      res.json(placementdetailsCount);
    }
  });
};