'use strict';

var YearWiseLRStatus = require('../model/YearwiselrstatusModel.js');

exports.read_a_YearWiseLRStatus = function (req, res) {
  YearWiseLRStatus.getYearWiseLRStatus(req.params, function (err, YearWiseLRStatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(YearWiseLRStatus);
    }
  });
};