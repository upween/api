'use strict';

var Statewisegenderdetail = require('../model/statewisegenderdetailModel.js');

exports.read_a_Statewisegenderdetail = function (req, res) {
  Statewisegenderdetail.getStatewisegenderdetail(req.params, function (err, Statewisegenderdetail) {
    if (err) {
      res.send(err);
    } else {
      res.json(Statewisegenderdetail);
    }
  });
};