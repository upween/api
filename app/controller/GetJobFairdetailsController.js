'use strict';

var GetJobFairdetails = require('../model/GetJobFairdetailsModel.js');

exports.read_a_GetJobFairdetails = function (req, res) {

  var DashBoard_USP_GetJobfairStatus = req.params;
  GetJobFairdetails.getGetJobFairdetails(req.params, function (err, GetJobFairdetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(GetJobFairdetails);
    }
  });
};