'use strict';

var TypingInstitute = require('../model/typingInstituteModel.js');

exports.read_a_TypingInstitute = function (req, res) {
  var read_a_TypingInstitute = req.params;
  TypingInstitute.getTypingInstitute(req.params, function (err, TypingInstitute) {
    if (err) {
      res.send(err);
    } else {
      res.json(TypingInstitute);
    }
  });
};