'use strict';

var MstDivision = require('../model/MstDivisionModel.js');

exports.read_a_MstDivision = function (req, res) {

  var fetch_Adm_Mst_Division = req.params;
  MstDivision.getMstDivision(req.params, function (err, MstDivision) {
    if (err) {
      res.send(err);
    } else {
      res.json(MstDivision);
    }
  });
};