'use strict';

var Resume = require('../model/jobseekerResumeModel.js');

exports.create_a_Resume = function (req, res) {
  var new_Resume = new Resume(req.body);

  Resume.createResume(new_Resume, function (err, Resume) {
    if (err) {
      res.send(err);
    } else {
      res.json(Resume);
    }
  });
};
exports.read_a_Resume = function (req, res) {
  var fetch_Resume = req.params;
  Resume.getResume(req.params, function (err, Resume) {
    if (err) {
      res.send(err);
    } else {
      res.json(Resume);
    }
  });
};
exports.read_a_Profile = function (req, res) {
  var fetch_Profile = req.params;
  Resume.getProfile(req.params, function (err, Profile) {
    if (err) {
      res.send(err);
    } else {
      res.json(Profile);
    }
  });
};
exports.read_a_DeleteResume = function (req, res) {
  var fetch_DeleteResume = req.params;
  Resume.getDeleteResume(req.params, function (err, DeleteResume) {
    if (err) {
      res.send(err);
    } else {
      res.json(DeleteResume);
    }
  });
};