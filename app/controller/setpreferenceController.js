'use strict';

var SetPreference = require('../model/setpreferenceModel.js');

exports.create_a_setpreference = function (req, res) {
  var new_setpreference = new SetPreference(req.body);

  SetPreference.createSetPreference(new_setpreference, function (err, setpreference) {
    if (err) {
      res.send(err);
    } else {
      res.json(setpreference);
    }
  });
};
exports.read_a_setpreference = function (req, res) {
  var fetch_setpreference = req.params;

  SetPreference.getSetPreference(req.params, function (err, setpreference) {
    if (err) {
      res.send(err);
    } else {
      res.json(setpreference);
    }
  });
};