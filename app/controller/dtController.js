'use strict';

var dtCandidate = require('../model/dtModel.js');

exports.read_a_dtCandidate = function (req, res) {

    dtCandidate.getdtCandidatedetails(req.params, function (err, candidate) {
    if (err) {
      res.send(err);
    } else {
      res.json(candidate);
    }
  });
};
exports.read_a_dtJobfair= function (req, res) {

  dtCandidate.getdtjobfairdetails(req.params, function (err, jf) {
  if (err) {
    res.send(err);
  } else {
    res.json(jf);
  }
});
};
exports.read_a_dtJobfairEmp= function (req, res) {

  dtCandidate.getdtjobfairemp(req.params, function (err, jfemp) {
  if (err) {
    res.send(err);
  } else {
    res.json(jfemp);
  }
});
};
exports.read_a_dtEmpvacancycount= function (req, res) {

  dtCandidate.getdtempvacancycount(req.params, function (err, jfemp) {
  if (err) {
    res.send(err);
  } else {
    res.json(jfemp);
  }
});
};

exports.read_a_yearwisecandidate= function (req, res) {

  dtCandidate.getyearwisecandidate(req.params, function (err, candidate) {
  if (err) {
    res.send(err);
  } else {
    res.json(candidate);
  }
});
};

