'use strict';

var LIveRegisterReport = require('../model/LIveRegisterReportModel.js');

exports.read_a_LIveRegisterReport = function (req, res) {

  var Jobseeker_LIveRegister_Report = req.params;
  LIveRegisterReport.getLIveRegisterReport(req.params, function (err, LIveRegisterReport) {
    if (err) {
      res.send(err);
    } else {
      res.json(LIveRegisterReport);
    }
  });
};