
'use strict';

var Typing = require('../model/typingstenoModel.js');

exports.create_a_Typing = function (req, res) {
  var new_Typing = new Typing(req.body);

  Typing.createTyping(new_Typing, function (err, Typing) {
    if (err) {
      res.send(err);
    } else {
      res.json(Typing);
    }
  });
};
exports.read_a_Typing = function (req, res) {
  var fetch_TypingSteno_master = req.params;
  Typing.getTyping(req.params, function (err, Typing) {
    if (err) {
      res.send(err);
    } else {
      res.json(Typing);
    }
  });
};