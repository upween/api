'use strict';

var officetype = require('../model/officetypeModel.js');

exports.create_a_officetype = function (req, res) {
  var new_officetype = new officetype(req.body);

  officetype.createofficetype(new_officetype, function (err, officetype) {
    if (err) {
      res.send(err);
    } else {
      res.json(officetype);
    }
  });
};
exports.read_a_officetype = function (req, res) {

  var fetch_adm_exch_mst_officetype = req.params;
  officetype.getofficetype(req.params, function (err, officetype) {
    if (err) {
      res.send(err);
    } else {
      res.json(officetype);
    }
  });
};