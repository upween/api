'use strict';

var SelectedCandJobFair = require('../model/SelectedCandJobFairModel.js');

exports.create_a_SelectedCandJobFair = function (req, res) {
  var InsUpd_Selected_Candidate_inJobFair = new SelectedCandJobFair(req.body);

  SelectedCandJobFair.createSelectedCandJobFair(InsUpd_Selected_Candidate_inJobFair, function (err, SelectedCandJobFair) {
    if (err) {
      res.send(err);
    } else {
      res.json(SelectedCandJobFair);
    }
  });
};