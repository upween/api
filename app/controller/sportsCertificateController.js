'use strict';

var SportsCertificate = require('../model/sportsCertificateModel.js');

exports.read_a_SportsCertificate = function (req, res) {
  var read_a_SportsCertificate = req.params;
  SportsCertificate.getSportsCertificate(req.params, function (err, SportsCertificate) {
    if (err) {
      res.send(err);
    } else {
      res.json(SportsCertificate);
    }
  });
};