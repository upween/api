'use strict';

var District = require('../model/districtModel.js');

exports.create_a_district = function (req, res) {
  var new_district = new District(req.body);

  District.createDistrict(new_district, function (err, district) {
    if (err) {
      res.send(err);
    } else {
      res.json(district);
    }
  });
};
exports.read_a_district = function (req, res) {
  var fetch_district = req.params;
  District.getDistrict(req.params, function (err, district) {
    if (err) {
      res.send(err);
    } else {
      res.json(district);
    }
  });
};