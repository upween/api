
'use strict';

var Nationality = require('../model/NationalityModel.js');

exports.read_a_Nationality = function (req, res) {
  var fetch_Nationality_master = req.params;
  Nationality.getNationality(req.params, function (err, Nationality) {
    if (err) {
      res.send(err);
    } else {
      res.json(Nationality);
    }
  });
};
exports.create_a_Nationality = function (req, res) {
  var new_Nationality = new Nationality(req.body);

  Nationality.createNationality(new_Nationality, function (err, Nationality) {
    if (err) {
      res.send(err);
    } else {
      res.json(Nationality);
    }
  });
};