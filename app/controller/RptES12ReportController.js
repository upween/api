'use strict';
var ES12Rpt  = require('../model/RptES12ReportModel.js');


exports.read_a_ES12Annual = function(req, res) {
    var Fetch_ES12Annual= req.params;
    ES12Rpt.getES12Annual(req.params, function(err, ES12Rpt) {
      if (err){
        res.send(err);
      }
      else{
      res.json(ES12Rpt);
      }
    });
  };
  exports.read_a_NCOCodeavail = function(req, res) {
    var NCOCodeavail= req.params;
    ES12Rpt.getNCOCodeavail(req.params, function(err, NCOCodeavail) {
      if (err){
        res.send(err);
      }
      else{
      res.json(NCOCodeavail);
      }
    });
  };
  exports.create_a_RptES12 = function (req, res) {
    var ES12EntryRpt = new ES12Rpt(req.body);
  
    ES12Rpt.createES12Annual(ES12EntryRpt, function (err, ES12Rpt) {
      if (err) {
        res.send(err);
      } else {
        res.json(ES12Rpt);
      }
    });
  };