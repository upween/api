'use strict';

var RptCIJSX63 = require('../model/rptCIJSX63Model.js');

exports.read_a_RptCIJSX63 = function (req, res) {
  RptCIJSX63.getRptCIJSX63(req.params, function (err, RptCIJSX63) {
    if (err) {
      res.send(err);
    } else {
      res.json(RptCIJSX63);
    }
  });
};

exports.read_a_ReportX63 = function (req, res) {
  var Report_X63 = req.params;

  RptCIJSX63.getReportX63(req.params, function (err, ReportX63) {
    if (err) {
      res.send(err);
    } else {
      res.json(ReportX63);
    }
  });
};