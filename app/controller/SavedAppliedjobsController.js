'use strict';

var SavedAppliedjobs = require('../model/SavedAppliedjobsModel.js');

exports.create_a_SavedAppliedjobs = function (req, res) {
  var newSavedAppliedjobs = new SavedAppliedjobs(req.body);

  SavedAppliedjobs.createSavedAppliedjobs(newSavedAppliedjobs, function (err, SavedAppliedjobs) {
    if (err) {
      res.send(err);
    } else {
      res.json(SavedAppliedjobs);
    }
  });
};

exports.read_a_SavedAppliedjobs = function (req, res) {
  var fetch_Saved_appliedjobs = req.params;
  SavedAppliedjobs.getSavedAppliedjobs(req.params, function (err, SavedAppliedjobs) {
    if (err) {
      res.send(err);
    } else {
      res.json(SavedAppliedjobs);
    }
  });
};