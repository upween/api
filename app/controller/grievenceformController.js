'use strict';

var Grievenceform = require('../model/grievenceformModel.js');

exports.create_a_grievenceform = function (req, res) {
  var new_grievenceform = new Grievenceform(req.body);

  Grievenceform.createGrievenceform(new_grievenceform, function (err, grievenceform) {
    if (err) {
      res.send(err);
    } else {
      res.json(grievenceform);
    }
  });
};
exports.read_a_grievenceform = function (req, res) {
  var fetch_grievenceform = req.params;
  Grievenceform.getGrievenceform(req.params, function (err, grievenceform) {
    if (err) {
      res.send(err);
    } else {
      res.json(grievenceform);
    }
  });
};