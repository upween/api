'use strict';

var ProfileStatus = require('../model/profileStatusModel.js');

exports.read_a_ProfileStatus = function (req, res) {

  var Fetch_JobSeeker_ProfileStatus = req.params;
  ProfileStatus.getProfileStatus(req.params, function (err, ProfileStatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(ProfileStatus);
    }
  });
};