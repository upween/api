'use strict';

var monthlytotal = require('../model/monthlytotalModel.js');

exports.create_a_monthlytotal = function (req, res) {
  var new_InsUpd_ci_jf_jobfair_monthly_totalhlytotal = new monthlytotal(req.body);

  monthlytotal.createmonthlytotal(new_InsUpd_ci_jf_jobfair_monthly_totalhlytotal, function (err, monthlytotal) {
    if (err) {
      res.send(err);
    } else {
      res.json(monthlytotal);
    }
  });
};
exports.read_a_monthlytotal = function (req, res) {

  var Fetch_ci_jf_jobfair_monthly_total = req.params;
  monthlytotal.getmonthlytotal(req.params, function (err, monthlytotal) {
    if (err) {
      res.send(err);
    } else {
      res.json(monthlytotal);
    }
  });
};