'use strict';

var jobseeker_verification = require('../model/jobseeker_verificationModal.js');

exports.create_a_jobseeker_verification = function (req, res) {
  var new_jobseeker_verification = new jobseeker_verification(req.body);

  jobseeker_verification.createjobseeker_verification(new_jobseeker_verification, function (err, jobseeker_verification) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobseeker_verification);
    }
  });
};
exports.read_a_JobSeekerVerification = function (req, res) {

  jobseeker_verification.getJobSeekerVerification(req.params, function (err, JobSeekerVerification) {
    if (err) {
      res.send(err);
    } else {

      res.json(JobSeekerVerification);
    }
  });
};