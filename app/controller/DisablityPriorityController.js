
'use strict';

var DisablityPriority = require('../model/DisablityPriorityModel.js');

exports.read_a_DisablityPriority = function (req, res) {
  var fetch_DisablityPriority_master = req.params;
  DisablityPriority.getDisablityPriority(req.params, function (err, DisablityPriority) {
    if (err) {
      res.send(err);
    } else {
      res.json(DisablityPriority);
    }
  });
};