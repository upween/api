'use strict';

var QualififcationWiseDetail = require('../model/QualififcationWiseDetailModel.js');

exports.read_a_QualififcationWiseDetail = function (req, res) {

  var Fetch_Qualififcation_Wise_JSDetail = req.params;
  QualififcationWiseDetail.getQualififcationWiseDetail(req.params, function (err, QualififcationWiseDetail) {
    if (err) {
      res.send(err);
    } else {

      res.json(QualififcationWiseDetail);
    }
  });
};