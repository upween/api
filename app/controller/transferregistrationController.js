'use strict';

var TransferRegistration = require('../model/transferregistrationModel.js');

exports.create_a_TransferRegistration = function (req, res) {
  var new_TransferRegistration = new TransferRegistration(req.body);

  TransferRegistration.createTransferRegistration(new_TransferRegistration, function (err, TransferRegistration) {
    if (err) {
      res.send(err);
    } else {
      res.json(TransferRegistration);
    }
  });
};