'use strict';

var Qualification = require('../model/qualificationLevelModel.js');

exports.read_a_Qualification = function (req, res) {
  var fetch_qualification_level = req.params;
  Qualification.getQualification(req.params, function (err, Qualification) {
    if (err) {
      res.send(err);
    } else {
      res.json(Qualification);
    }
  });
};

exports.create_a_Qualificatione = function (req, res) {
  var InsUpd_adm_js_mst_qualification_level = new Qualification(req.body);

  Qualification.createQualification(InsUpd_adm_js_mst_qualification_level, function (err, Qualification) {
    if (err) {
      res.send(err);
    } else {
      res.json(Qualification);
    }
  });
};

// exports.create_a_QualificationLevel = function (req, res) {
//   var InsUpd_adm_js_mst_qualification_level = new ActivityMaster(req.body);

//   ActivityMaster.createQualification(InsUpd_adm_js_mst_qualification_level, function (err, qualification_level) {
//     if (err) {
//       res.send(err);
//     } else {
//       res.json(qualification_level);
//     }
//   });
// };


exports.read_a_JS_Rpt_AgeWise_QualifName = function (req, res) {
  var CI_Pro_JS_Rpt_AgeWise_QualifName = req.params;
  Qualification.getJS_Rpt_AgeWise_QualifName(req.params, function (err, JS_Rpt_AgeWise_QualifName) {
    if (err) {
      res.send(err);
    } else {
      res.json(JS_Rpt_AgeWise_QualifName);
    }
  });
};
exports.read_a_JS_Rpt_AgeWise_QualifLevel = function (req, res) {
  var CI_Pro_JS_Rpt_AgeWise_QualifName = req.params;
  Qualification.getJS_Rpt_AgeWise_QualifLevel(req.params, function (err, JS_Rpt_AgeWise_QualifLevel) {
    if (err) {
      res.send(err);
    } else {
      res.json(JS_Rpt_AgeWise_QualifLevel);
    }
  });
};