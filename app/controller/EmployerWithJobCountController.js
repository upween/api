'use strict';

var EmployerWithJobCount = require('../model/EmployerWithJobCountModel.js');

exports.read_a_EmployerWithJobCount = function (req, res) {

  var Fetch_EmployerWithJobCount = req.params;
  EmployerWithJobCount.getEmployerWithJobCount(req.params, function (err, EmployerWithJobCount) {
    if (err) {
      res.send(err);
    } else {
      res.json(EmployerWithJobCount);
    }
  });
};