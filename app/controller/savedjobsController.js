'use strict';

var savedjobs = require('../model/savedjobsModel.js');

exports.create_a_savedjobs = function (req, res) {
  var insupd_saved_jobs = new savedjobs(req.body);

  savedjobs.createsavedjobs(insupd_saved_jobs, function (err, savedjobs) {
    if (err) {
      res.send(err);
    } else {
      res.json(savedjobs);
    }
  });
};