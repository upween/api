'use strict';

var Duplicateregstatus = require('../model/DuplicateregstatusModel.js');

exports.read_a_Duplicateregstatus = function (req, res) {
  Duplicateregstatus.getDuplicateregstatus(req.params, function (err, Duplicateregstatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(Duplicateregstatus);
    }
  });
};