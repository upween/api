
'use strict';

var subjectgroup = require('../model/subjectgroupModel.js');

exports.read_a_subjectgroup = function (req, res) {
  var fetch_subject_group = req.params;
  subjectgroup.getsubjectgroup(req.params, function (err, subjectgroup) {
    if (err) {
      res.send(err);
    } else {
      res.json(subjectgroup);
    }
  });
};