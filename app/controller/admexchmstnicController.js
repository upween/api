'use strict';

var admexchmstnic = require('../model/admexchmstnicModel.js');

exports.read_a_admexchmstnic = function (req, res) {

  var fetch_adm_exch_mst_nic = req.params;
  admexchmstnic.getadmexchmstnic(req.params, function (err, admexchmstnic) {
    if (err) {
      res.send(err);
    } else {
      res.json(admexchmstnic);
    }
  });
};

exports.create_a_indexnote = function (req, res) {
  var InsUpd_index_note = new admexchmstnic(req.body);

  admexchmstnic.createindexnote(InsUpd_index_note, function (err, indexnote) {
    if (err) {
      res.send(err);
    } else {
      res.json(indexnote);
    }
  });
};

exports.read_a_indexnote = function (req, res) {

  var Fetch_index_note = req.params;
  admexchmstnic.getindexnote(req.params, function (err, indexnote) {
    if (err) {
      res.send(err);
    } else {
      res.json(indexnote);
    }
  });
};