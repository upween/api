'use strict';
var categorywise  = require('../model/categorywisequalificationreportModel.js');



exports.read_a_JS_Rpt_CategoryWise_QualifName= function(req, res) {
    var CI_Pro_JS_Rpt_CategoryWise_QualifName = req.params;
    categorywise.getJS_Rpt_CategoryWise_QualifName(req.params, function (err,Rpt_CategoryWise_QualifName) {
      if (err) {
        res.send(err);
      } else {
        res.json(Rpt_CategoryWise_QualifName);
      }
    });
  };
  
  exports.read_a_JS_Rpt_CategoryWise_QualifLevel= function(req, res) {
    var CI_Pro_JS_Rpt_CategoryWise_QualifLevel = req.params;
    categorywise.getJSCI_Pro_JS_Rpt_CategoryWise_QualifLevel(req.params, function (err,Rpt_CategoryWise_QualifLevel) {
      if (err) {
        res.send(err);
      } else {
        res.json(Rpt_CategoryWise_QualifLevel);
      }
    });
  };
  

  