
'use strict';

var subjectnew = require('../model/subjectnewModel.js');

exports.read_a_subjectnew = function (req, res) {
  var fetch_subject = req.params;
  subjectnew.getsubjectnew(req.params, function (err, subjectnew) {
    if (err) {
      res.send(err);
    } else {
      res.json(subjectnew);
    }
  });
};