'use strict';

var currentstatusrt = require('../model/currentstatusrtModel.js');

exports.read_a_currentstatusrt = function (req, res) {
  currentstatusrt.getcurrentstatusrt(req.params, function (err, currentstatusrt) {
    if (err) {
      res.send(err);
    } else {
      res.json(currentstatusrt);
    }
  });
};