'use strict';

var HCsubCategory = require('../model/HCsubCategoryModel.js');

exports.read_a_HCsubCategory = function (req, res) {
  var read_a_HCsubCategory = req.params;
  HCsubCategory.getHCsubCategory(req.params, function (err, HCsubCategory) {
    if (err) {
      res.send(err);
    } else {
      res.json(HCsubCategory);
    }
  });
};

exports.create_a_HCsubCategory = function (req, res) {
  var InsUpd_HC_subcategory_master = new HCsubCategory(req.body);

  HCsubCategory.createHCsubCategory(InsUpd_HC_subcategory_master, function (err, HCsubCategory) {
    if (err) {
      res.send(err);
    } else {
      res.json(HCsubCategory);
    }
  });
};