'use strict';

var fileuploaddtls = require('../model/fileuploaddtlsModel.js');

exports.create_a_fileuploaddtls = function (req, res) {
  var InsUpd_ci_adv_fileuploaddtls = new fileuploaddtls(req.body);

  fileuploaddtls.createfileuploaddtls(InsUpd_ci_adv_fileuploaddtls, function (err, fileuploaddtls) {
    if (err) {
      res.send(err);
    } else {
      res.json(fileuploaddtls);
    }
  });
};
exports.read_a_fileuploaddtls = function (req, res) {

  var Fetch_ci_adv_fileuploaddtls = req.params;
  fileuploaddtls.getfileuploaddtls(req.params, function (err, fileuploaddtls) {
    if (err) {
      res.send(err);
    } else {
      res.json(fileuploaddtls);
    }
  });
};