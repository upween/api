'use strict';

var Duplicatereg = require('../model/DuplicateregModel.js');

exports.read_a_Duplicatereg = function (req, res) {

  var Duplicate_reg = req.params;
  Duplicatereg.getDuplicatereg(req.params, function (err, Duplicatereg) {
    if (err) {
      res.send(err);
    } else {
      res.json(Duplicatereg);
    }
  });
};