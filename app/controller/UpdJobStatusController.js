'use strict';

var UpdJobStatus = require('../model/UpdJobStatusModel.js');

exports.create_a_UpdJobStatus = function (req, res) {
  var JobStatusUpd = new UpdJobStatus(req.body);

  UpdJobStatus.createUpdJobStatus(JobStatusUpd, function (err, UpdJobStatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(UpdJobStatus);
    }
  });
};