'use strict';

var ccregjobseeker = require('../model/ccregjobseekerModel.js');

exports.create_a_ccregjobseeker = function (req, res) {
  var InsUpd_ci_cc_ccregjobseeker = new ccregjobseeker(req.body);

  ccregjobseeker.createccregjobseeker(InsUpd_ci_cc_ccregjobseeker, function (err, ccregjobseeker) {
    if (err) {
      res.send(err);
    } else {
      res.json(ccregjobseeker);
    }
  });
};