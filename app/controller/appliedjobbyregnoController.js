'use strict';

var appliedjobbyregno = require('../model/appliedjobbyregnoModel.js');

exports.read_a_appliedjobbyregno = function (req, res) {
  var fetch_appliedjobbyregno = req.params;
  appliedjobbyregno.getappliedjobbyregno(req.params, function (err, appliedjobbyregno) {
    if (err) {
      res.send(err);
    } else {
      res.json(appliedjobbyregno);
    }
  });
};