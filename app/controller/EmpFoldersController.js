'use strict';
var EmpFolders  = require('../model/EmpFoldersModel.js');

exports.read_a_EmpFolders = function (req, res) {
    var empfolder = req.params;
    EmpFolders.getEmpFolders(req.params, function (err,empfolder) {
      if (err) {
        res.send(err);
      } else {
        res.json(empfolder);
      }
    });
  };
  exports.create_a_EmpFolders = function(req, res) {
    var InsUpd_EmpFolders = new EmpFolders(req.body);
   
    EmpFolders.createEmpFolders(InsUpd_EmpFolders, function(err, EmpFolders) {
        if (err){
          res.send(err);
        }
        else{
          res.json(EmpFolders);
        }
      });
  };
  
  exports.read_a_DeleteEmpFolders = function (req, res) {
    var empfolder = req.params;
    EmpFolders.getDeleteEmpFolders(req.params, function (err,dlempfolder) {
      if (err) {
        res.send(err);
      } else {
        res.json(dlempfolder);
      }
    });
  };
