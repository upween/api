
'use strict';

var SkillSet = require('../model/skillSetModel.js');

exports.read_a_SkillSet = function (req, res) {
  var fetch_Employment_sector_SkillSet = req.params;
  SkillSet.getSkillSet(req.params, function (err, SkillSet) {
    if (err) {
      res.send(err);
    } else {
      res.json(SkillSet);
    }
  });
};