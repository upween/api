'use strict';

var Graduation = require('../model/graduationModel.js');

exports.create_a_graduation = function (req, res) {
  var new_graduation = new Graduation(req.body);

  Graduation.createGraduation(new_graduation, function (err, graduation) {
    if (err) {

      res.send(err);
    } else {
      res.json(graduation);
    }
  });
};
exports.read_a_graduation = function (req, res) {
  var fetch_graduation = req.params;
  Graduation.getGraduation(req.params, function (err, graduation) {
    if (err) {
      res.send(err);
    } else {
      res.json(graduation);
    }
  });
};