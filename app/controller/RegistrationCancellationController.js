'use strict';

var RegistrationCancellation = require('../model/RegistrationCancellationModel.js');

exports.create_a_RegistrationCancellation = function (req, res) {
  var Registration_Cancellation = new RegistrationCancellation(req.body);

  RegistrationCancellation.createRegistrationCancellation(Registration_Cancellation, function (err, fileuploaddtls) {
    if (err) {
      res.send(err);
    } else {
      res.json(RegistrationCancellation);
    }
  });
};