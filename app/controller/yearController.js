'use strict';

var Year = require('../model/yearModel.js');

exports.read_a_year = function (req, res) {
  var fetch_year = req.params;
  Year.getYear(req.params, function (err, year) {
    if (err) {
      res.send(err);
    } else {
      res.json(year);
    }
  });
};