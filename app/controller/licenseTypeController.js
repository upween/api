'use strict';

var LicenseType = require('../model/licenseTypeModel.js');

exports.read_a_LicenseType = function (req, res) {
  var read_a_LicenseType = req.params;
  LicenseType.getLicenseType(req.params, function (err, LicenseType) {
    if (err) {
      res.send(err);
    } else {
      res.json(LicenseType);
    }
  });
};