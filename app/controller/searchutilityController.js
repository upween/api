'use strict';

var Searchutility = require('../model/searchutilityModel.js');

exports.read_a_Searchutility = function (req, res) {
  Searchutility.getSearchutility(req.params, function (err, Searchutility) {
    if (err) {
      res.send(err);
    } else {
      res.json(Searchutility);
    }
  });
};