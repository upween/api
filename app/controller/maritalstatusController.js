'use strict';

var Maritalstatus = require('../model/maritalstatusModel.js');

exports.read_a_Maritalstatus = function (req, res) {
  var read_a_Maritalstatus = req.params;
  Maritalstatus.getMaritalstatus(req.params, function (err, Maritalstatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(Maritalstatus);
    }
  });
};

exports.create_a_Maritalstatus = function (req, res) {
  var InsUpd_Maritalstatus_master = new Maritalstatus(req.body);

  Maritalstatus.createMaritalstatus(InsUpd_Maritalstatus_master, function (err, Maritalstatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(Maritalstatus);
    }
  });
};