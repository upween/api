
'use strict';

var area = require('../model/areaModel.js');

exports.read_a_area = function (req, res) {
  var fetch_area_master = req.params;
  area.getarea(req.params, function (err, area) {
    if (err) {
      res.send(err);
    } else {
      res.json(area);
    }
  });
};