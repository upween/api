'use strict';

var Subject = require('../model/subjectModel.js');

exports.create_a_subject = function (req, res) {
  var new_subject = new Subject(req.body);

  Subject.createSubject(new_subject, function (err, subject) {
    if (err) {
      res.send(err);
    } else {
      res.json(subject);
    }
  });
};
exports.read_a_subject = function (req, res) {
  var fetch_Subjectmaster = req.params;
  Subject.getSubject(req.params, function (err, subject) {
    if (err) {
      res.send(err);
    } else {
      res.json(subject);
    }
  });
};