'use strict';

var Gradingsystem = require('../model/gradingsystemModel.js');

exports.read_a_gradingsystem = function (req, res) {
  var fetch_gradingsystem = req.params;
  Gradingsystem.getGradingsystem(req.params, function (err, gradingsystem) {
    if (err) {
      res.send(err);
    } else {
      res.json(gradingsystem);
    }
  });
};