'use strict';

var Course = require('../model/courseModel.js');

exports.create_a_course = function (req, res) {
  var new_course = new Course(req.body);

  Course.createCourse(new_course, function (err, course) {
    if (err) {
      res.send(err);
    } else {
      res.json(course);
    }
  });
};
exports.read_a_course = function (req, res) {
  var fetch_course = req.params;
  Course.getCourse(req.params, function (err, course) {
    if (err) {
      res.send(err);
    } else {
      res.json(course);
    }
  });
};