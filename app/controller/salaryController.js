
'use strict';

var salary = require('../model/salaryModel.js');

exports.read_a_salary = function (req, res) {
  var fetch_salary_master = req.params;
  salary.getsalary(req.params, function (err, salary) {
    if (err) {
      res.send(err);
    } else {
      res.json(salary);
    }
  });
};