'use strict';

var job_alert = require('../model/job_alertModel.js');

exports.create_a_job_alert = function (req, res) {
  var new_job_alert = new job_alert(req.body);

  job_alert.createjob_alert(new_job_alert, function (err, job_alert) {
    if (err) {
      res.send(err);
    } else {
      res.json(job_alert);
    }
  });
};