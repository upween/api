'use strict';

var AdminRegtype = require('../model/adminRegTypeModel.js');

exports.read_a_AdminRegtype = function (req, res) {
  var fetch_adm_js_mst_reg_type = req.params;
  AdminRegtype.getAdminRegtype(req.params, function (err, AdminRegtype) {
    if (err) {
      res.send(err);
    } else {
      res.json(AdminRegtype);
    }
  });
};