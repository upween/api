'use strict';

var Deadregister = require('../model/deadregisterModel.js');

exports.read_a_Deadregister = function (req, res) {
  Deadregister.getDeadregister(req.params, function (err, Deadregister) {
    if (err) {
      res.send(err);
    } else {
      res.json(Deadregister);
    }
  });
};