'use strict';

var Role = require('../model/roleModel.js');

exports.create_a_role = function (req, res) {
  var new_role = new Role(req.body);

  Role.createRole(new_role, function (err, role) {
    if (err) {
      res.send(err);
    } else {
      res.json(role);
    }
  });
};
exports.read_a_role = function (req, res) {

  var fetch_role = req.params;
  Role.getRole(req.params, function (err, role) {
    if (err) {
      res.send(err);
    } else {
      res.json(role);
    }
  });
};