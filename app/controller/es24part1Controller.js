'use strict';

var ES24P1Entry = require('../model/es24part1Model.js');


exports.read_a_es24P1Rpt = function (req, res) {
  var ES24P1Entrypara = req.params;
  ES24P1Entry.getES24P1Rpt(ES24P1Entrypara, function (err, es24p1) {
    if (err) {
      res.send(err);
    } else {
      res.json(es24p1);
    }
  });
};
exports.create_a_es24P1RptEntry = function (req, res) {
  var es24P1RptEntry = new ES24P1Entry(req.body);

  ES24P1Entry.createES24P1Entry(es24P1RptEntry, function (err, es24p1) {
    if (err) {
      res.send(err);
    } else {
      res.json(es24p1);
    }
  });
};