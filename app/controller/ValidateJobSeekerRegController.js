'use strict';

var ValidateJobSeekerReg = require('../model/ValidateJobSeekerRegModel.js');

exports.read_a_validateJobSeekerReg = function (req, res) {
  var validate_jobseeker_registration = req.params;
  ValidateJobSeekerReg.getValidateJobSeekerReg(req.params, function (err, validateJobSeekerReg) {
    if (err) {
      res.send(err);
    } else {
      res.json(validateJobSeekerReg);
    }
  });
};
exports.create_a_ChangePassword = function (req, res) {
  var new_changepassword = new ValidateJobSeekerReg(req.body);

  ValidateJobSeekerReg.createChangePassword(new_changepassword, function (err, validateJobSeekerReg) {
    if (err) {
      res.send(err);
    } else {
      res.json(validateJobSeekerReg);
    }
  });
  //}
};