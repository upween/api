'use strict';

var Interestedcandidatejf = require('../model/InterestedcandidatejfModel.js');

exports.read_a_Interestedcandidatejf = function (req, res) {
  var Fetch_Interestedcandidate_jf = req.params;
  Interestedcandidatejf.getInterestedcandidatejf(req.params, function (err, Interestedcandidatejf) {
    if (err) {
      res.send(err);
    } else {
      res.json(Interestedcandidatejf);
    }
  });
};

exports.create_a_Interestedcandidatejf = function (req, res) {
  var Ins_interestedcandidate_jf = new Interestedcandidatejf(req.body);

  Interestedcandidatejf.createInterestedcandidatejf(Ins_interestedcandidate_jf, function (err, Interestedcandidatejf) {
    if (err) {
      res.send(err);
    } else {
      res.json(Interestedcandidatejf);
    }
  });
};