'use strict';

var Transfer = require('../model/transferModel.js');

exports.read_a_Transfer = function (req, res) {
  Transfer.getTransfer(req.params, function (err, Transfer) {
    if (err) {
      res.send(err);
    } else {
      res.json(Transfer);
    }
  });
};