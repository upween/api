'use strict';
var searchcandidatereports  = require('../model/SearchCandidateReportsModel.js');

exports.read_a_JSsavedsearch = function (req, res) {
    var savedsearch = req.params;
    searchcandidatereports.getJSsavedsearch(req.params, function (err,savedsearch) {
      if (err) {
        res.send(err);
      } else {
        res.json(savedsearch);
      }
    });
  };
  exports.create_a_savedsearch = function(req, res) {
    var savedsearch = new searchcandidatereports(req.body);
   
    searchcandidatereports.createsavedsearch(savedsearch, function(err, savedsearch) {
        if (err){
          res.send(err);
        }
        else{
          res.json(savedsearch);
        }
      });
  };
  

  exports.create_a_CandidateSearch = function (req, res) {
    var new_candidatejobfair = new searchcandidatereports(req.body);
    searchcandidatereports.createCandidateSearch(new_candidatejobfair, function (err, CandidateSearch) {
      if (err) {
        res.send(err);
      } else {
        res.json(CandidateSearch);
      }
    });
  };