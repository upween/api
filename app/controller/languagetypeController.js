'use strict';

var Languagetype = require('../model/languagetypeModel.js');

exports.read_a_Languagetype = function (req, res) {
  var read_a_Languagetype = req.params;
  Languagetype.getLanguagetype(req.params, function (err, Languagetype) {
    if (err) {
      res.send(err);
    } else {

      res.json(Languagetype);
    }
  });
};

exports.create_a_Languagetype = function (req, res) {
  var InsUpd_Languagetype_mster = new Languagetype(req.body);

  Languagetype.createLanguagetype(InsUpd_Languagetype_mster, function (err, Languagetype) {
    if (err) {
      res.send(err);
    } else {
      res.json(Languagetype);
    }
  });
};