'use strict';

var exchangeoffice = require('../model/exchangeofficeModel.js');

exports.read_a_exchangeoffice = function (req, res) {
  var fetch_adm_exch_mst_office = req.params;
  exchangeoffice.getexchangeoffice(req.params, function (err, exchangeoffice) {
    if (err) {
      res.send(err);
    } else {
      res.json(exchangeoffice);
    }
  });
};