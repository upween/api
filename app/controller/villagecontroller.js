'use strict';

var Village = require('../model/villageModel.js');

exports.create_a_Village = function (req, res) {
  var new_Village = new Village(req.body);

  Village.createVillage(new_Village, function (err, Village) {
    if (err) {
      res.send(err);
    } else {
      res.json(Village);
    }
  });
};
exports.read_a_Village = function (req, res) {
  var fetch_Village = req.params;
  Village.getVillage(req.params, function (err, Village) {
    if (err) {
      res.send(err);
    } else {
      res.json(Village);
    }
  });
};