'use strict';

var JobSeekerForgotPass = require('../model/JobSeekerForgotPassModel.js');

exports.read_a_jobSeekerForgotPass = function (req, res) {
  var fetch_jobSeekerForgotPass = req.params;
  JobSeekerForgotPass.getJobSeekerForgotPass(req.params, function (err, jobSeekerForgotPass) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobSeekerForgotPass);
    }
  });
};