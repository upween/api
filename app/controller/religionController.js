'use strict';

var Religion = require('../model/religionModel.js');

exports.read_a_Religion = function (req, res) {
  var read_a_Religion = req.params;
  Religion.getReligion(req.params, function (err, Religion) {
    if (err) {
      res.send(err);
    } else {
      res.json(Religion);
    }
  });
};

exports.create_a_Religion = function (req, res) {
  var InsUpd_Religion_master = new Religion(req.body);

  Religion.createReligion(InsUpd_Religion_master, function (err, Religion) {
    if (err) {
      res.send(err);
    } else {
      res.json(Religion);
    }
  });
};