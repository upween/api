'use strict';

var Jobfair = require('../model/jobfairModel.js');

exports.create_a_jobfair = function (req, res) {
  var new_jobfair = new Jobfair(req.body);

  Jobfair.createJobfair(new_jobfair, function (err, jobfair) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobfair);
    }
  });
};
exports.read_a_jobfair = function (req, res) {
  var fetch_jobfair = req.params;
  Jobfair.getJobfair(req.params, function (err, jobfair) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobfair);
    }
  });
};

exports.read_a_latestJobfair = function (req, res) {
  Jobfair.getLatestJobfair(function (err, latestJobfair) {

    //console.log('controller')
    if (err) {
      res.send(err);
    } else {
      //console.log('res', designation);
      res.send(latestJobfair);
    }
  });
};

exports.create_a_CandidateJobFair = function (req, res) {
  var new_candidatejobfair = new Jobfair(req.body);
  Jobfair.createCandidateJobFair(new_candidatejobfair, function (err, CandidateJobFair) {
    if (err) {
      res.send(err);
    } else {
      res.json(CandidateJobFair);
    }
  });
};
exports.read_a_JobFairMonthlyReporting = function (req, res) {
  var JobFairMonthlyReporting = req.params;
  Jobfair.getJobFairMonthlyRpt(req.params, function (err, JobFairMonthlyRpt) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobFairMonthlyRpt);
    }
  });
};
exports.create_a_ParticipantJobFair = function (req, res) {
  var ParticipantJobFair = new Jobfair(req.body);
  Jobfair.createJobfairParticipant(ParticipantJobFair, function (err, JobFair) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobFair);
    }
  });
};
exports.read_a_JobFairParticipant = function (req, res) {
  Jobfair.getJobfairParticipant(req.params, function (err, JobFairMonthlyRpt) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobFairMonthlyRpt);
    }
  });
};
exports.update_a_ParticipantJobFair = function (req, res) {
  var ParticipantJobFair = new Jobfair(req.body);
  Jobfair.updateParticipantJobFair(ParticipantJobFair, function (err, JobFair) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobFair);
    }
  });
};



exports.read_a_JobFairreport = function (req, res) {
  Jobfair.getjobfairreport(req.params, function (err, JobFairRpt) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobFairRpt);
    }
  });
};