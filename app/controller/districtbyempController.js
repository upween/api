'use strict';

var DistrictbyEmp = require('../model/districtbyempModel.js');


exports.read_a_districtbyemp = function (req, res) {
  var fetch_district = req.params;
  DistrictbyEmp.getDistrictbyemp(req.params, function (err, district) {
    if (err) {
      res.send(err);
    } else {
      res.json(district);
    }
  });
};