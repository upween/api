'use strict';
var crisp  = require('../model/crispModel.js');


exports.read_a_crisp = function(req, res) {
  var CRISP_Fetch_DistrictMaster= req.params;
  crisp.getcrisp(req.params, function(err, crisp) {
    if (err){
      res.send(err);
    }
    else{
    res.json(crisp);
    }
  });
};


exports.read_a_crispYearMaster = function(req, res) {
  var CRISP_Fetch_YearMaster= req.params;
  crisp.getcrispYearMaster(req.params, function(err, crispYearMaster) {
    if (err){
      res.send(err);
    }
    else{
    res.json(crispYearMaster);
    }
  });
};

exports.read_a_crispDistrictWiseCandidateCount = function(req, res) {
  var CRISP_Fetch_DistrictWiseCandidateCount= req.params;
  crisp.getcrispDistrictWiseCandidateCount(req.params, function(err, crispDistrictWiseCandidateCount) {
    if (err){
      res.send(err);
    }
    else{
    res.json(crispDistrictWiseCandidateCount);
    }
  });
};


exports.read_a_crispDistrictWiseCandidateDetail = function(req, res) {
  var CRISP_Fetch_DistrictWiseCandidateDetail= req.params;
  crisp.getcrispDistrictWiseCandidateDetail(req.params, function(err, crispDistrictWiseCandidateDetail) {
    if (err){
      res.send(err);
    }
    else{
    res.json(crispDistrictWiseCandidateDetail);
    }
  });
};


exports.read_a_crispCategoryWiseCandidateCount = function(req, res) {
  var CRISP_Fetch_CategoryWiseCandidateCount= req.params;
  crisp.getcrispCategoryWiseCandidateCount(req.params, function(err, crispCategoryWiseCandidateCount) {
    if (err){
      res.send(err);
    }
    else{
    res.json(crispCategoryWiseCandidateCount);
    }
  });
};

exports.read_a_crispCategoryWiseCandidateDetail = function(req, res) {
  var CRISP_Fetch_CategoryWiseCandidateDetail= req.params;
  crisp.getcrispCategoryWiseCandidateDetail(req.params, function(err, crispCategoryWiseCandidateDetail) {
    if (err){
      res.send(err);
    }
    else{
    res.json(crispCategoryWiseCandidateDetail);
    }
  });
};

exports.read_a_JSCRISP_QualificationWiseCandidateDetail = function (req, res) {
  var CRISP_Fetch_QualificationWiseCandidateDetail = req.params;
  crisp.getJSCRISP_QualificationWiseCandidateDetail(req.params, function (err,CRISP_QualificationWiseCandidateDetail) {
    if (err) {
      res.send(err);
    } else {
      res.json(CRISP_QualificationWiseCandidateDetail);
    }
  });
};
exports.read_a_JSCRISP_QualificationWiseCandidateCount = function (req, res) {
  var CRISP_Fetch_QualificationWiseCandidateCount = req.params;
  crisp.getJSCRISP_QualificationWiseCandidateCount(req.params, function (err,CRISP_QualificationWiseCandidateCount) {
    if (err) {
      res.send(err);
    } else {
      res.json(CRISP_QualificationWiseCandidateCount);
    }
  });
};
exports.read_a_JSCRISP_QualificationMaster = function (req, res) {
  var CRISP_Fetch_QualificationMaster = req.params;
  crisp.getJSCRISP_QualificationMaster(req.params, function (err,CRISP_QualificationMaster) {
    if (err) {
      res.send(err);
    } else {
      res.json(CRISP_QualificationMaster);
    }
  });
};
exports.read_a_JSCRISP_DistrictWisePlacementDetail = function (req, res) {
  crisp.getJSCRISP_DistrictWisePlacementDetail(req.params, function (err,getJSCRISP_DistrictWisePlacementDetail) {
    if (err) {
      res.send(err);
    } else {
      res.json(getJSCRISP_DistrictWisePlacementDetail);
    }
  });
};
exports.read_a_JSCRISP_DistrictWisePlacementCount = function (req, res) {
  var CRISP_Fetch_DistrictWisePlacementCount = req.params;
  crisp.getJSCRISP_DistrictWisePlacementCount(req.params, function (err,CRISP_DistrictWisePlacementCount) {
    if (err) {
      res.send(err);
    } else {
      res.json(CRISP_DistrictWisePlacementCount);
    }
  });
};
exports.read_a_JSCRISP_DistrictWiseJobFairCandidateCount = function (req, res) {
  var CRISP_Fetch_DistrictWiseJobFairCandidateCount = req.params;
  crisp.getJSCRISP_DistrictWiseJobFairCandidateCount(req.params, function (err,CRISP_DistrictWiseJobFairCandidateCount) {
    if (err) {
      res.send(err);
    } else {
      res.json(CRISP_DistrictWiseJobFairCandidateCount);
    }
  });
};
exports.read_a_JSCRISPDistrictWiseCareerCandidateCount = function (req, res) {
  var CRISP_Fetch_DistrictWiseCareerCandidateCount = req.params;
  crisp.getJSCRISPDistrictWiseCareerCandidateCount(req.params, function (err,CRISPDistrictWiseCareerCandidateCount) {
    if (err) {
      res.send(err);
    } else {
      res.json(CRISPDistrictWiseCareerCandidateCount);
    }
  });
};
exports.read_a_JSCategoryMaster = function (req, res) {
  var CategoryMaster = req.params;
  crisp.getJSCategoryMaster(req.params, function (err,CategoryMaster) {
    if (err) {
      res.send(err);
    } else {
      res.json(CategoryMaster);
    }
  });
};
exports.read_a_JSAgeWiseCandidateCount = function (req, res) {
  var AgeWiseCandidateCount = req.params;
  crisp.getJSAgeWiseCandidateCount(req.params, function (err,AgeWiseCandidateCount) {
    if (err) {
      res.send(err);
    } else {
      res.json(AgeWiseCandidateCount);
    }
  });
};
exports.read_a_JSAgeWiseCandidateDetail = function (req, res) {
  var AgeWiseCandidateDetail = req.params;
  crisp.getJSAgeWiseCandidateDetail(req.params, function (err,AgeWiseCandidateDetail) {
    if (err) {
      res.send(err);
    } else {
      res.json(AgeWiseCandidateDetail);
    }
  });
};