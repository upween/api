'use strict';

var ExchJSSearch = require('../model/ExchJSSearchModel.js');

exports.read_a_ExchJSSearch = function (req, res) {

  var CI_Pro_Exch_JSSearch = req.params;
  ExchJSSearch.getExchJSSearch(req.params, function (err, ExchJSSearch) {
    if (err) {
      res.send(err);
    } else {
      res.json(ExchJSSearch);
    }
  });
};