'use strict';

var PrefLocation = require('../model/preflocationModel.js');

exports.create_a_PrefLocation = function (req, res) {
  var new_PrefLocation = new PrefLocation(req.body);

  PrefLocation.createPrefLocation(new_PrefLocation, function (err, PrefLocation) {
    if (err) {
      res.send(err);
    } else {
      res.json(PrefLocation);
    }
  });
};
exports.read_a_PrefLocation = function (req, res) {
  var fetch_Language_master = req.params;
  PrefLocation.getPrefLocation(req.params, function (err, PrefLocation) {
    if (err) {

      res.send(err);
    } else {
      res.json(PrefLocation);
    }
  });
};