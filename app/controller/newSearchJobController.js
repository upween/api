
'use strict';

var newSearchJob = require('../model/newSearchJobModel.js');

exports.create_a_newSearchJob = function (req, res) {
  var SearchJob_new = new newSearchJob(req.body);

  newSearchJob.createnewSearchJob(SearchJob_new, function (err, newSearchJob) {
    if (err) {
      res.send(err);
    } else {
      res.json(newSearchJob);
    }
  });
};
exports.create_a_newSearchJobRecommended = function (req, res) {
  var SearchJob_new = new newSearchJob(req.body);

  newSearchJob.createnewSearchJobRecommended(SearchJob_new, function (err, newSearchJob) {
    if (err) {
      res.send(err);
    } else {
      res.json(newSearchJob);
    }
  });
};