
'use strict';

var Counselling = require('../model/counsellingformModel.js');

exports.create_a_Counselling = function (req, res) {
  var new_Counselling = new Counselling(req.body);

  Counselling.createCounselling(new_Counselling, function (err, counselling) {
    if (err) {
      res.send(err);
    } else {
      res.json(counselling);
    }
  });
};


exports.create_a_Counsellingfamily = function (req, res) {
    var new_Counsellingfamily = new Counselling(req.body);
  
    Counselling.createCounsellingfamily(new_Counsellingfamily, function (err, counsellingfamily) {
      if (err) {
        res.send(err);
      } else {
        res.json(counsellingfamily);
      }
    });
  };

  exports.create_a_Counsellingeducation = function (req, res) {
    var new_Counsellingeducation = new Counselling(req.body);
  
    Counselling.createCounsellingeducation(new_Counsellingeducation, function (err,Counsellingeducation) {
      if (err) {
        res.send(err);
      } else {
        res.json(Counsellingeducation);
      }
    });
  };
  exports.read_a_counsellingform = function (req, res) {
    var fetch_companytype_master = req.params;
    Counselling.getCounselling(req.params, function (err, counselling) {
      if (err) {
        res.send(err);
      } else {
        res.json(counselling);
      }
    });
  };

  exports.read_a_Counselling_ByMonthConsolidate = function (req, res) {
    var CI_Pro_Counselling_ByMonthConsolidate = req.params;
    Counselling.getCounselling_ByMonthConsolidate(req.params, function (err, Counselling_ByMonthConsolidate) {
      if (err) {
        res.send(err);
      } else {
        res.json(Counselling_ByMonthConsolidate);
      }
    });
  };


  exports.read_a_CounsellingByMonthWise = function (req, res) {
    var CI_Pro_Counselling_ByMonthWise = req.params;
    Counselling.getCounsellingByMonthWise(req.params, function (err, CounsellingByMonthWise) {
      if (err) {
        res.send(err);
      } else {
        res.json(CounsellingByMonthWise);
      }
    });
  };