'use strict';

var AdminVerification = require('../model/AdminVerificationModel.js');

exports.read_a_AdminVerification = function (req, res) {

  var Fetch_Jobseeker_AdminVerification = req.params;
  AdminVerification.getAdminVerification(req.params, function (err, AdminVerification) {
    if (err) {
      res.send(err);
    } else {
      res.json(AdminVerification);
    }
  });
};