'use strict';
var olexdashboard = require('../model/olexdashboardModel.js');


exports.read_a_olexdashboard = function(req, res) {
 
  var Dashboard_JobseekerRecord = req.params;
  olexdashboard.getolexdashboard(req.params, function(err, olexdashboard) {
    if (err){
      res.send(err);
    }
      else{
    res.json(olexdashboard);
      }
  });
};

exports.read_a_olexdashboardemp = function(req, res) {
 
  var Dashboard_EmployerRecords = req.params;
  olexdashboard.getolexdashboardemp(req.params, function(err, olexdashboard) {
    if (err){
      res.send(err);
    }
      else{
    res.json(olexdashboard);
      }
  });
};