'use strict';

var saveapplyCounter = require('../model/saveapplyCounterModel.js');

exports.read_a_saveapplyCounter = function (req, res) {
  var Fetch_save_apply_Counter = req.params;
  saveapplyCounter.getsaveapplyCounter(req.params, function (err, saveapplyCounter) {
    if (err) {
      res.send(err);
    } else {
      res.json(saveapplyCounter);
    }
  });
};