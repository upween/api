'use strict';

var SendMailer = require('../model/sendmailModel.js');

exports.read_a_candidatelist = function (req, res) {
  
    SendMailer.getCandidatelist(req.params, function (err, data) {
    if (err) {
      res.send(err);
    } else {
      res.json(data);
    }
  });
};