'use strict';

var RenewalCurrentStatus = require('../model/RenewalCurrentStatusModel.js');

exports.read_a_RenewalCurrentStatus = function (req, res) {

  RenewalCurrentStatus.getRenewalCurrentStatus(req.params, function (err, RenewalCurrentStatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(RenewalCurrentStatus);
    }
  });
};