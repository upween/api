'use strict';

var image = require('../model/jobseekerimageModel.js');

exports.create_a_image = function (req, res) {
  var new_image = new image(req.body);

  image.createimage(new_image, function (err, image) {
    if (err) {
      res.send(err);
    } else {
      res.json(image);
    }
  });
};
exports.read_a_DeleteImage = function (req, res) {
  var fetch_image = req.params;
  image.getDeleteImage(req.params, function (err, image) {
    if (err) {
      res.send(err);
    } else {
      res.json(image);
    }
  });
};
exports.read_a_image = function (req, res) {
  var fetch_image = req.params;
  image.getimage(req.params, function (err, image) {
    if (err) {
      res.send(err);
    } else {
      res.json(image);
    }
  });
};