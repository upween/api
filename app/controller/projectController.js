'use strict';

var Project = require('../model/projectModel.js');

exports.create_a_project = function (req, res) {
  var new_project = new Project(req.body);

  Project.createProject(new_project, function (err, project) {
    if (err) {
      res.send(err);
    } else {
      res.json(project);
    }
  });
};
exports.read_a_project = function (req, res) {
  var fetch_ProjectDetailmaster = req.params;
  Project.getProject(req.params, function (err, project) {
    if (err) {
      res.send(err);
    } else {
      res.json(project);
    }
  });
};