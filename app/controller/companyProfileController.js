'use strict';

var companyprofile = require('../model/companyProfileModel.js');

exports.create_a_companyprofile = function (req, res) {
  var insUpd_ci_mst_companyprofile = new companyprofile(req.body);

  companyprofile.createcompanyprofile(insUpd_ci_mst_companyprofile, function (err, companyprofile) {
    if (err) {
      res.send(err);
    } else {

      res.json(companyprofile);
    }
  });
};

exports.read_a_companyprofile = function (req, res) {

  var fetch_ci_mst_companyprofile = req.params;
  companyprofile.getcompanyprofile(req.params, function (err, companyprofile) {
    if (err) {
      res.send(err);
    } else {
      res.json(companyprofile);
    }
  });
};