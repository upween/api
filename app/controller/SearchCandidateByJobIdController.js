'use strict';

var SearchCandidateByJobId = require('../model/SearchCandidateByJobIdModel.js');

exports.read_a_SearchCandidateByJobId = function (req, res) {

  var SearchCandidateJobId = req.params;
  SearchCandidateByJobId.getSearchCandidateByJobId(req.params, function (err, SearchCandidateByJobId) {
    if (err) {
      res.send(err);
    } else {
      res.json(SearchCandidateByJobId);
    }
  });
};