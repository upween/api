'use strict';

var BuildResume = require('../model/buildresumeModel.js');

exports.read_a_buildResume = function (req, res) {

  var fetch_BuildResume = req.params;
  BuildResume.getBuildResume(req.params, function (err, buildResume) {
    if (err) res.send(err);else res.json(buildResume);
  });
};

exports.read_a_educationBuildCV = function (req, res) {

  var fetch_EducationBuildCV = req.params;
  BuildResume.getEducationBuildCV(req.params, function (err, educationBuildCV) {
    if (err) {
      res.send(err);
    } else {
      res.json(educationBuildCV);
    }
  });
};

exports.read_a_experienceCV = function (req, res) {

  var fetch_ExperienceCV = req.params;
  BuildResume.getExperienceCV(req.params, function (err, experienceCV) {
    if (err) {
      res.send(err);
    } else {
      res.json(experienceCV);
    }
  });
};