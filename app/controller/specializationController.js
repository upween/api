'use strict';

var Specialization = require('../model/specializationModel.js');

exports.create_a_specialization = function (req, res) {
  var new_specialization = new Specialization(req.body);

  Specialization.createSpecialization(new_specialization, function (err, specialization) {
    if (err) {
      res.send(err);
    } else {
      res.json(specialization);
    }
  });
};
exports.read_a_specialization = function (req, res) {
  var fetch_specialization = req.params;
  Specialization.getSpecialization(req.params, function (err, specialization) {
    if (err) {
      res.send(err);
    } else {
      res.json(specialization);
    }
  });
};