'use strict';

var GroupDiscussion = require('../model/GroupDiscussionModel.js');

exports.create_a_GroupDiscussion = function (req, res) {
  var CI_Pro_Coun_GroupDiscussion = new GroupDiscussion(req.body);

  GroupDiscussion.createGroupDiscussion(CI_Pro_Coun_GroupDiscussion, function (err, GroupDiscussion) {
    if (err) {
      res.send(err);
    } else {
      res.json(GroupDiscussion);
    }
  });
};