'use strict';

var Exchangejobfair = require('../model/ExchangejobfairModel.js');

exports.create_a_exchangejobfair = function (req, res) {
  var InsUpd_exchangejobfair = new Exchangejobfair(req.body);

  Exchangejobfair.createexchangejobfair(InsUpd_exchangejobfair, function (err, exchangejob) {
    if (err) {
      res.send(err);
    } else {
      res.json(exchangejob);
    }
  });
};

exports.create_a_employerexchangejf = function (req, res) {
  var InsUpd_employerexchangejf = new Exchangejobfair(req.body);

  Exchangejobfair.createemployerexchangejf(InsUpd_employerexchangejf, function (err, exchangejobfair) {
    if (err) {
      res.send(err);
    } else {
      res.json(exchangejobfair);
    }
  });
};



exports.read_a_Exjobfairreport = function (req, res) {
  var savedsearch = req.params;
  Exchangejobfair.getexjobfairreport(req.params, function (err,savedsearch) {
    if (err) {
      res.send(err);
    } else {
      res.json(savedsearch);
    }
  });
};