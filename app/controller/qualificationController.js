
'use strict';

var qualification = require('../model/qualificationModel.js');

exports.read_a_qualification = function (req, res) {
  var fetch_qualification = req.params;
  qualification.getqualification(req.params, function (err, qualification) {
    if (err) {
      res.send(err);
    } else {
      res.json(qualification);
    }
  });
};