'use strict';

var wlurl = require('../model/wlurlModel.js');

exports.create_a_wlurl = function (req, res) {
  var InsUpd_ci_wl_url = new wlurl(req.body);

  wlurl.createwlurl(InsUpd_ci_wl_url, function (err, wlurl) {
    if (err) {
      res.send(err);
    } else {
      res.json(wlurl);
    }
  });
};
exports.read_a_wlurl = function (req, res) {

  var Fetch_ci_wl_url = req.params;
  wlurl.getwlurl(req.params, function (err, wlurl) {
    if (err) {
      res.send(err);
    } else {
      res.json(wlurl);
    }
  });
};