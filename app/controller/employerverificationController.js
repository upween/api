'use strict';

var employerverification = require('../model/employerverificationModel.js');

exports.create_a_employerverification = function (req, res) {
  var InsUpd_employer_verification = new employerverification(req.body);

  employerverification.createemployerverification(InsUpd_employer_verification, function (err, employerverification) {
    if (err) {
      res.send(err);
    } else {
      res.json(employerverification);
    }
  });
};