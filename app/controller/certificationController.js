'use strict';

var Certification = require('../model/certificationModel.js');

exports.create_a_certification = function (req, res) {
  var new_certification = new Certification(req.body);

  Certification.createCertification(new_certification, function (err, certification) {
    if (err) {
      res.send(err);
    } else {
      res.json(certification);
    }
  });
};
exports.read_a_certification = function (req, res) {
  var fetch_certification = req.params;
  Certification.getCertification(req.params, function (err, certification) {
    if (err) {
      res.send(err);
    } else {
      res.json(certification);
    }
  });
};