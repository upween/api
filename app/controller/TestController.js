'use strict';

var Test = require('../model/TestModel.js');

exports.create_a_Test = function (req, res) {
  var InsUpd_user_info = new Test(req.body);

  Test.createTest(InsUpd_user_info, function (err, Test) {
    if (err) {
      res.send(err);
    } else {
      res.json(Test);
    }
  });
};
exports.read_a_Test = function (req, res) {
  var fetch_user_info = req.params;
  Test.getTest(req.params, function (err, Test) {
    if (err) {
      res.send(err);
    } else {
      res.json(Test);
    }
  });
};

exports.read_a_TestDelete = function (req, res) {
  var insupd_user_info = req.params;
  Test.getTestDelete(req.params, function (err, TestDelete) {
    if (err) {
      res.send(err);
    } else {
      res.json(TestDelete);
    }
  });
};