'use strict';

var HandicappedDocs = require('../model/HandicappedDocsModel.js');

exports.create_a_HandicappedDocs = function (req, res) {
  var Upd_HandicappedDocs = new HandicappedDocs(req.body);

  HandicappedDocs.createHandicappedDocs(Upd_HandicappedDocs, function (err, HandicappedDocs) {
    if (err) {
      res.send(err);
    } else {
      res.json(HandicappedDocs);
    }
  });
};