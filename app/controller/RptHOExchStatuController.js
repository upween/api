'use strict';

var RptHOExchStatu = require('../model/RptHOExchStatuModel.js');

exports.read_a_RptHOExchStatu = function (req, res) {

  var CI_Pro_Rpt_HO_ExchStatus = req.params;
  RptHOExchStatu.getRptHOExchStatu(req.params, function (err, RptHOExchStatu) {
    if (err) {
      res.send(err);
    } else {
      res.json(RptHOExchStatu);
    }
  });
};

exports.read_a_NotSendHOExchStatus = function (req, res) {

  var CI_Pro_Rpt_NotSend_HO_ExchStatus = req.params;
  RptHOExchStatu.getNotSendHOExchStatus(req.params, function (err, NotSendHOExchStatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(NotSendHOExchStatus);
    }
  });
};