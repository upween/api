'use strict';

var Employmentstatus = require('../model/employmentStatusModel.js');

exports.read_a_Employmentstatus = function (req, res) {
  var read_a_Employmentstatus = req.params;
  Employmentstatus.getEmploymentstatus(req.params, function (err, Employmentstatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(Employmentstatus);
    }
  });
};

exports.read_a_EmployerUser = function (req, res) {
  var Check_EmployerUser = req.params;
  Employmentstatus.getEmployerUser(req.params, function (err, EmployerUser) {
    if (err) {
      res.send(err);
    } else {
      res.json(EmployerUser);
    }
  });
};