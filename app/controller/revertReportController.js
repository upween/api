'use strict';

var RevertRpt = require('../model/revertReportModel.js');

exports.update_a_RptStatus = function (req, res) {
  var parameters = new RevertRpt(req.body);

  RevertRpt.updateRptStatus(parameters, function (err, revertrpt) {
    if (err) {
      res.send(err);
    } else {
      res.json(revertrpt);
    }
  });
};
