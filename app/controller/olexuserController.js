'use strict';
var olexuser = require('../model/olexuserModel.js');

exports.create_a_olexuser = function(req, res) {
  var ins_olexuser = new olexuser(req.body);
 
  olexuser.createolexuser(ins_olexuser, function (err, olexuser) {
    if (err) {
      res.send(err);
    } else {
      res.json(olexuser);
    }
  });
};


exports.create_a_pass_update = function(req, res) {
  var adm_pass_update = new olexuser(req.body);
 
  olexuser.createolexuserPass(adm_pass_update, function (err, pass_update) {
    if (err) {
      res.send(err);
    } else {
      res.json(pass_update);
    }
  });
};



exports.read_a_olexusertype = function(req, res) {
  var fetch_olexusertype = req.params;
  olexuser.getolexusertype(req.params, function(err, olexusertype) {
    if (err){
      res.send(err);
    }
    else{
    res.json(olexusertype);
    }
  });
};
exports.read_a_olexuser = function(req, res) {
  var fetch_olexusertype = req.params;
  olexuser.getolexuser(req.params, function(err, olexuser) {
    if (err){
      res.send(err);
    }
    else{
    res.json(olexuser);
    }
  });
};

