'use strict';

var Interestedcandidatereport = require('../model/interestedcandidatereportModel.js');

exports.read_a_Interestedcandidatereport = function (req, res) {
  var Fetch_Interestedcandidate_jf = req.params;
  Interestedcandidatereport.getInterestedcandidatereport(req.params, function (err, Interestedcandidatereport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Interestedcandidatereport);
    }
  });
};