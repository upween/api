'use strict';

var FeaturedJob = require('../model/FeaturedJobModel.js');

exports.read_a_FeaturedJob = function (req, res) {
  var read_a_FeaturedJob = req.params;
  FeaturedJob.getFeaturedJob(req.params, function (err, FeaturedJob) {
    if (err) {
      res.send(err);
    } else {
      res.json(FeaturedJob);
    }
  });
};