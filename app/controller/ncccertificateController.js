
'use strict';

var ncccertificate = require('../model/ncccertificateModel.js');

exports.read_a_ncccertificate = function (req, res) {
  var fetch_NCCcertificate_master = req.params;
  ncccertificate.getncccertificate(req.params, function (err, ncccertificate) {
    if (err) {
      res.send(err);
    } else {
      res.json(ncccertificate);
    }
  });
};

exports.create_a_ncccertificate = function (req, res) {
  var InsUpd_NCCcertificate_master = new ncccertificate(req.body);

  ncccertificate.createncccertificate(InsUpd_NCCcertificate_master, function (err, ncccertificate) {
    if (err) {
      res.send(err);
    } else {
      res.json(ncccertificate);
    }
  });
};