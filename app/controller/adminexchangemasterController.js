'use strict';

var AdminExchangeOffice = require('../model/adminexchangemasterModel.js');

exports.create_a_AdminExchangeOffice = function (req, res) {
  var new_AdminExchangeOffice = new AdminExchangeOffice(req.body);

  AdminExchangeOffice.createAdminExchangeOffice(new_AdminExchangeOffice, function (err, AdminExchangeOffice) {
    if (err) {
      res.send(err);
    } else {
      res.json(AdminExchangeOffice);
    }
  });
};