
'use strict';

var BloodGroup = require('../model/bloodGroupModel.js');

exports.read_a_BloodGroup = function (req, res) {
  var fetch_bloodgroup_master = req.params;
  BloodGroup.getBloodGroup(req.params, function (err, BloodGroup) {
    if (err) {
      res.send(err);
    } else {
      res.json(BloodGroup);
    }
  });
};
exports.create_a_BloodGroup = function (req, res) {
  var new_BloodGroup = new BloodGroup(req.body);

  BloodGroup.createBloodGroup(new_BloodGroup, function (err, BloodGroup) {
    if (err) {
      res.send(err);
    } else {
      res.json(BloodGroup);
    }
  });
};