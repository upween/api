'use strict';

var selectdtls = require('../model/selectdtlsModel.js');

exports.create_a_selectdtls = function (req, res) {
  var InsUpd_ci_cc_js_select_dtls = new selectdtls(req.body);

  selectdtls.createselectdtls(InsUpd_ci_cc_js_select_dtls, function (err, selectdtls) {
    if (err) {
      res.send(err);
    } else {
      res.json(selectdtls);
    }
  });
};