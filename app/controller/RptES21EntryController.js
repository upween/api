'use strict';

var ES21Entry = require('../model/RptES21EntryModel.js');



exports.create_a_ES21Entry = function (req, res) {
  var CI_Pro_Rpt_ES21_Entry = new ES21Entry(req.body);

  ES21Entry.createES21Entry(CI_Pro_Rpt_ES21_Entry, function (err, ES21Entry) {
    if (err) {
      res.send(err);
    } else {
      res.json(ES21Entry);
    }
  });
};


exports.read_a_Fetch_ES21_Master = function (req, res) {
  var Fetch_ES21_Master = req.params;
  ES21Entry.getFetch_ES21_Master(Fetch_ES21_Master, function (err, ES21_Master) {
    if (err) {
      res.send(err);
    } else {
      res.json(ES21_Master);
    }
  });
};