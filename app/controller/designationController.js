'use strict';

var Designation = require('../model/designationModel.js');

exports.create_a_designation = function (req, res) {
  var new_designation = new Designation(req.body);

  Designation.createDesignation(new_designation, function (err, designation) {
    if (err) {
      res.send(err);
    } else {
      res.json(designation);
    }
  });
};
exports.read_a_designation = function (req, res) {
  var fetch_designation = req.params;
  Designation.getDesignation(req.params, function (err, designation) {
    if (err) {
      res.send(err);
    } else {
      res.json(designation);
    }
  });
};

exports.read_a_latestdesignation = function (req, res) {
  Designation.getLatestDesignation(function (err, latestdesignation) {

    //console.log('controller')
    if (err) {
      res.send(err);
    } else {
      //console.log('res', designation);
      res.send(latestdesignation);
    }
  });
};

exports.read_a_designationJob = function (req, res) {
  var fetch_8Designation = req.params;
  Designation.getDesignationJob(req.params, function (err, designationJob) {
    if (err) {
      res.send(err);
    } else {
      res.json(designationJob);
    }
  });
};