'use strict';

var monthlyreporting = require('../model/monthlyreportingModel.js');

exports.create_a_monthlyreporting = function (req, res) {
  var InsUpd_ci_jf_monthlyreporting = new monthlyreporting(req.body);

  monthlyreporting.createmonthlyreporting(InsUpd_ci_jf_monthlyreporting, function (err, monthlyreporting) {
    if (err) {
      res.send(err);
    } else {
      res.json(monthlyreporting);
    }
  });
};
exports.read_a_monthlyreporting = function (req, res) {

  var Fetch_ci_jf_monthlyreporting = req.params;
  monthlyreporting.getmonthlyreporting(req.params, function (err, monthlyreporting) {
    if (err) {
      res.send(err);
    } else {
      res.json(monthlyreporting);
    }
  });
};