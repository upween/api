'use strict';

var FooterDetail = require('../model/FooterDetailModel.js');

exports.read_a_footerDetail = function (req, res) {
  var fetch_feedfetch_FooterDataback = req.params;
  FooterDetail.getFooterDetail(req.params, function (err, footerDetail) {
    if (err) {
      res.send(err);
    } else {
      res.json(footerDetail);
    }
  });
};