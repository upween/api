'use strict';

var JobSeeker = require('../model/careercounsellingjobseekerModel.js');

exports.read_a_jobseeker = function (req, res) {

  JobSeeker.getJobSeeker(req.params, function (err, jobseeker) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobseeker);
    }
  });
};

exports.read_a_CounsellingByMonth = function (req, res) {

  JobSeeker.getCounsellingByMonth(req.params, function (err, CounsellingByMonth) {
    if (err) {
      res.send(err);
    } else {
      res.json(CounsellingByMonth);
    }
  });
};

exports.read_a_CounsellingByMonthWise = function (req, res) {

  JobSeeker.getCounsellingByMonthWise(req.params, function (err, CounsellingByMonthWise) {
    if (err) {
      res.send(err);
    } else {
      res.json(CounsellingByMonthWise);
    }
  });
};
exports.read_a_Counsellingmonthlyreporting = function (req, res) {

  JobSeeker.getCounsellingMonthlyReporting(req.params, function (err, Counselling) {
    if (err) {
      res.send(err);
    } else {
      res.json(Counselling);
    }
  });
};



exports.create_a_CareerCounsel_Pro_Reporting = function (req, res) {
  var CI_CareerCounsel_Pro_Reporting = new JobSeeker(req.body);
  JobSeeker.createCareerCounsel_Pro_Reporting(CI_CareerCounsel_Pro_Reporting, function (err, CareerCounsel_Pro_Reporting) {
    if (err) {
      res.send(err);
    } else {
      res.json(CareerCounsel_Pro_Reporting);
    }
  });
};


