'use strict';

var emailconfig = require('../model/emailconfigModel.js');

exports.read_a_emailconfig = function (req, res) {

  var fetch_email_config = req.params;
  emailconfig.getemailconfig(req.params, function (err, emailconfig) {
    if (err) {
      res.send(err);
    } else {
      res.json(emailconfig);
    }
  });
};