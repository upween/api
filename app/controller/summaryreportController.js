'use strict';

var Summaryreport = require('../model/summaryreportModel.js');

exports.read_a_Summaryreport = function (req, res) {
  Summaryreport.getSummaryreport(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};

exports.read_a_SummaryreportCategoryWise = function (req, res) {
  Summaryreport.getSummaryreportCategoryWise(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};

exports.read_a_SummaryreportDifferentlyAbled = function (req, res) {
  Summaryreport.getSummaryreportDifferentlyAbled(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};

exports.read_a_SummaryreportAreaWise = function (req, res) {
  Summaryreport.getSummaryreportAreaWise(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};

exports.read_a_SummaryreportReligionWise = function (req, res) {
  Summaryreport.getSummaryreportReligionWise(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};

exports.read_a_ITIQualificationWiseSummaryReport = function (req, res) {
  Summaryreport.getITIQualificationWiseSummaryReport(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};

exports.read_a_ConsolidateSummaryReport = function (req, res) {
  Summaryreport.getConsolidateSummaryReport(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};

exports.read_a_DetailedSummaryReport = function (req, res) {
  Summaryreport.getDetailedSummaryReport(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};

exports.read_a_DetailedSummaryReportReligion = function (req, res) {
  Summaryreport.getDetailedSummaryReportReligion(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};

exports.read_a_DetailedSummaryReportAreaWise = function (req, res) {
  Summaryreport.getDetailedSummaryReportAreaWise(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};

exports.read_a_DetailedSummaryReportPHWise = function (req, res) {
  Summaryreport.getDetailedSummaryReportPHWise(req.params, function (err, Summaryreport) {
    if (err) {
      res.send(err);
    } else {
      res.json(Summaryreport);
    }
  });
};