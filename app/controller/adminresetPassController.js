'use strict';

var adminresetPass = require('../model/adminresetPassModel.js');

exports.create_a_adminresetPass = function (req, res) {
  var new_adminresetPass = new adminresetPass(req.body);

  adminresetPass.createadminresetPass(new_adminresetPass, function (err, adminresetPass) {
    if (err) res.send(err);
    res.json(adminresetPass);
  });
};
exports.read_a_adminresetPass = function (req, res) {

  var fetch_ResetPassword = req.params;
  adminresetPass.getadminresetPass(req.params, function (err, adminresetPass) {
    if (err) {
      res.send(err);
    } else {
      res.json(adminresetPass);
    }
  });
};