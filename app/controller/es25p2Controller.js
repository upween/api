'use strict';

var ES25P2Entry = require('../model/es25p2Model.js');


exports.read_a_es25P2Rpt = function (req, res) {
  var ES25P2Entrypara = req.params;
  ES25P2Entry.getES25P2Rpt(ES25P2Entrypara, function (err, es25P2) {
    if (err) {
      res.send(err);
    } else {
      res.json(es25P2);
    }
  });
};



exports.create_a_ES25Part2 = function (req, res) {
  var CI_Pro_Rpt_ES25_Part_II_Entry = new ES25P2Entry(req.body);

  ES25P2Entry.createES25Part2(CI_Pro_Rpt_ES25_Part_II_Entry, function (err, ES25P2Entry) {
    if (err) {
      res.send(err);
    } else {
      res.json(ES25P2Entry);
    }
  });
};