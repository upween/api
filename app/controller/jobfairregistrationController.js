'use strict';

var jobfairregistration = require('../model/jobfairregistrationModel.js');

exports.create_a_jobfairregistration = function (req, res) {
  var insupd_job_fair_registration = new jobfairregistration(req.body);

  jobfairregistration.createjobfairregistration(insupd_job_fair_registration, function (err, jobfairregistration) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobfairregistration);
    }
  });
};

exports.create_a_jobfairempreg = function (req, res) {
  var InsUpd_jobfair_emp_reg = new jobfairregistration(req.body);

  jobfairregistration.createjobfairempreg(InsUpd_jobfair_emp_reg, function (err, jobfairempreg) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobfairempreg);
    }
  });
};
exports.read_a_itiEmployer = function (req, res) {

  jobfairregistration.getITIEmployer(req.params, function (err, itiEmployer) {
    if (err) {
      res.send(err);
    } else {
      res.json(itiEmployer);
    }
  });
};

exports.read_a_jobfairEmpRegbyId = function (req, res) {

  jobfairregistration.getjobfairEmpRegbyId(req.params, function (err, jobfairEmpRegbyId) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobfairEmpRegbyId);
    }
  });
};
exports.read_a_JobfairEmployer = function (req, res) {

  jobfairregistration.getJobfairEmployer(req.params, function (err, jobfairEmpRegbyId) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobfairEmpRegbyId);
    }
  });
};