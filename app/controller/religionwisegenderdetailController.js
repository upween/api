'use strict';

var Religionwisegenderdetail = require('../model/religionwisegenderdetailModel.js');

exports.read_a_Religionwisegenderdetail = function (req, res) {
  Religionwisegenderdetail.getReligionwisegenderdetail(req.params, function (err, Religionwisegenderdetail) {
    if (err) {
      res.send(err);
    } else {
      res.json(Religionwisegenderdetail);
    }
  });
};