'use strict';

var Resumeheadline = require('../model/resumeheadlineModel.js');

exports.create_a_resumeheadline = function (req, res) {
  var new_resumeheadline = new Resumeheadline(req.body);

  Resumeheadline.createResumeheadline(new_resumeheadline, function (err, resumeheadline) {
    if (err) {
      res.send(err);
    } else {
      res.json(resumeheadline);
    }
  });
};
exports.read_a_resumeheadline = function (req, res) {
  var fetch_resumeheadline = req.params;
  Resumeheadline.getResumeheadline(req.params, function (err, resumeheadline) {
    if (err) {
      res.send(err);
    } else {
      res.json(resumeheadline);
    }
  });
};