'use strict';

var sectortype = require('../model/sectortypeModel.js');

exports.read_a_sectortype = function (req, res) {
  var fetch_sectortype_master = req.params;
  sectortype.getsectortype(req.params, function (err, sectortype) {
    if (err) {
      res.send(err);
    } else {
      res.json(sectortype);
    }
  });
};

exports.create_a_sectortype = function (req, res) {
  var InsUpd_sectortype = new sectortype(req.body);

  sectortype.createsectortype(InsUpd_sectortype, function (err, sectortype) {
    if (err) {
      res.send(err);
    } else {
      res.json(sectortype);
    }
  });
};