'use strict';

var RoleMenuMapping = require('../model/rolemenumappingModel.js');

exports.create_a_rolemenumapping = function (req, res) {
  var new_rolemenumapping = new RoleMenuMapping(req.body);

  RoleMenuMapping.createRoleMenuMapping(new_rolemenumapping, function (err, rolemenumapping) {
    if (err) {
      res.send(err);
    } else {
      res.json(rolemenumapping);
    }
  });
};
exports.create_a_rolesubmenumapping = function (req, res) {
  var new_rolesubmenumapping = new RoleMenuMapping(req.body);

  RoleMenuMapping.createRoleSubMenuMapping(new_rolesubmenumapping, function (err, rolesubmenumapping) {
    if (err) {
      res.send(err);
    } else {
      res.json(rolesubmenumapping);
    }
  });
};
exports.read_a_rolemenumapping = function (req, res) {

  var fetch_rolemenumapping = req.params;
  RoleMenuMapping.getRoleMenuMapping(req.params, function (err, rolemenumapping) {
    if (err) {
      res.send(err);
    } else {

      res.json(rolemenumapping);
    }
  });
};
exports.read_a_rolesubmenumapping = function (req, res) {

  var fetch_rolemenumapping = req.params;
  RoleMenuMapping.getRoleSubMenuMapping(req.params, function (err, rolesubmenumapping) {
    if (err) {
      res.send(err);
    } else {

      res.json(rolesubmenumapping);
    }
  });
};

exports.read_a_deletemenus = function (req, res) {

  var Delete_menu_and_submenu = req.params;
  RoleMenuMapping.getDeleteMenus(req.params, function (err, detetemenus) {
    if (err) {
      res.send(err);
    } else {

      res.json(detetemenus);
    }
  });
};