'use strict';

var Delete = require('../model/deleteModel.js');

exports.read_a_Delete = function (req, res) {
  var Delete_Detail = req.params;
  Delete.getDelete(req.params, function (err, Delete) {
    if (err) {
      res.send(err);
    } else {
      res.json(Delete);
    }
  });
};