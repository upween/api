'use strict';

var Skill = require('../model/skillModel.js');

exports.create_a_skill = function (req, res) {
  var new_skill = new Skill(req.body);

  Skill.createSkill(new_skill, function (err, skill) {
    if (err) {
      res.send(err);
    } else {
      res.json(skill);
    }
  });
};
exports.read_a_skill = function (req, res) {
  var fetch_skill = req.params;
  Skill.getSkill(req.params, function (err, skill) {
    if (err) {
      res.send(err);
    } else {
      res.json(skill);
    }
  });
};