'use strict';

var Monthlyregstatus = require('../model/monthlyregstatusModel.js');

exports.read_a_Monthlyregstatus = function (req, res) {
  Monthlyregstatus.getMonthlyregstatus(req.params, function (err, Monthlyregstatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(Monthlyregstatus);
    }
  });
};

exports.read_a_MonthlyRegistrationDateWise = function (req, res) {
  Monthlyregstatus.getMonthlyRegistrationDateWise(req.params, function (err, Monthlyregstatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(Monthlyregstatus);
    }
  });
};


exports.read_a_FetchMonthlyregstatus = function (req, res) {
  Monthlyregstatus.getFetchMonthlyregstatus(req.params, function (err, fetcMonthlyregstatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(fetcMonthlyregstatus);
    }
  });
};