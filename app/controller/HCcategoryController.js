'use strict';

var HCCategory = require('../model/HCcategoryModel.js');

exports.read_a_HCCategory = function (req, res) {
  var read_a_HCCategory = req.params;
  HCCategory.getHCCategory(req.params, function (err, HCCategory) {
    if (err) {
      res.send(err);
    } else {
      res.json(HCCategory);
    }
  });
};