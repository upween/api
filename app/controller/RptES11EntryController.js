'use strict';

var ES11Entry = require('../model/RptES11EntryModel.js');

exports.create_a_ES11Entry = function (req, res) {
  var ES11Entry1 = new ES11Entry(req.body);

  ES11Entry.createES11Entry(ES11Entry1, function (err, ES11Entry) {
    if (err) {
      res.send(err);
    } else {
      res.json(ES11Entry);
    }
  });
};

exports.create_a_RptES11Entry = function (req, res) {
  var ES11EntryRpt = new ES11Entry(req.body);

  ES11Entry.createRptES11Entry(ES11EntryRpt, function (err, ES11Entry) {
    if (err) {
      res.send(err);
    } else {
      res.json(ES11Entry);
    }
  });
};

exports.read_a_RptESS11 = function (req, res) {
  var RptESS11 = req.params;
  ES11Entry.getRptESS11(RptESS11, function (err, ES11Entry) {
    if (err) {
      res.send(err);
    } else {
      res.json(ES11Entry);
    }
  });
};
exports.create_a_RptES11Submit = function (req, res) {
  var ES11EntryRptsubmit = new ES11Entry(req.body);

  ES11Entry.createRptES11Submit(ES11EntryRptsubmit, function (err, ES11Submit) {
    if (err) {
      res.send(err);
    } else {
      res.json(ES11Submit);
    }
  });
};
exports.read_a_exchangeRptESS11 = function (req, res) {
  var exchangeRptESS11 = req.params;
  ES11Entry.getExchangeforRptESS11(exchangeRptESS11, function (err, exchangeES11Entry) {
    if (err) {
      res.send(err);
    } else {
      res.json(exchangeES11Entry);
    }
  });
};