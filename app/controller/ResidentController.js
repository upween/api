
'use strict';

var Resident = require('../model/ResidentModel.js');

exports.read_a_Resident = function (req, res) {
  var fetch_Resident_master = req.params;
  Resident.getResident(req.params, function (err, Resident) {
    if (err) {
      res.send(err);
    } else {
      res.json(Resident);
    }
  });
};