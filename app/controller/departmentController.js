'use strict';

var Department = require('../model/departmentModel.js');

exports.read_a_department = function (req, res) {
  var fetch_department = req.params;
  Department.getDepartment(req.params, function (err, department) {
    if (err) {
      res.send(err);
    } else {
      res.json(department);
    }
  });
};
exports.read_a_departmentJob = function (req, res) {
  var fetch_8Department = req.params;
  Department.getDepartmentjob(req.params, function (err, departmentJob) {
    if (err) {
      res.send(err);
    } else {
      res.json(departmentJob);
    }
  });
};