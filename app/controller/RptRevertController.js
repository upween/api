'use strict';

var RptRevert = require('../model/RptRevertModel.js');

exports.read_a_RptRevert = function (req, res) {

  var CI_Pro_Rpt_Revert = req.params;
  RptRevert.getRptRevert(req.params, function (err, RptRevert) {
    if (err) {
      res.send(err);
    } else {
      res.json(RptRevert);
    }
  });
};