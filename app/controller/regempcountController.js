'use strict';

var regempcount = require('../model/regempcountModel.js');

exports.read_a_regempcount = function (req, res) {
  var read_a_regempcount = req.params;
  regempcount.getregempcount(req.params, function (err, regempcount) {
    if (err) {
      res.send(err);
    } else {
      res.json(regempcount);
    }
  });
};