'use strict';

var CCDetails = require('../model/CCDetailsModel.js');

exports.create_a_CCDetails = function (req, res) {
  var CI_Pro_CC_Details = new CCDetails(req.body);

  CCDetails.createCCDetails(CI_Pro_CC_Details, function (err, CCDetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(CCDetails);
    }
  });
};