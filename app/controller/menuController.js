'use strict';

var Menu = require('../model/menuModel.js');

exports.create_a_menu = function (req, res) {
  var new_menu = new Menu(req.body);

  Menu.createMenu(new_menu, function (err, menu) {
    if (err) {
      res.send(err);
    } else {
      res.json(menu);
    }
  });
};
exports.read_a_menu = function (req, res) {

  var fetch_menu = req.params;
  Menu.getMenu(req.params, function (err, menu) {
    if (err) {
      res.send(err);
    } else {
      res.json(menu);
    }
  });
};