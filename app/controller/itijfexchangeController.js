'use strict';

var itijfexchange = require('../model/itijfexchangeModel.js');

exports.create_a_itijfexchange = function (req, res) {
  var InsUpd_iti_jf_exchange = new itijfexchange(req.body);

  itijfexchange.createitijfexchange(InsUpd_iti_jf_exchange, function (err, itijfexchange) {
    if (err) {
      res.send(err);
    } else {
      res.json(itijfexchange);
    }
  });
};

exports.create_a_itijfempmapping = function (req, res) {
  var InsUpd_iti_jf_emp_mapping = new itijfexchange(req.body);

  itijfexchange.createitijfempmapping(InsUpd_iti_jf_emp_mapping, function (err, itijfempmapping) {
    if (err) {
      res.send(err);
    } else {
      res.json(itijfempmapping);
    }
  });
};
exports.read_a_itijfexchange = function (req, res) {

  var Fetch_ci_jf_exchjfdtls = req.params;
  itijfexchange.getitijfexchange(req.params, function (err, itijfexchange) {
    if (err) {
      res.send(err);
    } else {
      res.json(itijfexchange);
    }
  });
};
exports.read_a_hallticket = function (req, res) {

  var Fetch_ci_jf_exchjfdtls = req.params;
  itijfexchange.gethallticket(req.params, function (err, hallticket) {
    if (err) {
      res.send(err);
    } else {
      res.json(hallticket);
    }
  });
};
exports.read_a_itiinstitute = function (req, res) {

  itijfexchange.getitinstitute(req.params, function (err, getitinstitute) {
    if (err) {
      res.send(err);
    } else {
      res.json(getitinstitute);
    }
  });
};

// exports.read_a_exchjfdtlsbyStatus = function(req, res) {

//   var Fetch_ci_jf_exchjfdtlsByStatus = req.params;
//   exchjfdtls.getexchjfdtlsbyStatus(req.params, function(err, exchjfdtlsByStatus) {
//     if (err){
//       res.send(err);
//     }
//       else{
//     res.json(exchjfdtlsByStatus);
//       }
//   });
// };