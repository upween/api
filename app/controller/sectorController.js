'use strict';

var sector = require('../model/sectorModel.js');

exports.read_a_sector = function (req, res) {
  var fetch_sector_master = req.params;
  sector.getsector(req.params, function (err, sector) {
    if (err) {
      res.send(err);
    } else {
      res.json(sector);
    }
  });
};