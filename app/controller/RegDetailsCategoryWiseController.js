
'use strict';

var RegDetailsCategoryWise = require('../model/RegDetailsCategoryWiseModel.js');

exports.read_a_RegDetailsCategoryWise = function (req, res) {
  var RegistrationDetails_CategoryWise = req.params;
  RegDetailsCategoryWise.getRegDetailsCategoryWise(req.params, function (err, RegDetailsCategoryWise) {
    if (err) {
      res.send(err);
    } else {
      res.json(RegDetailsCategoryWise);
    }
  });
};