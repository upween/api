'use strict';

var exchjfdtls = require('../model/exchjfdtlsModel.js');

exports.create_a_exchjfdtls = function (req, res) {
  var InsUpd_ci_jf_exchjfdtls = new exchjfdtls(req.body);

  exchjfdtls.createexchjfdtls(InsUpd_ci_jf_exchjfdtls, function (err, exchjfdtls) {
    if (err) {
      res.send(err);
    } else {
      res.json(exchjfdtls);
    }
  });
};
exports.read_a_exchjfdtls = function (req, res) {

  var Fetch_ci_jf_exchjfdtls = req.params;
  exchjfdtls.getexchjfdtls(req.params, function (err, exchjfdtls) {
    if (err) {
      res.send(err);
    } else {
      res.json(exchjfdtls);
    }
  });
};

exports.read_a_exchjfdtlsbyStatus = function (req, res) {

  var Fetch_ci_jf_exchjfdtlsByStatus = req.params;
  exchjfdtls.getexchjfdtlsbyStatus(req.params, function (err, exchjfdtlsByStatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(exchjfdtlsByStatus);
    }
  });
};
exports.create_a_empexchjfdtls = function (req, res) {
  var InsUpd_ci_jf_exchjfdtls = new exchjfdtls(req.body);

  exchjfdtls.createempexchjfdtls(InsUpd_ci_jf_exchjfdtls, function (err, exchjfdtls) {
    if (err) {
      res.send(err);
    } else {
      res.json(exchjfdtls);
    }
  });
};
exports.read_a_empexchjfdtls = function (req, res) {

  var Fetch_ci_jf_exchjfdtls = req.params;
  exchjfdtls.getempexchjfdtls(req.params, function (err, exchjfdtls) {
    if (err) {
      res.send(err);
    } else {
      res.json(exchjfdtls);
    }
  });
};

exports.create_a_empjfReport = function (req, res) {
  var rptexchjfdtls = new exchjfdtls(req.body);

  exchjfdtls.createempjfreport(rptexchjfdtls, function (err, empjfrpt) {
    if (err) {
      res.send(err);
    } else {
      res.json(empjfrpt);
    }
  });
};

