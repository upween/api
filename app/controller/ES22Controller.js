'use strict';

var ES22Entry = require('../model/ES22Model.js');


exports.read_a_es22RptEntry = function (req, res) {
  var ES22 = req.params;
  ES22Entry.getES22Annual(ES22, function (err, es22entry) {
    if (err) {
      res.send(err);
    } else {
      res.json(es22entry);
    }
  });
};

exports.update_a_ES22Entry = function (req, res) {
  var CI_Pro_Rpt_ES22RptInsert = new ES22Entry(req.body);

  ES22Entry.updateES22Rpt(CI_Pro_Rpt_ES22RptInsert, function (err, ES22Rpt) {
    if (err) {
      res.send(err);
    } else {
      res.json(ES22Rpt);
    }
  });
};
exports.create_a_ES22Rpt = function (req, res) {
  var CI_Pro_Rpt_ES22RptUpdate = new ES22Entry(req.body);

  ES22Entry.createES22Rpt(CI_Pro_Rpt_ES22RptUpdate, function (err, ES22Rpt) {
    if (err) {
      res.send(err);
    } else {
      res.json(ES22Rpt);
    }
  });
};