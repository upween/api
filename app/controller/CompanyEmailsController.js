'use strict';

var CompanyEmails = require('../model/CompanyEmailsModel.js');

exports.create_a_CompanyEmails = function (req, res) {
  var insUpd_CompanyMails = new CompanyEmails(req.body);

  CompanyEmails.createCompanyEmails(insUpd_CompanyMails, function (err, CompanyEmails) {
    if (err) {
      res.send(err);
    } else {
      res.json(CompanyEmails);
    }
  });
};
exports.read_a_CompanyEmails = function (req, res) {
  var fetch_CompanyMails = req.params;
  CompanyEmails.getCompanyEmails(req.params, function (err, CompanyEmails) {
    if (err) {
      res.send(err);
    } else {
      res.json(CompanyEmails);
    }
  });
};