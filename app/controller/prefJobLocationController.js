'use strict';

var PrefJobLocation = require('../model/prefJobLocationModel.js');

exports.read_a_PrefJobLocation = function (req, res) {
  var read_a_PrefJobLocation = req.params;
  PrefJobLocation.getPrefJobLocation(req.params, function (err, PrefJobLocation) {
    if (err) {
      res.send(err);
    } else {
      res.json(PrefJobLocation);
    }
  });
};