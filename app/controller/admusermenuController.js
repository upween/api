'use strict';

var admusermenu = require('../model/admusermenuModel.js');

exports.read_a_admusermenu = function (req, res) {

  var fetch_adm_user_menu = req.params;
  admusermenu.getadmusermenu(req.params, function (err, admusermenu) {
    if (err) {
      res.send(err);
    } else {
      res.json(admusermenu);
    }
  });
};