'use strict';

var PhysicalDetails = require('../model/physicaldetailModel.js');

exports.create_a_physicaldetails = function (req, res) {
  var new_physicaldetails = new PhysicalDetails(req.body);

  PhysicalDetails.createPhysicalDetails(new_physicaldetails, function (err, physicaldetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(physicaldetails);
    }
  });
};
exports.read_a_physicaldetails = function (req, res) {
  var fetch_physicalOtherDetail = req.params;
  PhysicalDetails.getPhysicalDetails(req.params, function (err, physicaldetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(physicaldetails);
    }
  });
};