'use strict';

var Month = require('../model/monthModel.js');

exports.read_a_month = function (req, res) {
  var fetch_month = req.params;
  Month.getMonth(req.params, function (err, month) {
    if (err) {
      res.send(err);
    } else {
      res.json(month);
    }
  });
};