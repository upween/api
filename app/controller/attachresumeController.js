'use strict';

var Attachresume = require('../model/attachresumeModel.js');

exports.create_a_attachresume = function (req, res) {
  var new_attachresume = new Attachresume(req.body);

  Attachresume.createAttachresume(new_attachresume, function (err, attachresume) {
    if (err) {
      res.send(err);
    } else {
      res.json(attachresume);
    }
  });
};
exports.read_a_attachresume = function (req, res) {
  var fetch_attachresume = req.params;
  Attachresume.getAttachresume(req.params, function (err, attachresume) {
    if (err) {
      res.send(err);
    } else {
      res.json(attachresume);
    }
  });
};