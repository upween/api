'use strict';

var Registration = require('../model/registrationModel.js');

exports.create_a_registration = function (req, res) {
  var new_registration = new Registration(req.body);

  Registration.createRegistration(new_registration, function (err, registration) {
    if (err) {
      res.send(err);
    } else {
      res.json(registration);
    }
  });
};
exports.read_a_registration = function (req, res) {
  var fetch_registration = req.params;
  Registration.getRegistration(req.params, function (err, registration) {
    if (err) {
      res.send(err);
    } else {
      res.json(registration);
    }
  });
};
exports.read_a_knowregistration = function (req, res) {
  var fetch_registration = req.params;
  Registration.getKnowRegistration(req.params, function (err, registration) {
    if (err) {
      res.send(err);
    } else {
      res.json(registration);
    }
  });
};
exports.read_a_RegistrationCard = function (req, res) {
  var fetch_RegistrationCard = req.params;
  Registration.getRegistrationCard(req.params, function (err, registration) {
    if (err) {
      res.send(err);
    } else {
      res.json(registration);
    }
  });
};
exports.create_a_renewregistration = function (req, res) {
  var renew_registration = new Registration(req.body);

  Registration.createRegistrationrenew(renew_registration, function (err, renewregistration) {
    if (err) {
      res.send(err);
    } else {
      res.json(renewregistration);
    }
  });
};
exports.create_a_renewregistrationforolduser = function (req, res) {
  var renew_registrationforolduser = new Registration(req.body);

  Registration.createrenewforolduser(renew_registrationforolduser, function (err, renewregistrationforolduser) {
    if (err) {
      res.send(err);
    } else {
      res.json(renewregistrationforolduser);
    }
  });
};