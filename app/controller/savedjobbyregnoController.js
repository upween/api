
'use strict';

var savedjobbyregno = require('../model/savedjobbyregnoModel.js');

exports.read_a_savedjobbyregno = function (req, res) {
  var fetch_savedjobbyregno = req.params;
  savedjobbyregno.getsavedjobbyregno(req.params, function (err, savedjobbyregno) {
    if (err) {
      res.send(err);
    } else {
      res.json(savedjobbyregno);
    }
  });
};