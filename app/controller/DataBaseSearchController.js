'use strict';

var DataBaseSearch = require('../model/DataBaseSearchModel.js');

exports.create_a_DataBaseSearch = function (req, res) {
  var DataBaseSearch1 = new DataBaseSearch(req.body);

  DataBaseSearch.createDataBaseSearch(DataBaseSearch1, function (err, DataBaseSearch) {
    if (err) {
      res.send(err);
    } else {
      res.json(DataBaseSearch);
    }
  });
};


exports.create_a_searchengineutility_logs = function (req, res) {
  var searchengineutility_log = new DataBaseSearch(req.body);

  DataBaseSearch.createsearchengineutility_logs(searchengineutility_log, function (err, searchengineutilitylog) {
    if (err) {
      res.send(err);
    } else {
      res.json(searchengineutilitylog);
    }
  });
};
