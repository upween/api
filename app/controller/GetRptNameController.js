'use strict';

var GetRptName = require('../model/GetRptNameModel.js');

exports.read_a_GetRptName = function (req, res) {
  var Adm_Pro_GetRptName = req.params;
  GetRptName.getGetRptName(req.params, function (err, GetRptName) {
    if (err) {
      res.send(err);
    } else {
      res.json(GetRptName);
    }
  });
};

exports.read_a_GetOfficeReport = function (req, res) {
  var ADM_PRO_CI_GetOffice_Reportwise = req.params;
  GetRptName.getGetOfficeReport(req.params, function (err, GetOfficeReport) {
    if (err) {
      res.send(err);
    } else {
      res.json(GetOfficeReport);
    }
  });
};