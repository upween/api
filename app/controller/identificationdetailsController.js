
'use strict';

var IdentificationDetails = require('../model/identificationdetailsModel.js');

exports.create_a_identificationdetails = function (req, res) {
  var new_identificationdetails = new IdentificationDetails(req.body);

  IdentificationDetails.createIdentificationDetails(new_identificationdetails, function (err, identificationdetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(identificationdetails);
    }
  });
};
exports.read_a_identificationdetails = function (req, res) {
  var fetch_identificationdetails = req.params;
  IdentificationDetails.getIdentificationDetails(req.params, function (err, identificationdetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(identificationdetails);
    }
  });
};