'use strict';

var City = require('../model/cityModel.js');

exports.create_a_city = function (req, res) {
  var new_city = new City(req.body);

  City.createCity(new_city, function (err, city) {
    if (err) {
      res.send(err);
    } else {
      res.json(city);
    }
  });
};
exports.read_a_city = function (req, res) {
  var fetch_city = req.params;
  City.getCity(req.params, function (err, city) {
    if (err) {
      res.send(err);
    } else {
      res.json(city);
    }
  });
};
exports.read_some_city = function (req, res) {
  var fetch_8City = req.params;
  City.getsomeCity(req.params, function (err, city) {
    if (err) {
      res.send(err);
    } else {
      res.json(city);
    }
  });
};