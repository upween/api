'use strict';

var LoginInfo = require('../model/logininfoModel.js');

exports.read_a_logininfo = function (req, res) {
  LoginInfo.getLoginInfo(function (err, logininfo) {

    if (err) {

      res.send(err);
    } else {

      res.send(logininfo);
    }
  });
};