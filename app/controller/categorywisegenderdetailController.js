'use strict';

var Categorywisegenderdetail = require('../model/categorywisegenderdetailModel.js');

exports.read_a_Categorywisegenderdetail = function (req, res) {
  Categorywisegenderdetail.getCategorywisegenderdetail(req.params, function (err, Categorywisegenderdetail) {
    if (err) {
      res.send(err);
    } else {
      res.json(Categorywisegenderdetail);
    }
  });
};