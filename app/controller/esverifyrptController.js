'use strict';

var esverifyrpt = require('../model/esverifyrptModel.js');

exports.create_a_esverifyrpt = function (req, res) {
  var new_esverifyrpt = new esverifyrpt(req.body);

  esverifyrpt.createesverifyrpt(new_esverifyrpt, function (err, esverifyrpt) {
    if (err) {
      res.send(err);
    } else {
      res.json(esverifyrpt);
    }
  });
};