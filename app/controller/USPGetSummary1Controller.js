'use strict';

var USPGetSummary1 = require('../model/USPGetSummary1Model.js');

exports.read_a_USPGetSummary1 = function (req, res) {

  var Rpt_USP_GetSummary1 = req.params;
  USPGetSummary1.getUSPGetSummary1(req.params, function (err, USPGetSummary1) {
    if (err) {
      res.send(err);
    } else {
      res.json(USPGetSummary1);
    }
  });
};

exports.read_a_USPGetSummary2 = function (req, res) {

  var Rpt_USP_GetSummary2 = req.params;
  USPGetSummary1.getUSPGetSummary2(req.params, function (err, USPGetSummary2) {
    if (err) {
      res.send(err);
    } else {
      res.json(USPGetSummary2);
    }
  });
};