'use strict';

var EmpChangePass = require('../model/EmployerChangePasswordModel.js');

exports.create_a_empChangepass = function (req, res) {
  var new_empChangepass = new EmpChangePass(req.body);

  EmpChangePass.createEmpChangePassword(new_empChangepass, function (err, empChangepass) {
    if (err) {
      res.send(err);
    } else {
      res.json(empChangepass);
    }
  });
};