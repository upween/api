'use strict';

var jobseekerregistration = require('../model/jobseekerregistrationModel.js');

exports.create_a_jobseekerregistration = function (req, res) {
  var upd_jobseeker_registration = new jobseekerregistration(req.body);

  jobseekerregistration.createjobseekerregistration(upd_jobseeker_registration, function (err, jobseekerregistration) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobseekerregistration);
    }
  });
};




exports.read_a_DeleteJobseekerRegistration = function (req, res) {
  var Delete_Detail = req.params;
  jobseekerregistration.getDeleteJobseekerRegistration(req.params, function (err, jobseekerregistration) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobseekerregistration);
    }
  });
};