'use strict';

var Lrstatus = require('../model/LrstatusModel.js');

exports.read_a_Lrstatus = function (req, res) {
  Lrstatus.getLrstatus(req.params, function (err, Lrstatus) {
    if (err) {
      res.send(err);
    } else {
      res.json(Lrstatus);
    }
  });
};