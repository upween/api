'use strict';

var typingstenomaster = require('../model/typingstenomasterModel.js');

exports.read_a_typingstenomaster = function (req, res) {
  var read_a_typingstenomaster = req.params;
  typingstenomaster.gettypingstenomaster(req.params, function (err, typingstenomaster) {
    if (err) {
      res.send(err);
    } else {
      res.json(typingstenomaster);
    }
  });
};