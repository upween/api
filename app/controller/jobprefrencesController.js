
'use strict';

var jobprefrences = require('../model/jobprefrencesModel.js');

exports.read_a_jobprefrences = function (req, res) {
  var fetch_Job_Prefrences = req.params;
  jobprefrences.getjobprefrences(req.params, function (err, jobprefrences) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobprefrences);
    }
  });
};