'use strict';

var University = require('../model/universityModel.js');

exports.create_a_university = function (req, res) {
  var new_university = new University(req.body);

  University.createUniversity(new_university, function (err, university) {
    if (err) {
      res.send(err);
    } else {
      res.json(university);
    }
  });
};
exports.read_a_university = function (req, res) {
  var fetch_university = req.params;
  University.getUniversity(req.params, function (err, university) {
    if (err) {
      res.send(err);
    } else {
      res.json(university);
    }
  });
};