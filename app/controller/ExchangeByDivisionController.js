'use strict';

var ExchangeByDivision = require('../model/ExchangeOfficeByDivisionModel.js');

exports.read_a_ExchangeByDivision = function (req, res) {

  var Adm_Pro_GetOnlyWorkingExchange = req.params;
  ExchangeByDivision.getExchangeByDivision(req.params, function (err, ExchangeByDivision) {
    if (err) {
      res.send(err);
    } else {
      res.json(ExchangeByDivision);
    }
  });
};