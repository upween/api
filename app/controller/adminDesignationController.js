'use strict';

var adminDesignation = require('../model/adminDesignationModel.js');

exports.create_a_adminDesignation = function (req, res) {
  var InsUpd_ci_mst_designation = new adminDesignation(req.body);

  adminDesignation.createadminDesignation(InsUpd_ci_mst_designation, function (err, adminDesignation) {
    if (err) {
      res.send(err);
    } else {
      res.json(adminDesignation);
    }
  });
};

exports.read_a_adminDesignation = function (req, res) {

  var Fetch_ci_mst_designation = req.params;
  adminDesignation.getadminDesignation(req.params, function (err, adminDesignation) {
    if (err) {
      res.send(err);
    } else {
      res.json(adminDesignation);
    }
  });
};