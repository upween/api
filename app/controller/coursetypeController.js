'use strict';

var CourseType = require('../model/coursetypeModel.js');

exports.create_a_coursetype = function (req, res) {
  var new_coursetype = new CourseType(req.body);

  CourseType.createCourseType(new_coursetype, function (err, coursetype) {
    if (err) {
      res.send(err);
    } else {
      res.json(coursetype);
    }
  });
};
exports.read_a_coursetype = function (req, res) {
  var fetch_coursetype = req.params;
  CourseType.getCourseType(req.params, function (err, coursetype) {
    if (err) {
      res.send(err);
    } else {
      res.json(coursetype);
    }
  });
};