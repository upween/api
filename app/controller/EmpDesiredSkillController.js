'use strict';

var EmpDesiredSkill = require('../model/EmpDesiredSkillModel.js');

exports.read_a_EmpDesiredSkill = function (req, res) {

  var Employer_DesiredSkill = req.params;
  EmpDesiredSkill.getEmpDesiredSkill(req.params, function (err, EmpDesiredSkill) {
    if (err) {
      res.send(err);
    } else {
      res.json(EmpDesiredSkill);
    }
  });
};