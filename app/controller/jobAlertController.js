'use strict';

var JobAlert = require('../model/jobAlertModel.js');

exports.create_a_jobAlert = function (req, res) {
  var new_jobAlert = new JobAlert(req.body);

  JobAlert.createJobAlert(new_jobAlert, function (err, jobAlert) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobAlert);
    }
  });
};