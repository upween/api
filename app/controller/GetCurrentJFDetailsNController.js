'use strict';

var GetCurrentJFDetailsN = require('../model/GetCurrentJFDetailsNModel.js');

exports.read_a_GetCurrentJFDetailsN = function (req, res) {

  var SP_GetCurrentActiveJFDetailsN = req.params;
  GetCurrentJFDetailsN.getGetCurrentJFDetailsN(req.params, function (err, GetCurrentJFDetailsN) {
    if (err) {
      res.send(err);
    } else {
      res.json(GetCurrentJFDetailsN);
    }
  });
};