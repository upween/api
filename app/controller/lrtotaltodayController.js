'use strict';

var LRtotaltoday = require('../model/lrtotaltodayModel.js');

exports.read_a_LRtotaltoday = function (req, res) {
  LRtotaltoday.getLRtotaltoday(req.params, function (err, LRtotaltoday) {
    if (err) {
      res.send(err);
    } else {
      res.json(LRtotaltoday);
    }
  });
};