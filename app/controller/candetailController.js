'use strict';

var candetail = require('../model/candetailModel.js');

exports.read_a_candetail = function (req, res) {
  var fetch_can_detail = req.params;
  candetail.getcandetail(req.params, function (err, candetail) {
    if (err) {
      res.send(err);
    } else {
      res.json(candetail);
    }
  });
};