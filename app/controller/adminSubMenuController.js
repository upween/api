'use strict';

var AdminSubMenu = require('../model/adminSubMenuModel.js');

exports.read_a_AdminSubMenu = function (req, res) {
  var fetch_Submenu = req.params;
  AdminSubMenu.getAdminSubMenu(req.params, function (err, AdminSubMenu) {
    if (err) {
      res.send(err);
    } else {
      res.json(AdminSubMenu);
    }
  });
};

exports.create_a_AdminSubMenu = function (req, res) {
  var new_AdminSubMenu = new AdminSubMenu(req.body);

  AdminSubMenu.createAdminSubMenu(new_AdminSubMenu, function (err, AdminSubMenu) {
    if (err) {
      res.send(err);
    } else {
      res.json(AdminSubMenu);
    }
  });
};