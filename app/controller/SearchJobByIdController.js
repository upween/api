'use strict';

var SearchJobById = require('../model/SearchJobByIdModel.js');

exports.read_a_SearchJobById = function (req, res) {

  var SearchJobId = req.params;
  SearchJobById.getSearchJobById(req.params, function (err, SearchJobById) {
    if (err) {
      res.send(err);
    } else {
      res.json(SearchJobById);
    }
  });
};