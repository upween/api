'use strict';

var FeedBack = require('../model/feedbackModel.js');

exports.create_a_feedback = function (req, res) {
  var new_feedback = new FeedBack(req.body);

  FeedBack.createFeedBack(new_feedback, function (err, feedback) {
    if (err) {
      res.send(err);
    } else {
      res.json(feedback);
    }
  });
};
exports.read_a_feedback = function (req, res) {
  var fetch_feedback = req.params;
  FeedBack.getFeedBack(req.params, function (err, feedback) {
    if (err) {
      res.send(err);
    } else {
      res.json(feedback);
    }
  });
};

exports.read_a_FeedbackUser = function (req, res) {
  var Check_UserAvailability = req.params;
  FeedBack.getFeedbackUser(req.params, function (err, feedback) {
    if (err) {
      res.send(err);
    } else {
      res.json(feedback);
    }
  });
};