'use strict';

var CounsellingSummary = require('../model/summarycounsellingModel.js');

exports.create_a_summaryform = function (req, res) {
  var insupd_form = new CounsellingSummary(req.body);

  CounsellingSummary.createCounsellingsummary(insupd_form, function (err, summary) {
    if (err) {
      res.send(err);
    } else {
      res.json(summary);
    }
  });
};