'use strict';

var AreaOfInterest = require('../model/areaOfInterestModel.js');

exports.read_a_AreaOfInterest = function (req, res) {
  var fetch_AreaOfInterest_master = req.params;
  AreaOfInterest.getAreaOfInterest(req.params, function (err, AreaOfInterest) {
    if (err) {
      res.send(err);
    } else {
      res.json(AreaOfInterest);
    }
  });
};