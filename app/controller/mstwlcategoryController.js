'use strict';

var mstwlcategory = require('../model/mstwlcategoryModel.js');

exports.create_a_mstwlcategory = function (req, res) {
  var InsUpd_ci_wl_mst_wlcategory = new mstwlcategory(req.body);

  mstwlcategory.createmstwlcategory(InsUpd_ci_wl_mst_wlcategory, function (err, mstwlcategory) {
    if (err) {
      res.send(err);
    } else {
      res.json(mstwlcategory);
    }
  });
};
exports.read_a_mstwlcategory = function (req, res) {

  var Fetch_ci_wl_mst_wlcategory = req.params;
  mstwlcategory.getmstwlcategory(req.params, function (err, mstwlcategory) {
    if (err) {
      res.send(err);
    } else {
      res.json(mstwlcategory);
    }
  });
};