'use strict';

var advertisement = require('../model/advertisementModel.js');

exports.create_a_advertisement = function (req, res) {
  var InsUpd_ci_advertisement = new advertisement(req.body);

  advertisement.createadvertisement(InsUpd_ci_advertisement, function (err, advertisement) {
    if (err) {
      res.send(err);
    } else {
      res.json(advertisement);
    }
  });
};
exports.read_a_advertisement = function (req, res) {

  var Fetch_ci_advertisement = req.params;
  advertisement.getadvertisement(req.params, function (err, advertisement) {
    if (err) {
      res.send(err);
    } else {
      res.json(advertisement);
    }
  });
};