'use strict';

var rtpnotsendho = require('../model/rtpnotsendhoModel.js');

exports.read_a_rtpnotsendho = function (req, res) {
  var fetch_rtpnotsendho = req.params;
  rtpnotsendho.getrtpnotsendho(req.params, function (err, rtpnotsendho) {
    if (err) {
      res.send(err);
    } else {
      res.json(rtpnotsendho);
    }
  });
};