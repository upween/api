'use strict';

var PersonalDetails = require('../model/personaldetailsModel.js');

exports.create_a_personaldetails = function (req, res) {
  var new_personaldetails = new PersonalDetails(req.body);

  PersonalDetails.createPersonalDetails(new_personaldetails, function (err, personaldetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(personaldetails);
    }
  });
};
exports.read_a_personaldetails = function (req, res) {
  var fetch_personaldetails = req.params;
  PersonalDetails.getPersonalDetails(req.params, function (err, personaldetails) {
    if (err) {
      res.send(err);
    } else {
      res.json(personaldetails);
    }
  });
};


exports.create_a_Transfer_location = function (req, res) {
  var Transfer_location = new PersonalDetails(req.body);

  PersonalDetails.createTransfer_location(Transfer_location, function (err, Transfer_location) {
    if (err) {
      res.send(err);
    } else {
      res.json(Transfer_location);
    }
  });
};