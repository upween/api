'use strict';

var WorkingExchange = require('../model/WorkingExchangeModel.js');

exports.read_a_WorkingExchange = function (req, res) {

  var Adm_Pro_GetOnlyWorkingExchange = req.params;
  WorkingExchange.getWorkingExchange(req.params, function (err, WorkingExchange) {
    if (err) {
      res.send(err);
    } else {
      res.json(WorkingExchange);
    }
  });
};