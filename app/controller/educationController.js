'use strict';

var Education = require('../model/educationModel.js');

exports.create_a_education = function (req, res) {
  var new_education = new Education(req.body);

  Education.createEducation(new_education, function (err, education) {
    if (err) {
      res.send(err);
    } else {
      res.json(education);
    }
  });
};
exports.read_a_education = function (req, res) {
  var fetch_education = req.params;
  Education.getEducation(req.params, function (err, education) {
    if (err) {
      res.send(err);
    } else {
      res.json(education);
    }
  });
};