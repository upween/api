'use strict';

var Jobseekerinfo = require('../model/JobseekerinfoModel.js');

exports.read_a_Jobseekerinfo = function (req, res) {
  var Jobseeker_info = req.params;
  Jobseekerinfo.getJobseekerinfo(req.params, function (err, Jobseekerinfo) {
    if (err) {
      res.send(err);
    } else {
      res.json(Jobseekerinfo);
    }
  });
};