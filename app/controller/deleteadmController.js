
'use strict';

var deleteadm = require('../model/deleteadmModel.js');

exports.read_a_deleteadm = function (req, res) {
  var Delete_Adm_Details = req.params;
  deleteadm.getdeleteadm(req.params, function (err, deleteadm) {
    if (err) {
      res.send(err);
    } else {
      res.json(deleteadm);
    }
  });
};