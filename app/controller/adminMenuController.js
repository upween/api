'use strict';

var AdminMenu = require('../model/adminMenuModel.js');

exports.read_a_AdminMenu = function (req, res) {
  var fetch_adm_user_mst_menu = req.params;
  AdminMenu.getAdminMenu(req.params, function (err, AdminMenu) {
    if (err) {
      res.send(err);
    } else {
      res.json(AdminMenu);
    }
  });
};

exports.create_a_AdminMenu = function (req, res) {
  var new_AdminMenu = new AdminMenu(req.body);

  AdminMenu.createAdminMenu(new_AdminMenu, function (err, AdminMenu) {
    if (err) {
      res.send(err);
    } else {
      res.json(AdminMenu);
    }
  });
};