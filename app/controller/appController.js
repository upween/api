'use strict';

var Category = require('../model/appModel.js');
/* exports.list_all_category = function(req, res) {
    Category.getAllCategory(function(err, category) {

    console.log('controller')
    if (err)
      res.send(err);
      console.log('res', category);
    res.send(category);
  });
}; Vijay Bhargava */
exports.create_a_category = function (req, res) {
  var new_category = new Category(req.body);
  //console.log(new_category,'insert');
  //handles null error 
  //if(!new_category.CategoryName || !new_category.IsActive){
  //  res.status(400).send({ error:true, message: 'Please provide Name/Active' });
  //}
  //else{
  Category.createCategory(new_category, function (err, category) {
    if (err) {
      res.send(err);
    } else {
      res.json(category);
    }
  });
  //}
};
exports.read_a_category = function (req, res) {
  var fetch_category = req.params;
  //console.log(req.params,'select');
  //console.log(req.params.p_categoryid,'select');
  Category.getCategoryById(req.params, function (err, category) {
    if (err) {
      res.send(err);
    } else {

      res.json(category);
    }
  });
};

exports.read_a_JobByIndustry = function (req, res) {
  var fetch_feedfetch_Category = req.params;
  Category.getJobByIndustry(req.params, function (err, JobByIndustry) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobByIndustry);
    }
  });
};
exports.read_a_JobByLocation = function (req, res) {
  var fetch_State21 = req.params;
  Category.getJobByLocation(req.params, function (err, JobByLocation) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobByLocation);
    }
  });
};
exports.read_a_GovtJobByTitle = function (req, res) {
  var fetch_State21 = req.params;
  Category.getGovtJobByTitle(req.params, function (err, GovtJobByTitle) {
    if (err) {
      res.send(err);
    } else {
      res.json(GovtJobByTitle);
    }
  });
};
exports.read_a_GovtJobByIndustry = function (req, res) {
  var fetch_feedfetch_Category = req.params;
  Category.getGovtJobByIndustry(req.params, function (err, GovtJobByIndustry) {
    if (err) {
      res.send(err);
    } else {
      res.json(GovtJobByIndustry);
    }
  });
};
exports.read_a_GovtJobByLocation = function (req, res) {
  var fetch_State21 = req.params;
  Category.getGovtJobByLocation(req.params, function (err, GovtJobByLocation) {
    if (err) {
      res.send(err);
    } else {
      res.json(GovtJobByLocation);
    }
  });
};
exports.read_a_JobByTitle = function (req, res) {
  var fetch_State21 = req.params;
  Category.getJobByTitle(req.params, function (err, JobByTitle) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobByTitle);
    }
  });
};
exports.read_a_HomePageCounter = function (req, res) {
  // var fetch_feedfetch_Category = req.params;
  Category.getHomePageCounter(req.params, function (err, HomePageCounter) {
    if (err) {
      res.send(err);
    } else {
      res.json(HomePageCounter);
    }
  });
};


exports.read_a_JobFairCounterDashboard = function (req, res) {
  // var fetch_feedfetch_Category = req.params;
  Category.getJobFairCounterDashboard(req.params, function (err, JobFairCounterDashboard) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobFairCounterDashboard);
    }
  });
};
/* exports.update_a_category = function(req, res) {
    Category.updateById(req.params.categoryId, new Category(req.body), function(err, category) {
    if (err)
      res.send(err);
    res.json(category);
  });
};
exports.delete_a_category = function(req, res) {


    Category.remove( req.params.categoryId, function(err, category) {
    if (err)
      res.send(err);
    res.json({ message: 'Category successfully deleted' });
  });
}; */