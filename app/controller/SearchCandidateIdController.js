'use strict';

var SearchCandidateId = require('../model/SearchCandidateIdModel.js');

exports.create_a_SearchCandidateId = function (req, res) {
  var CandidateId = req.params;
  var searchcandidate = new SearchCandidateId(req.body);
  SearchCandidateId.createSearchCandidateId(searchcandidate, function (err, SearchCandidateId) {
    if (err) {
      res.send(err);
    } else {
      res.json(SearchCandidateId);
    }
  });
};

exports.read_a_JobSearch = function (req, res) {
  var Candidate_JobSearch = req.params;
  SearchCandidateId.getJobSearch(req.params, function (err, JobSearch) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobSearch);
    }
  });
};
exports.create_a_jobparticipant = function(req, res) {
  var jobparticipant = new SearchCandidateId(req.body);
 
  SearchCandidateId.createjobparticipant(jobparticipant, function(err, jobparticipant1) {
      if (err){
        res.send(err);
      }
      else{
        res.send(jobparticipant1);
      }
    });
};
exports.read_a_JSjobparticipant = function (req, res) {
  var jobparticipant = req.params;
  SearchCandidateId.getJSjobparticipant(req.params, function (err,jobparticipant) {
    if (err) {
      res.send(err);
    } else {
      res.json(jobparticipant);
    }
  });
};