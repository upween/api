'use strict';

var advcategory = require('../model/advcategoryModel.js');

exports.create_a_advcategory = function (req, res) {
  var InsUpd_ci_adv_mst_advcategory = new advcategory(req.body);

  advcategory.createadvcategory(InsUpd_ci_adv_mst_advcategory, function (err, advcategory) {
    if (err) {
      res.send(err);
    } else {
      res.json(advcategory);
    }
  });
};
exports.read_a_advcategory = function (req, res) {

  var Fetch_ci_adv_mst_advcategory = req.params;
  advcategory.getadvcategory(req.params, function (err, advcategory) {
    if (err) {
      res.send(err);
    } else {
      res.json(advcategory);
    }
  });
};