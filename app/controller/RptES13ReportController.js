'use strict';
var ES13Rpt  = require('../model/RptES13ReportModel.js');


exports.read_a_ES13 = function(req, res) {
    var Fetch_ES13= req.params;
    ES13Rpt.getES13(req.params, function(err, ES13Rpt) {
      if (err){
        res.send(err);
      }
      else{
      res.json(ES13Rpt);
      }
    });
  };
  exports.create_a_ES13Entry = function (req, res) {
    var es13rptentry = new ES13Rpt(req.body);
  
    ES13Rpt.createES13Rpt(es13rptentry, function (err, es13) {
      if (err) {
        res.send(err);
      } else {
        res.json(es13);
      }
    });
  };