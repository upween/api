'use strict';

var Itiinstitute = require('../model/itiinstituteModel.js');

exports.create_a_Itiinstitute = function (req, res) {
  var new_Itiinstitute = new Itiinstitute(req.body);

  Itiinstitute.createItiinstitute(new_Itiinstitute, function (err, Itiinstitute) {
    if (err) {
      res.send(err);
    } else {
      res.json(Itiinstitute);
    }
  });
};

exports.read_a_Districtwiseitiinstitute = function (req, res) {

  var fetch_Districtwise_itiinstitute = req.params;
  Itiinstitute.getDistrictwiseitiinstitute(req.params, function (err, Districtwiseitiinstitute) {
    if (err) {
      res.send(err);
    } else {
      res.json(Districtwiseitiinstitute);
    }
  });
};

exports.read_a_JobFairFiltersById = function (req, res) {

  var fetch_ci_mst_companyprofile = req.params;
  Itiinstitute.getJobFairFiltersById(req.params, function (err, JobFairFiltersById) {
    if (err) {
      res.send(err);
    } else {
      res.json(JobFairFiltersById);
    }
  });
};

exports.read_a_ZonewiseDistrict = function (req, res) {

  var fetch_Zonewise_District = req.params;
  Itiinstitute.getZonewiseDistrict(req.params, function (err, ZonewiseDistrict) {
    if (err) {
      res.send(err);
    } else {
      res.json(ZonewiseDistrict);
    }
  });
};