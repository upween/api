'use strict';

var vacancytype = require('../model/vacancytypeModel.js');

exports.read_a_vacancytype = function (req, res) {
  var fetch_vacancytype_master = req.params;
  vacancytype.getvacancytype(req.params, function (err, vacancytype) {
    if (err) {
      res.send(err);
    } else {
      res.json(vacancytype);
    }
  });
};

exports.create_a_vacancytype = function (req, res) {
  var InsUpd_vacancytype_master = new vacancytype(req.body);

  vacancytype.createvacancytype(InsUpd_vacancytype_master, function (err, vacancytype) {
    if (err) {
      res.send(err);
    } else {
      res.json(vacancytype);
    }
  });
};