'use strict';
var knowyourregnew = require('../model/knowyourregnewModel.js');


exports.read_a_knowyourregnew = function(req, res) {
  var KnowYourRegistration_New= req.params;
  knowyourregnew.getknowyourregnew(req.params, function(err, knowyourregnew) {
    if (err){
      res.send(err);
    }
    else{
    res.json(knowyourregnew);
    }
  });
};

exports.read_a_JobSeekerProfileStatusOLEX = function(req, res) {
  var Fetch_JobSeeker_ProfileStatus_OLEX= req.params;
  knowyourregnew.getJobSeekerProfileStatusOLEX(req.params, function(err, JobSeekerProfileStatusOLEX) {
    if (err){
      res.send(err);
    }
    else{
    res.json(JobSeekerProfileStatusOLEX);
    }
  });
};

exports.create_a_updjobseekerregistrationlogin = function (req, res) {
  var upd_jobseeker_registration_login = new knowyourregnew(req.body);

  knowyourregnew.createupdjobseekerregistrationlogin(upd_jobseeker_registration_login, function (err, updjobseekerregistrationlogin) {
    if (err) {
      res.send(err);
    } else {
      res.json(updjobseekerregistrationlogin);
    }
  });
};


exports.create_a_KnowYourRegistration_New = function (req, res) {
  var KnowYourRegistration_New = new knowyourregnew(req.body);

  knowyourregnew.createKnowYourRegistration_New(KnowYourRegistration_New, function (err, KnowYourRegistrationNew) {
    if (err) {
      res.send(err);
    } else {
      res.json(KnowYourRegistrationNew);
    }
  });
};