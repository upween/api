'use strict';

var ER1Form = require('../model/er1Model.js');

exports.create_a_er1form = function (req, res) {
  var insupd_er1form = new ER1Form(req.body);

  ER1Form.createer1form(insupd_er1form, function (err, er1) {
    if (err) {
      res.send(err);
    } else {
      res.json(er1);
    }
  });
};
exports.create_a_createer1formvacancy = function (req, res) {
    var insupd_er1formvacancy = new ER1Form(req.body);
  
    ER1Form.createer1formvacancy(insupd_er1formvacancy, function (err, er1) {
      if (err) {
        res.send(err);
      } else {
        res.json(er1);
      }
    });
  };
  exports.create_a_createer1formvacancyunfilled = function (req, res) {
    var insupd_er1formvacancyunfilled = new ER1Form(req.body);
  
    ER1Form.createer1formvacancyunfilled(insupd_er1formvacancyunfilled, function (err, er1) {
      if (err) {
        res.send(err);
      } else {
        res.json(er1);
      }
    });
  };
  exports.create_a_createer1formvacancyyashaswi = function (req, res) {
    var insupd_er1formvacancyyashaswi = new ER1Form(req.body);
  
    ER1Form.createer1formvacancyunyashaswi(insupd_er1formvacancyyashaswi, function (err, er1) {
      if (err) {
        res.send(err);
      } else {
        res.json(er1);
      }
    });
  };


  exports.read_a_ER1Form = function (req, res) {
    var fetch_er1_form_rpt = req.params;
    ER1Form.getER1Form(req.params, function (err, ER1Form) {
      if (err) {
        res.send(err);
      } else {
        res.json(ER1Form);
      }
    });
  };

  exports.read_a_er1_vacancy_filled = function (req, res) {
    var fetch_er1_vacancy_filled = req.params;
    ER1Form.geter1_vacancy_filled(req.params, function (err, er1_vacancy_filled) {
      if (err) {
        res.send(err);
      } else {
        res.json(er1_vacancy_filled);
      }
    });
  };

  exports.read_a_er1_vacancy_unfilled = function (req, res) {
    var fetch_er1_vacancy_unfilled = req.params;
    ER1Form.geter1_vacancy_unfilled(req.params, function (err, er1_vacancy_unfilled) {
      if (err) {
        res.send(err);
      } else {
        res.json(er1_vacancy_unfilled);
      }
    });
  };

  exports.read_a_er1_vacancy_yashaswi = function (req, res) {
    var fetch_er1_vacancy_yashaswi = req.params;
    ER1Form.geter1_vacancy_yashaswi(req.params, function (err, er1_vacancy_yashaswi) {
      if (err) {
        res.send(err);
      } else {
        res.json(er1_vacancy_yashaswi);
      }
    });
  };
  exports.read_a_ER1FormbyEmp = function (req, res) {
    var fetch_er1_form_rpt = req.params;
    ER1Form.getER1FormByEmp(req.params, function (err, ER1Form) {
      if (err) {
        res.send(err);
      } else {
        res.json(ER1Form);
      }
    });
  };


  exports.read_a_Fetch_ER1_Rpt = function (req, res) {
    var Fetch_ER1_Rpt = req.params;
    ER1Form.getFetch_ER1_Rpt(req.params, function (err, ER1RPT) {
      if (err) {
        res.send(err);
      } else {
        res.json(ER1RPT);
      }
    });
  };

  exports.read_a_Fetch_ER1_RptCount = function (req, res) {
    var Fetch_ER1_Rpt_EmpCount = req.params;
    ER1Form.getFetch_ER1_RptCount(req.params, function (err, ER1RPT) {
      if (err) {
        res.send(err);
      } else {
        res.json(ER1RPT);
      }
    });
  };

  exports.read_a_Fetch_Rpt_EmpDetail = function (req, res) {
    var Fetch_Rpt_EmpDetail = req.params;
    ER1Form.getFetch_Rpt_EmpDetail(req.params, function (err, EmpDetail) {
      if (err) {
        res.send(err);
      } else {
        res.json(EmpDetail);
      }
    });
  };