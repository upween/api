'use strict';

var _jsonwebtoken = require('jsonwebtoken');

var _jsonwebtoken2 = _interopRequireDefault(_jsonwebtoken);

var _config = require('./config.json');

var _config2 = _interopRequireDefault(_config);

var _TokenValidator = require('./middleware/TokenValidator');

var _TokenValidator2 = _interopRequireDefault(_TokenValidator);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    port = process.env.PORT || 3003;
var cors = require('cors');
app.use(cors());

var mysql = require('mysql');

// connection configurations
//var mc = mysql.createConnection({
  //host: '182.70.254.93',
//  host:'10.115.195.88',
//  user:'sa',
//  password:'Mysql@2020',
//  database:'rojgarportal'
//});
const cluster = require('cluster');

if (cluster.isMaster) {

// Keep track of http requests
let numReqs = 0;

// Count requests
function messageHandler(msg) {
if (msg.cmd && msg.cmd === 'notifyRequest') {
numReqs += 1;
}
}

// Start workers and listen for messages containing notifyRequest
const numCPUs = require('os').cpus().length;
console.log("cpu---");
console.log(numCPUs);
for (let i = 0; i < numCPUs; i++) {
cluster.fork();
}

for (const id in cluster.workers) {
cluster.workers[id].on('message', messageHandler);
}

} else {


// connect to database
//mc.connect();
//app.use(cors());
app.listen(port);

console.log('API server started on: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./app/routes/approutes'); //importing route

routes(app, { mergeParams: true });

app.use(_TokenValidator2.default);
app.get('/health', function (req, res) {
  res.status(200).send({ status: true });
});
app.post('/generateToken', function (req, res) {
  var tokenObject = {
    userId: req.body.userName,
    name: "some name"
  };
  var refreshToken = _jsonwebtoken2.default.sign(tokenObject, _config2.default.refreshToken.secret, {
    expiresIn: _config2.default.refreshToken.expiresIn
  });
  var token = _jsonwebtoken2.default.sign(tokenObject, _config2.default.token.secret, {
    expiresIn: _config2.default.token.expiresIn
  });
  res.status(200).send({
    error: false,
    result: {
      token: token,
      refreshToken: refreshToken
    }
  });
});
app.post('/secured/validateToken', function (req, res) {
  var token = req.headers["authorization"];
  _jsonwebtoken2.default.verify(token, _config2.default.token.secret, function (err, decoded) {
    if (err) {
      if (req.body.refreshToken) {
        _jsonwebtoken2.default.verify(req.body.refreshToken, _config2.default.refreshToken.secret, function (err, decoded) {
          if (err) return res.status(401).send({ "error": true, "message": 'Failed to authenticate token.' });else {
            delete decoded["exp"];
            delete decoded["iat"];
            var token = _jsonwebtoken2.default.sign(decoded, _config2.default.token.secret, {
              expiresIn: _config2.default.token.expiresIn
            });
            res.status(401).send({ error: true, data: { token: token }, message: 'New token generated' });
          }
        });
      } else return res.status(401).send({ "error": true, "message": 'Failed to authenticate token.' });
    } else {
      req.body ? req.body["tokenDetails"] = decoded : null;
      res.status(200).send(decoded);
    }
  });
});



}
